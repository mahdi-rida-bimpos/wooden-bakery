package com.bimpos.wooden.bakery.models;

import java.io.Serializable;

public class Address implements Serializable {

    private int addressId, addressType, cityCode, hsAddressId, defaultAddress;
    private String description, phone, street, bldg, cityName, direction,floor;
    private double geoLong, geoLat;

    public Address(int addressId, int addressType, int cityCode, int hsAddressId, String floor, int defaultAddress, String description, String phone, String direction, String street, String bldg, String cityName, double geoLong, double geoLat) {
        this.addressId = addressId;
        this.addressType = addressType;
        this.cityCode = cityCode;
        this.hsAddressId = hsAddressId;
        this.floor = floor;
        this.description = description;
        this.phone = phone;
        this.street = street;
        this.bldg = bldg;
        this.direction = direction;
        this.geoLong = geoLong;
        this.cityName = cityName;
        this.geoLat = geoLat;
        this.defaultAddress = defaultAddress;
    }

    public Address() {
    }

    public void setGeoLong(double geoLong) {
        this.geoLong = geoLong;
    }

    public void setGeoLat(double geoLat) {
        this.geoLat = geoLat;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(int defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public int getHsAddressId() {
        return hsAddressId;
    }

    public void setHsAddressId(int hsAddressId) {
        this.hsAddressId = hsAddressId;
    }

    public int getAddressId() {
        return addressId;
    }

    public int getAddressType() {
        return addressType;
    }

    public void setAddressType(int addressType) {
        this.addressType = addressType;
    }

    public int getCityCode() {
        return cityCode;
    }

    public void setCityCode(int cityCode) {
        this.cityCode = cityCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBldg() {
        return bldg;
    }

    public void setBldg(String bldg) {
        this.bldg = bldg;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public double getGeoLong() {
        return geoLong;
    }

    public double getGeoLat() {
        return geoLat;
    }

}
