package com.bimpos.wooden.bakery.cakeGallery.activities;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.cakeGallery.adapters.CakeGalleryRvAdapter;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityCakeGalleryBinding;
import com.bimpos.wooden.bakery.models.CakeGallery;

import java.util.ArrayList;
import java.util.List;

public class CakeGalleryActivity extends AppCompatActivity {

    private ActivityCakeGalleryBinding binding;
    private DatabaseHelper databaseHelper;
    private CakeGalleryRvAdapter cakeGalleryRvAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCakeGalleryBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initClickListeners();
        initRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<CakeGallery> cakeGalleryList = databaseHelper.getCakeGalleryList();
        cakeGalleryRvAdapter.setData(cakeGalleryList);
    }

    private void initVariables() {
        databaseHelper = MainApplication.getDatabase();
    }

    private void initRecyclerView() {
        binding.recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        cakeGalleryRvAdapter = new CakeGalleryRvAdapter(new ArrayList<>(), this, this);
        binding.recyclerView.setAdapter(cakeGalleryRvAdapter);
    }

    private void initClickListeners() {
        binding.back.setOnClickListener(v -> finish());
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
