package com.bimpos.wooden.bakery.models;

public class PaymentData {

    private  int paymentId;
    private String description;
    private int defaultPayment,picture;

    public PaymentData(int paymentId, String description, int defaultPayment, int picture) {
        this.paymentId = paymentId;
        this.description = description;
        this.defaultPayment = defaultPayment;
        this.picture = picture;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDefaultPayment() {
        return defaultPayment;
    }

    public void setDefaultPayment(int defaultPayment) {
        this.defaultPayment = defaultPayment;
    }

    public int getPicture() {
        return picture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }
}
