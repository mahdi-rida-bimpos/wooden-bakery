package com.bimpos.wooden.bakery.helpers;

import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.models.PriceList;

import java.text.DecimalFormat;

public class FormatPrice {
    private static final String TAG = "FormatPrice";

    public static String getFormattedPrice(double price) {

        String mask = MainApplication.getPreferences().getString(Constant.SETTINGS_PRICE_MASK, "###,###,###.###");
        String currency = MainApplication.getPreferences().getString(Constant.SETTINGS_CURRENCY, "LBP");
        StringBuilder currMask = new StringBuilder(mask);

        if (MainApplication.getDatabase().getPriceList() != null) {
            PriceList priceList = MainApplication.getDatabase().getPriceList();
            mask = priceList.getMask();
            int decimals = priceList.getDecimals();
//            decimals = 4;
            if (null == mask || mask.equalsIgnoreCase("null") || mask.length() == 0) {
                mask = "###,###,###.###";
            }
            currMask = new StringBuilder(mask.substring(0, mask.indexOf('.')));
            for (int i = 0; i < decimals; i++) {
                if (i == 0) {
                    currMask.append(".");
                }
                currMask.append("#");
            }
            if (priceList.getShouldShowCurr() == 1) {
                currency = priceList.getCurrencyName();
            }
        }

        try {
            DecimalFormat df = new DecimalFormat(currMask.toString());
            return currency + "  " + df.format(price);
        } catch (Exception e) {
            DecimalFormat df = new DecimalFormat("###,###,###.###");
            return currency + "  " + df.format(price);
        }
    }

    public static String getCustomFormattedPrice(double price, String currencyName) {

        String mask = "###,###,###.###";
        DecimalFormat df = new DecimalFormat(mask);
        return currencyName + "  " + df.format(price);
    }
}
