package com.bimpos.wooden.bakery.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bimpos.wooden.bakery.tables.AddressTable;
import com.bimpos.wooden.bakery.tables.BranchCitiesTable;
import com.bimpos.wooden.bakery.tables.BranchesTable;
import com.bimpos.wooden.bakery.tables.CakeItemFavoriteTable;
import com.bimpos.wooden.bakery.tables.CategoriesTable;
import com.bimpos.wooden.bakery.tables.CitiesTable;
import com.bimpos.wooden.bakery.tables.ComboGroupTable;
import com.bimpos.wooden.bakery.tables.ComboHeaderTable;
import com.bimpos.wooden.bakery.tables.ComboModifierTable;
import com.bimpos.wooden.bakery.tables.CountriesTable;
import com.bimpos.wooden.bakery.tables.CakeGalleryTable;
import com.bimpos.wooden.bakery.tables.CakeItemTable;
import com.bimpos.wooden.bakery.tables.MemberTable;
import com.bimpos.wooden.bakery.tables.OrderHistoryTable;
import com.bimpos.wooden.bakery.tables.OrderTable;
import com.bimpos.wooden.bakery.tables.ParentTable;
import com.bimpos.wooden.bakery.tables.PriceListTable;
import com.bimpos.wooden.bakery.tables.ProductStockTable;
import com.bimpos.wooden.bakery.tables.ProductsTable;
import com.bimpos.wooden.bakery.tables.QuestionHeaderTable;
import com.bimpos.wooden.bakery.tables.QuestionModifierTable;
import com.bimpos.wooden.bakery.tables.QuestionsGroupTable;
import com.bimpos.wooden.bakery.tables.RegionsTable;

public class AppDatabase extends SQLiteOpenHelper {

    private static final String TAG = "AppDatabase";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "woodenBakery.db";
    public static AppDatabase instance;

    AppDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(TAG, "AppDatabase: start");
    }

    public static AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = new AppDatabase(context.getApplicationContext());
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate: start creation ");

        createParentTable(db);
        createCategoriesTable(db);
        CreateProductsTable(db);

        createBranchesTable(db);
        createBranchCitiesTable(db);

        createRegionsTable(db);
        createCitiesTable(db);
        createCountriesTable(db);

        createCakeGalleryTable(db);
        createCakeItemTable(db);
        createCakeItemFavoriteTable(db);

        createMemberTable(db);
        createAddressTable(db);

        createOrderTable(db);
        createOrderHistoryTable(db);

        createProductStockTable(db);

        createQuestionGroupTable(db);
        createQuestionHeader(db);
        createQuestionModifiersTable(db);

        createComboGroupTable(db);
        createComboHeaderTable(db);
        createComboModifiersTable(db);

        createPriceListTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /**
     * Table Creations
     */

    private void createOrderHistoryTable(SQLiteDatabase db) {
        String createOrderHistory =
                "CREATE TABLE " + OrderHistoryTable.TABLE_NAME + "("
                        + OrderHistoryTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + OrderHistoryTable.Columns.IS_NEW + " INTEGER DEFAULT 1,"
                        + OrderHistoryTable.Columns.TRANS_ID + " INTEGER, "
                        + OrderHistoryTable.Columns.TRANS_DATE + " TEXT, "
                        + OrderHistoryTable.Columns.SCHEDULE_DATE + " TEXT, "
                        + OrderHistoryTable.Columns.TOTAL_ITEMS + " TEXT, "
                        + OrderHistoryTable.Columns.TOTAL_PRICE + " DOUBLE, "
                        + OrderHistoryTable.Columns.REMARK + " TEXT, "
                        + OrderHistoryTable.Columns.ORDER_TYPE + " INTEGER, "
                        + OrderHistoryTable.Columns.CONVERSION_RATE + " DOUBLE, "
                        + OrderHistoryTable.Columns.DEFAULT_CURRENCY + " TEXT, "
                        + OrderHistoryTable.Columns.PRICE_LIST_CURRENCY + " TEXT, "
                        + OrderHistoryTable.Columns.CART_DATA + " TEXT,"
                        + OrderHistoryTable.Columns.ORDER_STATUS + " TEXT,"
                        + OrderHistoryTable.Columns.BranchName + " TEXT, "
                        + OrderHistoryTable.Columns.DELIVERY_CHARGE + " TEXT, "
                        + OrderHistoryTable.Columns.BRANCH_ID + " TEXT) ";

        db.execSQL(createOrderHistory);
    }

    private void createBranchCitiesTable(SQLiteDatabase db) {
        String createParent =
                "CREATE TABLE " + BranchCitiesTable.TABLE_NAME + " ( "
                        + BranchCitiesTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                        + BranchCitiesTable.Columns.BRANCH_ID + " INTEGER, "
                        + BranchCitiesTable.Columns.CITY_CODE + " INTEGER) ";

        db.execSQL(createParent);
    }


    private void createCakeGalleryTable(SQLiteDatabase db) {
        String createGalleriesTable =
                "CREATE TABLE " + CakeGalleryTable.TABLE_NAME + "("
                        + CakeGalleryTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + CakeGalleryTable.Columns.GALLERY_NAME + " TEXT ,"
                        + CakeGalleryTable.Columns.IS_ACTIVE + " INTEGER DEFAULT 1,"
                        + CakeGalleryTable.Columns.GALLERY_ID + " INTEGER) ";

        db.execSQL(createGalleriesTable);
    }

    private void createCakeItemTable(SQLiteDatabase db) {
        String createGalleryPhotoTable =
                "CREATE TABLE " + CakeItemTable.TABLE_NAME + " ( "
                        + CakeItemTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                        + CakeItemTable.Columns.CAKE_GALLERY_ID + " INTEGER, "
                        + CakeItemTable.Columns.CAKE_ITEM_ID + " INTEGER, "
                        + CakeItemTable.Columns.PRICE + " INTEGER, "
                        + CakeItemTable.Columns.IS_ACTIVE + " INTEGER, "
                        + CakeItemTable.Columns.MIN_ORDER_QTY + " INTEGER, "
                        + CakeItemTable.Columns.TITLE + " TEXT, "
                        + CakeItemTable.Columns.REF_NUM + " TEXT, "
                        + CakeItemTable.Columns.DESCRIPTION + " TEXT, "
                        + CakeItemTable.Columns.PIC_PATH + " TEXT) ";

        db.execSQL(createGalleryPhotoTable);
    }

    private void createCakeItemFavoriteTable(SQLiteDatabase db) {
        String createGalleryPhotoTable =
                "CREATE TABLE " + CakeItemFavoriteTable.TABLE_NAME + " ( "
                        + CakeItemFavoriteTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                        + CakeItemFavoriteTable.Columns.CAKE_GALLERY_ID + " INTEGER, "
                        + CakeItemFavoriteTable.Columns.CAKE_ITEM_ID + " INTEGER, "
                        + CakeItemFavoriteTable.Columns.PRICE + " INTEGER, "
                        + CakeItemFavoriteTable.Columns.PIC_STORAGE_PATH + " INTEGER, "
                        + CakeItemFavoriteTable.Columns.MIN_ORDER_QTY + " INTEGER, "
                        + CakeItemFavoriteTable.Columns.TITLE + " TEXT, "
                        + CakeItemFavoriteTable.Columns.REF_NUM + " TEXT, "
                        + CakeItemFavoriteTable.Columns.DESCRIPTION + " TEXT, "
                        + MemberTable.Columns.IMAGE + " BLOB, "
                        + CakeItemFavoriteTable.Columns.PIC_PATH + " TEXT) ";

        db.execSQL(createGalleryPhotoTable);
    }


    private void createQuestionGroupTable(SQLiteDatabase db) {
        String createQuestionGroup =
                "CREATE TABLE " + QuestionsGroupTable.TABLE_NAME + "("
                        + QuestionsGroupTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + QuestionsGroupTable.Columns.QUESTION_GROUP_ID + " INTEGER,"
                        + QuestionsGroupTable.Columns.DESCRIPTION + " TEXT) ";

        db.execSQL(createQuestionGroup);
    }

    private void createQuestionHeader(SQLiteDatabase db) {
        String createQuestionHeader =
                "CREATE TABLE " + QuestionHeaderTable.TABLE_NAME + "( "
                        + QuestionHeaderTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + QuestionHeaderTable.Columns.QUESTION_GROUP_ID + " INTEGER,"
                        + QuestionHeaderTable.Columns.QUESTION_HEADER_ID + " INTEGER,"
                        + QuestionHeaderTable.Columns.MIN + " INTEGER,"
                        + QuestionHeaderTable.Columns.MAX + " INTEGER,"
                        + QuestionHeaderTable.Columns.REQUIRED + " INTEGER,"
                        + QuestionHeaderTable.Columns.SEQ + " INTEGER,"
                        + QuestionHeaderTable.Columns.DESCRIPTION + " TEXT) ";

        db.execSQL(createQuestionHeader);
    }

    private void createQuestionModifiersTable(SQLiteDatabase db) {
        String createQuestionModifiers =
                "CREATE TABLE " + QuestionModifierTable.TABLE_NAME + "("
                        + QuestionModifierTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                        + QuestionModifierTable.Columns.PROD_NUM + " INTEGER, "
                        + QuestionModifierTable.Columns.CAT_ID + " INTEGER, "
                        + QuestionModifierTable.Columns.DESCRIPTION + " TEXT, "
                        + QuestionModifierTable.Columns.DESCRIPTION_2 + " TEXT, "
                        + QuestionModifierTable.Columns.PROD_INFO + " TEXT, "
                        + QuestionModifierTable.Columns.PROD_INFO_2 + " TEXT, "
                        + QuestionModifierTable.Columns.PRICE + " double, "
                        + QuestionModifierTable.Columns.PIC_PATH + " TEXT, "
                        + QuestionModifierTable.Columns.PICTURE + " TEXT, "
                        + QuestionModifierTable.Columns.QUESTION_HEADER_ID + " INTEGER, "
                        + QuestionModifierTable.Columns.QUESTION_GROUP_ID + " INTEGER, "
                        + QuestionModifierTable.Columns.STOCK + " INTEGER, "
                        + QuestionModifierTable.Columns.ENABLED + " INTEGER, "
                        + QuestionModifierTable.Columns.P_ID + " INTEGER, "
                        + QuestionModifierTable.Columns.CLIENT_ID + " TEXT, "
                        + QuestionModifierTable.Columns.THUMB_PICTURE + " TEXT, "
                        + QuestionModifierTable.Columns.BRAND + " TEXT, "
                        + QuestionModifierTable.Columns.REF_CODE_1 + " TEXT, "
                        + QuestionModifierTable.Columns.REF_CODE_2 + " TEXT, "
                        + QuestionModifierTable.Columns.IS_TAXABLE_1 + " INTEGER, "
                        + QuestionModifierTable.Columns.IS_TAXABLE_2 + " INTEGER, "
                        + QuestionModifierTable.Columns.IS_TAXABLE_3 + " INTEGER, "
                        + QuestionModifierTable.Columns.VERSION_ID + " INTEGER ,"
                        + QuestionModifierTable.Columns.CREATED_AT + " TEXT ,"
                        + QuestionModifierTable.Columns.UPDATED_AT + " TEXT )";

        db.execSQL(createQuestionModifiers);
    }


    private void createComboGroupTable(SQLiteDatabase db) {
        String createQuestionGroup =
                "CREATE TABLE " + ComboGroupTable.TABLE_NAME + "("
                        + ComboGroupTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + ComboGroupTable.Columns.COMBO_GROUP_ID + " INTEGER)";

        db.execSQL(createQuestionGroup);
    }

    private void createComboHeaderTable(SQLiteDatabase db) {
        String createQuestionHeader =
                "CREATE TABLE " + ComboHeaderTable.TABLE_NAME + "( "
                        + ComboHeaderTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + ComboHeaderTable.Columns.COMBO_GROUP_ID + " INTEGER,"
                        + ComboHeaderTable.Columns.COMBO_HEADER_ID + " INTEGER,"
                        + ComboHeaderTable.Columns.MIN + " INTEGER,"
                        + ComboHeaderTable.Columns.MAX + " INTEGER,"
                        + ComboHeaderTable.Columns.REQUIRED + " INTEGER,"
                        + ComboHeaderTable.Columns.SEQ + " INTEGER,"
                        + ComboHeaderTable.Columns.DESCRIPTION + " TEXT) ";

        db.execSQL(createQuestionHeader);
    }

    private void createComboModifiersTable(SQLiteDatabase db) {
        String createQuestionModifiers =
                "CREATE TABLE " + ComboModifierTable.TABLE_NAME + "("
                        + ComboModifierTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                        + ComboModifierTable.Columns.PROD_NUM + " INTEGER, "
                        + ComboModifierTable.Columns.CAT_ID + " INTEGER, "
                        + ComboModifierTable.Columns.DESCRIPTION + " TEXT, "
                        + ComboModifierTable.Columns.DESCRIPTION_2 + " TEXT, "
                        + ComboModifierTable.Columns.PROD_INFO + " TEXT, "
                        + ComboModifierTable.Columns.PROD_INFO_2 + " TEXT, "
                        + ComboModifierTable.Columns.PRICE + " double, "
                        + ComboModifierTable.Columns.PIC_PATH + " TEXT, "
                        + ComboModifierTable.Columns.PICTURE + " TEXT, "
                        + ComboModifierTable.Columns.COMBO_HEADER_ID + " INTEGER, "
                        + ComboModifierTable.Columns.COMBO_GROUP_ID + " INTEGER, "
                        + ComboModifierTable.Columns.STOCK + " INTEGER, "
                        + ComboModifierTable.Columns.ENABLED + " INTEGER, "
                        + ComboModifierTable.Columns.P_ID + " INTEGER, "
                        + ComboModifierTable.Columns.CLIENT_ID + " TEXT, "
                        + ComboModifierTable.Columns.THUMB_PICTURE + " TEXT, "
                        + ComboModifierTable.Columns.BRAND + " TEXT, "
                        + ComboModifierTable.Columns.REF_CODE_1 + " TEXT, "
                        + ComboModifierTable.Columns.REF_CODE_2 + " TEXT, "
                        + ComboModifierTable.Columns.IS_TAXABLE_1 + " INTEGER, "
                        + ComboModifierTable.Columns.IS_TAXABLE_2 + " INTEGER, "
                        + ComboModifierTable.Columns.IS_TAXABLE_3 + " INTEGER, "
                        + ComboModifierTable.Columns.VERSION_ID + " INTEGER ,"
                        + ComboModifierTable.Columns.CREATED_AT + " TEXT ,"
                        + ComboModifierTable.Columns.UPDATED_AT + " TEXT )";

        db.execSQL(createQuestionModifiers);
    }


    private void createProductStockTable(SQLiteDatabase db) {
        String createProductStock =
                "CREATE TABLE " + ProductStockTable.TABLE_NAME + "("
                        + ProductStockTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + ProductStockTable.Columns.PROD_NUM + " INTEGER,"
                        + ProductStockTable.Columns.BRANCH_ID + " INTEGER,"
                        + ProductStockTable.Columns.VARIATION_1 + " INTEGER, "
                        + ProductStockTable.Columns.VARIATION_2 + " INTEGER, "
                        + ProductStockTable.Columns.STOCK + " INTEGER, "
                        + ProductStockTable.Columns.UPDATED_AT + " TEXT) ";

        db.execSQL(createProductStock);
    }

    private void createOrderTable(SQLiteDatabase db) {
        String createOrder =
                "CREATE TABLE " + OrderTable.TABLE_NAME + "("
                        + OrderTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + OrderTable.Columns.CART_ITEM_LIST + " TEXT)";

        db.execSQL(createOrder);
    }

    private void createAddressTable(SQLiteDatabase db) {
        String createAddress =
                "CREATE TABLE " + AddressTable.TABLE_NAME + "("
                        + AddressTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + AddressTable.Columns.DESCRIPTION + " TEXT, "
                        + AddressTable.Columns.ADDRESS_TYPE + " INTEGER,"
                        + AddressTable.Columns.PHONE + " TEXT,"
                        + AddressTable.Columns.HS_ADDRESS_ID + " TEXT,"
                        + AddressTable.Columns.GEO_LONG + " DOUBLE,"
                        + AddressTable.Columns.GEO_LAT + " DOUBLE,"
                        + AddressTable.Columns.CITY_CODE + " INTEGER,"
                        + AddressTable.Columns.STREET + " TEXT,"
                        + AddressTable.Columns.CITY_NAME + " TEXT,"
                        + AddressTable.Columns.DIRECTION + " TEXT,"
                        + AddressTable.Columns.BUILDING + " TEXT ,"
                        + AddressTable.Columns.DEFAULT_ADDRESS + " INTEGER ,"
                        + AddressTable.Columns.FLOOR + " TEXT)";

        db.execSQL(createAddress);
    }

    private void createMemberTable(SQLiteDatabase db) {
        String createMember =
                "CREATE TABLE " + MemberTable.TABLE_NAME + "( "
                        + MemberTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + MemberTable.Columns.MEMBER_ID + " INTEGER , "
                        + MemberTable.Columns.MEMBER_HS_ID + " INTEGER, "
                        + MemberTable.Columns.FIRST_NAME + " TEXT, "
                        + MemberTable.Columns.LAST_NAME + " TEXT, "
                        + MemberTable.Columns.DATE_OF_BIRTH + " TEXT, "
                        + MemberTable.Columns.MOBILE + " TEXT, "
                        + MemberTable.Columns.MOBILE_VALIDATED + " INTEGER, "
                        + MemberTable.Columns.EMAIL + " TEXT, "
                        + MemberTable.Columns.IMAGE + " BLOB)";

        db.execSQL(createMember);
    }

    private void createCountriesTable(SQLiteDatabase db) {
        String createCountries =
                "CREATE TABLE " + CountriesTable.TABLE_NAME + " ( "
                        + CountriesTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                        + CountriesTable.Columns.COUNTRY_ID + " INTEGER, "
                        + CountriesTable.Columns.NAME + " TEXT, "
                        + CountriesTable.Columns.REGION_CODE + " INTEGER, "
                        + CountriesTable.Columns.COUNTRY_CODE + " INTEGER, "
                        + CountriesTable.Columns.ZIP_CODE + " INTEGER, "
                        + CountriesTable.Columns.IS_ACTIVE + " INTEGER) ";

        db.execSQL(createCountries);
    }

    private void createCitiesTable(SQLiteDatabase db) {
        String createCities =
                "CREATE TABLE " + CitiesTable.TABLE_NAME + " ( "
                        + CitiesTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                        + CitiesTable.Columns.CITY_CODE + " INTEGER, "
                        + CitiesTable.Columns.CITY_NAME + " TEXT, "
                        + CitiesTable.Columns.REGION_CODE + " INTEGER, "
                        + CitiesTable.Columns.COUNTRY_CODE + " INTEGER, "
                        + CitiesTable.Columns.ZIP_CODE + " INTEGER, "
                        + CitiesTable.Columns.IS_ACTIVE + " INTEGER)";

        db.execSQL(createCities);
    }

    private void createRegionsTable(SQLiteDatabase db) {
        String createRegions =
                "CREATE TABLE " + RegionsTable.TABLE_NAME + " ( "
                        + RegionsTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                        + RegionsTable.Columns.REGION_ID + " INTEGER, "
                        + RegionsTable.Columns.NAME + " TEXT, "
                        + RegionsTable.Columns.PHONE_CODE + " TEXT, "
                        + RegionsTable.Columns.IS_ACTIVE + " INTEGER) ";

        db.execSQL(createRegions);
    }

    private void CreateProductsTable(SQLiteDatabase db) {
        String createProducts =
                "CREATE TABLE " + ProductsTable.TABLE_NAME + "( "
                        + ProductsTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                        + ProductsTable.Columns.PROD_NUM + " INTEGER, "
                        + ProductsTable.Columns.CAT_ID + " INTEGER, "
                        + ProductsTable.Columns.DESCRIPTION + " TEXT, "
                        + ProductsTable.Columns.DESCRIPTION_2 + " TEXT, "
                        + ProductsTable.Columns.PROD_INFO + " TEXT, "
                        + ProductsTable.Columns.PROD_INFO_2 + " TEXT, "
                        + ProductsTable.Columns.PRICE + " double, "
                        + ProductsTable.Columns.PRICE2 + " double, "
                        + ProductsTable.Columns.PIC_PATH + " TEXT, "
                        + ProductsTable.Columns.PICTURE + " TEXT, "
                        + ProductsTable.Columns.PRODUCT_PICTURE + " TEXT, "
                        + ProductsTable.Columns.TITLE + " TEXT, "
                        + ProductsTable.Columns.P_ID + " INTEGER, "
                        + ProductsTable.Columns.THUMB_PICTURE + " TEXT, "
                        + ProductsTable.Columns.BRAND + " TEXT, "
                        + ProductsTable.Columns.REF_CODE_1 + " TEXT, "
                        + ProductsTable.Columns.REF_CODE_2 + " TEXT, "
                        + ProductsTable.Columns.IS_TAXABLE_1 + " INTEGER, "
                        + ProductsTable.Columns.IS_TAXABLE_2 + " INTEGER, "
                        + ProductsTable.Columns.IS_TAXABLE_3 + " INTEGER, "
                        + ProductsTable.Columns.MODIFIERS_GROUP_ID + " INTEGER ,"
                        + ProductsTable.Columns.COMBO_GROUP_ID + " INTEGER ,"
                        + ProductsTable.Columns.TAGS + " TEXT )";

        db.execSQL(createProducts);
    }

    private void createCategoriesTable(SQLiteDatabase db) {
        String createCategories =
                "CREATE TABLE " + CategoriesTable.TABLE_NAME + "( "
                        + CategoriesTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                        + CategoriesTable.Columns.CAT_ID + " INTEGER, "
                        + CategoriesTable.Columns.MENU_ID + " INTEGER, "
                        + CategoriesTable.Columns.PARENT_ID + " INTEGER, "
                        + CategoriesTable.Columns.CAT_NAME + " TEXT, "
                        + CategoriesTable.Columns.DESCRIPTION + " TEXT, "
                        + CategoriesTable.Columns.DESCRIPTION_2 + " TEXT, "
                        + CategoriesTable.Columns.PIC_PATH + " TEXT, "
                        + CategoriesTable.Columns.PICTURE + " TEXT, "
                        + CategoriesTable.Columns.THUMB_PICTURE + " TEXT, "
                        + CategoriesTable.Columns.HEADER_PIC_PATH + " INTEGER, "
                        + CategoriesTable.Columns.FOOTER_PIC_PATH + " TEXT, "
                        + CategoriesTable.Columns.BIG_PIC_PATH + " TEXT, "
                        + CategoriesTable.Columns.SEQ + " INTEGER) ";

        db.execSQL(createCategories);
    }

    private void createParentTable(SQLiteDatabase db) {
        String createParent =
                "CREATE TABLE " + ParentTable.TABLE_NAME + " ( "
                        + ParentTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                        + ParentTable.Columns.PARENT_ID + " INTEGER, "
                        + ParentTable.Columns.DESCRIPTION + " TEXT, "
                        + ParentTable.Columns.IS_ACTIVE + " INTEGER, "
                        + ParentTable.Columns.VERSION_ID + " INTEGER )";

        db.execSQL(createParent);
    }

    private void createBranchesTable(SQLiteDatabase db) {
        String createBranches =
                "CREATE TABLE " + BranchesTable.TABLE_NAME + " ( "
                        + BranchesTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                        + BranchesTable.Columns.BRANCH_ID + " INTEGER, "
                        + BranchesTable.Columns.NAME + " TEXT, "
                        + BranchesTable.Columns.ADDRESS + " TEXT, "
                        + BranchesTable.Columns.PHONE + " TEXT, "
                        + BranchesTable.Columns.EMAIL + " TEXT, "
                        + BranchesTable.Columns.LONGITUDE + " DOUBLE, "
                        + BranchesTable.Columns.LATITUDE + " DOUBLE, "
                        + BranchesTable.Columns.CLIENT_ID + " TEXT, "
                        + BranchesTable.Columns.PIC_PATH + " TEXT, "
                        + BranchesTable.Columns.IS_ACTIVE + " INTEGER, "
                        + BranchesTable.Columns.OPENING_HOURS + " TEXT, "
                        + BranchesTable.Columns.PICTURE + " TEXT, "
                        + BranchesTable.Columns.ACCEPTS_DELIVERY + " INTEGER, "
                        + BranchesTable.Columns.ACCEPTS_TAKE_AWAY + " INTEGER, "
                        + BranchesTable.Columns.DELIVERY_HOURS + " TEXT )";

        db.execSQL(createBranches);
    }

    private void createPriceListTable(SQLiteDatabase db) {
        String createQuestionModifiers =
                "CREATE TABLE " + PriceListTable.TABLE_NAME + "("
                        + PriceListTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                        + PriceListTable.Columns.CURRENCY_DESCRIPTION + " TEXT, "
                        + PriceListTable.Columns.CONVERSION + " DOUBLE, "
                        + PriceListTable.Columns.MASK + " TEXT, "
                        + PriceListTable.Columns.SHOW_IN_PRICE_LIST_CURRENCY + " INTEGER,"
                        + PriceListTable.Columns.DECIMALS + " INTEGER) ";


        db.execSQL(createQuestionModifiers);
    }

}
