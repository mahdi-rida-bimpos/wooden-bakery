package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.models.CartComboHeader;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.models.CartQuestionHeader;

import java.util.List;


public class CartOrderRecyclerAdapter extends RecyclerView.Adapter<CartOrderRecyclerAdapter.ViewHolder> {
    private final Context context;
    private final List<Object> objectList;
    private final CartProduct cartProduct;

    public CartOrderRecyclerAdapter(Context context, List<Object> objectList, CartProduct cartProduct) {
        this.context = context;
        this.cartProduct=cartProduct;
        this.objectList = objectList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_cart_order_question_header, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Object object = objectList.get(position);
        if (object instanceof CartComboHeader) {
            CartComboHeader cartComboHeader = (CartComboHeader) object;
            if (cartComboHeader.getCartComboModifiers().size() != 0) {
                holder.headerDescription.setText(cartComboHeader.getDescription());
                CartOrderComboModifierRvAdapter adapter = new CartOrderComboModifierRvAdapter(cartProduct,cartComboHeader.getCartComboModifiers());
                holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
                holder.recyclerView.setAdapter(adapter);
            }
        } else {
            CartQuestionHeader cartQuestionHeader = (CartQuestionHeader) object;
            if (cartQuestionHeader.getCartQuestionModifiers().size() != 0) {
                holder.headerDescription.setText(cartQuestionHeader.getDescription());
                CartOrderQModifierRvAdapter adapter = new CartOrderQModifierRvAdapter(cartProduct,cartQuestionHeader.getCartQuestionModifiers());
                holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
                holder.recyclerView.setAdapter(adapter);
            }
        }
    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;
        TextView headerDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recyclerView = itemView.findViewById(R.id.recycler_cart_order_question_header_recyclerView);
            headerDescription = itemView.findViewById(R.id.recycler_cart_order_question_header_description);
        }
    }

}
