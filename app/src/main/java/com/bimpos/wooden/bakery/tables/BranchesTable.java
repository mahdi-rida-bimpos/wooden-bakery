package com.bimpos.wooden.bakery.tables;

public class BranchesTable {

    public static final String TABLE_NAME= "branches";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String BRANCH_ID = "branchId";
        public static final String NAME = "name";
        public static final String ADDRESS = "address";
        public static final String PHONE = "phone";
        public static final String EMAIL = "email";
        public static final String LONGITUDE = "longitude";
        public static final String LATITUDE = "latitude";
        public static final String CLIENT_ID = "clientId";
        public static final String PIC_PATH = "picPath";
        public static final String IS_ACTIVE = "isActive";
        public static final String OPENING_HOURS = "openingHours";
        public static final String PICTURE = "picture";
        public static final String ACCEPTS_DELIVERY = "acceptsDelivery";
        public static final String ACCEPTS_TAKE_AWAY = "acceptsTakeAway";
        public static final String DELIVERY_HOURS = "deliveryHours";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
