package com.bimpos.wooden.bakery.helpers;

public class Constant {

    public static String INTENT_FILTER_LAUNCHER = "INTENT_FILTER_LAUNCHER";
    public static String BASE_URL = "https://posapis.com/api/v1/";
    public static String TOKEN = "TOKEN";
    public static String UID = "UID";
    public static String FIRST_SETUP = "FIRST_SETUP";
    public static String FACEBOOK_URL = "https://www.facebook.com/WoodenBakeryLebanon";
    public static String FACEBOOK_PAGE_ID = "266391366740178";
    public static String DATABASE_EXISTS = "DATABASE_EXISTS";
    public static String WOODEN_BAKERY_URL = "http://woodenbakery.com/";

    public static String SETTINGS_CURRENCY = "CURRENCY";
    public static String SETTINGS_MOBILE_PREFIX = "MOBILE_PREFIX";
    public static String SETTINGS_MIN_VERSION = "MIN_VERSION";
    public static String SETTINGS_CURR_VERSION = "CURR_VERSION";
    public static String SETTINGS_PRICE_MASK = "PRICE_MASK";
    public static String SETTINGS_PHONE_MASK = "PHONE_MASK";
    public static String SETTINGS_SHOW_REMARK_QUESTIONS = "SHOW_REMARK_QUESTIONS";
    public static String SETTINGS_SHOW_REMARK_ALL_ITEMS = "SHOW_REMARK_ALL";
    public static String SETTINGS_MAINTENANCE_MODE = "MAINTENANCE";
    public static String SETTINGS_SHOW_BIG_CATEGORIES = "SHOW_BIG_CAT";
    public static String SETTINGS_ORDER_MENU_CATALOG_ID = "PARENT_ID";
    public static String SETTINGS_GALLERY_CATALOG_ID = "GALLERY_ID";
    public static String SETTINGS_PRODUCT_CATALOG_ID = "CATALOG_ID";
    public static double SETTINGS_DELIVERY_CHARGE = 0;

    public static String userName1 = "headoffice@woodenbakery.lb";
    public static String password1 = "W00denB@kery";

//    public static String userName1 = "Store_bim@bimpos.com";
//    public static String password1 = "St0reB!M";

    public static String IS_FROM_MAIN_ACTIVITY = "isFromHistory";

    public static String IS_SMS_SENT = "sms";
    public static String PHONE_NUMBER = "phone";
    public static String BIRTHDAY_DATE_CHANGE = "birthdayDateChange";

    public static String ZOPIM_KEY = "4ROcPw5Kpgtw6J5sO78QnflklcP3n2uW";

    public static String PICTURE_URL = "PictureUrl";
    public static String MEMBER = "member";

    public static int cartId = 0;
}
