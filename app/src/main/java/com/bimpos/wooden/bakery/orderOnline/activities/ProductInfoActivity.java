package com.bimpos.wooden.bakery.orderOnline.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bimpos.wooden.bakery.BuildConfig;
import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityProductInfoBinding;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.helpers.Helpers;
import com.bimpos.wooden.bakery.models.Cart;
import com.bimpos.wooden.bakery.models.CartComboHeader;
import com.bimpos.wooden.bakery.models.CartComboModifier;
import com.bimpos.wooden.bakery.models.CartQuestionHeader;
import com.bimpos.wooden.bakery.models.CartQuestionModifier;
import com.bimpos.wooden.bakery.models.ComboHeader;
import com.bimpos.wooden.bakery.models.ComboModifier;
import com.bimpos.wooden.bakery.models.Products;
import com.bimpos.wooden.bakery.models.QuestionHeader;
import com.bimpos.wooden.bakery.models.QuestionModifier;
import com.bimpos.wooden.bakery.orderOnline.adapters.ProductInfoRvAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import java.util.ArrayList;
import java.util.List;

public class ProductInfoActivity extends AppCompatActivity implements CustomDialog.DialogEvents {

    private static final String TAG = "ProductInfoActivity";

    private final int DIALOG_MISSING_MODIFIER = 1;
    public ActivityProductInfoBinding binding;
    private Products product;
    private Cart cartData;
    private CustomDialog dialog;
    private DatabaseHelper databaseHelper;
    private int SHOW_REMARK_QUESTIONS;
    private int SHOW_REMARK_ALL;
    public ProductInfoRvAdapter recyclerAdapter;
    private final List<Object> objectList = new ArrayList<>();
    public ProductInfoActivity productInfoActivity;
    private InputMethodManager imm;
    private int holdCount = 0;
    private boolean placeOrderFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProductInfoBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());
        initVariables();
        getExtras();
        initClickListeners();
    }

    private void initVariables() {
        Log.d(TAG, "initVariables: start");
        SHOW_REMARK_ALL = getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE).getInt(Constant.SETTINGS_SHOW_REMARK_ALL_ITEMS, 1);
        SHOW_REMARK_QUESTIONS = getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE).getInt(Constant.SETTINGS_SHOW_REMARK_QUESTIONS, 1);
        dialog = new CustomDialog(this);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        databaseHelper = MainApplication.getDatabase();
        cartData = Cart.getCartData();
        productInfoActivity = this;
        recyclerAdapter = new ProductInfoRvAdapter(this, this, objectList);
        binding.remarkText.setFilters(new InputFilter[]{Helpers.filter, new InputFilter.LengthFilter(50)});

        binding.remarkText.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
    }

    private void getExtras() {
        product = (Products) getIntent().getSerializableExtra(Products.class.getSimpleName());
        if (product != null) {
            Log.d(TAG, "getExtras: combo id " + product.getComboGroupId());
            Log.d(TAG, "getExtras: modifier " + product.getModifiersGroupId());
            if (product.getComboGroupId() > 0 || product.getModifiersGroupId() > 0) {
                initRecyclerView();
            } else {
                binding.recyclerView.setVisibility(View.GONE);
            }
            fillContent();
            holdCount = cartData.getProductCount(product.getProdNum());
            if (holdCount == 0) {
                holdCount = 1;
            }
            boolean canAdd = holdCount < 999;
            if (!canAdd) {
                disableAddButton();
            }
        } else {
            finish();
        }
    }

    private void disableAddButton() {
        binding.addToCart.setEnabled(false);
        binding.addToCart.setClickable(false);
        binding.addToCart.setBackgroundColor(ContextCompat.getColor(this, R.color.pantoneRedDisabled));
        binding.addToCartText.setTextColor(ContextCompat.getColor(this, R.color.greyDark));
    }

    private void enableAddButton() {
        binding.addToCart.setClickable(true);
        binding.addToCart.setEnabled(true);
        binding.addToCart.setBackgroundColor(ContextCompat.getColor(this, R.color.pantoneRed));
        binding.addToCartText.setTextColor(ContextCompat.getColor(this, R.color.white));
    }

    private void initRecyclerView() {
        Log.d(TAG, "initRecyclerView: start");
        int questionGroupId = product.getModifiersGroupId();

        List<QuestionModifier> questionModifierList = new ArrayList<>();
        List<ComboModifier> comboModifierList = new ArrayList<>();

        if (questionGroupId > 0) {
            Log.d(TAG, "initRecyclerView: get questions ");
            List<QuestionHeader> questionHeaderList = databaseHelper.getQuestionHeaderByQgId(questionGroupId);

            Log.d(TAG, "initRecyclerView:  questions size " + questionHeaderList.size());
            for (int i = 0; i < questionHeaderList.size(); i++) {
                int questionHeaderId = questionHeaderList.get(i).getQuestionHeaderId();
                questionModifierList = databaseHelper.getQuestionModifierByQhId(questionGroupId, questionHeaderId);
                questionHeaderList.get(i).setModifiersList(questionModifierList);
            }
            if (questionModifierList.size() > 0) {
                objectList.addAll(questionHeaderList);
            }
        }

        int comboGroupId = product.getComboGroupId();
        Log.d(TAG, "initRecyclerView: combo group id " + comboGroupId);

        if (comboGroupId > 0) {
            Log.d(TAG, "initRecyclerView: get combos");
            List<ComboHeader> comboHeaderList = databaseHelper.getComboHeaderByGroupId(comboGroupId);

            Log.d(TAG, "initRecyclerView: combo list size " + comboHeaderList.size());
            for (int i = 0; i < comboHeaderList.size(); i++) {
                Log.d(TAG, "initRecyclerView: combo " + i + ", " + comboHeaderList.get(i).getDescription());
                int comboHeaderId = comboHeaderList.get(i).getComboHeaderId();
                comboModifierList = databaseHelper.getComboModifierByQuestionHeaderId(comboGroupId, comboHeaderId);
                for (int j = 0; j < comboModifierList.size(); j++) {
                    Log.d(TAG, "initRecyclerView: modifier " + j + ", " + comboModifierList.get(j).getDescription());
                }
                comboHeaderList.get(i).setModifiersList(comboModifierList);
            }
            if (comboModifierList.size() > 0) {
                objectList.addAll(comboHeaderList);
            }
        }

        if (comboModifierList.size() == 0 && questionModifierList.size() == 0) {
            binding.recyclerView.setVisibility(View.GONE);
        } else {
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
            binding.recyclerView.setAdapter(recyclerAdapter);
        }
    }

    private void fillContent() {
        Log.d(TAG, "fillContent: start");
        binding.productDescription.setText(product.getDescription());
        binding.productInfo.setText(product.getProdInfo());
        binding.totalPrice.setText(FormatPrice.getFormattedPrice(product.getFinalPrice()));

        if (SHOW_REMARK_ALL == 1) {
            binding.remarkLayout.setVisibility(View.VISIBLE);
        } else if (product.getModifiersGroupId() > 0) {
            if (SHOW_REMARK_QUESTIONS == 1) {
                binding.remarkLayout.setVisibility(View.VISIBLE);
            }
        }

        Glide.with(this)
                .load(product.getProduct_picture())
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(R.drawable.category_place_holder)
                .into(binding.productImage);
    }

    public void calculatePrice() {
        double price = product.getFinalPrice();
        int count = Integer.parseInt(binding.quantity.getText().toString());
        price = price * count;
        binding.totalPrice.setText(FormatPrice.getFormattedPrice(price));
        calculateExtraPrice();
    }

    public void calculateExtraPrice() {
        Log.d(TAG, "calculateExtraPrice: start");
        int totalPrice = 0;

        List<CartQuestionHeader> cartQuestionHeaderList = recyclerAdapter.cartQuestionHeaderList;
        if (cartQuestionHeaderList != null && cartQuestionHeaderList.size() > 0) {
            for (int i = 0; i < cartQuestionHeaderList.size(); i++) {
                CartQuestionHeader cartQuestionHeader = cartQuestionHeaderList.get(i);
                for (int j = 0; j < cartQuestionHeader.getCartQuestionModifiers().size(); j++) {
                    CartQuestionModifier cartQuestionModifier = cartQuestionHeader.getCartQuestionModifiers().get(j);
                    if (cartQuestionModifier.isSelected()) {
                        double price = cartQuestionModifier.getPrice();
                        totalPrice += price;
                    }
                }
            }
            if (totalPrice == 0) {
                binding.extraLayout.animate().alpha(0f).setDuration(500).start();
            } else {
                if (binding.extraLayout.getAlpha() == 0) {
                    binding.extraLayout.animate().alpha(1f).setDuration(500).start();
                }
            }

            int prodCount = Integer.parseInt(productInfoActivity.binding.quantity.getText().toString());
            int price = totalPrice * prodCount;
            binding.extraPrice.setText(String.format("+ %s", FormatPrice.getFormattedPrice(price)));
        }

        List<CartComboHeader> cartComboHeaderList = recyclerAdapter.cartComboHeaderList;
        if (cartComboHeaderList != null && cartComboHeaderList.size() > 0) {
            for (int i = 0; i < cartComboHeaderList.size(); i++) {
                CartComboHeader cartComboHeader = cartComboHeaderList.get(i);
                for (int j = 0; j < cartComboHeader.getCartComboModifiers().size(); j++) {
                    CartComboModifier cartComboModifier = cartComboHeader.getCartComboModifiers().get(j);
                    if (cartComboModifier.getCount() > 0) {
                        double price = cartComboModifier.getPrice();
                        price = price * cartComboModifier.getCount();
                        totalPrice += price;
                    }
                }
            }
            if (totalPrice == 0) {
                binding.extraLayout.animate().alpha(0f).setDuration(500).start();
            } else {
                binding.extraLayout.animate().alpha(1f).setDuration(500).start();
            }

            int prodCount = Integer.parseInt(productInfoActivity.binding.quantity.getText().toString());
            int price = totalPrice * prodCount;
            binding.extraPrice.setText(String.format("+ %s", FormatPrice.getFormattedPrice(price)));
        }

    }

    private void initClickListeners() {
        binding.plusBtn.setOnClickListener(view -> {
            if (holdCount < 998) {
                int quantity = Integer.parseInt(binding.quantity.getText().toString());
                if (holdCount == 0 || quantity <= holdCount) {
                    quantity++;
                    holdCount++;
                    binding.quantity.setText(String.valueOf(quantity));
                    calculatePrice();
                }
            }
        });

        binding.minusBtn.setOnClickListener(view -> {
            int quantity = Integer.parseInt(binding.quantity.getText().toString());
            if (quantity == 1) {
                binding.quantity.setText("1");
            } else {
                quantity--;
                holdCount--;
                binding.quantity.setText(String.valueOf(quantity));
            }
            calculatePrice();

        });

        binding.addToCart.setOnClickListener(view -> {
            Log.d(TAG, "initClickListeners: product " + product.getDescription());

            String remark = "";
            int found = 0;
            if (binding.remarkText.getText().toString().length() != 0) {
                remark = binding.remarkText.getText().toString().trim().replace("'", " ");
            }

            if (product.getModifiersGroupId() > 0 || product.getComboGroupId() > 0) {
                List<CartComboModifier> newCartComboModifierList;
                List<CartComboHeader> newCartComboHeaderList = new ArrayList<>();
                List<CartQuestionModifier> newCartQuestionModifierList;
                List<CartQuestionHeader> newCartQuestionHeaderList = new ArrayList<>();

                if (recyclerAdapter.cartQuestionHeaderList.size() != 0) {
                    for (int i = 0; i < recyclerAdapter.cartQuestionHeaderList.size(); i++) {
                        CartQuestionHeader cartQuestionHeader = recyclerAdapter.cartQuestionHeaderList.get(i);

                        if (cartQuestionHeader.getRequired() == 1 && cartQuestionHeader.getCount() == 0) {
                            showAlertDialog();
                            return;
                        }
                        if (cartQuestionHeader.getCount() > 0) {
                            newCartQuestionModifierList = new ArrayList<>();
                            for (int j = 0; j < cartQuestionHeader.getCartQuestionModifiers().size(); j++) {
                                CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(j);
                                if (modifier.isSelected()) {
                                    Log.d(TAG, "initClickListeners: keep " + modifier.getDescription());
                                    newCartQuestionModifierList.add(modifier);
                                    found = 1;
                                }
                            }
                            newCartQuestionHeaderList.add(cartQuestionHeader);
                            newCartQuestionHeaderList.get(newCartQuestionHeaderList.indexOf(cartQuestionHeader)).setCartQuestionModifiers(newCartQuestionModifierList);

                        }
                    }
                }
                if (recyclerAdapter.cartComboHeaderList.size() != 0) {
                    for (int i = 0; i < recyclerAdapter.cartComboHeaderList.size(); i++) {
                        CartComboHeader cartComboHeader = recyclerAdapter.cartComboHeaderList.get(i);

                        if (cartComboHeader.getRequired() == 1 && cartComboHeader.getSelectedItemCount() == 0) {
                            showAlertDialog();
                            return;
                        }
                        if (cartComboHeader.getSelectedItemCount() > 0) {
                            newCartComboModifierList = new ArrayList<>();
                            for (int j = 0; j < cartComboHeader.getCartComboModifiers().size(); j++) {
                                CartComboModifier modifier = cartComboHeader.getCartComboModifiers().get(j);
                                if (modifier.getCount() > 0) {
//                                    modifier.setCount(modifier.getCount()*Integer.parseInt(binding.quantity.getText().toString()));
                                    newCartComboModifierList.add(modifier);
                                    found = 1;
                                }
                            }
                            newCartComboHeaderList.add(cartComboHeader);
                            newCartComboHeaderList.get(newCartComboHeaderList.indexOf(cartComboHeader)).setCartComboModifiers(newCartComboModifierList);

                        }

                    }
                }
                if (found == 1) {
                    cartData.addProductWithModifiers(product, remark, newCartQuestionHeaderList, newCartComboHeaderList, Integer.parseInt(binding.quantity.getText().toString()));
                } else {
                    cartData.addProduct(product, remark, Integer.parseInt(binding.quantity.getText().toString()));
                }

            } else {
                cartData.addProduct(product, remark, Integer.parseInt(binding.quantity.getText().toString()));
            }

            setResult(2);
            placeOrderFlag = true;
            onBackPressed();

        });

        binding.back.setOnClickListener(view -> onBackPressed());

        binding.productImage.setOnClickListener(v ->
        {
//            binding.quantity.setText("998");
//            calculatePrice();
            Intent intent = new Intent(ProductInfoActivity.this, GalleryActivity.class);
            intent.putExtra("PictureUrl", product.getProduct_picture());
            startActivity(intent);
        });

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        if (placeOrderFlag) {
            super.onBackPressed();
            supportFinishAfterTransition();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            if (binding.remarkLayout.getVisibility() == View.VISIBLE) {
                if (binding.remarkText.isFocused()) {
                    binding.remarkText.clearFocus();
                    imm.hideSoftInputFromWindow(binding.remarkText.getWindowToken(), 0);
                } else {
                    super.onBackPressed();
                    supportFinishAfterTransition();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
            } else {
                super.onBackPressed();
                supportFinishAfterTransition();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        }
    }

    private void showAlertDialog() {
        StringBuilder message = new StringBuilder();
        message.append("You must choose: ").append("\n");

        if (recyclerAdapter.cartQuestionHeaderList.size() != 0) {
            for (int i = 0; i < recyclerAdapter.cartQuestionHeaderList.size(); i++) {
                CartQuestionHeader cartQuestionHeader = recyclerAdapter.cartQuestionHeaderList.get(i);
                if (cartQuestionHeader.getRequired() == 1 && cartQuestionHeader.getCount() == 0) {
                    message.append("\n-").append(cartQuestionHeader.getDescription());
                }
            }
        }

        if (recyclerAdapter.cartComboHeaderList.size() != 0) {
            for (int i = 0; i < recyclerAdapter.cartComboHeaderList.size(); i++) {
                CartComboHeader cartComboHeader = recyclerAdapter.cartComboHeaderList.get(i);
                if (cartComboHeader.getRequired() == 1 && cartComboHeader.getSelectedItemCount() == 0) {
                    message.append("\n-").append(cartComboHeader.getDescription());
                }
            }
        }

        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, message.toString());
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "cancel");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
        arguments.putInt(CustomDialog.DIALOG_ID, DIALOG_MISSING_MODIFIER);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        if (dialogId == DIALOG_MISSING_MODIFIER) {
            dialog.dismiss();
        }
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {

    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }

    public int getProdNum() {
        return product.getProdNum();
    }

}