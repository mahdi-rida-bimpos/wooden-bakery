package com.bimpos.wooden.bakery.tables;

public class ProductsTable {

    public static final String TABLE_NAME = "products";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String PROD_NUM = "prodNum";
        public static final String CAT_ID = "catId";
        public static final String DESCRIPTION = "description";
        public static final String DESCRIPTION_2 = "description2";
        public static final String PROD_INFO = "prodInfo";
        public static final String PROD_INFO_2 = "prodInfo2";
        public static final String PRICE = "price";
        public static final String PRICE2 = "price2";
        public static final String PIC_PATH = "picPath";
        public static final String PICTURE = "picture";
        public static final String PRODUCT_PICTURE = "productPicture";
        public static final String TITLE = "title";
        public static final String P_ID = "pId";
        public static final String THUMB_PICTURE = "thumbPicture";
        public static final String BRAND = "brand";
        public static final String REF_CODE_1 = "refCode1";
        public static final String REF_CODE_2 = "refCode2";
        public static final String IS_TAXABLE_1 = "isTaxable1";
        public static final String IS_TAXABLE_2 = "isTaxable2";
        public static final String IS_TAXABLE_3 = "isTaxable3";
        public static final String MODIFIERS_GROUP_ID = "ModifiersGroupId";
        public static final String COMBO_GROUP_ID = "ComboGroupId";
        public static final String TAGS = "tags";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
