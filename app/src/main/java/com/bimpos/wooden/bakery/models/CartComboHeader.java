package com.bimpos.wooden.bakery.models;

import java.util.List;

public class CartComboHeader {

    private String description;
    private int min,max,required,selectedItemCount;
    private List<CartComboModifier> cartComboModifiers;

    public CartComboHeader(String description, int min, int max, int required, int selectedItemCount, List<CartComboModifier> cartComboModifiers) {
        this.description = description;
        this.cartComboModifiers = cartComboModifiers;
        this.min = min;
        this.max = max;
        this.required = required;
        this.selectedItemCount =selectedItemCount;
    }

    public int getSelectedItemCount() {
        return selectedItemCount;
    }

    public void setSelectedItemCount(int selectedItemCount) {
        this.selectedItemCount = selectedItemCount;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getRequired() {
        return required;
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CartComboModifier> getCartComboModifiers() {
        return cartComboModifiers;
    }

    public void setCartComboModifiers(List<CartComboModifier> cartComboModifiers) {
        this.cartComboModifiers = cartComboModifiers;
    }
}
