package com.bimpos.wooden.bakery.cakeGallery.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.Bundle;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.cakeGallery.adapters.CakeItemRvAdapter;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityCakeGalleryBinding;
import com.bimpos.wooden.bakery.helpers.CustomLoadingOrder;
import com.bimpos.wooden.bakery.models.CakeGallery;
import com.bimpos.wooden.bakery.models.CakeItem;

import java.util.List;

public class CakeItemsActivity extends AppCompatActivity {

    private ActivityCakeGalleryBinding binding;
    private List<CakeItem> cakeItemList;
    private DatabaseHelper databaseHelper;
    public CustomLoadingOrder loadingOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCakeGalleryBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        getContent();
        initClickListeners();

    }

    private void getContent() {
        int galleryId = getIntent().getIntExtra("galleryId", -1);
        if (galleryId != -1) {
            CakeGallery cakeGallery = databaseHelper.getCakeGallery(galleryId);
            cakeItemList = cakeGallery.getGalleryPhotosList();
            binding.galleryTitle.setText(cakeGallery.getGalleryName());
            initItemsRecyclerView();
        }
    }

    private void initItemsRecyclerView() {
        binding.recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        CakeItemRvAdapter adapter = new CakeItemRvAdapter(cakeItemList, this,this);
        binding.recyclerView.setAdapter(adapter);
        loadingOrder = new CustomLoadingOrder(this);
    }

    private void initClickListeners() {
        binding.back.setOnClickListener(v -> finish());
    }

    private void initVariables() {
        databaseHelper = MainApplication.getDatabase();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}