package com.bimpos.wooden.bakery.application;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.inputmethod.InputMethodManager;

import com.bimpos.wooden.bakery.BuildConfig;
import com.bimpos.wooden.bakery.database.AppDatabase;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.helpers.Constant;



public class MainApplication extends Application {

    private Context context;
    @SuppressLint("StaticFieldLeak")
    private static MainApplication mainApplication;
    private static SharedPreferences preferences;
    private static DatabaseHelper databaseHelper;
    private static InputMethodManager imm;

    @Override
    public void onCreate() {
        super.onCreate();
        mainApplication = this;
        context = getApplicationContext();
        databaseHelper = new DatabaseHelper(this);
        preferences = context.getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE);
        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
    }

    public Context getContext() {
        return context;
    }

    public static MainApplication getMainApplication() {
        return mainApplication;
    }

    public static DatabaseHelper getDatabase() {
        return databaseHelper;
    }

    public static SharedPreferences getPreferences() {
        return preferences;
    }

    public static InputMethodManager getImm() {
        return imm;
    }
}
