package com.bimpos.wooden.bakery.branches.adapters;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.branches.activities.BranchesActivity;
import com.bimpos.wooden.bakery.branches.activities.BranchesDetailsActivity;
import com.bimpos.wooden.bakery.models.Branches;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import java.util.ArrayList;
import java.util.List;

public class BranchesRvAdapter extends RecyclerView.Adapter<BranchesRvAdapter.ViewHolder> implements Filterable {

    private final List<Branches> branchList;
    private List<Branches> filteredList;
    private final Context context;
    private final BranchesActivity activity;
    private long mLastClick = 0;
    private static final String TAG = "BranchesRvAdapter";

    public BranchesRvAdapter(List<Branches> branchList, Context context, BranchesActivity activity) {
        this.branchList = branchList;
        this.activity = activity;
        this.filteredList = branchList;
        this.context = context;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    filteredList = branchList;
                } else {
                    List<Branches> newFilteredList = new ArrayList<>();
                    for (Branches branches : branchList) {
                        if (branches.getName().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            newFilteredList.add(branches);
                        }
                    }
                    filteredList = newFilteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @SuppressLint("NotifyDataSetChanged")
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<Branches>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_branch, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Branches branch = filteredList.get(position);

        Log.d(TAG, "onBindViewHolder: branch " + branch.getName() + " lon:" + branch.getLongitude() + ", lat: " + branch.getLatitude());
        holder.isOpenText.setVisibility(View.GONE);
        Glide.with(context)
                .load(branch.getPicPath())
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(R.drawable.wooden_branch)
                .into(holder.branchImage);
        //todo we need start time and end time for branch open time
        holder.branchName.setText(branch.getName());
        holder.openingHours.setText(branch.getOpeningHours());

        if (branch.getLatitude() > 0.0 && branch.getLongitude() > 0.0) {
            Log.d(TAG, "onBindViewHolder: assign click");
            holder.mapBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.branch_map_gold_icon));

            holder.mainLayout.setOnClickListener(v -> {

                if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                    return;
                }
                mLastClick = SystemClock.elapsedRealtime();

                Intent intent = new Intent(context, BranchesDetailsActivity.class).putExtra(Branches.class.getSimpleName(), branch);
                ActivityOptions options = ActivityOptions
                        .makeSceneTransitionAnimation(activity, holder.mainLayout, "activity_branches_details_mainLayout");
                // start the new activity
                activity.startActivity(intent, options.toBundle());
            });

            holder.mapBtn.setOnClickListener(v -> {
                Log.d(TAG, "onBindViewHolder: start");
                if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                    return;
                }
                mLastClick = SystemClock.elapsedRealtime();

                Intent intent = new Intent(context, BranchesDetailsActivity.class).putExtra(Branches.class.getSimpleName(), branch);
                ActivityOptions options = ActivityOptions
                        .makeSceneTransitionAnimation(activity, holder.mainLayout, "activity_branches_details_mainLayout");
                // start the new activity
                activity.startActivity(intent, options.toBundle());
            });
        } else {
            Log.d(TAG, "onBindViewHolder: dont assign ");
            Log.d(TAG, "onBindViewHolder: branch "+branch);
            holder.mainLayout.setOnClickListener(null);
            holder.mapBtn.setOnClickListener(null);
            holder.mapBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.branch_map_grey_icon));
        }

        if (branch.getPhone().length() == 0) {
            holder.phone.setText("Not available");
        } else {
            holder.phone.setText(branch.getPhone());
        }

        if (branch.getOpeningHours().length() == 0) {
//            holder.isOpenText.setText("The store is closed now");
//            holder.isOpenText.setTextColor(ContextCompat.getColor(context, R.color.pantoneRed));
            holder.openingHours.setText("Not available");
        } else {
//            holder.isOpenText.setText("The store is open now");
//            holder.isOpenText.setTextColor(ContextCompat.getColor(context, R.color.green));
            holder.openingHours.setText(branch.getOpeningHours());
        }

        if (branch.getAcceptsDelivery() == 1) {
            holder.deliveryIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check));
        } else {
            holder.deliveryIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_close_red));
        }

        if (branch.getAcceptsTakeAway() == 1) {
            holder.takeAwayIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check));
        } else {
            holder.takeAwayIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_close_red));
        }

        if (branch.getPhone().length() != 0) {

            if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();

            holder.callBtn.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + branch.getPhone() + ""));
                context.startActivity(intent);

            });
        } else {
            holder.callBtn.setOnClickListener(null);
            holder.callBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.branch_call_grey_icon));
        }

        if (branch.getEmail().length() != 0) {
            holder.emailBtn.setOnClickListener(v -> {

                if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                    return;
                }
                mLastClick = SystemClock.elapsedRealtime();

                Intent i = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", branch.getEmail(), null));
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                context.startActivity(Intent.createChooser(i, "Send email"));
            });
        } else {
            holder.emailBtn.setOnClickListener(null);
            holder.emailBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.branch_email_grey_icon));
        }
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView branchName, openingHours, isOpenText, phone;
        ImageView emailBtn, mapBtn, callBtn, branchImage, takeAwayIcon, deliveryIcon;
        LinearLayout mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            branchName = itemView.findViewById(R.id.recycler_branch_name);
            openingHours = itemView.findViewById(R.id.recycler_branch_openingHours);
            isOpenText = itemView.findViewById(R.id.recycler_branch_isOpenText);
            phone = itemView.findViewById(R.id.recycler_branch_phone);
            emailBtn = itemView.findViewById(R.id.recycler_branch_emailBtn);
            mapBtn = itemView.findViewById(R.id.recycler_branch_mapBtn);
            callBtn = itemView.findViewById(R.id.recycler_branch_callBtn);
            branchImage = itemView.findViewById(R.id.recycler_branch_image);
            mainLayout = itemView.findViewById(R.id.recycler_branch_mainLayout);
            takeAwayIcon = itemView.findViewById(R.id.recycler_branch_takeAwayIcon);
            deliveryIcon = itemView.findViewById(R.id.recycler_branch_deliveryIcon);
        }
    }
}
