package com.bimpos.wooden.bakery.homePage.activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bimpos.wooden.bakery.BuildConfig;
import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.about_us.AboutUsActivity;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.branches.activities.BranchesActivity;
import com.bimpos.wooden.bakery.cakeGallery.activities.CakeGalleryActivity;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityMainBinding;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.networkRequest.GetToken;
import com.bimpos.wooden.bakery.networkRequest.ListAnOrder;
import com.bimpos.wooden.bakery.homePage.fragments.ContactUsFragment;
import com.bimpos.wooden.bakery.homePage.fragments.PicPhotoFragment;
import com.bimpos.wooden.bakery.models.Cart;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.models.Member;
import com.bimpos.wooden.bakery.models.Order;
import com.bimpos.wooden.bakery.models.OrderHistory;
import com.bimpos.wooden.bakery.orderHistory.activities.OrderHistoryActivity;
import com.bimpos.wooden.bakery.orderOnline.activities.BigCategoriesActivity;
import com.bimpos.wooden.bakery.orderOnline.activities.CartOrderActivity;
import com.bimpos.wooden.bakery.orderOnline.activities.GalleryActivity;
import com.bimpos.wooden.bakery.orderOnline.activities.OrdersActivity;
import com.bimpos.wooden.bakery.product_catalog.activities.CategoriesCatalogActivity;
import com.bimpos.wooden.bakery.profile.activities.LoginActivity;
import com.bimpos.wooden.bakery.profile.activities.ProfileActivity;
import com.bimpos.wooden.bakery.profile.activities.SmsVerificationActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;


public class MainActivity extends AppCompatActivity implements ListAnOrder.OnListOrderComplete,
        GetToken.OnTokenReceived, PicPhotoFragment.OnPicPhotoSelect, CustomDialog.DialogEvents {

    private static final String TAG = "MainActivity";
    public static final int GALLERY = 10;
    public static final int CAMERA = 20;
    private final int YES_NO_EXIT = 1;
    private final int DIALOG_DISMISS = 2;
    private static final int CAMERA_RESULT_CODE = 5;
    private static final int GALLERY_RESULT_CODE = 6;
    private boolean canClick = true;
    private ActivityMainBinding binding;
    private DatabaseHelper databaseHelper;
    private ContactUsFragment contactUsFragment;
    private PicPhotoFragment picPhotoFragment;
    private Cart cartData;
    private Member member;
    private int historyCount = 0;
    private SharedPreferences sharedPreferences;
    private CustomDialog dialog;
    private boolean newOrdersFound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initClickListeners();
        checkMember();
    }

    private void checkMember() {
        member = databaseHelper.getMember();
        if (member != null) {
            if (member.getFirstName().length() == 0 || member.getLastName().length() == 0) {
                showMessageDialog("Please complete your profile information before placing orders.", DIALOG_DISMISS);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: start");

        if (contactUsFragment.isAdded()) {
            contactUsFragment.dismiss();
        }
        if (picPhotoFragment.isAdded()) {
            picPhotoFragment.dismiss();
        }
        checkOrderHistory();
        checkCartOrderCount();
        initMember();
        canClick = true;
    }

    private void initMember() {
        member = databaseHelper.getMember();
        if (member != null) {
            String fullName = member.getFirstName() + " " + member.getLastName();
            binding.memberName.setText(fullName);
            Bitmap profileBitmap = databaseHelper.getMemberPhoto();
            if (profileBitmap != null) {
                binding.profileImage.setImageBitmap(profileBitmap);
            }
        }
    }

    private void checkCartOrderCount() {
        Order order = databaseHelper.getCurrentOrder();
        if (order != null) {
            Type orderCartType = new TypeToken<List<CartProduct>>() {
            }.getType();
            Cart.getCartData().setCartItemsList(new Gson().fromJson(order.getCartItemList(), orderCartType));
        }
        Log.d(TAG, "checkCartOrderCount: " + cartData.getCartCount());
        if (cartData.getCartCount() > 0) {
            binding.orderNotification.setVisibility(View.VISIBLE);
            binding.orderCount.setText(String.valueOf(cartData.getCartCount()));
            binding.cart.setOnClickListener(v -> gotoCartActivity());
        } else {
            binding.orderNotification.setVisibility(View.GONE);
            binding.orderCount.setText("0");
            binding.cart.setOnClickListener(null);
        }
    }

    private void gotoCartActivity() {
        Intent intent = new Intent(this, CartOrderActivity.class)
                .putExtra(Constant.IS_FROM_MAIN_ACTIVITY, true);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void initVariables() {
        sharedPreferences = MainApplication.getPreferences();
        databaseHelper = MainApplication.getDatabase();
        contactUsFragment = new ContactUsFragment();
        picPhotoFragment = new PicPhotoFragment(this);
        dialog = new CustomDialog(this);

        cartData = Cart.getCartData();
        if (sharedPreferences.getInt(Constant.SETTINGS_GALLERY_CATALOG_ID, 0) == 0) {
            binding.cakes3D.setVisibility(View.GONE);
        }

        if (sharedPreferences.getInt(Constant.SETTINGS_PRODUCT_CATALOG_ID, 0) == 0) {
            binding.productCatalog.setVisibility(View.GONE);
        }

        binding.version.setText(BuildConfig.VERSION_NAME);
    }

    private void checkOrderHistory() {
        int count = databaseHelper.getCartHistoryCount();
        historyCount = 0;
        Log.d(TAG, "checkCartHistory: count " + count);
        if (count > 0) {
            binding.orderHistory.setVisibility(View.VISIBLE);
            getOrderHistoryStatus();
        } else {
            binding.orderHistory.setVisibility(View.GONE);
        }
    }

    private void getOrderHistoryStatus() {
        Log.d(TAG, "getOrderHistoryStatus: start");
        int found = 0;
        List<OrderHistory> orderHistoryList = databaseHelper.getOrderHistoryList(-1);
        for (int i = 0; i < orderHistoryList.size(); i++) {
            OrderHistory history = orderHistoryList.get(i);
            if (history.getIsNew() == 1) {
                found = 1;
                new ListAnOrder(this, history).execute();
            }
        }
        if (found == 0) {
            binding.orderHistoryNotification.setVisibility(View.GONE);
        }
    }

    @Override
    public void onListOrderComplete(int status, OrderHistory history) {
//        Log.d(TAG, "onListOrderComplete: status " + status);
        if (status == -1) {
            new GetToken(this).execute();
        } else if (status == -2) {
            //order not found, ignored
            Log.d(TAG, "onListOrderComplete: order not found");
        } else {
            history.setOrderStatus(status);
            if (status == 5 || status == 11) {
                history.setIsNew(0);
            }
            databaseHelper.updateOrderHistory(history);
            updateHistoryCount();
        }
    }

    private void updateHistoryCount() {
        historyCount = 0;
        List<OrderHistory> orderHistoryList = databaseHelper.getOrderHistoryList(-1);
        for (int i = 0; i < orderHistoryList.size(); i++) {
            OrderHistory history = orderHistoryList.get(i);
            if (history.getIsNew() == 1) {
                historyCount++;
            }
        }
        if (historyCount == 0) {
            binding.orderHistoryNotification.setVisibility(View.GONE);
        } else {
            binding.orderHistoryNotification.setVisibility(View.VISIBLE);
            binding.orderHistoryCount.setVisibility(View.VISIBLE);
            binding.orderHistoryCount.setText(String.valueOf(historyCount));
        }
    }


    @Override
    public void onTokenReceived(int status) {
        if (status == 1) {
            getOrderHistoryStatus();
        }
    }

    private void initClickListeners() {

        binding.orderHistory.setOnClickListener(v -> {
            if (canClick) {
                gotoOrderHistoryActivity();
            }
        });

        binding.settings.setOnClickListener(v -> {
            if (canClick) {
                if (member != null) {
                    if (member.getMobileValidated() == 0) {
                        gotoGetSmsVerificationActivity();
                    } else {
                        gotoProfileActivity();
                    }
                } else {
                    gotoLoginActivity();
                }
            }
        });

        binding.facebookIcon.setOnClickListener(view -> {
            if (canClick) {
                Log.d(TAG, "onCreate: click");
                canClick = false;
                openFacebookPage();
            }
        });

        binding.instagramIcon.setOnClickListener(view -> {
            if (canClick) {
                Log.d(TAG, "onCreate: click");
                canClick = false;
                openInstagramPage();
            }
        });

        binding.profileImage.setOnClickListener(v -> {
            if (canClick) {
                if (member == null) {
                    gotoLoginActivity();
                } else if (databaseHelper.getMemberPhoto() != null) {
                    showProfileImage();
                } else {
                    gotoProfileActivity();
                }
            }

        });

        binding.welcomeLayout.setOnClickListener(v -> {
            if (canClick) {
                if (member != null) {
                    if (member.getMobileValidated() == 0) {
                        gotoGetSmsVerificationActivity();
                    } else {
                        gotoProfileActivity();
                    }
                } else {
                    gotoLoginActivity();
                }
            }
        });

        binding.poweredBy.setOnClickListener(view -> {
            if (canClick) {
                Log.d(TAG, "onCreate: click");
                canClick = false;
                openBimWebsite();
            }

        });

        binding.cakes3D.setOnClickListener(v -> {
            if (canClick) {
                Log.d(TAG, "onCreate: click");
                canClick = false;
                gotoCakeGalleryActivity();
            }
        });

        binding.orderOnline.setOnClickListener(view -> {

            if (canClick) {
                canClick = false;
                if (sharedPreferences.getInt(Constant.SETTINGS_SHOW_BIG_CATEGORIES, 0) == 1) {
                    gotoBigCategoriesActivity();
                } else {
                    gotoOrdersActivity();
                }
            }
        });

        binding.aboutUs.setOnClickListener(view -> {
            if (canClick) {
                gotoAboutUsActivity();
            }
        });

        binding.contactUs.setOnClickListener(view -> {
            if (canClick) {
                contactUsFragment.show(getSupportFragmentManager(), "");
            }
        });

        binding.productCatalog.setOnClickListener(v -> {
            if (canClick) {
                gotoCategoryCatalogActivity();
            }
        });

        binding.branches.setOnClickListener(view -> {
            if (canClick) {
                gotoBranchesActivity();
            }
        });
    }

    private void showProfileImage() {

        Intent intent = new Intent(this, GalleryActivity.class)
                .putExtra("PictureUrl", "member");
        startActivity(intent);
    }

    private void performImgPicAction(int which) {
        switch (which) {
            case GALLERY:
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, GALLERY_RESULT_CODE);
                break;

            case CAMERA:
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_RESULT_CODE);
                break;
        }
    }

    @Override
    public void onPicPhotoSelect(int position) {
        performImgPicAction(position);
    }

    private void gotoOrderHistoryActivity() {
        startActivity(new Intent(MainActivity.this, OrderHistoryActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void gotoBranchesActivity() {
        startActivity(new Intent(MainActivity.this, BranchesActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void gotoCategoryCatalogActivity() {
        startActivity(new Intent(MainActivity.this, CategoriesCatalogActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void gotoAboutUsActivity() {
        Intent i = new Intent(this, AboutUsActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void gotoLoginActivity() {
        if (sharedPreferences.getInt(Constant.IS_SMS_SENT, 0) == 1) {
            gotoGetSmsVerificationActivity();
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    private void gotoBigCategoriesActivity() {
        startActivity(new Intent(this, BigCategoriesActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    private void gotoProfileActivity() {
        Intent intent = new Intent(this, ProfileActivity.class);
        if (member != null && (member.getFirstName().length() == 0 || member.getLastName().length() == 0)) {
            intent.putExtra("status", 1);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_down, android.R.anim.fade_out);
    }

    private void gotoCakeGalleryActivity() {
        startActivity(new Intent(this, CakeGalleryActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void gotoGetSmsVerificationActivity() {
        startActivity(
                new Intent(this, SmsVerificationActivity.class)
                        .putExtra("phone", sharedPreferences.getString(Constant.PHONE_NUMBER, "")));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void gotoOrdersActivity() {
        Intent intent = new Intent(this, OrdersActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void openBimWebsite() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.bimpos.com/"));
        startActivity(browserIntent);
    }

    private void openInstagramPage() {
        Uri uri = Uri.parse("http://instagram.com/_u/woodenbakery.leb");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
        likeIng.setPackage("com.instagram.android");
        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/woodenbakery.leb")));
        }
    }

    private void openFacebookPage() {
        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        String facebookUrl = getFacebookPageURL(MainActivity.this);
        facebookIntent.setData(Uri.parse(facebookUrl));
        startActivity(facebookIntent);
    }

    public String getFacebookPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                Log.d(TAG, "getFacebookPageURL: new");
                return "fb://page/" + Constant.FACEBOOK_PAGE_ID;
            } else { //older versions of fb app
                Log.d(TAG, "getFacebookPageURL: old");
                return "fb://page/" + Constant.FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, "getFacebookPageURL: catch");
            return Constant.FACEBOOK_URL; //normal web url
        }
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        if (dialogId == YES_NO_EXIT) {
            dialog.dismiss();
            finish();
        } else if (dialogId == DIALOG_DISMISS) {
            dialog.dismiss();
        }
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        dialog.dismiss();
    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }

    @Override
    public void onBackPressed() {
        showMessageDialog("Are you sure you want to exit ?", YES_NO_EXIT);
    }

    private void showMessageDialog(String message, int dialogId) {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, message);
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        switch (dialogId) {
            case YES_NO_EXIT:
                arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Yes");
                arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "No");
                arguments.putInt("positiveColor", ContextCompat.getColor(this, R.color.pantoneRed));
                break;
            case DIALOG_DISMISS:
                arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Ok");
                arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
                break;
        }
        arguments.putInt(CustomDialog.DIALOG_ID, dialogId);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }
}
