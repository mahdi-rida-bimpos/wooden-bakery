package com.bimpos.wooden.bakery.networkRequest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.wooden.bakery.BuildConfig;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.helpers.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class CheckNewPrices extends AsyncTask<String, String, String> {

    private static final String TAG = "check new order";
    HttpURLConnection conn;
    private final SharedPreferences prefs;
    private final OnCheckNewPrice listener;
    URL url = null;
    private final int prodNum;
    private final int lastItem;
    private final int cartId;

    public interface OnCheckNewPrice {
        void onCheckNewPrice(int status, int lastItem, int prodNum, int cartId, double newPrice,double newPrice2);
    }

    public CheckNewPrices(int cartId, int prodNum, int lastItem, OnCheckNewPrice listener) {
        prefs = MainApplication.getPreferences();
        this.listener = listener;
        this.prodNum = prodNum;
        this.lastItem = lastItem;
        this.cartId = cartId;
    }

    @Override
    protected String doInBackground(String... params) {

        try {
            Log.d(TAG, "doInBackground: check price start");

            url = new URL(Constant.BASE_URL + "product/" + prodNum);
            Log.d(TAG, "doInBackground: url " + url);
            conn = (HttpURLConnection) url.openConnection();
            String token = prefs.getString(Constant.TOKEN, "");
            Log.d(TAG, "doInBackground: token is " + token);
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestMethod("GET");
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(10000);
            conn.connect();

            int response_code = conn.getResponseCode();
            Log.d(TAG, "doInBackground: response code " + response_code);
            if (response_code == 429) {
                return "retry";
            }
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result " + result.toString());
                return (result.toString());
            } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                return ("token");
            } else {
                return ("notFound");
            }
        } catch (Exception e) {
            try {
                Log.d(TAG, "doInBackground: error " + e.getMessage() + ", " + conn.getResponseMessage());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onPostExecute(String result) {

//        if (prodNum >-1) {
//            listener.onCheckNewPrice(0, lastItem, prodNum, 45000);
//            return;
//        }

//        listener.onCheckNewPrice(1, lastItem, prodNum,cartId, 100000);


        Log.d(TAG, "onPostExecute: result " + result);
        if (result.equalsIgnoreCase("retry")) {
            listener.onCheckNewPrice(-2, lastItem, prodNum, cartId, 0,0);
        } else {
            if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("notFound")) {
                listener.onCheckNewPrice(0, lastItem, prodNum, cartId, 0,0);
            } else if (result.equalsIgnoreCase("token")) {
                listener.onCheckNewPrice(-1, 0, prodNum, cartId, 0,0);
            } else {
                try {
                    JSONObject object = new JSONObject(result);
                    JSONArray parentArray = object.getJSONArray("products");
                    if (parentArray.length() != 0) {
                        JSONObject productObject = parentArray.getJSONObject(0);
                        double price = productObject.getDouble("price");
                        double price2 = 0;
                        if(!productObject.isNull("price2")){
                            price2 = productObject.getDouble("price2");
                        }
                        listener.onCheckNewPrice(1, lastItem, prodNum, cartId, price,price2);
                    } else {
                        listener.onCheckNewPrice(0, lastItem, prodNum, cartId, 0,0);
                    }
                } catch (JSONException e) {
                    Log.d(TAG, "onPostExecute: error " + e.getMessage());
                    listener.onCheckNewPrice(0, lastItem, prodNum, cartId, 0,0);
                }
            }
        }
    }
}
