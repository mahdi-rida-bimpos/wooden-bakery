package com.bimpos.wooden.bakery.orderOnline.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.databinding.ActivityBigCategoriesBinding;
import com.bimpos.wooden.bakery.models.Categories;
import com.bimpos.wooden.bakery.orderOnline.adapters.BigCategoriesRvAdapter;

import java.util.List;

public class BigCategoriesActivity extends AppCompatActivity {

    private ActivityBigCategoriesBinding binding;
    private List<Categories> categoriesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBigCategoriesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initRecyclerView();

        binding.back.setOnClickListener(v -> finish());
    }

    private void initVariables() {
        categoriesList = MainApplication.getDatabase().getMenuCategories();
    }

    private void initRecyclerView() {
        BigCategoriesRvAdapter adapter = new BigCategoriesRvAdapter(categoriesList, this,this);
        binding.recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }
}