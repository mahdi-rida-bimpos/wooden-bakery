package com.bimpos.wooden.bakery.tables;

public class QuestionModifierTable {

    public static final String TABLE_NAME= "questionModifiers";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String PROD_NUM = "prodNum";
        public static final String CAT_ID = "catId";
        public static final String DESCRIPTION = "description";
        public static final String DESCRIPTION_2 = "description2";
        public static final String PROD_INFO = "prodInfo";
        public static final String PROD_INFO_2 = "prodInfo2";
        public static final String PRICE = "price";
        public static final String PIC_PATH = "picPath";
        public static final String PICTURE = "picture";
        public static final String QUESTION_HEADER_ID = "questionHeaderId";
        public static final String QUESTION_GROUP_ID = "questionGroupId";
        public static final String STOCK = "stock";
        public static final String ENABLED = "enabled";
        public static final String P_ID = "pId";
        public static final String CLIENT_ID = "clientId";
        public static final String THUMB_PICTURE = "thumbPicture";
        public static final String BRAND = "brand";
        public static final String REF_CODE_1 = "refCode1";
        public static final String REF_CODE_2 = "refCode2";
        public static final String IS_TAXABLE_1 = "isTaxable1";
        public static final String IS_TAXABLE_2 = "isTaxable2";
        public static final String IS_TAXABLE_3 = "isTaxable3";
        public static final String VERSION_ID = "versionId";
        public static final String CREATED_AT = "createdAt";
        public static final String UPDATED_AT = "updatedAt";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
