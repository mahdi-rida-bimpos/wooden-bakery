package com.bimpos.wooden.bakery.models;


public class MessageEvent {

    private int level;
    private String message,status;

    public MessageEvent(String status, int level, String message) {
        this.status = status;
        this.level = level;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public int getLevel() {
        return level;
    }

    public String getMessage() {
        return message;
    }
}
