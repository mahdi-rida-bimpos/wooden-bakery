package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.models.CartComboHeader;
import com.bimpos.wooden.bakery.models.CartProduct;

import java.util.List;

public class CartOrderComboHeaderRvAdapter extends RecyclerView.Adapter<CartOrderComboHeaderRvAdapter.ViewHolder> {

    private final List<CartComboHeader> cartComboHeaderList;
    private final Context context;
    private final CartProduct cartProduct;

    public CartOrderComboHeaderRvAdapter(Context context ,CartProduct cartProduct) {
        this.cartComboHeaderList = cartProduct.getComboList();
        this.cartProduct=cartProduct;
        this.context = context;
    }

    @NonNull
    @Override
    public CartOrderComboHeaderRvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cart_order_question_header, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartOrderComboHeaderRvAdapter.ViewHolder holder, int position) {
        CartComboHeader cartComboHeader = cartComboHeaderList.get(position);
        if(cartComboHeader.getCartComboModifiers().size()!=0){
            holder.headerDescription.setText(cartComboHeader.getDescription());
            CartOrderComboModifierRvAdapter adapter = new CartOrderComboModifierRvAdapter(cartProduct,cartComboHeader.getCartComboModifiers());
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.recyclerView.setAdapter(adapter);
        }

    }

    @Override
    public int getItemCount() {
        return cartComboHeaderList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;
        TextView headerDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recyclerView = itemView.findViewById(R.id.recycler_cart_order_question_header_recyclerView);
            headerDescription = itemView.findViewById(R.id.recycler_cart_order_question_header_description);
        }
    }
}
