package com.bimpos.wooden.bakery.networkRequest;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;

import com.bimpos.wooden.bakery.BuildConfig;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.models.BranchCities;
import com.bimpos.wooden.bakery.models.Branches;
import com.bimpos.wooden.bakery.models.CakeGallery;
import com.bimpos.wooden.bakery.models.CakeItem;
import com.bimpos.wooden.bakery.models.Categories;
import com.bimpos.wooden.bakery.models.Cities;
import com.bimpos.wooden.bakery.models.ComboGroup;
import com.bimpos.wooden.bakery.models.ComboHeader;
import com.bimpos.wooden.bakery.models.ComboModifier;
import com.bimpos.wooden.bakery.models.MessageEvent;
import com.bimpos.wooden.bakery.models.PriceList;
import com.bimpos.wooden.bakery.models.Products;
import com.bimpos.wooden.bakery.models.QuestionGroup;
import com.bimpos.wooden.bakery.models.QuestionHeader;
import com.bimpos.wooden.bakery.models.QuestionModifier;
import com.bimpos.wooden.bakery.tables.BranchCitiesTable;
import com.bimpos.wooden.bakery.tables.BranchesTable;
import com.bimpos.wooden.bakery.tables.CakeGalleryTable;
import com.bimpos.wooden.bakery.tables.CakeItemTable;
import com.bimpos.wooden.bakery.tables.CategoriesTable;
import com.bimpos.wooden.bakery.tables.CitiesTable;
import com.bimpos.wooden.bakery.tables.ComboGroupTable;
import com.bimpos.wooden.bakery.tables.ComboHeaderTable;
import com.bimpos.wooden.bakery.tables.ComboModifierTable;
import com.bimpos.wooden.bakery.tables.PriceListTable;
import com.bimpos.wooden.bakery.tables.ProductStockTable;
import com.bimpos.wooden.bakery.tables.ProductsTable;
import com.bimpos.wooden.bakery.tables.QuestionHeaderTable;
import com.bimpos.wooden.bakery.tables.QuestionModifierTable;
import com.bimpos.wooden.bakery.tables.QuestionsGroupTable;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


public class WoodenNetwork {

    private final DatabaseHelper databaseHelper;
    public Context context;
    private int shouldShowPriceListCurr = 0;
    private final WoodenNetwork woodenNetwork;
    private static final String TAG = "WoodenNetwork";
    private static final String TAG2 = "Products";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private final SharedPreferences prefs;

    public WoodenNetwork(Context context) {
        prefs = MainApplication.getPreferences();
        this.context = context;
        woodenNetwork = this;
        databaseHelper = MainApplication.getDatabase();
    }

    private void sendMessage(String status, String message, int level) {
        Log.d("sender", "Broadcasting message");
        Intent intent = new Intent(Constant.INTENT_FILTER_LAUNCHER);
        intent.putExtra("status", status);
        intent.putExtra("message", message);
        intent.putExtra("level", level);
        EventBus.getDefault().post(new MessageEvent(status,level,message));
//        context.sendBroadcast(intent);
    }

    public class FirstSetup extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... params) {
            try {
                sendMessage("success", "get token", 10);
                Log.d(TAG, "doInBackground: FirstSetup start");
                String username = Constant.userName1;
                String password = Constant.password1;
                url = new URL(Constant.BASE_URL + "login");
                Log.d(TAG, "doInBackground: url " + url);
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("username", username)
                        .appendQueryParameter("password", password);

                String query = builder.build().getEncodedQuery();
                OutputStream outputStream = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
                writer.write(query);
                writer.flush();
                writer.close();
                outputStream.close();

                conn.connect();

                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    return (result.toString());

                } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return ("token");
                } else {
                    return ("notFound");
                }
            } catch (Exception e) {
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @SuppressLint("CommitPrefEdits")
        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute: result " + result);
            if (result.equalsIgnoreCase("notFound") || result.equalsIgnoreCase("exception")) {
                sendMessage(result, "There is something wrong while getting data... please try again later", -1);
            } else {

                try {
                    JSONObject object = new JSONObject(result);
                    String token = object.getString("access_token");
                    @SuppressLint("HardwareIds")
                    String id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(Constant.TOKEN, token);
                    editor.putString(Constant.UID, id + "4");
                    editor.putBoolean(Constant.FIRST_SETUP, false);
                    editor.apply();

                    woodenNetwork.new GetSettings().execute();

                } catch (JSONException e) {
                    sendMessage("error", e.getMessage(), -1);
                }
            }
        }
    }

    public class GetSettings extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... strings) {
            try {

                sendMessage("success", "get settings", 20);
                Log.d(TAG, "doInBackground: get settings start");
                url = new URL(Constant.BASE_URL + "app/settings");
                Log.d(TAG, "doInBackground: url " + url);
                conn = (HttpURLConnection) url.openConnection();
                String token = prefs.getString(Constant.TOKEN, "");
                Log.d(TAG, "doInBackground: token " + token);
                conn.setRequestProperty("Authorization", "Bearer " + token);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestMethod("GET");
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.connect();

                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    return (result.toString());
                } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return ("token");
                } else {
                    return ("notFound");
                }
            } catch (Exception e) {
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute: result " + result);
            if (result.equalsIgnoreCase("token")) {
                woodenNetwork.new FirstSetup().execute();
            } else {
                try {
                    JSONObject parentObject = new JSONObject(result);
                    JSONArray parentArray = parentObject.getJSONArray("appSettings");
                    JSONObject settingsObject = parentArray.getJSONObject(0);

                    prefs.edit()
                            .putString(Constant.SETTINGS_CURRENCY, settingsObject.getString("currency"))
                            .putString(Constant.SETTINGS_MOBILE_PREFIX, settingsObject.getString("mobilePrefix"))
                            .putString(Constant.SETTINGS_MIN_VERSION, settingsObject.getString("androidMinVersion"))
                            .putString(Constant.SETTINGS_CURR_VERSION, settingsObject.getString("androidCurVersion"))
                            .putString(Constant.SETTINGS_PRICE_MASK, settingsObject.getString("moneyFormat"))
                            .putString(Constant.SETTINGS_PHONE_MASK, settingsObject.getString("phoneMask"))
                            .putInt(Constant.SETTINGS_SHOW_REMARK_ALL_ITEMS, settingsObject.getInt("showRemarksOnQuestions"))
                            .putInt(Constant.SETTINGS_SHOW_REMARK_QUESTIONS, settingsObject.getInt("showRemarksOnAllItems"))
                            .putInt(Constant.SETTINGS_MAINTENANCE_MODE, settingsObject.getInt("androidMaintenanceMode"))
                            .putInt(Constant.SETTINGS_SHOW_BIG_CATEGORIES, settingsObject.getInt("showBigCategory"))
                            .putInt(Constant.SETTINGS_ORDER_MENU_CATALOG_ID, settingsObject.getInt("orderMenuCatalogId"))
                            .putInt(Constant.SETTINGS_GALLERY_CATALOG_ID, settingsObject.getInt("galleryCatalogId"))
                            .putInt(Constant.SETTINGS_PRODUCT_CATALOG_ID, settingsObject.getInt("productCatalogId"))
                            .apply();

                    int appVersion = Integer.parseInt(BuildConfig.VERSION_NAME.replace(".", ""));
                    int minVersion = Integer.parseInt(settingsObject.getString("androidMinVersion").replace(".", ""));
                    int currVersion = Integer.parseInt(settingsObject.getString("androidCurVersion").replace(".", ""));
                    int maintenanceMode = settingsObject.getInt("androidMaintenanceMode");


                    Log.d(TAG, "onPostExecute: app version " + appVersion);
                    Log.d(TAG, "onPostExecute: minVersion " + minVersion);
                    Log.d(TAG, "onPostExecute: currVersion " + currVersion);
                    Log.d(TAG, "onPostExecute: maintenanceMode " + maintenanceMode);
                    Log.d(TAG, "onPostExecute: curr mask " + settingsObject.getString("moneyFormat"));
                    if (maintenanceMode == 1) {
                        sendMessage("maintenance", "maintenance", -1);
                    } else if (appVersion < minVersion) {
                        sendMessage("shouldUpdate", "shouldUpdate", -1);
                    } else if (appVersion < currVersion) {
                        sendMessage("canUpdate", "canUpdate", -1);
                    } else {
                        woodenNetwork.new GetCategories().execute();
                    }

                } catch (JSONException e) {
                    Log.d(TAG, "onPostExecute: error " + e.getMessage());
                    prefs.edit()
                            .putString(Constant.SETTINGS_CURRENCY, "LBP")
                            .putString(Constant.SETTINGS_MOBILE_PREFIX, "03,70,71,78,81")
                            .putString(Constant.SETTINGS_MIN_VERSION, "1.0.0")
                            .putString(Constant.SETTINGS_CURR_VERSION, "1.0.0")
                            .putString(Constant.SETTINGS_PRICE_MASK, "###,###,###")
                            .putString(Constant.SETTINGS_PHONE_MASK, "(##)(######)")
                            .putInt(Constant.SETTINGS_SHOW_REMARK_ALL_ITEMS, 1)
                            .putInt(Constant.SETTINGS_SHOW_REMARK_QUESTIONS, 1)
                            .putInt(Constant.SETTINGS_MAINTENANCE_MODE, 0)
                            .putInt(Constant.SETTINGS_SHOW_BIG_CATEGORIES, 1)
                            .putInt(Constant.SETTINGS_ORDER_MENU_CATALOG_ID, 1)
                            .putInt(Constant.SETTINGS_GALLERY_CATALOG_ID, 0)
                            .putInt(Constant.SETTINGS_PRODUCT_CATALOG_ID, 0)
                            .apply();

                    woodenNetwork.new GetCategories().execute();
//                    sendMessage("error", e.getMessage(), -1);
                }
            }
        }
    }

    public class GetCategories extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... params) {

            Log.d(TAG, "doInBackground: get categories start");
            int parentId = prefs.getInt(Constant.SETTINGS_ORDER_MENU_CATALOG_ID, -1);

            if (parentId == -1) {
                sendMessage("Warning", "Missing Categories for the menu", 0);
                return "unsuccessful";
            } else {
                try {

                    sendMessage("success", "start", 40);
                    url = new URL(Constant.BASE_URL + "parents/" + parentId + "/categories");
                    Log.d(TAG, "doInBackground: url " + url);
                    conn = (HttpURLConnection) url.openConnection();
                    String token = prefs.getString(Constant.TOKEN, "");
                    conn.setRequestProperty("Authorization", "Bearer " + token);
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestMethod("GET");
                    conn.setReadTimeout(READ_TIMEOUT);
                    conn.setConnectTimeout(CONNECTION_TIMEOUT);
                    conn.connect();

                    int response_code = conn.getResponseCode();
                    if (response_code == HttpURLConnection.HTTP_OK) {
                        InputStream input = conn.getInputStream();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                        StringBuilder result = new StringBuilder();
                        String line;
                        while ((line = reader.readLine()) != null) {
                            result.append(line);
                        }
                        return (result.toString());
                    } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        return ("token");
                    } else {
                        return ("notFound");
                    }

                } catch (Exception e) {
                    return "exception";

                } finally {
                    conn.disconnect();
                }
            }
        }

        @SuppressLint("CommitPrefEdits")
        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute: result " + result);
            if (result.equalsIgnoreCase("token")) {
                woodenNetwork.new FirstSetup().execute();
            } else {
                try {
//                    Log.d(TAG, "onPostExecute: result "+result);
                    List<Categories> categoriesList = new ArrayList<>();
                    int catId, menuId, parentId, seq;
                    String description, description2, catName, picPath, picture, thumb_picture, footerPicPath, bigPicPath, headerPicPath;

                    JSONArray parentArray = new JSONArray(result);
                    databaseHelper.cleanTable(CategoriesTable.TABLE_NAME);
                    for (int i = 0; i < parentArray.length(); i++) {
//                        Log.d(TAG, "onPostExecute: enter "+i);
                        JSONObject categoryObject = parentArray.getJSONObject(i);
                        catId = categoryObject.getInt("catid");
                        menuId = categoryObject.getInt("menuid");
                        parentId = categoryObject.getInt("parentid");
                        headerPicPath = categoryObject.getString("headerpicpath");
                        seq = categoryObject.getInt("seq");
                        description = categoryObject.getString("descript");
                        description2 = categoryObject.getString("descript2");
                        catName = categoryObject.getString("catname");
                        picPath = categoryObject.getString("picpath");
                        picture = categoryObject.getString("picture");
                        thumb_picture = categoryObject.getString("thumb_picture");
                        footerPicPath = categoryObject.getString("footerpicpath");
                        bigPicPath = categoryObject.getString("bgpicpath");


                        Categories category = new Categories(catId, menuId, parentId, catName, description, description2, picPath, picture, thumb_picture, headerPicPath, footerPicPath, bigPicPath, seq);
//                        Log.d(TAG, "onPostExecute: category " + category.toString());
                        categoriesList.add(category);

                    }
                    Log.d(TAG, "onPostExecute: categories list " + categoriesList.size());
                    databaseHelper.insertCategoryList(categoriesList);
                    woodenNetwork.new GetCatalogCategories().execute();

                } catch (JSONException e) {
                    woodenNetwork.new GetCatalogCategories().execute();
                }
            }
        }
    }

    public class GetCatalogCategories extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... params) {

            Log.d(TAG, "doInBackground: get categories start");
            int parentId = prefs.getInt(Constant.SETTINGS_PRODUCT_CATALOG_ID, 0);

            try {

                sendMessage("success", "start", 40);
                url = new URL(Constant.BASE_URL + "parents/" + parentId + "/categories");
                Log.d(TAG, "doInBackground: url " + url);
                conn = (HttpURLConnection) url.openConnection();
                String token = prefs.getString(Constant.TOKEN, "");
                conn.setRequestProperty("Authorization", "Bearer " + token);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestMethod("GET");
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.connect();

                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    return (result.toString());
                } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return ("token");
                } else {
                    return ("notFound");
                }

            } catch (Exception e) {
                return "exception";

            } finally {
                conn.disconnect();
            }

        }

        @SuppressLint("CommitPrefEdits")
        @Override
        protected void onPostExecute(String result) {
//            Log.d(TAG, "onPostExecute: result " + result);
            if (result.equalsIgnoreCase("token")) {
                woodenNetwork.new FirstSetup().execute();
            } else {
                try {
//                    Log.d(TAG, "onPostExecute: result "+result);
                    List<Categories> categoriesList = new ArrayList<>();
                    int catId, menuId, parentId, seq;
                    String description, description2, catName, picPath, picture, thumb_picture, footerPicPath, bigPicPath, headerPicPath;

                    JSONArray parentArray = new JSONArray(result);

                    for (int i = 0; i < parentArray.length(); i++) {
//                        Log.d(TAG, "onPostExecute: enter "+i);
                        JSONObject categoryObject = parentArray.getJSONObject(i);
                        catId = categoryObject.getInt("catid");
                        menuId = categoryObject.getInt("menuid");
                        parentId = categoryObject.getInt("parentid");
                        headerPicPath = categoryObject.getString("headerpicpath");
                        seq = categoryObject.getInt("seq");
                        description = categoryObject.getString("descript");
                        description2 = categoryObject.getString("descript2");
                        catName = categoryObject.getString("catname");
                        picPath = categoryObject.getString("picpath");
                        picture = categoryObject.getString("picture");
                        thumb_picture = categoryObject.getString("thumb_picture");
                        footerPicPath = categoryObject.getString("footerpicpath");
                        bigPicPath = categoryObject.getString("bgpicpath");


                        Categories category = new Categories(catId, menuId, parentId, catName, description, description2, picPath, picture, thumb_picture, headerPicPath, footerPicPath, bigPicPath, seq);
//                        Log.d(TAG, "onPostExecute: category " + category.toString());
                        categoriesList.add(category);

                    }
                    Log.d(TAG, "onPostExecute: categories list " + categoriesList.size());
                    databaseHelper.insertCategoryList(categoriesList);
                    woodenNetwork.new GetBranches().execute();

                } catch (JSONException e) {
                    woodenNetwork.new GetBranches().execute();
                }
            }
        }
    }

    public class GetBranches extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.d(TAG, "doInBackground: get branches start");
                sendMessage("success", "start", 50);
                url = new URL(Constant.BASE_URL + "branches");
                Log.d(TAG, "doInBackground: url " + url);
                conn = (HttpURLConnection) url.openConnection();
                String token = prefs.getString(Constant.TOKEN, "");
                conn.setRequestProperty("Authorization", "Bearer " + token);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestMethod("GET");
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.connect();

                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    return (result.toString());
                } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return ("token");
                } else {
                    return ("notFound");
                }
            } catch (Exception e) {
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @SuppressLint("CommitPrefEdits")
        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute: branches " + result);
            if (result.equalsIgnoreCase("token")) {
                woodenNetwork.new FirstSetup().execute();
            } else {
                try {
                    List<Branches> branchesList = new ArrayList<>();
                    List<BranchCities> branchCitiesList = new ArrayList<>();

                    int id, isactive, acceptsdelivery, acceptstakeaway;
                    double longitude, latitude;
                    String name, address, email, phone, clientid, picpath, openinghours, picture, deliveryhours;

                    JSONArray parentArray = new JSONArray(result);
                    databaseHelper.cleanTable(BranchesTable.TABLE_NAME);
                    databaseHelper.cleanTable(BranchCitiesTable.TABLE_NAME);

                    for (int i = 0; i < parentArray.length(); i++) {
                        JSONObject branchObject = parentArray.getJSONObject(i);
                        id = branchObject.getInt("id");
                        isactive = branchObject.getInt("isactive");
                        acceptsdelivery = branchObject.getInt("acceptsdelivery");
                        acceptstakeaway = branchObject.getInt("acceptstakeaway");
                        name = branchObject.getString("name");
                        address = branchObject.getString("address");
                        email = branchObject.getString("email");
                        phone = branchObject.getString("phone");
                        longitude = branchObject.getDouble("longitude");
                        latitude = branchObject.getDouble("latitude");
                        clientid = branchObject.getString("clientid");
                        picpath = branchObject.getString("picpath");
                        openinghours = branchObject.getString("openinghours");
                        picture = branchObject.getString("picture");
                        deliveryhours = branchObject.getString("deliveryhours");

                        Branches branch = new Branches(id, isactive, acceptsdelivery, acceptstakeaway, deliveryhours,
                                name, address, phone, email, longitude, latitude, clientid, picpath, openinghours, picture);
                        Log.d(TAG, "onPostExecute: branch " + branch.getName() + " lon:" + longitude + ", lat: " + latitude);
                        branchesList.add(branch);

                        if (branchObject.has("cities")) {
                            JSONArray citiesArray = branchObject.getJSONArray("cities");
                            for (int ii = 0; ii < citiesArray.length(); ii++) {
                                JSONObject cityObject = citiesArray.getJSONObject(ii);
                                int cityCode = cityObject.getInt("id");
                                BranchCities branchCities = new BranchCities(id, cityCode);
//                                Log.d(TAG, "onPostExecute: branch city " + branchCities.getCityCode());
                                branchCitiesList.add(branchCities);
                            }
                        }

                    }

                    databaseHelper.insertBranchCities(branchCitiesList);
                    databaseHelper.insertBranchList(branchesList);
                    woodenNetwork.new GetCities().execute();

                } catch (JSONException e) {
                    woodenNetwork.new GetCities().execute();
                }
            }
        }
    }

    public class GetCities extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... params) {
            try {
                sendMessage("success", "start", 60);
                Log.d(TAG, "doInBackground: get cities start");
                url = new URL(Constant.BASE_URL + "cities");
                Log.d(TAG, "doInBackground: url " + url);
                conn = (HttpURLConnection) url.openConnection();
                String token = prefs.getString(Constant.TOKEN, "");
                conn.setRequestProperty("Authorization", "Bearer " + token);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestMethod("GET");
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.connect();

                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    return (result.toString());
                } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return ("token");
                } else {
                    return ("notFound");
                }
            } catch (Exception e) {
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @SuppressLint("CommitPrefEdits")
        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute: result " + result);
            if (result.equalsIgnoreCase("token")) {
                woodenNetwork.new FirstSetup().execute();
            } else {

                try {
                    List<Cities> citiesList = new ArrayList<>();

                    int cityCode, countryCode, regionCode, isActive, zipCode;
                    String cityName;

                    JSONArray parentArray = new JSONArray(result);
                    databaseHelper.cleanTable(CitiesTable.TABLE_NAME);
                    for (int i = 0; i < parentArray.length(); i++) {
                        JSONObject categoryObject = parentArray.getJSONObject(i);
                        cityCode = categoryObject.getInt("citycode");
                        countryCode = categoryObject.getInt("countrycode");
                        regionCode = categoryObject.getInt("regioncode");
                        isActive = categoryObject.getInt("isactive");
                        zipCode = categoryObject.getInt("zipcode");
                        cityName = categoryObject.getString("name");

                        Cities cities = new Cities(cityCode, regionCode, cityName, zipCode, countryCode, isActive);
                        citiesList.add(cities);
                    }
                    databaseHelper.insertCitiesList(citiesList);
                    woodenNetwork.new GetProducts().execute();

                } catch (JSONException e) {
                    Log.d(TAG, "onPostExecute: error " + e.getMessage());
                    woodenNetwork.new GetProducts().execute();
                }
            }
        }
    }

    public class GetProducts extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... params) {
            Log.d(TAG, "doInBackground: get products start");
            try {
                sendMessage("success", "get products", 70);
                url = new URL(Constant.BASE_URL + "products");
                Log.d(TAG, "doInBackground: url " + url);
                conn = (HttpURLConnection) url.openConnection();
                String token = prefs.getString(Constant.TOKEN, "");
                conn.setRequestProperty("Authorization", "Bearer " + token);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestMethod("GET");
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.connect();

                int response_code = conn.getResponseCode();

                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    return (result.toString());
                } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return ("token");
                } else {
                    return ("notFound");
                }
            } catch (Exception e) {
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @SuppressLint("CommitPrefEdits")
        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase("token")) {
                woodenNetwork.new FirstSetup().execute();
            } else {
                try {
                    Log.d(TAG2, "onPostExecute: products response "+result);
                    List<Products> productsList = new ArrayList<>();

                    int prodnum, catid, pid, isTaxable1, isTaxable2, isTaxable3, ModifiersGroupID, ComboGroupID, isActive;
                    String descript, descript2, prodinfo, prodinfo2, picpath, brand,
                            picture, product_picture, title, thumb_picture,
                            refcode1, refcode2, tags;
                    double price, price2;

                    JSONObject parentObject = new JSONObject(result);
                    JSONArray parentArray = parentObject.getJSONArray("products");
                    databaseHelper.cleanTable(ProductsTable.TABLE_NAME);
                    databaseHelper.cleanTable(ProductStockTable.TABLE_NAME);

                    List<Integer> categoriesList = MainApplication.getDatabase().getAllCategoriesId();

                    for (int i = 0; i < parentArray.length(); i++) {
                        JSONObject productObject = parentArray.getJSONObject(i);
                        catid = productObject.getInt("catid");
                        if (categoriesList.contains(catid)) {
                            //create product
                            prodnum = productObject.getInt("prodnum");

                            catid = productObject.getInt("catid");
                            pid=0;
                            if(!productObject.isNull("pid")){
                                pid = productObject.getInt("pid");
                            }

                            price = productObject.getDouble("price");
                            price2 = 0;
                            if(!productObject.isNull("price2")){
                                price2 = productObject.getDouble("price2");
                            }

                            isTaxable1 = 0;
                            if(!productObject.isNull("istaxable1")){
                                isTaxable1 = productObject.getInt("istaxable1");
                            }
                            isTaxable2 = 0;
                            if(!productObject.isNull("istaxable2")){
                                isTaxable2 = productObject.getInt("istaxable2");
                            }
                            isTaxable3 = 0;
                            if(!productObject.isNull("istaxable3")){
                                isTaxable3 = productObject.getInt("istaxable3");
                            }

                            descript = productObject.getString("descript");
                            descript2 = productObject.getString("descript2");
                            prodinfo = productObject.getString("prodinfo");
                            prodinfo2 = productObject.getString("prodinfo2");
                            picpath = "";//productObject.getString("picpath");
                            brand = productObject.getString("brand");
                            picture = "";// productObject.getString("picture");
                            product_picture = productObject.getString("product_picture");
                            title = productObject.getString("title");
                            thumb_picture = productObject.getString("thumb_picture");
                            refcode1 = productObject.getString("refcode1");
                            refcode2 = productObject.getString("refcode2");
                            ModifiersGroupID = 0;
                            if(!productObject.isNull("ModifiersGroupID")){
                                ModifiersGroupID = productObject.getInt("ModifiersGroupID");
                            }
                            ComboGroupID = 0;
                            if(!productObject.isNull("ComboGroupID")){
                                ComboGroupID = productObject.getInt("ComboGroupID");
                            }

                            tags = productObject.getString("tags");
                            isActive = productObject.getInt("isactive");

                            Products products = new Products(prodnum, catid, pid, isTaxable1, isTaxable2, isTaxable3,
                                    ModifiersGroupID, ComboGroupID, descript, descript2,
                                    prodinfo, prodinfo2, picpath, picture, product_picture, title,
                                    thumb_picture, brand, refcode1, refcode2, tags, price, price2);
                            products.setFinalPrice(price);

                            if (isActive == 1) {
                                productsList.add(products);
                            }
                        }
                    }
                    Log.d(TAG, "onPostExecute: product list size "+productsList.size());
                    databaseHelper.insertProductList(productsList);
                    Log.d(TAG, "onPostExecute: done");
                    woodenNetwork.new GetAllQuestions().execute();

                } catch (JSONException e) {
                    Log.d(TAG, "onPostExecute: error " + e.getMessage());
                    woodenNetwork.new GetAllQuestions().execute();
                }
            }
        }
    }

    public class GetAllQuestions extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... params) {
            try {
                sendMessage("success", "get questions", 80);
                Log.d(TAG, "doInBackground: get questions start");
                url = new URL(Constant.BASE_URL + "questions");
                Log.d(TAG, "doInBackground: url " + url);
                conn = (HttpURLConnection) url.openConnection();
                String token = prefs.getString(Constant.TOKEN, "");
                conn.setRequestProperty("Authorization", "Bearer " + token);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestMethod("GET");
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.connect();

                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    return (result.toString());
                } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return ("token");
                } else {
                    return ("notFound");
                }
            } catch (Exception e) {

                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @SuppressLint("CommitPrefEdits")
        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute: result " + result);
            if (result.equalsIgnoreCase("token")) {
                woodenNetwork.new FirstSetup().execute();
            } else {

                try {
                    List<QuestionGroup> questionGroupList = new ArrayList<>();
                    List<QuestionHeader> questionHeaderList = new ArrayList<>();
                    List<QuestionModifier> questionModifierList = new ArrayList<>();

                    JSONArray parentArray = new JSONArray(result);

                    //get all questionsGroup

                    for (int i = 0; i < parentArray.length(); i++) {

                        JSONObject questionGroupObject = parentArray.getJSONObject(i);
                        int qGId = questionGroupObject.getInt("qgid");
                        String qGroupDescription = questionGroupObject.getString("descript");

                        QuestionGroup questionGroup = new QuestionGroup(qGId, qGroupDescription);
                        questionGroupList.add(questionGroup);

                        //get All questions
                        JSONArray questionHeaderArray = questionGroupObject.getJSONArray("questionheaders");
                        for (int ii = 0; ii < questionHeaderArray.length(); ii++) {
                            JSONObject questionHeaderObject = questionHeaderArray.getJSONObject(ii);
                            int qHid, min, max, seq, required, isActive;
                            String qDescription;
                            if (!questionHeaderObject.isNull("qhid")) {
                                qHid = questionHeaderObject.getInt("qhid");
                                qDescription = questionHeaderObject.getString("qhdescript");
                                min = questionHeaderObject.getInt("min");
                                max = questionHeaderObject.getInt("max");
                                required = questionHeaderObject.getInt("required");
                                seq = questionHeaderObject.getInt("seq");

                                QuestionHeader questionHeader = new QuestionHeader(qGId, qHid, min, max, required, seq, qDescription);
                                questionHeaderList.add(questionHeader);

                                JSONArray modifiersArray = questionHeaderObject.getJSONArray("items");
                                //get all questionsModifiers
                                for (int iii = 0; iii < modifiersArray.length(); iii++) {
                                    JSONObject modifiersObject = modifiersArray.getJSONObject(iii);
                                    int prodNum, catId, groupId, qHeaderId, stock, enabled, pid, isTaxable1, isTaxable2, isTaxable3, versionId;
                                    double price, price2;
                                    String clientId, descript, descript2, prodInfo, prodInfo2, picPath, picture, thumbPicture, brand, refCode1, refCode2, createdAt, updatedAt;

                                    if (!modifiersObject.isNull("prodnum")) {
                                        prodNum = modifiersObject.getInt("prodnum");
                                        catId = modifiersObject.getInt("catid");
                                        pid = modifiersObject.getInt("pid");
                                        groupId = questionHeader.getQuestionGroupId();
                                        qHeaderId = questionHeader.getQuestionHeaderId();
                                        price = modifiersObject.getDouble("price");
                                        stock = 1;
                                        enabled = modifiersObject.getInt("enabled");
                                        isTaxable1 = modifiersObject.getInt("istaxable1");
                                        isTaxable2 = modifiersObject.getInt("istaxable2");
                                        isTaxable3 = modifiersObject.getInt("istaxable3");
                                        descript = modifiersObject.getString("descript");
                                        descript2 = modifiersObject.getString("descript2");
                                        prodInfo = modifiersObject.getString("prodinfo");
                                        prodInfo2 = modifiersObject.getString("prodinfo2");
                                        picPath = modifiersObject.getString("picpath");
                                        brand = modifiersObject.getString("brand");
                                        picture = modifiersObject.getString("picture");
                                        thumbPicture = modifiersObject.getString("thumb_picture");
                                        refCode1 = modifiersObject.getString("refcode1");
                                        refCode2 = modifiersObject.getString("refcode2");
                                        versionId = modifiersObject.getInt("versionid");
                                        createdAt = modifiersObject.getString("created_at");
                                        updatedAt = modifiersObject.getString("updated_at");
                                        clientId = modifiersObject.getString("clientid");
                                        isActive = modifiersObject.getInt("isactive");
                                        QuestionModifier modifier = new QuestionModifier(prodNum, catId, qHeaderId, groupId, stock, enabled, pid, isTaxable1, isTaxable2, isTaxable3, versionId, price, descript, clientId, descript2, prodInfo, prodInfo2, picPath, picture, thumbPicture, brand, refCode1, refCode2, createdAt, updatedAt);
                                        modifier.setFinalPrice(price);
                                        if (isActive == 1) {
                                            questionModifierList.add(modifier);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    Log.d(TAG, "onPostExecute: question group size " + questionGroupList.size());
                    Log.d(TAG, "onPostExecute: question header size " + questionHeaderList.size());
                    Log.d(TAG, "onPostExecute: question modifier size " + questionModifierList.size());

                    databaseHelper.cleanTable(QuestionsGroupTable.TABLE_NAME);
                    databaseHelper.cleanTable(QuestionHeaderTable.TABLE_NAME);
                    databaseHelper.cleanTable(QuestionModifierTable.TABLE_NAME);

                    databaseHelper.insertQuestionGroupList(questionGroupList);
                    databaseHelper.insertQuestionHeaderList(questionHeaderList);
                    databaseHelper.insertQuestionModifierList(questionModifierList);

                    woodenNetwork.new GetAllCombos().execute();

                } catch (JSONException e) {
                    Log.d(TAG, "onPostExecute: error " + e.getMessage());
                    woodenNetwork.new GetAllCombos().execute();
                }
            }
        }
    }

    public class GetAllCombos extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... params) {
            try {

                sendMessage("success", "get gallery", 90);
                Log.d(TAG, "doInBackground: get combos start");
                url = new URL(Constant.BASE_URL + "combos");
                Log.d(TAG, "doInBackground: url " + url);
                conn = (HttpURLConnection) url.openConnection();
                String token = prefs.getString(Constant.TOKEN, "");
                conn.setRequestProperty("Authorization", "Bearer " + token);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urle-" +
                        "+ncoded");
                conn.setRequestMethod("GET");
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.connect();

                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    return (result.toString());
                } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return ("token");
                } else {
                    return ("notFound");
                }
            } catch (Exception e) {

                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @SuppressLint("CommitPrefEdits")
        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute: start");
            if (result.equalsIgnoreCase("token")) {
                woodenNetwork.new FirstSetup().execute();
            } else {

                try {

                    List<ComboGroup> comboGroupList = new ArrayList<>();
                    List<ComboHeader> comboHeaderList = new ArrayList<>();
                    List<ComboModifier> comboModifierList = new ArrayList<>();

                    databaseHelper.cleanTable(ComboGroupTable.TABLE_NAME);
                    databaseHelper.cleanTable(ComboHeaderTable.TABLE_NAME);
                    databaseHelper.cleanTable(ComboModifierTable.TABLE_NAME);

                    JSONArray parentArray = new JSONArray(result);

                    //get all questionsGroup

                    for (int i = 0; i < parentArray.length(); i++) {
                        JSONObject comboGroupObject = parentArray.getJSONObject(i);
                        int comboGroupId = comboGroupObject.getInt("combogroupid");
                        ComboGroup comboGroup = new ComboGroup(comboGroupId);
                        comboGroupList.add(comboGroup);

                        //get All questions
                        JSONArray comboHeaderArray = comboGroupObject.getJSONArray("headers");
                        for (int ii = 0; ii < comboHeaderArray.length(); ii++) {
                            JSONObject comboHeaderObject = comboHeaderArray.getJSONObject(ii);
                            int comboHeaderId, min, max, seq, required;
                            String description;
                            comboHeaderId = comboHeaderObject.getInt("comboid");
                            description = comboHeaderObject.getString("descript");
                            min = comboHeaderObject.getInt("min");
                            max = comboHeaderObject.getInt("max");
                            required = comboHeaderObject.getInt("required");
                            seq = comboHeaderObject.getInt("seq");
                            ComboHeader comboHeader = new ComboHeader(comboGroupId, comboHeaderId, min, max, required, seq, description);
                            comboHeaderList.add(comboHeader);

                            JSONArray modifiersArray = comboHeaderObject.getJSONArray("items");
                            //get all questionsModifiers
                            for (int iii = 0; iii < modifiersArray.length(); iii++) {
                                JSONObject modifiersObject = modifiersArray.getJSONObject(iii);
                                int prodNum, catId, groupId, stock, enabled, pid, isTaxable1, isTaxable2, isTaxable3, versionId, isActive;
                                double price;
                                String clientId, descript, descript2, prodInfo, prodInfo2, picPath, picture, thumbPicture, brand, refCode1, refCode2, createdAt, updatedAt;

                                prodNum = modifiersObject.getInt("prodnum");
                                catId = modifiersObject.getInt("catid");
                                pid = modifiersObject.getInt("pid");
                                groupId = comboHeader.getComboGroupId();
                                comboHeaderId = comboHeader.getComboHeaderId();
                                price = modifiersObject.getDouble("price");
                                stock = 1;
                                enabled = modifiersObject.getInt("enabled");
                                isTaxable1 = modifiersObject.getInt("istaxable1");
                                isTaxable2 = modifiersObject.getInt("istaxable2");
                                isTaxable3 = modifiersObject.getInt("istaxable3");
                                descript = modifiersObject.getString("descript");
                                descript2 = modifiersObject.getString("descript2");
                                prodInfo = modifiersObject.getString("prodinfo");
                                prodInfo2 = modifiersObject.getString("prodinfo2");
                                picPath = modifiersObject.getString("picpath");
                                brand = modifiersObject.getString("brand");
                                picture = modifiersObject.getString("picture");
                                thumbPicture = modifiersObject.getString("thumb_picture");
                                refCode1 = modifiersObject.getString("refcode1");
                                refCode2 = modifiersObject.getString("refcode2");
                                isActive = modifiersObject.getInt("isactive");

                                ComboModifier modifier = new ComboModifier(prodNum, catId, comboHeaderId, groupId, stock, enabled, pid, isTaxable1, isTaxable2, isTaxable3, 1, price, descript, "", descript2, prodInfo, prodInfo2, picPath, picture, thumbPicture, brand, refCode1, refCode2, "", "");
                                modifier.setFinalPrice(price);

                                if (isActive == 1) {
                                    comboModifierList.add(modifier);
                                }
                            }
                        }
                    }

                    databaseHelper.insertComboGroupList(comboGroupList);
                    databaseHelper.insertComboHeaderList(comboHeaderList);
                    databaseHelper.insertComboModifierList(comboModifierList);
                    Log.d(TAG, "onPostExecute: done");
                    woodenNetwork.new GetGallery().execute();

                } catch (JSONException e) {
                    Log.d(TAG, "onPostExecute: combo error : "+e.getMessage());
                    woodenNetwork.new GetGallery().execute();
                }
            }
        }
    }

    public class GetGallery extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... strings) {
            try {

                sendMessage("success", "get gallery", 100);
                Log.d(TAG, "doInBackground: get galleries start");
                url = new URL(Constant.BASE_URL + "galleries");
                Log.d(TAG, "doInBackground: url " + url);
                conn = (HttpURLConnection) url.openConnection();
                String token = prefs.getString(Constant.TOKEN, "");
                conn.setRequestProperty("Authorization", "Bearer " + token);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestMethod("GET");
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.connect();

                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    return (result.toString());
                } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return ("token");
                } else {
                    return ("notFound");
                }
            } catch (Exception e) {

                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute: result " + result);
            if (result.equalsIgnoreCase("token")) {
                woodenNetwork.new FirstSetup().execute();
            } else {
                try {
                    databaseHelper.cleanTable(CakeGalleryTable.TABLE_NAME);
                    databaseHelper.cleanTable(CakeItemTable.TABLE_NAME);

                    List<CakeGallery> cakeGalleryList = new ArrayList<>();
                    List<CakeItem> cakeItemList = new ArrayList<>();

                    JSONObject parentObject = new JSONObject(result);
                    JSONArray galleryArray = parentObject.getJSONArray("galleries");

                    for (int i = 0; i < galleryArray.length(); i++) {
                        JSONObject galleryObject = galleryArray.getJSONObject(i);
                        int galleryId, isActive;
                        String galleryName;
                        galleryId = galleryObject.getInt("id");
                        galleryName = galleryObject.getString("name");
                        isActive = galleryObject.getInt("isactive");

                        JSONArray photoArray = galleryObject.getJSONArray("gallery_photos");

                        for (int ii = 0; ii < photoArray.length(); ii++) {
                            JSONObject photoObject = photoArray.getJSONObject(ii);
                            int galleryPhotoId, price, minOrderQty, photoIsActive;
                            String title, description, picPath, refNum;
                            galleryPhotoId = photoObject.getInt("gallery_photo_id");
                            title = photoObject.getString("title");
                            price = photoObject.getInt("price");
                            minOrderQty = photoObject.getInt("minorderqty");
                            photoIsActive = photoObject.getInt("isactive");
                            description = photoObject.getString("descript");
                            picPath = photoObject.getString("picpath");
                            refNum = photoObject.getString("refnum");

                            CakeItem cakeItem = new CakeItem(galleryId, galleryPhotoId, price, photoIsActive, minOrderQty, title, description, picPath, refNum);
                            cakeItemList.add(cakeItem);
                        }
                        CakeGallery cakeGallery = new CakeGallery(galleryId, isActive, galleryName);
                        cakeGalleryList.add(cakeGallery);
                    }
                    databaseHelper.insertCakeGalleryList(cakeGalleryList);
                    databaseHelper.insertCakeItemList(cakeItemList);

                    woodenNetwork.new GetPriceListId().execute();

                } catch (JSONException e) {
                    woodenNetwork.new GetPriceListId().execute();
                }
            }
        }
    }

    public class GetPriceListId extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... strings) {
            try {

                sendMessage("success", "price list settings", 93);
                Log.d(TAG, "doInBackground: get price list start");
                url = new URL(Constant.BASE_URL + "branch/settings");
                Log.d(TAG, "doInBackground: url " + url);
                conn = (HttpURLConnection) url.openConnection();
                String token = prefs.getString(Constant.TOKEN, "");
                conn.setRequestProperty("Authorization", "Bearer " + token);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestMethod("GET");
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.connect();

                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    return (result.toString());
                } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return ("token");
                } else {
                    return ("notFound");
                }
            } catch (Exception e) {

                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute: result " + result);
            if (result.equalsIgnoreCase("token")) {
                woodenNetwork.new FirstSetup().execute();
            } else {
                try {
                    shouldShowPriceListCurr = 0;
                    int priceListId = 0;
                    JSONObject parentObject = new JSONObject(result);
                    JSONArray branchSettingsArray = parentObject.getJSONArray("branch_settings");
                    for (int i = 0; i < branchSettingsArray.length(); i++) {
                        JSONObject object = branchSettingsArray.getJSONObject(i);
//                        Log.d(TAG, "onPostExecute: object " + object.getString("settingkey"));
                        if (object.has("settingkey") && object.getString("settingkey").equalsIgnoreCase("SYSTEMPRICELISTID")) {
                            int numValue = object.getInt("numvalue");
                            if (numValue > 0) {
                                priceListId = numValue;
                            }
                        }
                        if (object.has("settingkey") && object.getString("settingkey").equalsIgnoreCase("MOBAPPPLCURRENCY")) {
                            shouldShowPriceListCurr = object.getInt("boolvalue");
                        }
                    }
                    Log.d(TAG, "onPostExecute: price list id " + priceListId);
                    Log.d(TAG, "onPostExecute: should show " + shouldShowPriceListCurr);
                    if (priceListId > 0) {
                        woodenNetwork.new GetPriceListCurrId().execute(String.valueOf(priceListId));
                    } else {
                        databaseHelper.cleanTable(PriceListTable.TABLE_NAME);
                        sendMessage("success", "done", 100);
                    }
                } catch (JSONException e) {
                    Log.d(TAG, "onPostExecute: error " + e.getMessage());
                    databaseHelper.cleanTable(PriceListTable.TABLE_NAME);
                    sendMessage("success", "done", 100);
                }
            }
        }
    }

    public class GetPriceListCurrId extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... strings) {
            try {

                sendMessage("success", "GetPriceListCurrId", 95);
                Log.d(TAG, "doInBackground: GetPriceListCurrId start");
                url = new URL(Constant.BASE_URL + "pricelists/" + Integer.parseInt(strings[0]));
                Log.d(TAG, "doInBackground: url " + url);
                conn = (HttpURLConnection) url.openConnection();
                String token = prefs.getString(Constant.TOKEN, "");
                conn.setRequestProperty("Authorization", "Bearer " + token);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestMethod("GET");
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.connect();

                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    return (result.toString());
                } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return ("token");
                } else {
                    return ("notFound");
                }
            } catch (Exception e) {

                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute: result " + result);
            if (result.equalsIgnoreCase("token")) {
                woodenNetwork.new FirstSetup().execute();
            } else {
                try {

                    JSONArray parentArray = new JSONArray(result);
                    JSONObject object = parentArray.getJSONObject(0);
                    int currencyId = object.getInt("currency_id");
                    woodenNetwork.new GetPriceListCurrency().execute(String.valueOf(currencyId));

                } catch (JSONException e) {
                    databaseHelper.cleanTable(PriceListTable.TABLE_NAME);
                    sendMessage("success", "done", 100);
                }
            }
        }
    }

    public class GetPriceListCurrency extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected String doInBackground(String... strings) {
            try {

                sendMessage("success", "get gallery", 98);
                Log.d(TAG, "doInBackground: GetPriceListCurrency start");
                url = new URL(Constant.BASE_URL + "currencies/" + Integer.parseInt(strings[0]));
                Log.d(TAG, "doInBackground: url " + url);
                conn = (HttpURLConnection) url.openConnection();
                String token = prefs.getString(Constant.TOKEN, "");
                conn.setRequestProperty("Authorization", "Bearer " + token);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestMethod("GET");
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.connect();

                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    return (result.toString());
                } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return ("token");
                } else {
                    return ("notFound");
                }
            } catch (Exception e) {

                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute: result " + result);
            if (result.equalsIgnoreCase("token")) {
                woodenNetwork.new FirstSetup().execute();
            } else {
                try {

                    JSONArray parentArray = new JSONArray(result);
                    JSONObject object = parentArray.getJSONObject(0);
                    int decimals = object.getInt("decimals");
                    String mask = object.getString("currency_mask");
                    String description = object.getString("description");
                    double conversion = object.getDouble("conversion");
                    PriceList priceList = new PriceList(
                            decimals,
                            conversion,
                            description,
                            mask
                    );
                    priceList.setShouldShowCurr(shouldShowPriceListCurr);
                    databaseHelper.cleanTable(PriceListTable.TABLE_NAME);
                    databaseHelper.insertPriceList(priceList);
                    Log.d(TAG, "onPostExecute: price list " + priceList);
                    sendMessage("success", "done", 100);
                } catch (JSONException e) {
                    databaseHelper.cleanTable(PriceListTable.TABLE_NAME);
                    sendMessage("success", "done", 100);
                }
            }
        }
    }
}