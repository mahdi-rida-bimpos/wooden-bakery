package com.bimpos.wooden.bakery.orderOnline.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.bimpos.wooden.bakery.BuildConfig;
import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityCartOrderBinding;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.helpers.CustomLoadingOrder;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.helpers.NetworkUtil;
import com.bimpos.wooden.bakery.homePage.activities.MainActivity;
import com.bimpos.wooden.bakery.models.Address;
import com.bimpos.wooden.bakery.models.Cart;
import com.bimpos.wooden.bakery.models.CartComboHeader;
import com.bimpos.wooden.bakery.models.CartComboModifier;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.models.CartQuestionHeader;
import com.bimpos.wooden.bakery.models.CartQuestionModifier;
import com.bimpos.wooden.bakery.models.Member;
import com.bimpos.wooden.bakery.models.OrderHistory;
import com.bimpos.wooden.bakery.models.PriceList;
import com.bimpos.wooden.bakery.networkRequest.AddOrder;
import com.bimpos.wooden.bakery.networkRequest.CheckNewPrices;
import com.bimpos.wooden.bakery.networkRequest.GetToken;
import com.bimpos.wooden.bakery.orderOnline.adapters.CartOrderRvAdapter;
import com.bimpos.wooden.bakery.orderOnline.fragments.CartOrderFragment;
import com.bimpos.wooden.bakery.orderOnline.fragments.CheckOutOrderFragment;
import com.bimpos.wooden.bakery.profile.activities.AddressActivity;
import com.bimpos.wooden.bakery.profile.activities.LoginActivity;
import com.bimpos.wooden.bakery.profile.activities.ProfileActivity;
import com.bimpos.wooden.bakery.profile.activities.SmsVerificationActivity;
import com.bimpos.wooden.bakery.receivers.CheckConnectivityAsync;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CartOrderActivity extends AppCompatActivity
        implements CartOrderRvAdapter.OnEmptyCard,
        AddOrder.OnOrderComplete,
        GetToken.OnTokenReceived,
        CheckConnectivityAsync.OnConnectionComplete,
        CheckNewPrices.OnCheckNewPrice,
        CustomDialog.DialogEvents {


    private final int YES_NO_DELETE_CART = 1;
    private final int DIALOG_EXIT = 2;
    private final int YES_NO_PLACE_ORDER = 3;
    private final int YES_NO_CREATE_ADDRESS = 4;
    private final int DIALOG_NOTHING = 5;
    private final int YES_NO_GOTO_PROFILE = 6;

    public ActivityCartOrderBinding binding;
    private Cart cartData;
    private List<CartProduct> cartItemsList;
    private Member member;
    private CustomDialog dialog;
    private ImageView cartImage;
    private TextView cartText;
    private TextView checkOutText;
    private DatabaseHelper databaseHelper;
    private OrderHistory orderHistory;
    public CustomLoadingOrder loadingOrder;
    private SharedPreferences preferences;

    private int itemPosition = 0;
    private boolean isFromMainActivity = false;
    private boolean isPlacingOrder = false;

    private int isFoundRemovedItems = 0;
    private int isFoundPriceChanged = 0;

    private CheckOutOrderFragment checkOutOrderFragment;
    private CartOrderFragment cartOrderFragment;

    private static final String TAG = "CartOrderActivity";
    private long mLastClick = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCartOrderBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Log.d(TAG, "onCreate: start");

        initVariables();
        initViewPager();
        setTotalPrice();
        initClickListeners();
        checkIntent();
    }

    private void checkIntent() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(Constant.IS_FROM_MAIN_ACTIVITY)) {
            isFromMainActivity = intent.getBooleanExtra(Constant.IS_FROM_MAIN_ACTIVITY, false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dialog.isAdded()) {
            dialog.dismiss();
        }
        cartItemsList = Cart.getCartData().getCartItemsList();
        if (cartItemsList.size() > 0) {
            checkInternetConnection();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initVariables() {

        preferences = MainApplication.getPreferences();
        dialog = new CustomDialog(this);
        loadingOrder = new CustomLoadingOrder(this);
        databaseHelper = MainApplication.getDatabase();
        member = databaseHelper.getMember();
        cartData = Cart.getCartData();
        cartItemsList = cartData.getCartItemsList();
        if (databaseHelper.shouldShowInPriceListCurrency()) {
            PriceList priceList = databaseHelper.getPriceList();
            binding.conversionRateLayout.setVisibility(View.VISIBLE);
            binding.txtConversionRate.setText(String.format("1 %s -> %s %s",
                    priceList.getCurrencyName(),
                    priceList.getConversion(),
                    preferences.getString(Constant.SETTINGS_CURRENCY, "LBP")));
        }
    }

    @SuppressLint("InflateParams")
    private void initViewPager() {

        LayoutInflater inflater = getLayoutInflater();

        View leftBarButton = inflater.inflate(R.layout.tab_layout_cart, null);
        binding.tabLayout.addTab(binding.tabLayout.newTab().setCustomView(leftBarButton));
        cartImage = leftBarButton.findViewById(R.id.tab_layout_cartImage);
        cartText = leftBarButton.findViewById(R.id.tab_layout_cartText);

        View centerBarButton = inflater.inflate(R.layout.tab_layout_checkout, null);
        binding.tabLayout.addTab(binding.tabLayout.newTab().setCustomView(centerBarButton));
        checkOutText = centerBarButton.findViewById(R.id.tab_layout_checkoutText);

        binding.tabLayout.setTabMode(TabLayout.MODE_FIXED);
        Objects.requireNonNull(binding.tabLayout.getTabAt(binding.tabLayout.getSelectedTabPosition())).select();

        binding.tabLayout.addOnTabSelectedListener(tabSelectedListener);

        cartOrderFragment = new CartOrderFragment(this);
        checkOutOrderFragment = new CheckOutOrderFragment(this);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFrag(cartOrderFragment, "Tab 1");
        adapter.addFrag(checkOutOrderFragment, "Tab 2");
        binding.viewPager.setAdapter(adapter);
        binding.viewPager.setOffscreenPageLimit(2);
        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));

    }

    TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onTabSelected(TabLayout.Tab tab) {

            if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();

            member = databaseHelper.getMember();

            if (tab.getPosition() == 0) {
                checkOutOrderFragment.canCheckDeliveryCharge = false;
                binding.placeOrderText.setText("Checkout");
                binding.deleteCart.setVisibility(View.VISIBLE);
                cartImage.setImageDrawable(ContextCompat.getDrawable(CartOrderActivity.this, R.drawable.shop_cart_gold));
                cartText.setTextColor(ContextCompat.getColor(CartOrderActivity.this, R.color.gold));
                checkOutText.setTextColor(ContextCompat.getColor(CartOrderActivity.this, R.color.black));
            } else {
                if (member == null) {
                    Objects.requireNonNull(binding.tabLayout.getTabAt(0)).select();
                    binding.viewPager.setCurrentItem(0);
                    gotoLoginActivity();
                } else {
                    checkOutOrderFragment.canCheckDeliveryCharge = true;
                    checkOutOrderFragment.getDeliveryCharge();
                    binding.placeOrderText.setText("Place Order");
                    binding.deleteCart.setVisibility(View.GONE);
                    cartImage.setImageDrawable(ContextCompat.getDrawable(CartOrderActivity.this, R.drawable.shop_cart_black));
                    checkOutText.setTextColor(ContextCompat.getColor(CartOrderActivity.this, R.color.gold));
                    cartText.setTextColor(ContextCompat.getColor(CartOrderActivity.this, R.color.black));
                }
            }
            if (member != null) {
                binding.viewPager.setCurrentItem(tab.getPosition());
            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    public void setTotalPrice() {
        double price = cartData.getCartTotalPrice();
//        price += Constant.SETTINGS_DELIVERY_CHARGE;
        int count = cartData.getCartCount();
        binding.totalItems.setText(String.valueOf(count));
        binding.totalPrice.setText(FormatPrice.getFormattedPrice(price));
    }

    private void initClickListeners() {

        binding.deleteCart.setOnClickListener(view -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            showMessageDialog("Note", "Are you sure you want to delete the items in your cart ?", YES_NO_DELETE_CART);
        });

        binding.placeOrderLayout.setOnClickListener(view -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();

            Log.d(TAG, "initClickListeners: clicked");
            if (binding.viewPager.getCurrentItem() == 0) {
                member = databaseHelper.getMember();
                if (member == null) {
                    gotoLoginActivity();
                } else {
                    int fountRemoved = 0;
                    for (CartProduct cartProduct : cartItemsList) {
                        if (cartProduct.getIsRemoved() == 1) {
                            fountRemoved++;
                        }
                    }
                    if (fountRemoved == cartItemsList.size()) {
                        showMessageDialog("Note", "Your cart is empty", DIALOG_NOTHING);
                    } else {
                        binding.viewPager.setCurrentItem(1);
                        checkOutOrderFragment.canCheckDeliveryCharge = true;
                        checkOutOrderFragment.getDeliveryCharge();
                        binding.placeOrderText.setText("Place Order");
                        binding.deleteCart.setVisibility(View.GONE);
                        cartImage.setImageDrawable(ContextCompat.getDrawable(CartOrderActivity.this, R.drawable.shop_cart_black));
                        checkOutText.setTextColor(ContextCompat.getColor(CartOrderActivity.this, R.color.gold));
                        cartText.setTextColor(ContextCompat.getColor(CartOrderActivity.this, R.color.black));
                    }
                }
            } else {
                member = databaseHelper.getMember();
                if (member == null) {
                    gotoLoginActivity();
                } else {
                    Log.d(TAG, "initClickListeners: member not null");
                    //member exist
                    if (member.getMobileValidated() == 1) {

                        if (member.getFirstName().length() == 0 || member.getLastName().length() == 0) {
                            showMessageDialog("Note", "Please complete your profile information before placing the order", YES_NO_GOTO_PROFILE);
                        } else {
                            if (checkOutOrderFragment.getOrderType() == 1) {
                                //delivery case
                                if (checkOutOrderFragment.hasAddress()) {
                                    //address exist
                                    if (checkOutOrderFragment.getDefaultAddress() == null) {
                                        //no default address
                                        showMessageDialog("Note", "Please select an address for delivery", DIALOG_NOTHING);
                                    } else {
                                        if (checkOutOrderFragment.hasBranchToDeliver()) {
                                            //can go with the order
                                            showMessageDialog("Note", "Are you sure you want to order?", YES_NO_PLACE_ORDER);
                                        } else {
                                            showMessageDialog("Sorry", "There is no branch that deliver to your address", DIALOG_NOTHING);
                                        }
                                    }
                                } else {

                                    showMessageDialog("Note", "You have no added address, do you want to create one now ?", YES_NO_CREATE_ADDRESS);
                                }
                            } else {
                                //pickup case
                                if (checkOutOrderFragment.hasBranchToTakeAway()) {
                                    showMessageDialog("Note", "Are you sure you want to order?", YES_NO_PLACE_ORDER);
                                } else {
                                    showMessageDialog("Sorry", "There is no branch that accepts take away", DIALOG_NOTHING);
                                }
                            }
                        }
                    } else {
                        showMessageDialog("Note", "Your mobile phone is not validated yet !", DIALOG_NOTHING);
                    }
                }
            }
        });

        binding.back.setOnClickListener(v -> onBackPressed());
    }

    private void gotoLoginActivity() {

        if (preferences.getInt(Constant.IS_SMS_SENT, 0) == 1) {
            startActivity(new Intent(this, SmsVerificationActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (binding.viewPager.getCurrentItem() == 1) {
            binding.viewPager.setCurrentItem(0);
        } else {
            for (int i = 0; i < cartData.getCartItemsList().size(); i++) {
                CartProduct cartProduct = cartData.getCartItemsList().get(i);
                if (cartProduct.getIsRemoved() == 1) {
                    cartData.removeItem(cartProduct.getCartId());
                    i--;
                }
            }
            if (isFromMainActivity) {
                startActivity(new Intent(this, OrdersActivity.class));
                finish();
            }
            super.onBackPressed();
        }

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void prepareJsonData() {
        Log.d(TAG, "prepareJsonData: start");

        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartProduct = cartItemsList.get(i);
            if (cartProduct.getIsRemoved() == 1) {
                cartItemsList.remove(cartProduct);
                i--;
            }
        }
        if (cartItemsList.size() == 0) {
            showMessageDialog("Note", "Your cart is empty", DIALOG_NOTHING);
            return;
        }

        member = databaseHelper.getMember();
        JSONObject dataObject = new JSONObject();
        JSONObject orderObject = new JSONObject();
        JSONObject memberObject = new JSONObject();
        JSONObject addressObject = new JSONObject();
        JSONArray itemsArray = new JSONArray();
        Address defaultAddress = null;
        if (checkOutOrderFragment.getOrderType() == 1) {
            defaultAddress = checkOutOrderFragment.getDefaultAddress();
        }
        //start with order
        try {
            Log.d(TAG, "prepareJsonData: remark " + checkOutOrderFragment.getRemark());
            orderObject.put("branchid", checkOutOrderFragment.getBranchId());
            orderObject.put("orderid", databaseHelper.getCartHistoryCount() + 1);
            orderObject.put("sourceid", 1);
            orderObject.put("status", 0);
            orderObject.put("ordertype", checkOutOrderFragment.getOrderType());
            orderObject.put("orderdate", checkOutOrderFragment.getOrderDate());//20210607
            orderObject.put("ordertime", checkOutOrderFragment.getOrderTime());//2021-06-07 08:55:23
            orderObject.put("paymenttype", 0);
            double deliveryCharge = 0;
            if (Constant.SETTINGS_DELIVERY_CHARGE > 0) {
                if (databaseHelper.shouldShowInPriceListCurrency()) {
                    deliveryCharge = (Constant.SETTINGS_DELIVERY_CHARGE * databaseHelper.getPriceList().getConversion());
                }else{
                    deliveryCharge = Constant.SETTINGS_DELIVERY_CHARGE ;
                }
            }
            orderObject.put("deliveryCost", (int) deliveryCharge);
            orderObject.put("note", checkOutOrderFragment.getRemark());
            memberObject.put("posreference", member.getMemberHsId());
            memberObject.put("membername", member.getFirstName() + " " + member.getLastName());
            memberObject.put("mobile", member.getMobile());
            memberObject.put("mobilevalidated", member.getMobileValidated());
            memberObject.put("email", member.getEmail());
            memberObject.put("dateofbirth", member.getDateOfBirth());

            if (defaultAddress != null) {
                String description = defaultAddress.getDescription();
                String directions = defaultAddress.getDirection();
                if (description.length() == 0) {
                    description = "NA";
                    directions = "NA";
                }
                addressObject.put("posreference", defaultAddress.getHsAddressId());
                addressObject.put("description", description);
                addressObject.put("addresstype", checkOutOrderFragment.getAddressType());
                addressObject.put("phone", defaultAddress.getPhone() + " ");
                addressObject.put("geolong", defaultAddress.getGeoLong());
                addressObject.put("geolat", defaultAddress.getGeoLat());
                addressObject.put("citycode", defaultAddress.getCityCode());
                addressObject.put("street", defaultAddress.getStreet());
                addressObject.put("bldg", defaultAddress.getBldg());
                addressObject.put("floor", defaultAddress.getFloor());
                addressObject.put("directions", directions);
            } else {
                addressObject.put("posreference", 0);
                addressObject.put("description", "NA");
                addressObject.put("addresstype", 0);
                addressObject.put("phone", "NA");
                addressObject.put("geolong", 0);
                addressObject.put("geolat", 0);
                addressObject.put("citycode", 0);
                addressObject.put("street", "NA");
                addressObject.put("bldg", "NA");
                addressObject.put("floor", "NA");
                addressObject.put("directions", "NA");
            }

            memberObject.put("address", addressObject);
            orderObject.put("member", memberObject);

            //items->products->modifiers
            for (int i = 0; i < cartItemsList.size(); i++) {

                JSONObject productObject = new JSONObject();
                JSONArray modifiersArray = new JSONArray();
                JSONObject modifierObject = new JSONObject();
                CartProduct cartProduct = cartItemsList.get(i);

                int productCode = cartProduct.getProdNum();
                int qty = cartProduct.getQty();
                int price = (int) cartProduct.getPrice();
                if (databaseHelper.shouldShowInPriceListCurrency()) {
                    price = (int) (price * databaseHelper.getPriceList().getConversion());
                }
                String remark = cartProduct.getRemark();
                Log.d(TAG, "prepareJsonData: remark " + remark);
                int comboGroupId = cartProduct.getComboGroupId();

                productObject.put("productcode", productCode);
                productObject.put("productqty", qty);
                productObject.put("productprice", price);
                productObject.put("remark", remark);

                //in case of question or combos
                if (cartProduct.getQuestionList().size() != 0 || cartProduct.getComboList().size() != 0) {
                    int modifiersCount = 0;

                    //get all questions :
                    if (cartProduct.getQuestionList().size() > 0) {
                        for (int j = 0; j < cartProduct.getQuestionList().size(); j++) {
                            CartQuestionHeader cartQuestionHeader = cartProduct.getQuestionList().get(j);

                            for (int k = 0; k < cartQuestionHeader.getCartQuestionModifiers().size(); k++) {
                                CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(k);

                                if (modifier.isSelected()) {
                                    Log.d(TAG, "prepareJsonData: question modifier id " + modifier.getModifierId());
                                    modifierObject.put("modifiercode", modifier.getModifierId());
                                    modifierObject.put("modifierqty", qty);
                                    modifierObject.put("modifier_description", modifier.getDescription());
                                    int modifierPrice = (int) modifier.getPrice();
                                    if (databaseHelper.shouldShowInPriceListCurrency()) {
                                        modifierPrice = (int) (modifierPrice * databaseHelper.getPriceList().getConversion());
                                    }
                                    modifierObject.put("modifierprice", modifierPrice);
                                    modifiersArray.put(modifiersCount, modifierObject);
                                    modifiersCount++;
                                    modifierObject = new JSONObject();
                                }
                            }
                        }
                        productObject.put("comboProductCode", comboGroupId);
                    }

                    //get all combos :
                    if (cartProduct.getComboList().size() > 0) {
                        for (int j = 0; j < cartProduct.getComboList().size(); j++) {
                            CartComboHeader cartComboHeader = cartProduct.getComboList().get(j);

                            for (int k = 0; k < cartComboHeader.getCartComboModifiers().size(); k++) {
                                CartComboModifier modifier = cartComboHeader.getCartComboModifiers().get(k);
                                Log.d(TAG, "prepareJsonData: combo modifier id " + modifier.getModifierId());
                                modifierObject.put("modifiercode", modifier.getModifierId());
                                modifierObject.put("modifierqty", modifier.getCount() * qty);
                                modifierObject.put("modifier_description", modifier.getDescription());
                                int modifierPrice = (int) modifier.getPrice();
                                if (databaseHelper.shouldShowInPriceListCurrency()) {
                                    modifierPrice = (int) (modifierPrice * databaseHelper.getPriceList().getConversion());
                                }
                                modifierObject.put("modifierprice", modifierPrice);
                                modifiersArray.put(modifiersCount, modifierObject);
                                modifiersCount++;
                                modifierObject = new JSONObject();
                            }
                        }
                        productObject.put("comboProductCode", comboGroupId);
                    }

                }

                //in case of nothing
                else {
                    Log.d(TAG, "prepareJsonData: modifiers null");
                    productObject.put("comboProductCode", 0);
                }
                productObject.put("modifiers", modifiersArray);
                itemsArray.put(i, productObject);
            }

            orderObject.put("items", itemsArray);
            dataObject.put("order", orderObject);
            Log.d(TAG, "prepareJsonData: data object " + dataObject);

            String cartDataHistory = new Gson().toJson(cartItemsList);

            double conversionRate = -1;
            String defaultCurrency = preferences.getString(Constant.SETTINGS_CURRENCY, "LBP");
            String priceListCurrency = "";

            if (databaseHelper.shouldShowInPriceListCurrency()) {
                conversionRate = databaseHelper.getPriceList().getConversion();
                priceListCurrency = databaseHelper.getPriceList().getCurrencyName();
            }
            orderHistory = new OrderHistory(1,
                    databaseHelper.getCartHistoryCount() + 1,
                    checkOutOrderFragment.getOrderType(),
                    0,
                    cartData.getCartItemsList().size(),
                    cartData.getCartTotalPrice(),
                    deliveryCharge,
                    checkOutOrderFragment.getBranchId(),
                    cartDataHistory,
                    checkOutOrderFragment.getNowDate(),
                    checkOutOrderFragment.getHistoryDate(),
                    checkOutOrderFragment.getBranchName(),
                    checkOutOrderFragment.getRemark(),
                    conversionRate,
                    defaultCurrency,
                    priceListCurrency
            );

            Log.d(TAG, "prepareJsonData: history " + orderHistory);

            new Handler().postDelayed(() -> new AddOrder(dataObject.toString(), CartOrderActivity.this).execute(), 200);

        } catch (JSONException e) {
            showMessageDialog("Note", "Something went wrong while sending your order", DIALOG_NOTHING);
        }
    }

    @Override
    public void onOrderComplete(int status, String result) {
        Log.d(TAG, "onOrderComplete: result " + result);

        new Handler().postDelayed(() -> {
            if (status == -1) {
                Log.d(TAG, "onOrderComplete: get new token");
                new GetToken(CartOrderActivity.this).execute();
            } else if (status == 0) {
                showMessageDialog("Sorry", "For some reason, your order is not sent, please try again in few minutes", DIALOG_NOTHING);
            } else {
                try {
                    JSONObject parentObject = new JSONObject(result);
                    JSONObject messageObject = parentObject.getJSONObject("MESSAGE");
                    int orderId = messageObject.getInt("orderid");
                    int hsAddressId = messageObject.getInt("hsaddressid");
                    int hsMemberId = messageObject.getInt("hsmemberid");
                    orderHistory.setOrderId(orderId);
                    Log.d(TAG, "onOrderComplete: " + orderId);
                    member.setMemberHsId(hsMemberId);
                    if (checkOutOrderFragment.getOrderType() == 1) {
                        Address address = checkOutOrderFragment.getDefaultAddress();
                        address.setHsAddressId(hsAddressId);
                        databaseHelper.updateAddress(address);
                    }
                    databaseHelper.updateOrderHistory(orderHistory);
                    databaseHelper.updateMember(member);
                    databaseHelper.insertOrderHistory(orderHistory);
                    cartData.clear();
                    showMessageDialog("Done", "Your order has been sent successfully", DIALOG_EXIT);

                } catch (JSONException e) {
                    Log.d(TAG, "onOrderComplete: error " + e.getMessage());
                }

            }
        }, 1000);
    }

    @Override
    public void onEmptyCard() {
        finish();
    }

    @Override
    public void onPriceChanged() {
        setTotalPrice();
    }

    private void checkInternetConnection() {
        loadingOrder.show();
        loadingOrder.startAnimation(R.raw.web_loading);

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            Log.d(TAG, "checkInternetConnection: check ping");
            CheckConnectivityAsync checkConnectivityAsync = new CheckConnectivityAsync(this);
            checkConnectivityAsync.execute();
        } else {
            showMessageDialog("Note", "Please check your connection", DIALOG_NOTHING);
        }
    }

    @Override
    public void onConnectionComplete(boolean status) {
        if (status) {
            checkItemsPrice();
        } else {
            showMessageDialog("Note", "Please check your connection", DIALOG_NOTHING);
        }
    }

    private void checkItemsPrice() {
        Log.d(TAG, "checkNewPrices: start");
        CartProduct cartProduct = cartItemsList.get(itemPosition);
        Log.d(TAG, "checkNewPrices: item " + cartProduct.toString());
        int prodNum = cartProduct.getProdNum();
        int lastItem = 0;
        if (itemPosition == cartItemsList.size() - 1) {
            lastItem = 1;
        }
        new CheckNewPrices(cartProduct.getCartId(), prodNum, lastItem, this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        itemPosition++;
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onCheckNewPrice(int status, int lastItem, int prodNum, int cartId, double newPrice, double newPrice2) {
        Log.d(TAG, "onCheckNewPrice:  status " + status + ", last item " + lastItem + ", prod num " + prodNum + ", new price " + newPrice + ", new price2 " + newPrice2);
        if (status == -2) {
            showMessageDialog("Note", "Please try again in few minutes", DIALOG_NOTHING);
            itemPosition = 0;
            return;
        }
        if (status == -1) {
            //get token
            Log.d(TAG, "onCheckNewPrice: get token ");
            itemPosition = 0;
            new GetToken(CartOrderActivity.this).execute();

        } else if (status == 1) {
            for (int i = 0; i < cartItemsList.size(); i++) {
                CartProduct cartProduct = cartItemsList.get(i);
                if (cartProduct.getProdNum() == prodNum && cartId == cartProduct.getCartId()) {

                    double comparePrice = newPrice;
                    if (databaseHelper.shouldShowInPriceListCurrency()) {
                        comparePrice = newPrice2;
                    }
                    if (cartProduct.getPrice() != comparePrice) {
                        isFoundPriceChanged = 1;
                    }

                    if (databaseHelper.shouldShowInPriceListCurrency()) {
                        cartProduct.setPrice(newPrice2);
                    } else {
                        cartProduct.setPrice(newPrice);
                    }

                    databaseHelper.updateProduct(prodNum, newPrice, newPrice2);
                    cartOrderFragment.adapter.notifyDataSetChanged();
                    onPriceChanged();
                }
            }
            if (lastItem == 1) {
                checkBeforeLeaving();
            } else {
                checkItemsPrice();
            }
        } else {
            for (int i = 0; i < cartItemsList.size(); i++) {
                CartProduct cartProduct = cartItemsList.get(i);
                if (cartProduct.getProdNum() == prodNum && cartId == cartProduct.getCartId()) {
                    isFoundRemovedItems = 1;
                    cartProduct.setIsRemoved(1);
                    databaseHelper.removeProduct(prodNum);
                    OrdersActivity.ordersActivity.removeProduct(prodNum);
                    break;
                }
            }
            cartOrderFragment.adapter.notifyDataSetChanged();
            onPriceChanged();

            if (lastItem == 1) {
                checkBeforeLeaving();
            } else {
                checkItemsPrice();
            }
        }
    }

    private void checkBeforeLeaving() {
        cartData.replaceCurrentOrder();
        itemPosition = 0;
        if (isFoundRemovedItems == 1 || isFoundPriceChanged == 1) {
            isFoundPriceChanged = 0;
            isFoundRemovedItems = 0;
            showMessageDialog("Note", "some changes occur to your cart, please check all products before placing order ", DIALOG_NOTHING);
            if (binding.tabLayout.getSelectedTabPosition() == 1) {
                Objects.requireNonNull(binding.tabLayout.getTabAt(0)).select();
                binding.viewPager.setCurrentItem(0);
            }
        } else {
            if (!isPlacingOrder) {
                loadingOrder.dismiss();
            } else {
                prepareJsonData();
                isPlacingOrder = false;
            }
        }
    }

    @Override
    public void onTokenReceived(int status) {
        Log.d(TAG, "onTokenReceived: status " + status);
        if (status == -1) {
            showMessageDialog("Note", "Please try again later", DIALOG_NOTHING);
        } else {
            if (!isPlacingOrder) {
                checkItemsPrice();
            } else {
                prepareJsonData();
                isPlacingOrder = false;
            }
        }
    }


    private void showMessageDialog(String title, String message, int dialogId) {
        if (dialog.isAdded()) {
            dialog.dismiss();
        }
        if (loadingOrder.isShowing()) {
            loadingOrder.dismiss();
        }
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, message);
        arguments.putString(CustomDialog.DIALOG_TITLE, title);
        arguments.putInt(CustomDialog.DIALOG_ID, dialogId);

        switch (dialogId) {

            case DIALOG_EXIT:
            case DIALOG_NOTHING:
                arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Ok");
                arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
                break;

            case YES_NO_DELETE_CART:
            case YES_NO_GOTO_PROFILE:
            case YES_NO_PLACE_ORDER:
            case YES_NO_CREATE_ADDRESS:
                arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Yes");
                arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "No");
                arguments.putInt("positiveColor", ContextCompat.getColor(this, R.color.pantoneRed));
                break;

        }

        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        switch (dialogId) {

            case DIALOG_NOTHING:
                dialog.dismiss();
                break;

            case DIALOG_EXIT:
                dialog.dismiss();
                startActivity(new Intent(CartOrderActivity.this, MainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                break;

            case YES_NO_PLACE_ORDER:
                dialog.dismiss();
                isPlacingOrder = true;
                new Handler().postDelayed(this::checkInternetConnection, 200);
                break;

            case YES_NO_CREATE_ADDRESS:
                dialog.dismiss();
                startActivity(new Intent(CartOrderActivity.this, AddressActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            case YES_NO_GOTO_PROFILE:
                dialog.dismiss();
                startActivityForResult(new Intent(CartOrderActivity.this, ProfileActivity.class).putExtra("status", 2), 3);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            case YES_NO_DELETE_CART:
                cartData.clear();
                dialog.dismiss();
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(TAG, "onActivityResult: request code " + requestCode + ", result code " + resultCode);
        if (requestCode == 3) {
            checkOutOrderFragment.setPickup(1);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        dialog.dismiss();
    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager, BEHAVIOR_SET_USER_VISIBLE_HINT);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}