package com.bimpos.wooden.bakery.helpers;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;


import com.airbnb.lottie.LottieAnimationView;
import com.bimpos.wooden.bakery.R;

import java.util.Objects;

public class CustomLoadingOrder extends Dialog {

    public Activity listener;
    public Dialog d;
    private LottieAnimationView animationView;
    private static final String TAG = "CustomProgressBar";

    public CustomLoadingOrder(Activity listener) {
        super(listener);
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_bar);
        setCancelable(false);
        animationView = findViewById(R.id.progress_bar_lottieAnimationLayout);
        Objects.requireNonNull(getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }

    public void startAnimation(int raw){
        animationView.setAnimation(raw);
        animationView.setRepeatCount(1000);
        animationView.playAnimation();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        Log.d(TAG, "dismiss: start");
        animationView.cancelAnimation();
    }
}
