package com.bimpos.wooden.bakery.profile.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bimpos.wooden.bakery.BuildConfig;
import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityProfileBinding;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.helpers.GetTime;
import com.bimpos.wooden.bakery.helpers.Helpers;
import com.bimpos.wooden.bakery.homePage.activities.MainActivity;
import com.bimpos.wooden.bakery.homePage.fragments.PicPhotoFragment;
import com.bimpos.wooden.bakery.models.Address;
import com.bimpos.wooden.bakery.models.Member;
import com.bimpos.wooden.bakery.models.PaymentData;
import com.bimpos.wooden.bakery.orderOnline.activities.GalleryActivity;
import com.bimpos.wooden.bakery.profile.adapters.AddressRvAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class ProfileActivity extends AppCompatActivity implements PicPhotoFragment.OnPicPhotoSelect, CustomDialog.DialogEvents {

    private static final int CAMERA_REQUEST = 10;
    private static final int GALLERY_REQUEST = 20;
    public ActivityProfileBinding binding;
    private static final String TAG = "ProfileActivity";
    private Member member;
    private Calendar calendar = Calendar.getInstance();
    private final Calendar calendarNow = Calendar.getInstance();
    private DatabaseHelper databaseHelper;
    private final ArrayList<PaymentData> paymentList = new ArrayList<>();
    private long mLastClick = 0;
    private AddressRvAdapter addressAdapter;
    private InputMethodManager imm;
    PicPhotoFragment picPhotoFragment;
    private boolean dateChanged = false;
    private Uri currentPhotoPath;
    private SharedPreferences preferences;
    private CustomDialog customDialog;
    private int setResult = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initContent();
        initClickListener();
        initPaymentArray();

        long nextYearChange = preferences.getLong(Constant.BIRTHDAY_DATE_CHANGE, -1);
        if (nextYearChange != -1) {
            long thisYear = calendarNow.getTimeInMillis();
            Log.d(TAG, "initClickListener: time now " + GetTime.getBirthDate(calendarNow.getTimeInMillis()));
            Log.d(TAG, "initClickListener: next year " + GetTime.getBirthDate(nextYearChange));
        }

    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getExtras();
    }

    private void initVariables() {
        customDialog = new CustomDialog(this);
        preferences = MainApplication.getPreferences();
        imm = MainApplication.getImm();
        databaseHelper = MainApplication.getDatabase();
        OverScrollDecoratorHelper.setUpOverScroll(binding.scrollView);
        picPhotoFragment = new PicPhotoFragment(this);
        paymentList.add(new PaymentData(1, "Cash On Delivery", 1, R.drawable.ic_cash_in_hand));
        member = databaseHelper.getMember();
        binding.firstName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        binding.lastName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        binding.firstName.setFilters(new InputFilter[]{Helpers.filter, new InputFilter.LengthFilter(15)});
        binding.lastName.setFilters(new InputFilter[]{Helpers.filter, new InputFilter.LengthFilter(15)});
        binding.email.setFilters(new InputFilter[]{new InputFilter.LengthFilter(40)});
    }

    @SuppressLint("SetTextI18n")
    private void getExtras() {
        int status = getIntent().getIntExtra("status", 0);
        Log.d(TAG, "getExtras: status = " + status);
        if (status == 1 || status == 2) {
            binding.firstName.setEnabled(true);
            binding.email.setEnabled(true);
            binding.firstName.requestFocus();
            imm.showSoftInput(binding.firstName, 0);
            binding.lastName.setEnabled(true);
            binding.infoSave.setTag("save");
            binding.infoSave.setText("save");
            binding.firstName.setTextColor(Color.BLACK);
            binding.lastName.setTextColor(Color.BLACK);
            for (Drawable drawable : binding.birthdayText.getCompoundDrawables()) {
                if (drawable != null) {
                    drawable.setTint(ContextCompat.getColor(this, R.color.pantoneOlive));
                }
            }
            binding.birthdayText.setTextColor(Color.BLACK);
            binding.addressLayout.setVisibility(View.VISIBLE);

            if (status == 2) {
                setResult = 1;
            }
        } else {
            for (Drawable drawable : binding.birthdayText.getCompoundDrawables()) {
                if (drawable != null) {
                    drawable.setTint(Color.GRAY);
                }
            }
            binding.mobile.setTextColor(Color.GRAY);
            binding.email.setTextColor(Color.GRAY);
            binding.firstName.setTextColor(Color.GRAY);
            binding.lastName.setTextColor(Color.GRAY);
            binding.birthdayText.setTextColor(Color.GRAY);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initAddressRecyclerView();
    }

    private void initPaymentArray() {
        String[] paymentSpinnerArray = new String[1];
        paymentSpinnerArray[0] = "Cash on delivery";
        if (paymentSpinnerArray.length < 2) {
            binding.spinnerPayment.setEnabled(false);
            binding.spinnerPayment.setClickable(false);
        }
        ArrayAdapter<String> paymentSpinnerAdapter = new ArrayAdapter<>(this, R.layout.spinner_text_style, paymentSpinnerArray);
        binding.spinnerPayment.setAdapter(paymentSpinnerAdapter);

    }

    private void initAddressRecyclerView() {
        List<Address> addressList = databaseHelper.getAddressList();
        binding.addressRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        addressAdapter = new AddressRvAdapter(this, addressList, null);
        binding.addressRecyclerView.setAdapter(addressAdapter);
        addressAdapter.setProfileActivity(this);
    }

    @SuppressLint({"SimpleDateFormat", "SetTextI18n", "NotifyDataSetChanged"})
    private void initClickListener() {
        binding.birthdayLayout.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 300) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();

            if (binding.infoSave.getTag().toString().equalsIgnoreCase("save")) {
                try {
                    int year, month, day;
                    if (!binding.birthdayText.getText().toString().equalsIgnoreCase("Not set")) {
                        Date date;
                        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                        date = df.parse(binding.birthdayText.getText().toString());
                        Calendar calendar = new GregorianCalendar();
                        assert date != null;
                        calendar.setTime(date);
                        year = calendar.get(Calendar.YEAR);
                        month = calendar.get(Calendar.MONTH);
                        day = calendar.get(Calendar.DAY_OF_MONTH);
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, day);

                        long nextYearChange = preferences.getLong(Constant.BIRTHDAY_DATE_CHANGE, -1);
                        if (nextYearChange != -1) {
                            long thisYear = calendarNow.getTimeInMillis();
                            Log.d(TAG, "initClickListener: time now " + GetTime.getBirthDate(calendarNow.getTimeInMillis()));
                            Log.d(TAG, "initClickListener: next year " + GetTime.getBirthDate(nextYearChange));
                            if (thisYear < nextYearChange) {
                                showWarningDialog("You cannot edit your date of birth before " + df.format(nextYearChange));
                                return;
                            }
                        }
                    }
                    showDatePickerDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        binding.back.setOnClickListener(v -> {
            if (binding.infoSave.getTag().toString().equalsIgnoreCase("save")) {
                onBackPressed();
            } else {
                onBackPressed();
            }
        });

        binding.addressLayout.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 300) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            startActivity(new Intent(ProfileActivity.this, AddressActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        binding.infoSave.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 300) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            if (binding.infoSave.getTag().toString().equalsIgnoreCase("edit")) {
                binding.firstName.setEnabled(true);
//                binding.firstName.requestFocus();
                imm.showSoftInput(binding.firstName, 0);
                binding.lastName.setEnabled(true);
                binding.email.setEnabled(true);
                binding.infoSave.setTag("save");
                binding.infoSave.setText("save");
                for (Drawable drawable : binding.birthdayText.getCompoundDrawables()) {
                    if (drawable != null) {
                        drawable.setTint(ContextCompat.getColor(this, R.color.pantoneOlive));
                    }
                }
                binding.firstName.setTextColor(Color.BLACK);
                binding.lastName.setTextColor(Color.BLACK);
                binding.birthdayText.setTextColor(Color.BLACK);
                binding.email.setTextColor(Color.BLACK);
                binding.addressLayout.setVisibility(View.VISIBLE);

                addressAdapter.notifyDataSetChanged();

            } else {
                updateMember();
            }
        });

        binding.logout.setOnClickListener(v -> showSignOutDialog());

        binding.profileImage.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 300) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            Bitmap bitmap = databaseHelper.getMemberPhoto();
            if (bitmap == null) {
                //open intent
                if (binding.infoSave.getTag().toString().equalsIgnoreCase("save")) {
                    checkPermission();
                }
            } else {
                Intent intent = new Intent(ProfileActivity.this, GalleryActivity.class);
                intent.putExtra("PictureUrl", "member");
                startActivity(intent);
            }
        });
    }

    private boolean canSave() {
        String firstName = binding.firstName.getText().toString().trim().replace("'", "");
        String lastName = binding.lastName.getText().toString().trim().replace("'", "");
        return !TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName);
    }

    private void showSignOutDialog() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Note");
//        builder.setMessage("Are you sure you want to Logout? Your profile will no longer be available.");
//        builder.setPositiveButton("Yes", (dialog, which) -> logOut());
//        builder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
//        builder.create().show();

        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Are you sure you want to Logout? Your profile will no longer be available.");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        arguments.putInt(CustomDialog.DIALOG_ID, 1);

        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Yes");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "No");
        arguments.putInt("positiveColor", ContextCompat.getColor(this, R.color.pantoneRed));

        customDialog.setArguments(arguments);
        customDialog.show(getSupportFragmentManager(), null);
        customDialog.setCancelable(false);
    }

    private void logOut() {
        databaseHelper.removeMember();
        preferences.edit().putLong(Constant.BIRTHDAY_DATE_CHANGE, -1).apply();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (canSave()) {
            updateMember();
            if (setResult == 1) {
                setResult(3);
            }
            super.onBackPressed();
            overridePendingTransition(android.R.anim.fade_in, R.anim.slide_out_down);
        } else {
            if (binding.firstName.getText().toString().trim().length() == 0) {
                showWarningDialog("Please fill your first name");
                binding.firstName.requestFocus();
                return;
            }
            if (binding.lastName.getText().toString().trim().length() == 0) {
                showWarningDialog("Please fill your last name");
                binding.lastName.requestFocus();
                return;
            }
        }
    }

    @SuppressLint({"SetTextI18n", "NotifyDataSetChanged"})
    private void updateMember() {

        String firstName = binding.firstName.getText().toString().trim().replace("'", "");
        String lastName = binding.lastName.getText().toString().trim().replace("'", "");
        String birthDate = binding.birthdayText.getText().toString();
        String emailText = binding.email.getText().toString().replace("'", "");
        member.setEmail("");
        if (TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName)) {
            if (binding.firstName.getText().toString().trim().length() == 0) {
                showWarningDialog("Please fill your first name");
                binding.firstName.requestFocus();
                return;
            }
            if (binding.lastName.getText().toString().trim().length() == 0) {
                showWarningDialog("Please fill your first name");
                binding.lastName.requestFocus();
                return;
            }
            return;
        }
        if (emailText.length() > 0) {
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (!emailText.matches(emailPattern)) {
                showWarningDialog("Please enter a valid email address");
                return;
            }
            member.setEmail(emailText);
        }

        firstName = firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
        lastName = lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
        member.setFirstName(firstName);
        member.setLastName(lastName);
        binding.firstName.setText(firstName);
        binding.lastName.setText(lastName);
        binding.topName.setText(firstName + " " + lastName);
        binding.infoSave.setTag("edit");
        binding.infoSave.setText("edit");
        binding.firstName.clearFocus();
        binding.lastName.clearFocus();
        binding.email.setEnabled(false);
        binding.firstName.setEnabled(false);
        binding.lastName.setEnabled(false);
        binding.firstName.setTextColor(Color.GRAY);
        binding.lastName.setTextColor(Color.GRAY);
        binding.email.setTextColor(Color.GRAY);
        binding.birthdayText.setTextColor(Color.GRAY);
        binding.addressLayout.setVisibility(View.GONE);

        for (Drawable drawable : binding.birthdayText.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setTint(Color.GRAY);
            }
        }
        addressAdapter.notifyDataSetChanged();

        if (!birthDate.equalsIgnoreCase("Not Set")) {
            member.setDateOfBirth(birthDate);
            if (dateChanged) {
                dateChanged = false;
                SharedPreferences preferences = getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE);
                Log.d(TAG, "updateMember: this year " + GetTime.getBirthDate(calendarNow.getTimeInMillis()));
                calendarNow.add(Calendar.YEAR, 1);
                preferences.edit().putLong(Constant.BIRTHDAY_DATE_CHANGE, calendarNow.getTimeInMillis()).apply();
                Log.d(TAG, "updateMember: next year " + GetTime.getBirthDate(calendarNow.getTimeInMillis()));
                calendarNow.add(Calendar.YEAR, -1);
            }
        }
        if (databaseHelper.updateMember(member) != 1) {
            showWarningDialog("Something went wrong while updating your profile");
        }
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    }

    private void showWarningDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Note");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }

    private void initContent() {
        binding.firstName.setText(member.getFirstName());
        binding.lastName.setText(member.getLastName());
        binding.mobile.setText(member.getMobile());
        String concatName = member.getFirstName() + " " + member.getLastName();
        binding.topName.setText(concatName);
        binding.topPhone.setText(member.getMobile());
        binding.email.setText(member.getEmail());
        if (databaseHelper.getMemberPhoto() != null) {
            binding.profileImage.setImageBitmap(databaseHelper.getMemberPhoto());
        }
        if (!member.getDateOfBirth().equalsIgnoreCase("")) {
            binding.birthdayText.setText(member.getDateOfBirth());
        }

        Log.d(TAG, "initContent: mobile " + member.getMobile());
    }


    private void showDatePickerDialog() {
        new DatePickerDialog(this, dateListener, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    DatePickerDialog.OnDateSetListener dateListener = (view, year, monthOfYear, dayOfMonth) -> {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        if (calendar.getTimeInMillis() > calendarNow.getTimeInMillis()) {
            showWarningDialog("Your date of birth can't be in the future");
            calendar = calendarNow;
        } else {
            setDateTime();
        }
    };

    private void setDateTime() {
        dateChanged = true;
        binding.birthdayText.setText(GetTime.getBirthDate(calendar.getTimeInMillis()));
        Log.d(TAG, "setDateTime: birthday " + GetTime.getBirthDate(calendar.getTimeInMillis()));
        member.setDateOfBirth(GetTime.getBirthDate(calendar.getTimeInMillis()));
        updateMember();
    }

    protected void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                + ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "checkPermission: not granted");
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.CAMERA)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Log.d(TAG, "checkPermission: should ask again");
                Toast.makeText(this, "Please Allow CAMERA and MEDIA Permission", Toast.LENGTH_LONG).show();
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, 3);
            } else {
                Log.d(TAG, "checkPermission: should not ask again");
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        }, 1
                );
            }
        } else {
            picPhoto();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequestPermissionsResult: start " + requestCode);
        Log.d(TAG, "onRequestPermissionsResult:grant " + grantResults[0]);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onRequestPermissionsResult: done ");
                picPhoto();
            } else {
                Log.d(TAG, "onRequestPermissionsResult: not done");
            }
        }

    }

    private void picPhoto() {
        picPhotoFragment.show(getSupportFragmentManager(), null);
    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == CAMERA_REQUEST) {
//            if ((resultCode == RESULT_OK) && (data != null)) {
//                CropImage.activity(currentPhotoPath)
//                        .start(this);
//            }
//        } else if (requestCode == GALLERY_REQUEST) {
//            if ((resultCode == RESULT_OK) && (data != null)) {
//                CropImage.activity(data.getData())
//                        .start(this);
//            }
//
//        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE
//                || requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE) {
//            Log.d(TAG, "onActivityResult: enter");
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//                Log.d(TAG, "onActivityResult: done from cropping");
//                assert result != null;
//                String imagePath = result.getUri().getPath();
//                storeMemberImage(imagePath);
//
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                assert result != null;
//                Log.d(TAG, "onActivityResult: error " + result.getError().getMessage());
//            } else {
//                Log.d(TAG, "onActivityResult: nothing");
//            }
//        }
//    }

    private File createImageFile() throws IOException {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".fileprovider",
                        photoFile);
                currentPhotoPath = photoURI;
                Log.d(TAG, "dispatchTakePictureIntent: photo uri " + photoURI);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }

    private void storeMemberImage(String imagePath) {
        Log.d(TAG, "storeMemberImage: imagepath " + imagePath);
        Bitmap b = getImageFromStorage(imagePath);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        if (b != null) {
            b.compress(Bitmap.CompressFormat.JPEG, 30, outputStream);
            byte[] data = outputStream.toByteArray();
            member.setImage(data);
            databaseHelper.updateMember(member);
            binding.profileImage.setImageBitmap(b);
        }
    }

    private Bitmap getImageFromStorage(String path) {

        try {

            File f = new File(path);
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            options.inSampleSize = calculateInSampleSize(options);
            return BitmapFactory.decodeStream(new FileInputStream(f), null, options);

        } catch (FileNotFoundException e) {
            Log.d(TAG, "getImageFromStorage: error " + e.getMessage());
        }
        return null;
    }

    private int calculateInSampleSize(

            BitmapFactory.Options options) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > 512 || width > 512) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > 512
                    && (halfWidth / inSampleSize) > 512) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    @Override
    public void onPicPhotoSelect(int position) {
        if (position == 0) {
            dispatchTakePictureIntent();
        } else {
            openGallery();
        }
        picPhotoFragment.dismiss();
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST);
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        customDialog.dismiss();
        logOut();
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        customDialog.dismiss();
    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }
}