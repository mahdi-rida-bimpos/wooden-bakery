package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.Cart;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.models.CartQuestionHeader;
import com.bimpos.wooden.bakery.models.CartQuestionModifier;

import java.util.HashMap;
import java.util.List;

public class MiniCartOrderRvAdapter extends RecyclerView.Adapter<MiniCartOrderRvAdapter.ViewHolder> {

    private List<CartProduct> cartItemsList;
    private final HashMap<Integer, Integer> cloneList;
    private final Context context;
    private final Cart cartData;
    private static final String TAG = "MiniCartOrder";


    public MiniCartOrderRvAdapter(List<CartProduct> cartItemsList, Context context) {
        this.cartItemsList = cartItemsList;
        Log.d(TAG, "MiniCartOrderRvAdapter: start");
        this.context = context;
        cartData = Cart.getCartData();
        cloneList = new HashMap<>();
        for (int i = 0; i < cartItemsList.size(); i++) {
            cloneList.put(cartItemsList.get(i).getCartId(), cartItemsList.get(i).getQty());
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cart_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CartProduct items = cartItemsList.get(position);
        double price = items.getPrice();
        holder.productPrice.setText(FormatPrice.getFormattedPrice(price));
        holder.productDesc.setText(items.getProd_desc());

        if (items.getRemark().equalsIgnoreCase("")) {
            holder.remarkLayout.setVisibility(View.GONE);
        } else {
            holder.remarkText.setText(items.getRemark());
            holder.remarkLayout.setVisibility(View.VISIBLE);
        }

        String qtyText = items.getQty() + "";
        holder.productQty.setText(qtyText);

        int count = items.getQty();
        double totalPrice = price * count;
        int modifierPrice = 0;
        for (int i = 0; i < items.getQuestionList().size(); i++) {
            CartQuestionHeader cartQuestionHeader = items.getQuestionList().get(i);
            for (int j = 0; j < cartQuestionHeader.getCartQuestionModifiers().size(); j++) {
                CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(j);
                modifierPrice += modifier.getPrice() * items.getQty();

            }
        }
        totalPrice += modifierPrice;
        holder.totalPrice.setText(FormatPrice.getFormattedPrice(totalPrice));

        holder.plus.setOnClickListener(view -> {
            int qty = Integer.parseInt(holder.productQty.getText().toString());
            qty++;
            holder.productQty.setText(String.valueOf(qty));
            cloneList.replace(items.getCartId(), qty);

        });

        holder.minus.setOnClickListener(view -> {
            int qty = Integer.parseInt(holder.productQty.getText().toString());
            if (qty != 0) {
                qty--;
            }
            holder.productQty.setText(String.valueOf(qty));
            cloneList.replace(items.getCartId(), qty);

        });

        if (null != items.getQuestionList() && items.getQuestionList().size() != 0) {
            CartOrderQHeaderRvAdapter adapter = new CartOrderQHeaderRvAdapter(context, items);
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.recyclerView.setAdapter(adapter);
        } else if (null != items.getComboList() && items.getComboList().size() != 0) {
            CartOrderComboHeaderRvAdapter adapter = new CartOrderComboHeaderRvAdapter(context, items);
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.recyclerView.setAdapter(adapter);
        }
    }

    public void modifyCart() {
        for (int i = 0; i < cartItemsList.size(); i++) {
            cartData.modifyCartProducts(cloneList);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<CartProduct> newList) {
        this.cartItemsList = newList;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        if (cloneList != null && cloneList.size() != 0) {
            return cloneList.size();
        }
        return 0;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView plus, minus;
        RecyclerView recyclerView;
        LinearLayout remarkLayout, noLongerLayout;
        RelativeLayout mainLayout;
        Button noLongerRemove;
        TextView productDesc, productPrice, totalPrice, productQty, remarkText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            productPrice = itemView.findViewById(R.id.recycler_cart_order_productPrice);
            productDesc = itemView.findViewById(R.id.recycler_cart_order_productDescript);
            totalPrice = itemView.findViewById(R.id.recycler_cart_order_totalPrice);
            productQty = itemView.findViewById(R.id.recycler_cart_order_quantity);
            plus = itemView.findViewById(R.id.recycler_cart_order_plus);
            minus = itemView.findViewById(R.id.recycler_cart_order_minus);
            recyclerView = itemView.findViewById(R.id.recycler_cart_order_recyclerView);
            remarkLayout = itemView.findViewById(R.id.recycler_cart_order_remarkLayout);
            remarkText = itemView.findViewById(R.id.recycler_cart_order_remarkText);
            mainLayout = itemView.findViewById(R.id.recycler_cart_order_mainLayout);
            noLongerLayout = itemView.findViewById(R.id.recycler_cart_order_noLongerLayout);
            noLongerRemove = itemView.findViewById(R.id.recycler_cart_order_noLongerLayout_remove);
        }
    }
}
