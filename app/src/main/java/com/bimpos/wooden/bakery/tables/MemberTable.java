package com.bimpos.wooden.bakery.tables;

public class MemberTable {

    public static final String TABLE_NAME= "member";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String MEMBER_ID = "memberId";
        public static final String MEMBER_HS_ID = "hsMemberId";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String MOBILE = "mobile";
        public static final String DATE_OF_BIRTH = "dateOfBirth";
        public static final String MOBILE_VALIDATED = "mobileValidated";
        public static final String EMAIL = "email";
        public static final String IMAGE = "image";


        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
