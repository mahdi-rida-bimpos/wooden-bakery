package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.CartQuestionHeader;
import com.bimpos.wooden.bakery.models.CartQuestionModifier;
import com.bimpos.wooden.bakery.orderOnline.activities.ProductInfoActivity;

import java.util.List;

public class QuestionModifierRvAdapter extends RecyclerView.Adapter<QuestionModifierRvAdapter.ModifierSelectionViewHolder> {

    private final ProductInfoActivity productInfoActivity;
    private final CartQuestionHeader cartQuestionHeader;
    private final List<CartQuestionModifier> cartQuestionModifierList;
    private static final String TAG = "QuestionModifierRv";

    public QuestionModifierRvAdapter(ProductInfoActivity productInfoActivity, CartQuestionHeader cartQuestionHeader) {
        this.productInfoActivity = productInfoActivity;
        this.cartQuestionHeader = cartQuestionHeader;
        cartQuestionModifierList = cartQuestionHeader.getCartQuestionModifiers();
    }

    @NonNull
    @Override
    public QuestionModifierRvAdapter.ModifierSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_question_modifier_selection, parent, false);
        return new ModifierSelectionViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull QuestionModifierRvAdapter.ModifierSelectionViewHolder holder, int position) {

        CartQuestionModifier cartQuestionModifier = cartQuestionModifierList.get(position);

        holder.description.setText(cartQuestionModifier.getDescription());
       if(cartQuestionModifier.getPrice()>0){
           holder.price.setText(String.format("+ %s", FormatPrice.getFormattedPrice(cartQuestionModifier.getPrice())));
       }else{
           holder.price.setText("");
       }
        int min = cartQuestionHeader.getMin();
        int max = cartQuestionHeader.getMax();
        int required = cartQuestionHeader.getRequired();
        Log.d(TAG, "onBindViewHolder: question modifier : "+cartQuestionModifier.getDescription()+", "+cartQuestionModifier.getPrice());

        holder.checkBox.setChecked(cartQuestionModifier.isSelected());

        holder.checkBox.setOnClickListener(view -> {

            if ((min == max) && required == 1) {
                if (holder.checkBox.isChecked()) {
                    cartQuestionModifier.setSelected(true);
                    for (int i = 0; i < cartQuestionModifierList.size(); i++) {
                        CartQuestionModifier otherModifier = cartQuestionModifierList.get(i);
                        if (cartQuestionModifier != otherModifier) {
                            otherModifier.setSelected(false);
                        }
                    }
                    cartQuestionHeader.setCount(1);
                } else {
                    cartQuestionModifier.setSelected(false);
                    cartQuestionHeader.setCount(0);
                }
                notifyDataSetChanged();

            }
            else if ((min < max)) {
                Log.d(TAG, "onBindViewHolder: min < max");
                if (holder.checkBox.isChecked()) {

                    if (cartQuestionHeader.getCount() != max) {
                        Log.d(TAG, "onBindViewHolder: first if");
                        cartQuestionModifier.setSelected(true);
                        cartQuestionHeader.setCount(cartQuestionHeader.getCount() + 1);
                    } else {
                        Log.d(TAG, "onBindViewHolder: else");
                        for(int i=0;i<cartQuestionModifierList.size();i++){
                            cartQuestionModifierList.get(i).setSelected(false);
                        }
                        cartQuestionModifier.setSelected(true);
                    }

                } else {
                    Log.d(TAG, "onBindViewHolder: second else");
                    cartQuestionModifier.setSelected(false);
                    cartQuestionHeader.setCount(cartQuestionHeader.getCount() - 1);
                }
                Log.d(TAG, "onBindViewHolder: count " + cartQuestionHeader.getCount());
                notifyDataSetChanged();

            } else if (min == max && required == 0) {

                if (holder.checkBox.isChecked()) {
                    cartQuestionModifier.setSelected(true);
                    cartQuestionHeader.setCount(cartQuestionHeader.getCount() + 1);
                } else {
                    cartQuestionModifier.setSelected(false);
                    cartQuestionHeader.setCount(cartQuestionHeader.getCount() - 1);
                }
                notifyItemChanged(holder.getAdapterPosition());
            }

            if(cartQuestionModifier.getPrice()>0){
                calculateExtraPrice();
            }
        });

        holder.mainLayout.setOnClickListener(v -> {
            if ((min == max) && required == 1) {
                if (!cartQuestionModifier.isSelected()) {
                    cartQuestionModifier.setSelected(true);
                    for (int i = 0; i < cartQuestionModifierList.size(); i++) {
                        CartQuestionModifier otherModifier = cartQuestionModifierList.get(i);
                        if (cartQuestionModifier != otherModifier) {
                            otherModifier.setSelected(false);
                        }
                    }
                    cartQuestionHeader.setCount(1);
                } else {
                    cartQuestionModifier.setSelected(false);
                    cartQuestionHeader.setCount(0);
                }
                notifyDataSetChanged();

            }

            else if ((min < max)) {

                if (!cartQuestionModifier.isSelected()) {

                    if (cartQuestionHeader.getCount() != max) {
                        cartQuestionModifier.setSelected(true);
                        cartQuestionHeader.setCount(cartQuestionHeader.getCount() + 1);
                    }else{
                        for(int i=0;i<cartQuestionModifierList.size();i++){
                            cartQuestionModifierList.get(i).setSelected(false);
                        }
                        cartQuestionModifier.setSelected(true);
                    }
                } else {
                    cartQuestionModifier.setSelected(false);
                    cartQuestionHeader.setCount(cartQuestionHeader.getCount() - 1);
                }
                Log.d(TAG, "onBindViewHolder: count " + cartQuestionHeader.getCount());
                notifyDataSetChanged();

            } else if (min == max && required == 0) {

                if (!cartQuestionModifier.isSelected()) {
                    cartQuestionModifier.setSelected(true);
                    cartQuestionHeader.setCount(cartQuestionHeader.getCount() + 1);
                } else {
                    cartQuestionModifier.setSelected(false);
                    cartQuestionHeader.setCount(cartQuestionHeader.getCount() - 1);
                }
                notifyItemChanged(holder.getAdapterPosition());
            }

           if(cartQuestionModifier.getPrice()>0){
               calculateExtraPrice();
           }
        });

    }

    public void calculateExtraPrice() {
        int totalPrice = 0;
        List<CartQuestionHeader> cartQuestionHeaderList = productInfoActivity.recyclerAdapter.cartQuestionHeaderList;
        for (int i = 0; i < cartQuestionHeaderList.size(); i++) {
            CartQuestionHeader cartQuestionHeader = cartQuestionHeaderList.get(i);
            for (int j = 0; j < cartQuestionHeader.getCartQuestionModifiers().size(); j++) {
                CartQuestionModifier cartQuestionModifier = cartQuestionHeader.getCartQuestionModifiers().get(j);
                if (cartQuestionModifier.isSelected()) {
                    double price = cartQuestionModifier.getPrice();
                    totalPrice += price;
                }
            }
        }
        if (totalPrice == 0) {
            productInfoActivity.binding.extraLayout.animate().alpha(0f).setDuration(500).start();
        }
        if (productInfoActivity.binding.extraLayout.getAlpha() == 0) {
            productInfoActivity.binding.extraLayout.animate().alpha(1f).setDuration(500).start();
        }
        int prodCount = Integer.parseInt(productInfoActivity.binding.quantity.getText().toString());
        totalPrice = totalPrice * prodCount;
        productInfoActivity.binding.extraPrice.setText(String.format("+ %s",FormatPrice.getFormattedPrice(totalPrice)));

    }

    @Override
    public int getItemCount() {
        return cartQuestionModifierList.size();
    }

    static class ModifierSelectionViewHolder extends RecyclerView.ViewHolder {
        TextView description, price;
        CheckBox checkBox;
        LinearLayout mainLayout;

        public ModifierSelectionViewHolder(@NonNull View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.recycler_question_modifier_selection_descript);
            price = itemView.findViewById(R.id.recycler_question_modifier_selection_price);
            checkBox = itemView.findViewById(R.id.recycler_question_modifier_selection_checkBox);
            mainLayout = itemView.findViewById(R.id.recycler_question_modifier_selection_mainLayout);
        }
    }

}