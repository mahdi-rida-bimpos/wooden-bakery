package com.bimpos.wooden.bakery.models;

public class CakeItem {
    private final int cakeGalleryId;
    private final int cakeId;
    private int price;
    private int isActive;
    private final int minOrderQty;
    private String title;
    private String description;
    private String picPath;
    private final String refNum;


    public CakeItem(int galleryId, int galleryPhotoId, int price, int isActive, int minOrderQty, String galleryTitle, String description, String picPath, String refNum) {
        this.cakeGalleryId = galleryId;
        this.refNum = refNum;
        this.cakeId = galleryPhotoId;
        this.price = price;
        this.isActive = isActive;
        this.minOrderQty = minOrderQty;
        this.title = galleryTitle;
        this.description = description;
        this.picPath = picPath;
    }

    public String getRefNum() {
        return refNum;
    }

    public int getCakeGalleryId() {
        return cakeGalleryId;
    }

    public int getCakeId() {
        return cakeId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getMinOrderQty() {
        return minOrderQty;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

}
