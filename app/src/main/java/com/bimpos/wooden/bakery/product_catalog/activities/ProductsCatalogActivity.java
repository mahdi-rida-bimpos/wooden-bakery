package com.bimpos.wooden.bakery.product_catalog.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityProductsCatalogBinding;
import com.bimpos.wooden.bakery.models.Products;
import com.bimpos.wooden.bakery.product_catalog.adapters.ProductsCatalogRvAdapter;

import java.util.List;
import java.util.Objects;

public class ProductsCatalogActivity extends AppCompatActivity {

    private ActivityProductsCatalogBinding binding;
    private List<Products> productsList;
    private DatabaseHelper databaseHelper;
    private ProductsCatalogRvAdapter adapter;
    private static final String TAG = "ProductsCatalogActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProductsCatalogBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initClickListener();
        initSearchView();
        getExtras();
    }

    private void initSearchView() {
        binding.searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void initClickListener() {
        binding.back.setOnClickListener(v -> onBackPressed());
    }

    private void initVariables() {
        databaseHelper = MainApplication.getDatabase();
    }

    private void getExtras() {
        Log.d(TAG, "getExtras: start");
        int catId = getIntent().getIntExtra("catId", -1);
        Log.d(TAG, "getExtras: cat id " + catId);
        String catName = getIntent().getStringExtra("catName");
        binding.categoryName.setText(catName);
        if (catId == -1) {

            finish();
        } else {
            productsList = databaseHelper.getProductsByCatId(catId);
            if (productsList.size() != 0) {
                initRecyclerView();
            }
        }
    }

    private void initRecyclerView() {
        Log.d(TAG, "initRecyclerView: start");
        adapter = new ProductsCatalogRvAdapter(productsList,this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        if (Objects.requireNonNull(binding.searchText.getText()).toString().length() != 0 || binding.searchText.isFocused()) {
            Log.d(TAG, "onBackPressed: focused");
            binding.searchText.setText("");
            binding.searchText.clearFocus();
            MainApplication.getImm().hideSoftInputFromWindow(binding.searchText.getWindowToken(), 0);
        }else{
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }
}