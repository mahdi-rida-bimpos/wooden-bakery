package com.bimpos.wooden.bakery.orderHistory.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.helpers.GetStatus;
import com.bimpos.wooden.bakery.helpers.GetTime;
import com.bimpos.wooden.bakery.models.Branches;
import com.bimpos.wooden.bakery.models.OrderHistory;
import com.bimpos.wooden.bakery.orderHistory.activities.OrderHistoryActivity;
import com.bimpos.wooden.bakery.orderHistory.activities.OrderHistoryDetailsActivity;

import java.util.Calendar;
import java.util.List;

public class OrderHistoryRvAdapter extends RecyclerView.Adapter<OrderHistoryRvAdapter.ViewHolder> {

    private final Context context;
    private List<OrderHistory> orderHistoryList;
    private final DatabaseHelper databaseHelper;
    private final OrderHistoryActivity activity;
    private final Calendar calendar = Calendar.getInstance();
    private long mLastClick = 0;
    private static final String TAG = "OrderHistoryRvAdapter";
    public OrderHistoryRvAdapter(Context context, OrderHistoryActivity activity, List<OrderHistory> orderHistoryList) {
        this.context = context;
        this.orderHistoryList = orderHistoryList;
        this.activity = activity;
        databaseHelper = MainApplication.getDatabase();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_order_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        OrderHistory history = orderHistoryList.get(position);
        Log.d(TAG, "onBindViewHolder: history "+history);
        double price = history.getTotalPrice();
        String orderNumber;
        if (history.getConversionRate() == -1) {
            orderNumber = history.getOrderId() + "  -  " + FormatPrice.getCustomFormattedPrice(price, history.getDefaultCurrency());
        } else {
            orderNumber = history.getOrderId() + "  -  " + FormatPrice.getCustomFormattedPrice(price, history.getPriceListCurrency());
        }

        holder.orderNumber.setText(orderNumber);

        if (GetTime.isDifferentDay(Long.parseLong(history.getTransDate()), calendar.getTimeInMillis())) {
            holder.orderDate.setText(GetTime.getFullTimeAmPm(Long.parseLong(history.getTransDate())));
        } else {
            holder.orderDate.setText(GetTime.getTimeOnly(Long.parseLong(history.getTransDate())));
        }

        if (history.getScheduleDate().equalsIgnoreCase(history.getTransDate())) {
            //now
            holder.scheduleLayout.setVisibility(View.GONE);
            holder.scheduleText.setVisibility(View.GONE);
        } else {
            if (GetTime.isDifferentDay(Long.parseLong(history.getScheduleDate()), calendar.getTimeInMillis())) {
                holder.scheduleDate.setText(GetTime.getFullTimeAmPm(Long.parseLong(history.getScheduleDate())));
            } else {
                holder.scheduleDate.setText(GetTime.getTimeOnly(Long.parseLong(history.getScheduleDate())));
            }
            holder.scheduleLayout.setVisibility(View.VISIBLE);
            holder.scheduleText.setVisibility(View.VISIBLE);
        }

        Branches branch = databaseHelper.getBranchById(history.getBranchId());

        if (branch != null) {
            String phone = branch.getPhone();
            if (phone.length() != 0) {

                holder.call.setOnClickListener(v -> {
                    if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                        return;
                    }
                    mLastClick = SystemClock.elapsedRealtime();
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + phone));
                    context.startActivity(intent);
                });

            } else {
                holder.call.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.branch_call_grey_icon));
            }

            double longitude = branch.getLongitude();
            double latitude = branch.getLatitude();
            if (longitude != 0 || latitude != 0) {
                holder.map.setOnClickListener(v -> {
                    if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                        return;
                    }
                    mLastClick = SystemClock.elapsedRealtime();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:<" + latitude + ">,<" + longitude + ">?q=<" + latitude + ">,<" + longitude + ">(" + branch.getName() + ")"));
                    context.startActivity(intent);
                });
            } else {
                holder.map.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.branch_map_grey_icon));
            }

            String orderType;
            String status = GetStatus.parseStatus(history.getOrderStatus());
            if (history.getOrderType() == 1) {
                orderType = "Delivery ( " + branch.getName().trim() + " )";
                if (history.getOrderStatus() == 7) {
                    status = status + "for delivering";
                }
            } else {
                orderType = "Pickup ( " + branch.getName().trim() + " )";
                if (history.getOrderStatus() == 7) {
                    status = status + "to pickup";
                }
            }
            holder.orderType.setText(orderType);


            holder.orderStatus.setText(status);

        } else {

            String orderType;
            if (history.getOrderType() == 1) {
                orderType = "Delivery";
            } else {
                orderType = "Pickup";
            }
            holder.orderStatus.setText("");
            holder.orderType.setText(orderType);
        }

        holder.mainLayout.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            Intent intent = new Intent(context, OrderHistoryDetailsActivity.class);
            intent.putExtra(OrderHistory.class.getSimpleName(), history);
            context.startActivity(intent);
            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<OrderHistory> newData) {
        this.orderHistoryList = newData;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderHistoryList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView orderNumber, orderDate, orderType, orderStatus, scheduleDate, scheduleText;
        CardView mainLayout;
        ImageView map, call, scheduleIcon;
        LinearLayout scheduleLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            orderNumber = itemView.findViewById(R.id.recycler_order_history_orderNumber);
            orderDate = itemView.findViewById(R.id.recycler_order_history_orderDate);
            orderType = itemView.findViewById(R.id.recycler_order_history_orderType);
            orderStatus = itemView.findViewById(R.id.recycler_order_history_orderStatus);
            mainLayout = itemView.findViewById(R.id.recycler_order_history_mainLayout);
            map = itemView.findViewById(R.id.recycler_order_history_map);
            call = itemView.findViewById(R.id.recycler_order_history_call);
            scheduleDate = itemView.findViewById(R.id.recycler_order_history_orderScheduleDate);
            scheduleIcon = itemView.findViewById(R.id.recycler_order_history_orderScheduleIcon);
            scheduleText = itemView.findViewById(R.id.recycler_order_history_scheduleText);
            scheduleLayout = itemView.findViewById(R.id.recycler_order_history_scheduleLayout);
        }
    }
}
