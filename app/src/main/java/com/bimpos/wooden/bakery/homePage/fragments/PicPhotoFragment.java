package com.bimpos.wooden.bakery.homePage.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bimpos.wooden.bakery.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


public class PicPhotoFragment extends BottomSheetDialogFragment {

    private final OnPicPhotoSelect listener;
    LinearLayout camera, gallery;
    private static final String TAG = "PicPhotoFragment";

    public interface OnPicPhotoSelect {
        void onPicPhotoSelect(int position);
    }

    public PicPhotoFragment(OnPicPhotoSelect listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pic_photo, container, false);
        Log.d(TAG, "onCreateView: start");
        camera = view.findViewById(R.id.fragment_pic_photo_camera);
        gallery = view.findViewById(R.id.fragment_pic_photo_gallery);

        camera.setOnClickListener(v -> {
            Log.d(TAG, "onCreateView: clicked");
            if (listener != null) {
                listener.onPicPhotoSelect(0);
            }
        });

        gallery.setOnClickListener(v -> {
            Log.d(TAG, "onCreateView: clicked");
            if (listener != null) {
                listener.onPicPhotoSelect(1);
            }
        });
        return view;
    }

}
