package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.models.Categories;
import com.bimpos.wooden.bakery.orderOnline.activities.OrdersActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import java.util.List;

public class CategoriesRvAdapter extends RecyclerView.Adapter<CategoriesRvAdapter.ViewHolder> {
    private final Context context;
    private final List<Categories> categoriesList;
    private final OrdersActivity ordersActivity;
    private int oldPosition = 0;

    public CategoriesRvAdapter(Context context, OrdersActivity ordersActivity, List<Categories> categoriesList) {
        this.context = context;
        this.categoriesList = categoriesList;
        this.ordersActivity = ordersActivity;
        if (categoriesList.size() != 0) {
            categoriesList.get(oldPosition).setSelected(true);
        }
    }

    @NonNull
    @Override
    public CategoriesRvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_orders_categories, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesRvAdapter.ViewHolder holder, int position) {

        final Categories categories = categoriesList.get(position);
        holder.catName.setText(categories.getDescription());
        if (!categories.getPicpath().equalsIgnoreCase("")) {
            Glide.with(context)
                    .load(categories.getPicpath())
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .placeholder(R.drawable.category_place_holder)
                    .into(holder.catImage);
        }

        holder.itemView.setOnClickListener(view -> {
            changeSelectedCategory(holder.getAdapterPosition());
            ordersActivity.productsRvAdapter.hideMiniCart();
            ordersActivity.productsRvAdapter.scrollCategories = false;
            ordersActivity.binding.productRecyclerView.stopScroll();
            ordersActivity.productsLayoutManager.scrollToPositionWithOffset(categories.getSeq(), 0);
        });

        if (categoriesList.get(holder.getAdapterPosition()).isSelected()) {
            holder.catName.setTextColor(ContextCompat.getColor(context, R.color.gold));
            holder.lineSelected.setVisibility(View.VISIBLE);
        } else {
            holder.catName.setTextColor(Color.BLACK);
            holder.lineSelected.setVisibility(View.GONE);
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    public void changeSelectedCategory(int position) {
        for (Categories categories : categoriesList) {
            categories.setSelected(false);
        }
        categoriesList.get(position).setSelected(true);
        oldPosition = position;
        notifyDataSetChanged();
    }

    public void setSelectedCat(int catId, int seq) {
        for (int i = 0; i < categoriesList.size(); i++) {
            if (categoriesList.get(i).getCatId() == catId) {
                categoriesList.get(oldPosition).setSelected(false);
                categoriesList.get(i).setSelected(true);
                notifyItemChanged(oldPosition);
                oldPosition = categoriesList.indexOf(categoriesList.get(i));
                notifyItemChanged(i);
                ordersActivity.productsRvAdapter.scrollCategories = false;
                ordersActivity.binding.productRecyclerView.stopScroll();
                ordersActivity.productsLayoutManager.scrollToPositionWithOffset(seq, 0);
                ordersActivity.binding.categoryRecyclerView.scrollToPosition(i);
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView catImage;
        TextView catName;
        RelativeLayout catLayout;
        View lineSelected;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            catImage = itemView.findViewById(R.id.recycler_order_categories_catImage);
            catName = itemView.findViewById(R.id.recycler_order_categories_catName);
            catLayout = itemView.findViewById(R.id.recycler_order_categories_catLayout);
            lineSelected = itemView.findViewById(R.id.recycler_order_categories_lineSelected);
        }
    }
}
