package com.bimpos.wooden.bakery.models;

public class CartComboModifier {

    private String description;
    private int count;
    private final int modifierId;
    private double price;

    public CartComboModifier(int modifierId, String description, int count, double price) {
        this.description = description;
        this.count = count;
        this.price = price;
        this.modifierId = modifierId;
    }

    public int getModifierId() {
        return modifierId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
