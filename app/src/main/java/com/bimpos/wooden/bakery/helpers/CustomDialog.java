package com.bimpos.wooden.bakery.helpers;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.bimpos.wooden.bakery.R;


public class CustomDialog extends AppCompatDialogFragment {


    public static final String DIALOG_ID = "id";
    public static final String DIALOG_MESSAGE = "message";
    public static final String DIALOG_TITLE = "title";
    public static final String DIALOG_POSITIVE_BUTTON = "positiveButton";
    public static final String DIALOG_NEGATIVE_BUTTON = "negativeButton";
    private final DialogEvents listener;
    private AlertDialog dialog;

    public interface DialogEvents {
        void onPositiveDialogResult(int dialogId, Bundle args);

        void onNegativeDialogResult(int dialogId, Bundle args);

        void onDialogCancelled(int dialogId);
    }

    public CustomDialog(DialogEvents listener) {
        this.listener = listener;
    }

    @Override
    public void onStart() {
        super.onStart();

//        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
//        InsetDrawable inset = new InsetDrawable(back, 100);
//        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        @SuppressLint("InflateParams")
        View dialogView = getLayoutInflater().inflate(R.layout.custom_dialog, null, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(dialogView);

        final Bundle arguments = getArguments();
        final int dialogId;
        String messageString;
        String titleText;
        String positiveButtonText;
        String negativeButtonText;

        TextView title = dialogView.findViewById(R.id.custom_dialog_title);
        TextView message = dialogView.findViewById(R.id.custom_dialog_message);
        LinearLayout titleLayout = dialogView.findViewById(R.id.custom_dialog_titleLayout);
        TextView positiveButton = dialogView.findViewById(R.id.custom_dialog_positiveButton);
        TextView negativeButton = dialogView.findViewById(R.id.custom_dialog_negativeButton);


        if (arguments != null) {
            dialogId = arguments.getInt(DIALOG_ID);
            messageString = arguments.getString(DIALOG_MESSAGE);
            positiveButtonText = arguments.getString(DIALOG_POSITIVE_BUTTON);
            negativeButtonText = arguments.getString(DIALOG_NEGATIVE_BUTTON);
            titleText = arguments.getString(DIALOG_TITLE);
            if(arguments.containsKey("positiveColor")){
                positiveButton.setTextColor(arguments.getInt("positiveColor"));
            }

        } else {
            throw new IllegalArgumentException("Must pass Dialog_id and Dialog_message in the bundle");
        }

        message.setText(messageString);

        if (titleText.equalsIgnoreCase("null")) {
            titleLayout.setVisibility(View.GONE);
        } else {
            title.setText(titleText);
        }

        positiveButton.setText(positiveButtonText);
        positiveButton.setOnClickListener(v -> {
            if (listener != null) {
                listener.onPositiveDialogResult(dialogId, arguments);
            }
        });

        if (negativeButtonText.equalsIgnoreCase("null")) {
            negativeButton.setVisibility(View.GONE);
        } else {
            negativeButton.setText(negativeButtonText);
            negativeButton.setOnClickListener(v -> listener.onNegativeDialogResult(dialogId, arguments));
        }

        dialog = builder.create();
        return dialog;
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        if (listener != null) {
            assert getArguments() != null;
            int dialogId = getArguments().getInt(DIALOG_ID);
            listener.onDialogCancelled(dialogId);
        }
    }
}
