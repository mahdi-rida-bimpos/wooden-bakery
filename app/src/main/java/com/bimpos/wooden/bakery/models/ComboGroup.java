package com.bimpos.wooden.bakery.models;

public class ComboGroup {

    private final int comboGroupId;

    public ComboGroup(int CGId) {
        this.comboGroupId = CGId;
    }

    public int getComboGroupId() {
        return comboGroupId;
    }

}
