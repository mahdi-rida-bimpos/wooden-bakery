package com.bimpos.wooden.bakery.models;

public class Department {

    private final int depId;
    private int isActive;
    private final String depName;
    private String depEmail;
    private final String depMobile;

    public Department(int depId, int isActive, String depName, String depEmail, String depMobile) {
        this.depId = depId;
        this.isActive = isActive;
        this.depName = depName;
        this.depEmail = depEmail;
        this.depMobile = depMobile;
    }

    public int getDepId() {
        return depId;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getDepName() {
        return depName;
    }

    public String getDepMobile() {
        return depMobile;
    }
}
