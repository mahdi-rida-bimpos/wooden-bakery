package com.bimpos.wooden.bakery.helpers;

import android.text.InputFilter;

public class Helpers {

    public static String blockCharacterSet = "?%~`!@#$%^&*();{}[]<>,./?'\\\"";
    public static InputFilter filter = (source, start, end, dest, dstart, dend) -> {

        if (source != null && blockCharacterSet.contains(("" + source))) {
            return "";
        }
        return null;
    };
}
