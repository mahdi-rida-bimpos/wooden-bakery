package com.bimpos.wooden.bakery.tables;

public class CategoriesTable {

    public static final String TABLE_NAME= "categories";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String CAT_ID = "catId";
        public static final String MENU_ID = "menuId";
        public static final String PARENT_ID = "parentId";
        public static final String CAT_NAME = "catName";
        public static final String DESCRIPTION = "description";
        public static final String DESCRIPTION_2 = "description2";
        public static final String PIC_PATH = "picPath";
        public static final String PICTURE = "picture";
        public static final String THUMB_PICTURE = "thumbPicture";
        public static final String HEADER_PIC_PATH = "headerPicPath";
        public static final String FOOTER_PIC_PATH = "footerPicPath";
        public static final String BIG_PIC_PATH = "bigPicPath";
        public static final String SEQ = "seq";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
