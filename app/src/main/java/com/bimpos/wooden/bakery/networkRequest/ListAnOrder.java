package com.bimpos.wooden.bakery.networkRequest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.wooden.bakery.BuildConfig;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.models.OrderHistory;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ListAnOrder extends AsyncTask<Integer, Void, String> {

    private static final String TAG = "ListAnOrder";
    private final SharedPreferences prefs;
    private final OnListOrderComplete listener;
    private final OrderHistory history;
    HttpURLConnection conn;
    URL url = null;

    public interface OnListOrderComplete {
        void onListOrderComplete(int status,OrderHistory history);
    }

    public ListAnOrder(OnListOrderComplete listener, OrderHistory history) {
        prefs = MainApplication.getPreferences();
        this.listener = listener;
        this.history = history;
    }

    @Override
    protected String doInBackground(Integer... integers) {
        try {
            Log.d(TAG, "doInBackground: list order start");
            int orderId = history.getOrderId();
            url = new URL(Constant.BASE_URL + "order/"+orderId+"/status");
            //order/232/status
            Log.d(TAG, "doInBackground: url "+url);

            conn = (HttpURLConnection) url.openConnection();
            String token = prefs.getString(Constant.TOKEN, "");
            Log.d(TAG, "doInBackground: token: "+token);
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestMethod("GET");
            int READ_TIMEOUT = 15000;
            conn.setReadTimeout(READ_TIMEOUT);
            int CONNECTION_TIMEOUT = 10000;
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.connect();

            int response_code = conn.getResponseCode();
            Log.d(TAG, "doInBackground: response code "+response_code);
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                return (result.toString());
            } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                return ("token");
            } else {
                return ("notFound");
            }
        } catch (Exception e) {
            try {
                Log.d(TAG, "doInBackground: error " + e.getMessage() + ", " + conn.getResponseMessage());
            } catch (IOException ioException) {
                Log.d(TAG, "doInBackground: error " + ioException.getMessage() );
            }
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, "onPostExecute: result "+result);
//        listener.onListOrderComplete(5,history);
        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("notFound")) {
            listener.onListOrderComplete(-2,history);
        }
        else if(result.equalsIgnoreCase("token")){
            listener.onListOrderComplete(-1,history);
        }
        else {
            try {
                JSONObject object = new JSONObject(result);
                int status = object.getInt("status");
                listener.onListOrderComplete(status,history);
            } catch (JSONException e) {
                listener.onListOrderComplete(-2,history);
            }
        }
    }
}
