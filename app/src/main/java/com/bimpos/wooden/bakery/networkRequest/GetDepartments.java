package com.bimpos.wooden.bakery.networkRequest;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.models.Department;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetDepartments extends AsyncTask<Integer, Void, String> {

    private static final String TAG = "ListAnOrder";
    private final OnGetDepartmentsComplete listener;
    HttpURLConnection conn;
    private final SharedPreferences prefs;
    URL url = null;

    public interface OnGetDepartmentsComplete {
        void onGetDepartmentsComplete(int status, List<Department> departmentList);
    }

    public GetDepartments(OnGetDepartmentsComplete listener) {
        this.listener = listener;
        prefs = MainApplication.getPreferences();
    }

    @Override
    protected String doInBackground(Integer... integers) {
        try {
            Log.d(TAG, "doInBackground: start");
            url = new URL(Constant.BASE_URL + "departments");
            conn = (HttpURLConnection) url.openConnection();
            String token = prefs.getString(Constant.TOKEN, "");
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestMethod("GET");
            int READ_TIMEOUT = 15000;
            conn.setReadTimeout(READ_TIMEOUT);
            int CONNECTION_TIMEOUT = 10000;
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.connect();

            int response_code = conn.getResponseCode();
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                return (result.toString());
            } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                return ("token");
            } else {
                return ("notFound");
            }
        } catch (Exception e) {
            try {
                Log.d(TAG, "doInBackground: error " + e.getMessage() + ", " + conn.getResponseMessage());
            } catch (IOException ioException) {
                Log.d(TAG, "doInBackground: error " + ioException.getMessage());
            }
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, "onPostExecute: result " + result);
        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("notFound")) {
            listener.onGetDepartmentsComplete(0, null);
        }
        else if(result.equalsIgnoreCase("token")){
            listener.onGetDepartmentsComplete(-1, null);
        }
        else {
            try {
                List<Department> departmentList = new ArrayList<>();
                JSONObject parentObject = new JSONObject(result);
                JSONArray depArray = parentObject.getJSONArray("departments");
                for (int i = 0; i < depArray.length(); i++) {
                    JSONObject depObject = depArray.getJSONObject(i);
                    int depId = depObject.getInt("id");
                    int isActive = depObject.getInt("isactive");
                    String depName = depObject.getString("name");
                    String depMobile = depObject.getString("mobile");
                    String depEmail = depObject.getString("email");

                    if (isActive == 1) {
                        Department department = new Department(depId, isActive, depName, depEmail, depMobile);
                        departmentList.add(department);
                    }
                }
                listener.onGetDepartmentsComplete(1,departmentList);
            } catch (JSONException e) {
                listener.onGetDepartmentsComplete(0, null);
            }
        }
    }
}
