package com.bimpos.wooden.bakery.models;

public class Order {

    private String cartItemList;

    public Order(String cartItemList) {
        this.cartItemList = cartItemList;
    }

    public String getCartItemList() {
        return cartItemList;
    }

    public void setCartItemList(String cartItemList) {
        this.cartItemList = cartItemList;
    }
}
