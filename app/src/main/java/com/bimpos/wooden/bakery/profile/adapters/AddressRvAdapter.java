package com.bimpos.wooden.bakery.profile.adapters;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.models.Address;
import com.bimpos.wooden.bakery.profile.activities.AddressActivity;
import com.bimpos.wooden.bakery.profile.activities.ProfileActivity;

import java.util.List;

public class AddressRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private final List<Address> addressList;
    private final DatabaseHelper databaseHelper;
    private final OnDefaultAddressChanged listener;
    private ProfileActivity profileActivity = null;
    private static final String TAG = "AddressRvAdapter";
    private long mLastClick = 0;

    public interface OnDefaultAddressChanged {
        void onDefaultAddressChanged(int cityCode);
    }

    public AddressRvAdapter(Context context, List<Address> addressList, OnDefaultAddressChanged listener) {
        this.context = context;
        this.listener = listener;
        this.addressList = addressList;
        databaseHelper = MainApplication.getDatabase();
    }

    public void setProfileActivity(ProfileActivity profileActivity) {
        this.profileActivity = profileActivity;
    }

    @Override
    public int getItemViewType(int position) {
        if (addressList.size() == 0) {
            Log.d(TAG, "getItemViewType: return 1");
            return 1;
        }
        return 2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == 1) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_no_address_found, parent, false);
            return new NoAddressViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_address, parent, false);
            return new ViewHolder(view);
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {

        if (addressList.size() != 0) {
            Address address = addressList.get(position);
            ViewHolder holder = (ViewHolder) holder1;
            Log.d(TAG, "onBindViewHolder: address " + address.getDefaultAddress());

            if (address.getDefaultAddress() == 1) {

                int colorFrom = Color.WHITE;
                ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, ContextCompat.getColor(context, R.color.greyLight));
                colorAnimation.setDuration(250); // milliseconds
                colorAnimation.addUpdateListener(animator -> holder.cardLayout.setCardBackgroundColor((int) animator.getAnimatedValue()));
                colorAnimation.start();
                holder.radioButton.setChecked(true);
                if (address.getAddressType() == 0) {
                    holder.imageLogo.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.home_gold));
                } else {
                    holder.imageLogo.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.work_gold));
                }
                holder.type.setTextColor(ContextCompat.getColor(context, R.color.gold));
                holder.isDefault.setVisibility(View.VISIBLE);
            } else {
                holder.cardLayout.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white));
                if (address.getAddressType() == 0) {
                    holder.imageLogo.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.home_black));
                } else {
                    holder.imageLogo.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.work_black));
                }
                holder.type.setTextColor(ContextCompat.getColor(context, R.color.black));
                holder.radioButton.setOnCheckedChangeListener(null);
                holder.radioButton.setChecked(false);
                holder.isDefault.setVisibility(View.INVISIBLE);
            }

            String addressDescription = address.getStreet() + ", " + address.getBldg() + ", " + address.getFloor();
            holder.description.setText(addressDescription);

//            String addressType = "";
//            switch (address.getAddressType()) {
//                case 0:
//                    addressType = "Home";
//                    break;
//                case 1:
//                    addressType = "Work";
//                    break;
//                case 2:
//                    addressType = "Other";
//                    break;
//            }

//            String description = addressType + " " + address.getDescription() + " (" + address.getCityName() + ")";
            String description = address.getDescription() + " (" + address.getCityName() + ")";
            holder.type.setText(description);

            if (addressList.size() == 1) {
                holder.radioButton.setOnClickListener(null);
                holder.radioButton.setClickable(false);
            }

            if (address.getDefaultAddress() == 1) {
                if (listener != null) {
                    listener.onDefaultAddressChanged(address.getCityCode());
                }
            }

            holder.mainLayout.setOnClickListener(v -> {
                if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                    return;
                }
                mLastClick = SystemClock.elapsedRealtime();
                if (profileActivity != null) {
                    if (profileActivity.binding.infoSave.getTag().toString().equalsIgnoreCase("save")) {
                        if (addressList.size() == 1) {
                            address.setDefaultAddress(1);
                        } else {
                            if (address.getDefaultAddress() == 0) {
                                address.setDefaultAddress(1);
                                holder.radioButton.setChecked(true);
                                for (int i = 0; i < addressList.size(); i++) {
                                    if (addressList.get(i) != address) {
                                        addressList.get(i).setDefaultAddress(0);
                                        databaseHelper.updateAddress(addressList.get(i));
                                    }
                                }
                                databaseHelper.updateAddress(address);
//                        if (listener != null) {
//                            listener.onDefaultAddressChanged(address.getCityCode());
//                        }
                            } else {
                                holder.radioButton.setChecked(true);
                            }
                            databaseHelper.updateAddress(address);

                        }
                        notifyDataSetChanged();
                    }
                } else {
                    if (addressList.size() == 1) {
                        address.setDefaultAddress(1);
                    } else {
                        if (address.getDefaultAddress() == 0) {
                            address.setDefaultAddress(1);
                            holder.radioButton.setChecked(true);
                            for (int i = 0; i < addressList.size(); i++) {
                                if (addressList.get(i) != address) {
                                    addressList.get(i).setDefaultAddress(0);
                                    databaseHelper.updateAddress(addressList.get(i));
                                }
                            }
                            databaseHelper.updateAddress(address);
//                        if (listener != null) {
//                            listener.onDefaultAddressChanged(address.getCityCode());
//                        }
                        } else {
                            holder.radioButton.setChecked(true);
                        }
                        databaseHelper.updateAddress(address);

                    }
                    notifyDataSetChanged();
                }

            });

            holder.radioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                    return;
                }
                mLastClick = SystemClock.elapsedRealtime();
                if (profileActivity != null) {
                    if (profileActivity.binding.infoSave.getTag().toString().equalsIgnoreCase("save")) {
                        if (addressList.size() == 1) {
                            address.setDefaultAddress(1);
                        } else {
                            if (isChecked) {
                                address.setDefaultAddress(1);
                                for (int i = 0; i < addressList.size(); i++) {
                                    if (addressList.get(i) != address) {
                                        addressList.get(i).setDefaultAddress(0);
                                        databaseHelper.updateAddress(addressList.get(i));
                                    }
                                }
                                databaseHelper.updateAddress(address);
                                notifyDataSetChanged();
                                //in case of checkout order
//                        if (listener != null) {
//                            listener.onDefaultAddressChanged(address.getCityCode());
//                        }
                            } else {
                                holder.radioButton.setChecked(true);
                            }
                            databaseHelper.updateAddress(address);

                        }
                        notifyDataSetChanged();
                    } else {
                        holder.radioButton.setChecked(false);
                    }
                } else {
                    if (addressList.size() == 1) {
                        address.setDefaultAddress(1);
                    } else {
                        if (isChecked) {
                            address.setDefaultAddress(1);
                            for (int i = 0; i < addressList.size(); i++) {
                                if (addressList.get(i) != address) {
                                    addressList.get(i).setDefaultAddress(0);
                                    databaseHelper.updateAddress(addressList.get(i));
                                }
                            }
                            databaseHelper.updateAddress(address);
                            notifyDataSetChanged();
                            //in case of checkout order
//                        if (listener != null) {
//                            listener.onDefaultAddressChanged(address.getCityCode());
//                        }
                        } else {
                            holder.radioButton.setChecked(true);
                        }
                        databaseHelper.updateAddress(address);

                    }
                    notifyDataSetChanged();
                }

            });

            holder.edit.setOnClickListener(v -> {
                if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                    return;
                }
                if (profileActivity != null) {
                    if (profileActivity.binding.infoSave.getTag().toString().equalsIgnoreCase("save")) {
                        mLastClick = SystemClock.elapsedRealtime();
                        Intent intent = new Intent(context, AddressActivity.class);
                        intent.putExtra(Address.class.getSimpleName(), address);
                        context.startActivity(intent);
                        profileActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                } else {
                    mLastClick = SystemClock.elapsedRealtime();
                    Intent intent = new Intent(context, AddressActivity.class);
                    intent.putExtra(Address.class.getSimpleName(), address);
                    context.startActivity(intent);
//                    profileActivity.overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                }

            });

            if (profileActivity != null) {
                if (profileActivity.binding.infoSave.getTag().toString().equalsIgnoreCase("save")) {
                    holder.radioButton.setEnabled(true);
                } else {
                    holder.radioButton.setEnabled(false);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (addressList != null && addressList.size() != 0) {
            return addressList.size();
        }
        return 1;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView type, description, isDefault;
        ImageButton edit;
        RadioButton radioButton;
        LinearLayout mainLayout;
        ImageView imageLogo;
        CardView cardLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            type = itemView.findViewById(R.id.recycler_address_typeText);
            description = itemView.findViewById(R.id.recycler_address_description);
            edit = itemView.findViewById(R.id.recycler_address_edit);
            radioButton = itemView.findViewById(R.id.recycler_address_radioButton);
            mainLayout = itemView.findViewById(R.id.recycler_address_mainLayout);
            imageLogo = itemView.findViewById(R.id.recycler_address_imageLogo);
            cardLayout = itemView.findViewById(R.id.recycler_address_cardLayout);
            isDefault = itemView.findViewById(R.id.recycler_address_isDefault);
        }
    }

    static class NoAddressViewHolder extends RecyclerView.ViewHolder {
        public NoAddressViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
