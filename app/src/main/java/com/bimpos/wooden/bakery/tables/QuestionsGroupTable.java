package com.bimpos.wooden.bakery.tables;

public class QuestionsGroupTable {

    public static final String TABLE_NAME= "questionGroup";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String QUESTION_GROUP_ID = "questionGroupId";
        public static final String DESCRIPTION = "description";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
