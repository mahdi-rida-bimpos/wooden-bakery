package com.bimpos.wooden.bakery.models;

public class PriceList {
    private int decimals,shouldShowCurr;
    private double conversion;
    private String currencyName, mask;

    public PriceList(int decimals, double conversion, String description, String mask) {
        this.decimals = decimals;
        this.conversion = conversion;
        this.currencyName = description;
        this.mask = mask;
    }

    public int getShouldShowCurr() {
        return shouldShowCurr;
    }

    public void setShouldShowCurr(int shouldShowCurr) {
        this.shouldShowCurr = shouldShowCurr;
    }

    public int getDecimals() {
        return decimals;
    }

    public void setDecimals(int decimals) {
        this.decimals = decimals;
    }

    public double getConversion() {
        return conversion;
    }

    public void setConversion(double conversion) {
        this.conversion = conversion;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    @Override
    public String toString() {
        return "PriceList{" +
                "decimals=" + decimals +
                ", conversion=" + conversion +
                ", description='" + currencyName + '\'' +
                ", mask='" + mask + '\'' +
                ", showInPriceCurrency='" + shouldShowCurr + '\'' +
                '}';
    }
}
