package com.bimpos.wooden.bakery.profile.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivitySmsVerificationBinding;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.helpers.CustomLoadingOrder;
import com.bimpos.wooden.bakery.models.Member;
import com.bimpos.wooden.bakery.networkRequest.RequestSms;
import com.bimpos.wooden.bakery.networkRequest.VerifySms;

public class SmsVerificationActivity extends AppCompatActivity implements VerifySms.OnSmsVerified, CustomDialog.DialogEvents, RequestSms.OnSmsReceived {

    private ActivitySmsVerificationBinding binding;

    private int i = 0;
    private final int[] timeArray = new int[]{60000, 300000, 1800000};
    private int timer;
    private CustomLoadingOrder loading;
    private CustomDialog dialog;
    private String phoneNumber;
    private SharedPreferences preferences;
    private static final String TAG = "SmsVerification";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySmsVerificationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        getExtras();
        disableReSendButton();
        disableValidateButton();
        initClickListeners();
        initTimer();
    }

    private void getExtras() {
        phoneNumber = getIntent().getStringExtra("phone");
        binding.phoneNumber.setText(phoneNumber);
    }


    private void initVariables() {
        preferences = MainApplication.getPreferences();
        dialog = new CustomDialog(this);
        loading = new CustomLoadingOrder(this);
        binding.textEntry.requestFocus();
        MainApplication.getImm().showSoftInput(binding.textEntry,0);
    }

    private void initClickListeners() {
        binding.resend.setOnClickListener(v -> {
            initTimer();
            sendSmsRequest();
        });

        binding.back.setOnClickListener(v -> finish());

        binding.textEntry.setOnPinEnteredListener(str -> {
            if (str.toString().length() == 5) {
                enableValidateButton();
                verifyPhoneNumber();
            }
        });

        binding.changeNumber.setOnClickListener(v -> {
            preferences.edit().remove(Constant.IS_SMS_SENT).apply();
            preferences.edit().remove(Constant.PHONE_NUMBER).apply();
            gotoLoginActivity();
        });
    }

    private void gotoLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    private void sendSmsRequest() {
        loading.show();
        loading.startAnimation(R.raw.web_loading);
        RequestSms requestSms = new RequestSms(phoneNumber, this);
        requestSms.execute();
    }

    @Override
    public void onSmsReceived(int status) {
        loading.dismiss();
    }

    private void verifyPhoneNumber() {
        loading.show();
        loading.startAnimation(R.raw.web_loading);
        VerifySms verifySms = new VerifySms(phoneNumber, this, binding.textEntry.getText().toString());
        verifySms.execute();
    }

    private void initTimer() {
//        Log.d(TAG, "initTimer: i "+i);
        binding.timerLayout.setVisibility(View.VISIBLE);
        disableReSendButton();
        timer = timeArray[i];
        new CountDownTimer(timer, 1000) {

            @SuppressLint("SetTextI18n")
            public void onTick(long millisUntilFinished) {
//                Log.d(TAG, "onTick: start");
                long remainedSecs = timer / 1000;
                String strSec;
                if ((remainedSecs % 60) < 10) {
                    strSec = "0" + remainedSecs % 60;
                } else {
                    strSec = String.valueOf((remainedSecs % 60));
                }
//                Log.d(TAG, "onTick: remainig secs "+remainedSecs);
//                Log.d(TAG, "onTick: string "+strSec);
                binding.txtSmsCounter.setText("" + (remainedSecs / 60) + ":" + strSec);
                timer -= 1000;
            }

            public void onFinish() {
//                Log.d(TAG, "onFinish: start");
                if (i < 2) {
                    i++;
                }
//                Log.d(TAG, "onFinish: i "+i);
                binding.timerLayout.setVisibility(View.GONE);
                enableReSendButton();
                cancel();
            }
        }.start();
    }

    private void disableReSendButton() {
        binding.resend.setEnabled(false);
        binding.resend.setClickable(false);
        binding.resend.setBackgroundColor(ContextCompat.getColor(this, R.color.pantoneRedDisabled));
        binding.resend.setTextColor(ContextCompat.getColor(this, R.color.greyDark));
    }

    private void enableReSendButton() {
        binding.resend.setClickable(true);
        binding.resend.setEnabled(true);
        binding.resend.setBackgroundColor(ContextCompat.getColor(this, R.color.pantoneRed));
        binding.resend.setTextColor(ContextCompat.getColor(this, R.color.white));
    }

    private void disableValidateButton() {
        binding.validate.setEnabled(false);
        binding.validate.setClickable(false);
        binding.validate.setBackgroundColor(ContextCompat.getColor(this, R.color.pantoneRedDisabled));
        binding.validate.setTextColor(ContextCompat.getColor(this, R.color.greyDark));
    }

    private void enableValidateButton() {
        binding.validate.setClickable(true);
        binding.validate.setEnabled(true);
        binding.validate.setBackgroundColor(ContextCompat.getColor(this, R.color.pantoneRed));
        binding.validate.setTextColor(ContextCompat.getColor(this, R.color.white));
    }

    @Override
    public void onSmsVerified(int status) {
        Log.d(TAG, "onSmsVerified: status " + status);
        loading.dismiss();
        if (status == 1) {
            preferences.edit().remove(Constant.IS_SMS_SENT).apply();
            preferences.edit().remove(Constant.PHONE_NUMBER).apply();
            createProfile();
        } else {
            showErrorDialog();
        }
    }

    private void createProfile() {
        Member member = new Member();
        member.setMemberHsId(0);
        member.setMobile(phoneNumber);
        member.setMobileValidated(1);
        member.setFirstName("");
        member.setLastName("");
        member.setEmail("");
        member.setDateOfBirth("Not set");

        MainApplication.getDatabase().insertMember(member);
        if (loading.isShowing()) {
            loading.dismiss();
        }
        gotoProfileActivity();
    }

    private void gotoProfileActivity() {
        binding.textEntry.clearFocus();
        startActivity(new Intent(this, ProfileActivity.class).putExtra("status", 1));
        finish();
    }

    private void showErrorDialog() {
        if (loading.isShowing()) {
            loading.dismiss();
        }
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Your verification code doesn't match");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "ok");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
        arguments.putInt(CustomDialog.DIALOG_ID, 1);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        dialog.dismiss();
        binding.textEntry.setText("");
        binding.textEntry.requestFocus();
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {

    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


}