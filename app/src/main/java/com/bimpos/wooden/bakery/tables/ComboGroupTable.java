package com.bimpos.wooden.bakery.tables;

public class ComboGroupTable {

    public static final String TABLE_NAME= "comboGroup";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String COMBO_GROUP_ID = "comboGroupId";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
