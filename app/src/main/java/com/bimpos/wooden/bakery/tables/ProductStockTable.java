package com.bimpos.wooden.bakery.tables;

public class ProductStockTable {

    public static final String TABLE_NAME= "productStock";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String PROD_NUM = "prodNum";
        public static final String BRANCH_ID = "branchId";
        public static final String VARIATION_1 = "variation1";
        public static final String VARIATION_2 = "variation2";
        public static final String STOCK = "stock";
        public static final String UPDATED_AT = "updatedAt";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
