package com.bimpos.wooden.bakery.cakeGallery.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.branches.activities.BranchesActivity;
import com.bimpos.wooden.bakery.databinding.ActivityCakeImageBinding;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.watermark.androidwm.WatermarkBuilder;
import com.watermark.androidwm.bean.WatermarkImage;

public class CakeImageActivity extends AppCompatActivity {

    private static final String TAG = "CakeImageActivity";
    private ActivityCakeImageBinding binding;
    private WatermarkImage watermarkImage;
    private Bitmap bitmap;
    private String title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCakeImageBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);
        initView();
        binding.back.setOnClickListener(v -> finish());
    }

    private void initView() {
        title = getIntent().getStringExtra("cakeTitle");
        binding.toolbarTitle.setText(title);
        String url = getIntent().getStringExtra("PictureUrl");
        if (url.equalsIgnoreCase("")) {
            byte[] imageByte = getIntent().getExtras().getByteArray("bitmap");
            bitmap = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
            setImage(bitmap);

        } else {
            Picasso.get().load(url)
                    .placeholder(R.drawable.category_place_holder)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap1, Picasso.LoadedFrom from) {
                            bitmap = bitmap1;
                            setImage(bitmap);
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                            binding.image.setImageDrawable(ContextCompat.getDrawable(CakeImageActivity.this, R.drawable.category_place_holder));
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
        }
    }

    private void setImage(Bitmap bitmap) {
        watermarkImage = new WatermarkImage(this, R.drawable.wooden_logo)
                .setSize(0.7)
                .setImageAlpha(80)
                .setPositionX(0.1)
                .setPositionY(0.2)
                .setRotation(15);

        WatermarkBuilder
                .create(CakeImageActivity.this, bitmap)
                .loadWatermarkImage(watermarkImage)
                .setTileMode(false)
                .getWatermark()
                .setToImageView(binding.image);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cake_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.cake_menu_share) {
            checkPermission();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void checkPermission() {
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(
                this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Storage" +
                        " permission is required .");
                builder.setTitle("Please grant this permission");
                builder.setPositiveButton("OK", (dialogInterface, i) -> ActivityCompat.requestPermissions(
                        CakeImageActivity.this,
                        new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        },
                        1
                ));
                builder.setNeutralButton("Cancel", (dialog, which) -> finish());
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                // Directly request for required permissions, without explanation
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        },
                        1
                );
            }
        } else {
            shareImage();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if ((grantResults.length > 0) && (grantResults[0] + grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                shareImage();
            }
        }
    }

    private void shareImage() {
        Bitmap bitmap1 =
                WatermarkBuilder
                        .create(CakeImageActivity.this, bitmap)
                        .loadWatermarkImage(watermarkImage)
                        .getWatermark()
                        .getOutputImage();

        String bitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap1, "Cake-" + title, null);
        Uri bitmapUri = Uri.parse(bitmapPath);

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
        shareIntent.setType("image/jpeg");
        startActivity(Intent.createChooser(shareIntent, "Send to"));
    }
}
