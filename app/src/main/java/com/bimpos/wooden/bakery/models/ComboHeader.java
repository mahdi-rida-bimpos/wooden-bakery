package com.bimpos.wooden.bakery.models;

import java.util.ArrayList;
import java.util.List;

public class ComboHeader {

    private final int comboGroupId;
    private final int comboHeaderId;
    private final int min;
    private int max;
    private int required;
    private final int seq;
    private final String description;
    private List<ComboModifier> modifiersList;

    public ComboHeader(int comboGroupId, int comboHeaderId, int min, int max, int required, int seq, String description) {
        this.comboGroupId = comboGroupId;
        this.comboHeaderId = comboHeaderId;
        this.min = min;
        this.max = max;
        this.required = required;
        this.seq = seq;
        this.description = description;
        modifiersList = new ArrayList<>();
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getComboGroupId() {
        return comboGroupId;
    }

    public int getComboHeaderId() {
        return comboHeaderId;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public int getRequired() {
        return required;
    }

    public int getSeq() {
        return seq;
    }

    public String getDescription() {
        return description;
    }

    public List<ComboModifier> getModifiersList() {
        return modifiersList;
    }

    public void setModifiersList(List<ComboModifier> modifiersList) {
        this.modifiersList = modifiersList;
    }

    @Override
    public String toString() {
        return "ComboHeader{" +
                "comboGroupId=" + comboGroupId +
                ", comboHeaderId=" + comboHeaderId +
                ", min=" + min +
                ", max=" + max +
                ", required=" + required +
                ", seq=" + seq +
                ", description='" + description + '\'' +
                ", modifiersList=" + modifiersList +
                '}';
    }
}
