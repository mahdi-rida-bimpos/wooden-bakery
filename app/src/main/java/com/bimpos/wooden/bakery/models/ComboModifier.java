package com.bimpos.wooden.bakery.models;

public class ComboModifier {

    private int prodNum;
    private int catId;
    private int comboHeaderId;
    private int groupId;
    private int stock;
    private int pid;
    private int isTaxable1;
    private int isTaxable2;
    private int isTaxable3;
    private int versionId;
    private double price,finalPrice;
    private String description, clientId, description2, prodInfo, prodInfo2, picPath, picture, thumbPicture, brand, refCode1, refCode2, createdAt, updatedAt;

    public ComboModifier() {
    }

    public ComboModifier(int prodNum, int catId, int comboHeaderId, int groupId, int stock, int enabled, int pid, int isTaxable1, int isTaxable2, int isTaxable3, int versionId, double price, String description, String clientId, String description2, String prodInfo, String prodInfo2, String picPath, String picture, String thumbPicture, String brand, String refCode1, String refCode2, String createdAt, String updatedAt) {
        this.prodNum = prodNum;
        this.catId = catId;
        this.groupId = groupId;
        this.stock = stock;
        this.pid = pid;
        this.isTaxable1 = isTaxable1;
        this.isTaxable2 = isTaxable2;
        this.isTaxable3 = isTaxable3;
        this.versionId = versionId;
        this.price = price;
        this.description = description;
        this.clientId = clientId;
        this.description2 = description2;
        this.prodInfo = prodInfo;
        this.prodInfo2 = prodInfo2;
        this.picPath = picPath;
        this.picture = picture;
        this.thumbPicture = thumbPicture;
        this.brand = brand;
        this.refCode1 = refCode1;
        this.refCode2 = refCode2;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.comboHeaderId = comboHeaderId;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public int getProdNum() {
        return prodNum;
    }

    public void setProdNum(int prodNum) {
        this.prodNum = prodNum;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public int getComboHeaderId() {
        return comboHeaderId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setEnabled(int enabled) {
    }

    public int getPid() {
        return pid;
    }

    public int getIsTaxable1() {
        return isTaxable1;
    }

    public int getIsTaxable2() {
        return isTaxable2;
    }

    public int getIsTaxable3() {
        return isTaxable3;
    }

    public int getVersionId() {
        return versionId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription2() {
        return description2;
    }

    public String getProdInfo() {
        return prodInfo;
    }

    public String getProdInfo2() {
        return prodInfo2;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getThumbPicture() {
        return thumbPicture;
    }

    public String getBrand() {
        return brand;
    }

    public String getRefCode1() {
        return refCode1;
    }

    public String getRefCode2() {
        return refCode2;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

}
