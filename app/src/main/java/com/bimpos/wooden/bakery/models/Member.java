package com.bimpos.wooden.bakery.models;


public class Member {

    private int memberId, memberHsId, mobileValidated;
    private String firstName, lastName, mobile, email, dateOfBirth;
    private byte[] image;

    public Member() {
    }

    public Member(int memberId, int memCode, String firstName, String lastName, String mobile, int mobileValidated, String email, String dateOfBirth) {
        this.memberId = memberId;
        this.dateOfBirth = dateOfBirth;
        this.memberHsId = memCode;
        this.mobileValidated = mobileValidated;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobile = mobile;
        this.email = email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getMemberId() {
        return memberId;
    }

    public int getMemberHsId() {
        return memberHsId;
    }

    public void setMemberHsId(int memberHsId) {
        this.memberHsId = memberHsId;
    }

    public int getMobileValidated() {
        return mobileValidated;
    }

    public void setMobileValidated(int mobileValidated) {
        this.mobileValidated = mobileValidated;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
