package com.bimpos.wooden.bakery.models;

import java.io.Serializable;

public class OrderHistory implements Serializable {

    private int isNew, orderId, orderStatus, orderType, totalItems, branchId;
    private String cartItemList, branchName, transDate, scheduleDate, remark, defaultCurrency, priceListCurrency;
    private double totalPrice, deliveryCharge, conversionRate;

    public OrderHistory(int isNew, int orderId, int orderType, int orderStatus, int totalItems, double totalPrice, double deliveryCharge, int branchId, String cartItemList, String transDate, String scheduleDate, String branchName, String remark, double conversionRate, String defaultCurrency, String priceListCurrency) {
        this.isNew = isNew;
        this.orderId = orderId;
        this.orderType = orderType;
        this.totalItems = totalItems;
        this.totalPrice = totalPrice;
        this.deliveryCharge = deliveryCharge;
        this.branchId = branchId;
        this.remark = remark;
        this.orderStatus = orderStatus;
        this.cartItemList = cartItemList;
        this.branchName = branchName;
        this.transDate = transDate;
        this.scheduleDate = scheduleDate;
        this.conversionRate = conversionRate;
        this.defaultCurrency = defaultCurrency;
        this.priceListCurrency = priceListCurrency;
    }

    public OrderHistory() {
    }

    public String getDefaultCurrency() {
        return defaultCurrency;
    }

    public String getPriceListCurrency() {
        return priceListCurrency;
    }

    public double getConversionRate() {
        return conversionRate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getBranchName() {
        return branchName;
    }


    public int getIsNew() {
        return isNew;
    }

    public void setIsNew(int isNew) {
        this.isNew = isNew;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getOrderType() {
        return orderType;
    }


    public int getTotalItems() {
        return totalItems;
    }


    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getDeliveryCharge() {
        return deliveryCharge;
    }


    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getCartItemList() {
        return cartItemList;
    }


    public String getTransDate() {
        return transDate;
    }


    public String getScheduleDate() {
        return scheduleDate;
    }


    @Override
    public String toString() {
        return "OrderHistory{" +
                "Id=" + orderId +
                ",defaultCurrency='" + defaultCurrency + '\'' +
                ", priceListCurrency='" + priceListCurrency + '\'' +
                ", totalPrice=" + totalPrice +
                ", deliveryCharge=" + deliveryCharge +
                ", conversionRate=" + conversionRate +
                '}';
    }
}
