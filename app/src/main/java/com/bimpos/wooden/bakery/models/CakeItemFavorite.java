package com.bimpos.wooden.bakery.models;

public class CakeItemFavorite {
    private final int cakeGalleryId;
    private final int cakeId;
    private int price;
    private final int minOrderQty;
    private String title;
    private String description;
    private String picPath;
    private final String refNum;
    private byte[] image;


    public CakeItemFavorite(int galleryId, int galleryPhotoId, int price, int minOrderQty, String galleryTitle, String description, String picPath, String refNum, byte[] image) {
        this.cakeGalleryId = galleryId;
        this.refNum = refNum;
        this.cakeId = galleryPhotoId;
        this.price = price;
        this.minOrderQty = minOrderQty;
        this.title = galleryTitle;
        this.description = description;
        this.picPath = picPath;
        this.image = image;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getRefNum() {
        return refNum;
    }

    public int getCakeGalleryId() {
        return cakeGalleryId;
    }

    public int getCakeId() {
        return cakeId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getMinOrderQty() {
        return minOrderQty;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

}
