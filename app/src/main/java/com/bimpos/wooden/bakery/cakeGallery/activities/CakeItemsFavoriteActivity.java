package com.bimpos.wooden.bakery.cakeGallery.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.cakeGallery.adapters.CakeItemFavoriteRvAdapter;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityCakeGalleryBinding;
import com.bimpos.wooden.bakery.models.CakeItemFavorite;

import java.util.List;

public class CakeItemsFavoriteActivity extends AppCompatActivity implements CakeItemFavoriteRvAdapter.OnEmptyFavoriteList {

    private ActivityCakeGalleryBinding binding;
    private List<CakeItemFavorite> cakeItemFavoriteList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCakeGalleryBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initFavoriteRecyclerView();
        initClickListeners();

    }

    private void initFavoriteRecyclerView() {
        binding.recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        CakeItemFavoriteRvAdapter adapter = new CakeItemFavoriteRvAdapter(cakeItemFavoriteList, this,this);
        binding.recyclerView.setAdapter(adapter);
    }

    private void initClickListeners() {
        binding.back.setOnClickListener(v -> finish());
    }

    private void initVariables() {
        cakeItemFavoriteList = MainApplication.getDatabase().getFavoriteCakes();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onEmptyFavoriteList() {
        finish();
    }
}