package com.bimpos.wooden.bakery.product_catalog.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.models.Categories;
import com.bimpos.wooden.bakery.product_catalog.activities.ProductsCatalogActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.squareup.picasso.Picasso;

import java.util.List;


public class CategoriesCatalogRvAdapter extends RecyclerView.Adapter<CategoriesCatalogRvAdapter.ViewHolder> {

    private final List<Categories> categoriesList;
    private final Context context;
    private final Activity activity;
    private long mLastClick=0;

    public CategoriesCatalogRvAdapter(List<Categories> categoriesList, Context context, Activity activity) {
        this.categoriesList = categoriesList;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_categories_catalog, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Categories category = categoriesList.get(position);
        holder.categoryDescription.setText(category.getDescription());
        Glide.with(context)
                .load(category.getPicpath())
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(R.drawable.category_place_holder)
                .into(holder.categoryImage);

        holder.itemView.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            Intent intent = new Intent(context, ProductsCatalogActivity.class);
            intent.putExtra("catId", category.getCatId());
            intent.putExtra("catName", category.getDescription());
            context.startActivity(intent);
            activity.overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView categoryImage;
        TextView categoryDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryImage = itemView.findViewById(R.id.recycler_categories_catalog_image);
            categoryDescription = itemView.findViewById(R.id.recycler_categories_catalog_catDescription);
        }
    }
}
