package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.Cart;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.models.Categories;
import com.bimpos.wooden.bakery.models.ComboHeader;
import com.bimpos.wooden.bakery.models.ComboModifier;
import com.bimpos.wooden.bakery.models.Products;
import com.bimpos.wooden.bakery.models.QuestionHeader;
import com.bimpos.wooden.bakery.models.QuestionModifier;
import com.bimpos.wooden.bakery.orderOnline.activities.OrdersActivity;
import com.bimpos.wooden.bakery.orderOnline.activities.ProductInfoActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.dk.animation.circle.CircleAnimationUtil;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.List;

public class ProductsRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>

        implements Filterable {

    private final Context context;
    private static final String TAG = "ProductsRvAdapter";
    private List<Object> objectList;
    public List<Object> objectFilteredList;
    private int oldSeq = -1;
    private final Cart cartData;
    private final OrdersActivity ordersActivity;
    private final int HEADER_VIEW_HOLDER = 0;
    private final int PRODUCT_VIEW_HOLDER = 1;
    private final int HEADER_PLACEHOLDER_VIEW_HOLDER = 2;
    private final int PRODUCT_PLACEHOLDER_VIEW_HOLDER = 3;
    public boolean checkCount = true;
    public boolean scrollCategories = true;
    private BottomSheetBehavior<View> miniCartBehaviour;
    private RecyclerView miniCartRecyclerView;
    private MiniCartOrderRvAdapter miniCartAdapter;
    List<CartProduct> cartProductList;
    public int clickedPosition;
    private long mLastClick = 0;
    private final DatabaseHelper databaseHelper;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();
                Log.d(TAG, "performFiltering: " + charString);

                if (charString.isEmpty()) {
                    objectFilteredList = objectList;
                } else {
                    List<Object> newFilteredList = new ArrayList<>();
                    for (Object object : objectList) {
                        if (object instanceof Products) {
                            Products product = (Products) object;
                            if (product.getDescription().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                newFilteredList.add(product);
                            }
                        }
                    }

                    objectFilteredList = newFilteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = objectFilteredList;
                return filterResults;
            }

            @SuppressLint("NotifyDataSetChanged")
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                objectFilteredList = (ArrayList<Object>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public ProductsRvAdapter(Context context, OrdersActivity ordersActivity) {
        this.context = context;
        Log.d(TAG, "ProductsRvAdapter: start");
        this.ordersActivity = ordersActivity;
        cartData = Cart.getCartData();
        databaseHelper = MainApplication.getDatabase();
        initBottomSheet();
    }

    private void initBottomSheet() {
        RelativeLayout miniCartBottomSheet = ordersActivity.findViewById(R.id.include_mini_cart_bottomSheet);
        Button miniCartCancel = ordersActivity.findViewById(R.id.include_mini_cart_cancel);
        Button miniCartSave = ordersActivity.findViewById(R.id.include_mini_cart_save);

        miniCartRecyclerView = ordersActivity.findViewById(R.id.include_mini_cart_recyclerView);
        miniCartRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        miniCartRecyclerView.setHasFixedSize(false);
        miniCartBehaviour = BottomSheetBehavior.from(miniCartBottomSheet);
        miniCartBehaviour.setFitToContents(true);
        miniCartBehaviour.setSkipCollapsed(false);
        miniCartBehaviour.setHideable(true);
        miniCartBehaviour.setDraggable(false);

        miniCartCancel.setOnClickListener(v -> hideMiniCart());

        miniCartSave.setOnClickListener(v -> {
            miniCartAdapter.modifyCart();
            hideMiniCart();
            updateCartCount();
            notifyItemChanged(clickedPosition);
        });

    }

    @Override
    public int getItemViewType(int position) {
        Log.d(TAG, "getItemViewType: start");
        if (objectFilteredList == null) {
            if (position == 0) {
                return HEADER_PLACEHOLDER_VIEW_HOLDER;
            }
            return PRODUCT_PLACEHOLDER_VIEW_HOLDER;
        } else {
            if (objectFilteredList.size() != 0) {
                Object object = objectFilteredList.get(position);
                if (object instanceof Products) {
                    return PRODUCT_VIEW_HOLDER;
                } else {
                    return HEADER_VIEW_HOLDER;
                }
            }
            return 4;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == PRODUCT_PLACEHOLDER_VIEW_HOLDER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_product_holder, parent, false);
            return new ProductHolderViewHolder(view);


        } else if (viewType == HEADER_PLACEHOLDER_VIEW_HOLDER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_product_header_holder, parent, false);
            return new HeaderViewHolder(view);


        } else if (viewType == HEADER_VIEW_HOLDER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_product_header, parent, false);
            return new HeaderViewHolder(view);


        } else if (viewType == PRODUCT_VIEW_HOLDER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_product_content, parent, false);
            return new ProductsViewHolder(view);

        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_no_result_found, parent, false);
            return new NoProductsViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {
        if (objectFilteredList != null) {

            if (objectFilteredList.size() != 0) {
                Object object = objectFilteredList.get(position);
                if (holder1 instanceof ProductsViewHolder) {

                    Products product = (Products) object;
                    ProductsViewHolder holder = (ProductsViewHolder) holder1;

                    holder.price.setText(FormatPrice.getFormattedPrice(product.getFinalPrice()));
                    holder.description.setText(product.getDescription());
                    holder.info.setText(product.getProdInfo());

                    Log.d(TAG, "onBindViewHolder: product pictiure " + product.getProduct_picture());

                    Glide.with(context)
                            .load(product.getProduct_picture())
                            .transition(DrawableTransitionOptions.withCrossFade())
                            .placeholder(R.drawable.category_place_holder)
                            .into(holder.productImage);

                    if (scrollCategories) {
                        if (oldSeq != product.getSeq()) {
                            oldSeq = product.getSeq();
                            ordersActivity.changeSelectedCategory(oldSeq);
                        }
                    }

                    int productCount = cartData.getProductCount(product.getProdNum());
                    if (productCount == 0) {
                        holder.btnMinus.setVisibility(View.INVISIBLE);
                        holder.foregroundLayout.setVisibility(View.INVISIBLE);
                        holder.endCountText.setText(R.string.add);
                    } else {
                        holder.btnMinus.setVisibility(View.VISIBLE);
                        holder.foregroundLayout.setVisibility(View.VISIBLE);
                        holder.endCountText.setText(String.valueOf(productCount));
                    }

                    holder.btnPlus.setOnClickListener(view -> {
                        if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                            return;
                        }
                        mLastClick = SystemClock.elapsedRealtime();

                        if (miniCartBehaviour.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                            hideMiniCart();
                        } else {
                            hideMiniCart();

                            int qty = cartData.getProductCount(product.getProdNum());
                            if (qty < 999) {
                                List<QuestionModifier> questionModifierList = new ArrayList<>();
                                List<ComboModifier> comboModifierList = new ArrayList<>();

                                if (product.getModifiersGroupId() > 0 || product.getComboGroupId() > 0) {
                                    if (product.getModifiersGroupId() > 0) {
                                        List<QuestionHeader> questionHeaderList = databaseHelper.getQuestionHeaderByQgId(product.getModifiersGroupId());
                                        for (int i = 0; i < questionHeaderList.size(); i++) {
                                            QuestionHeader questionHeader = questionHeaderList.get(i);
                                            if (questionHeader.getRequired() == 1) {
                                                questionModifierList.addAll(databaseHelper.getQuestionModifierByQhId(questionHeader.getQuestionGroupId(), questionHeader.getQuestionHeaderId()));
                                            }
                                        }
                                    }
                                    if (product.getComboGroupId() > 0) {
                                        List<ComboHeader> comboHeaderList = databaseHelper.getComboHeaderByGroupId(product.getComboGroupId());
                                        for (int i = 0; i < comboHeaderList.size(); i++) {
                                            ComboHeader comboHeader = comboHeaderList.get(i);
                                            comboModifierList.addAll(databaseHelper.getComboModifierByQuestionHeaderId(comboHeader.getComboGroupId(), comboHeader.getComboHeaderId()));
                                        }
                                    }

                                    if (questionModifierList.size() == 0 && comboModifierList.size() == 0) {
                                        addProduct(holder, product);
                                    } else {
                                        gotoProductInfoActivity(product, holder, holder.getAdapterPosition());
                                    }

                                } else {
                                    addProduct(holder, product);
                                }
                            }
                        }
                    });

                    holder.btnMinus.setOnClickListener(view -> {
                        if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                            return;
                        }
                        mLastClick = SystemClock.elapsedRealtime();
                        if (miniCartBehaviour.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                            hideMiniCart();
                        } else {

                            int qty = cartData.getProductCountInCart(product.getProdNum());
                            if (qty > 1) {
                                clickedPosition = holder.getAdapterPosition();
                                showBottomSheetBehaviour(product.getProdNum());
                            } else {
                                removeProduct(holder, product);
                            }
                        }
                    });

                    holder.plusPlus.setOnClickListener(view -> {
                        if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                            return;
                        }
                        mLastClick = SystemClock.elapsedRealtime();

                        if (miniCartBehaviour.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                            hideMiniCart();
                        } else {
                            hideMiniCart();

                            int qty = cartData.getProductCount(product.getProdNum());
                            if (qty < 999) {
                                List<QuestionModifier> questionModifierList = new ArrayList<>();
                                List<ComboModifier> comboModifierList = new ArrayList<>();

                                if (product.getModifiersGroupId() > 0 || product.getComboGroupId() > 0) {
                                    if (product.getModifiersGroupId() > 0) {
                                        List<QuestionHeader> questionHeaderList = databaseHelper.getQuestionHeaderByQgId(product.getModifiersGroupId());
                                        for (int i = 0; i < questionHeaderList.size(); i++) {
                                            QuestionHeader questionHeader = questionHeaderList.get(i);
                                            if (questionHeader.getRequired() == 1) {
                                                questionModifierList.addAll(databaseHelper.getQuestionModifierByQhId(questionHeader.getQuestionGroupId(), questionHeader.getQuestionHeaderId()));
                                            }
                                        }
                                    }
                                    if (product.getComboGroupId() > 0) {
                                        List<ComboHeader> comboHeaderList = databaseHelper.getComboHeaderByGroupId(product.getComboGroupId());
                                        for (int i = 0; i < comboHeaderList.size(); i++) {
                                            ComboHeader comboHeader = comboHeaderList.get(i);
                                            comboModifierList.addAll(databaseHelper.getComboModifierByQuestionHeaderId(comboHeader.getComboGroupId(), comboHeader.getComboHeaderId()));
                                        }
                                    }

                                    if (questionModifierList.size() == 0 && comboModifierList.size() == 0) {
                                        addProduct(holder, product);
                                    } else {
                                        gotoProductInfoActivity(product, holder, holder.getAdapterPosition());
                                    }

                                } else {
                                    addProduct(holder, product);
                                }
                            }

                        }
                    });

                    holder.minusMinus.setOnClickListener(view -> {
                        if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                            return;
                        }
                        mLastClick = SystemClock.elapsedRealtime();
                        if (miniCartBehaviour.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                            hideMiniCart();
                        } else {

                            int qty = cartData.getProductCountInCart(product.getProdNum());
                            if (qty > 1) {
                                clickedPosition = holder.getAdapterPosition();
                                showBottomSheetBehaviour(product.getProdNum());
                            } else {
                                removeProduct(holder, product);
                            }
                        }
                    });

                    holder.clickArea.setOnClickListener(v -> {

                        if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                            return;
                        }
                        mLastClick = SystemClock.elapsedRealtime();

                        if (miniCartBehaviour.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                            hideMiniCart();
                        } else {
                            hideMiniCart();

                            int qty = cartData.getProductCount(product.getProdNum());
                            if (qty < 999) {
                                List<QuestionModifier> questionModifierList = new ArrayList<>();
                                List<ComboModifier> comboModifierList = new ArrayList<>();

                                if (product.getModifiersGroupId() > 0 || product.getComboGroupId() > 0) {
                                    if (product.getModifiersGroupId() > 0) {
                                        List<QuestionHeader> questionHeaderList = databaseHelper.getQuestionHeaderByQgId(product.getModifiersGroupId());
                                        for (int i = 0; i < questionHeaderList.size(); i++) {
                                            QuestionHeader questionHeader = questionHeaderList.get(i);
                                            if (questionHeader.getRequired() == 1) {
                                                questionModifierList.addAll(databaseHelper.getQuestionModifierByQhId(questionHeader.getQuestionGroupId(), questionHeader.getQuestionHeaderId()));
                                            }
                                        }
                                    }
                                    if (product.getComboGroupId() > 0) {
                                        List<ComboHeader> comboHeaderList = databaseHelper.getComboHeaderByGroupId(product.getComboGroupId());
                                        for (int i = 0; i < comboHeaderList.size(); i++) {
                                            ComboHeader comboHeader = comboHeaderList.get(i);
                                            comboModifierList.addAll(databaseHelper.getComboModifierByQuestionHeaderId(comboHeader.getComboGroupId(), comboHeader.getComboHeaderId()));
                                        }
                                    }

                                    if (questionModifierList.size() == 0 && comboModifierList.size() == 0) {
                                        addProduct(holder, product);
                                    } else {
                                        gotoProductInfoActivity(product, holder, holder.getAdapterPosition());
                                    }

                                } else {
                                    addProduct(holder, product);
                                }
                            }
                        }

                    });

                    holder.clickArea.setOnLongClickListener(v -> {
                        if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                            return false;
                        }
                        mLastClick = SystemClock.elapsedRealtime();

                        if (miniCartBehaviour.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                            hideMiniCart();
                        } else {
                            hideMiniCart();

                            int qty = cartData.getProductCount(product.getProdNum());
                            if (qty < 999) {
                                List<QuestionModifier> questionModifierList = new ArrayList<>();
                                List<ComboModifier> comboModifierList = new ArrayList<>();

                                if (product.getModifiersGroupId() > 0 || product.getComboGroupId() > 0) {
                                    if (product.getModifiersGroupId() > 0) {
                                        List<QuestionHeader> questionHeaderList = databaseHelper.getQuestionHeaderByQgId(product.getModifiersGroupId());
                                        for (int i = 0; i < questionHeaderList.size(); i++) {
                                            QuestionHeader questionHeader = questionHeaderList.get(i);
                                            if (questionHeader.getRequired() == 1) {
                                                questionModifierList.addAll(databaseHelper.getQuestionModifierByQhId(questionHeader.getQuestionGroupId(), questionHeader.getQuestionHeaderId()));
                                            }
                                        }
                                    }
                                    if (product.getComboGroupId() > 0) {
                                        List<ComboHeader> comboHeaderList = databaseHelper.getComboHeaderByGroupId(product.getComboGroupId());
                                        for (int i = 0; i < comboHeaderList.size(); i++) {
                                            ComboHeader comboHeader = comboHeaderList.get(i);
                                            comboModifierList.addAll(databaseHelper.getComboModifierByQuestionHeaderId(comboHeader.getComboGroupId(), comboHeader.getComboHeaderId()));
                                        }
                                    }

                                    if (questionModifierList.size() == 0 && comboModifierList.size() == 0) {
                                        addProduct(holder, product);
                                    } else {
                                        gotoProductInfoActivity(product, holder, holder.getAdapterPosition());
                                    }

                                } else {
                                    addProduct(holder, product);
                                }
                            }

                        }
                        return true;
                    });


                } else {
                    HeaderViewHolder holder = (HeaderViewHolder) holder1;
                    Categories categories = (Categories) object;
                    holder.catName.setText(categories.getDescription());
                }
            }
        }
    }


    public void showBottomSheetBehaviour(int prodNum) {
        Log.d(TAG, "showBottomSheetBehaviour: start");
        cartProductList = new ArrayList<>();
        cartProductList = cartData.getCartByProductId(prodNum);
        miniCartAdapter = new MiniCartOrderRvAdapter(cartProductList, context);
        miniCartAdapter.setData(cartProductList);
        miniCartRecyclerView.setAdapter(miniCartAdapter);
        miniCartBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
        ordersActivity.binding.activityOrdersMainLayout.animate().alpha(0.5f).setDuration(200).start();

    }

    public void hideMiniCart() {
        Log.d(TAG, "hideMiniCart: " + miniCartBehaviour.getState());
        if (miniCartBehaviour.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            miniCartBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
            ordersActivity.binding.activityOrdersMainLayout.animate().alpha(1).setDuration(200).start();
        }
    }

    public void gotoProductInfoActivity(Products product, ProductsViewHolder holder,
                                        int position) {
        Intent intent = new Intent(context, ProductInfoActivity.class);
        intent.putExtra(Products.class.getSimpleName(), product);
        ordersActivity.position = position;
        ordersActivity.finalViewHolder = holder;
//        ordersActivity.someActivityResultLauncher.launch(intent);
//        ordersActivity.overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);

        Pair<View, String> p2 = Pair.create(holder.plusLayout, "plusLayout");
        Pair<View, String> p3 = Pair.create(holder.productImage, "productImageHolder");
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(ordersActivity, p2, p3);
        ordersActivity.someActivityResultLauncher.launch(intent, options);
    }

    private void addProduct(ProductsViewHolder holder, Products product) {
        Log.d(TAG, "addProduct: start");
        cartData.addProduct(product, "", 1);
        startAnimation(holder, product);
        updateCartCount();
    }

    private void updateCartCount() {
        int cartCount = cartData.getCartCount();
        double price = cartData.getCartTotalPrice();
        Log.d(TAG, "updateCartCount: cart count " + cartCount);
        if (cartCount > 0) {
            ordersActivity.binding.priceLayout.setEnabled(true);
            ordersActivity.binding.priceLayout.setClickable(true);
            ordersActivity.binding.totalItems.setText(String.valueOf(cartCount));
            ordersActivity.binding.totalPrice.setText(FormatPrice.getFormattedPrice(price));
//            ordersActivity.binding.priceLayout.animate().scaleX(1).setDuration(200).start();
            ordersActivity.binding.priceLayout.setVisibility(View.VISIBLE);
        } else if (cartCount == 0) {

            ordersActivity.binding.priceLayout.setEnabled(false);
            ordersActivity.binding.priceLayout.setClickable(false);
            cartData.clear();
            ordersActivity.binding.totalItems.setText(String.valueOf(0));
            ordersActivity.binding.priceLayout.setVisibility(View.GONE);
//            ordersActivity.binding.priceLayout.animate().scaleX(0).setDuration(200).start();
        }
    }

    @SuppressLint("SetTextI18n")
    private void removeProduct(ProductsViewHolder holder, Products products) {
        cartData.removeProduct(products);
        int count = cartData.getProductCount(products.getProdNum());
        if (count == 0) {
            holder.btnMinus.setVisibility(View.INVISIBLE);
            holder.endCountText.setText("Add");
        } else {
            holder.endCountText.setText(String.valueOf(count));
        }
        startFadeAnimation(holder);
        updateCartCount();
    }

    private void startAnimation(ProductsViewHolder holder, Products products) {
        holder.countAnimationText.setText("+1");
        holder.btnMinus.setVisibility(View.VISIBLE);
        holder.foregroundLayout.setVisibility(View.VISIBLE);
        holder.endCountText.setText(String.valueOf(cartData.getProductCount(products.getProdNum())));
        new CircleAnimationUtil().attachActivity((Activity) context).
                setTargetView(holder.foregroundLayout).
                setMoveDuration(300).
                setBorderColor(Color.TRANSPARENT).
                setCircleDuration(500).
                setDestView(ordersActivity.binding.hiddenLayout).
                startAnimation();

        checkCount = true;
    }

    private void startFadeAnimation(ProductsViewHolder holder) {
        holder.countAnimationText.setText("-1");
        holder.btnMinus.setVisibility(View.VISIBLE);
        holder.foregroundLayout.setVisibility(View.VISIBLE);
        new CircleAnimationUtil().attachActivity((Activity) context).
                setTargetView(ordersActivity.binding.hiddenLayout).
                setMoveDuration(300).
                setBorderColor(Color.TRANSPARENT).
                setCircleDuration(500).
                setDestView(holder.foregroundLayout).
                startAnimation();

        checkCount = true;
    }

    public void animateProduct(ProductsViewHolder holder, int position) {
        checkCount = false;
        Products products = (Products) objectFilteredList.get(position);
        startAnimation(holder, products);
    }

    public void animateFadeProduct(ProductsViewHolder holder, int position) {
        checkCount = false;
        startFadeAnimation(holder);
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Object> newData) {
        Log.d(TAG, "setData: start");
        updateCartCount();
        objectList = newData;
        objectFilteredList = newData;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        if (objectFilteredList != null && objectFilteredList.size() != 0) {
            return objectFilteredList.size();
        }
        return 9;
    }


    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView catName;

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            catName = itemView.findViewById(R.id.recycler_product_header_catName);
        }
    }

    public static class ProductHolderViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout mainLayout;

        public ProductHolderViewHolder(@NonNull View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.recycler_product_holder);
        }
    }

    public static class ProductsViewHolder extends RecyclerView.ViewHolder {
        TextView description, info, endCountText, price, swipeText, countAnimationText;
        ImageView btnPlus, btnMinus;
        RelativeLayout foregroundLayout, clickArea;
        LinearLayout mainLayout, plusPlus, minusMinus;
        ImageView productImage;
        CardView plusLayout;

        public ProductsViewHolder(@NonNull View itemView) {
            super(itemView);
            plusLayout = itemView.findViewById(R.id.recycler_product_content_plusLayout);
            description = itemView.findViewById(R.id.recycler_order_products_title);
            info = itemView.findViewById(R.id.recycler_order_products_info);
            btnPlus = itemView.findViewById(R.id.recycler_order_products_plus);
            btnMinus = itemView.findViewById(R.id.recycler_order_products_minus);
            foregroundLayout = itemView.findViewById(R.id.recycler_order_products_foregroundLayout);
            endCountText = itemView.findViewById(R.id.recycler_order_products_endCountText);
            mainLayout = itemView.findViewById(R.id.recycler_order_products_mainLayout);
            price = itemView.findViewById(R.id.recycler_order_products_price);
            swipeText = itemView.findViewById(R.id.recycler_order_products_swipeText);
            clickArea = itemView.findViewById(R.id.recycler_order_products_clickArea);
            productImage = itemView.findViewById(R.id.recycler_product_content_productImage);
            plusPlus = itemView.findViewById(R.id.recycler_order_products_plusPlus);
            minusMinus = itemView.findViewById(R.id.recycler_order_products_minusMinus);
            countAnimationText = itemView.findViewById(R.id.recycler_product_content_countAnimationText);
        }
    }

    public static class NoProductsViewHolder extends RecyclerView.ViewHolder {

        public NoProductsViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
