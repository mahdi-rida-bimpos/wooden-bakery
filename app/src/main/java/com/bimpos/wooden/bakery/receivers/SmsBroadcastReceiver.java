package com.bimpos.wooden.bakery.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;


public class SmsBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "SmsBroadcastReceiver";
    public static final String pdu_type = "pdus";
    private OnMessageReceived listener;

    public interface OnMessageReceived {
        void onMessageReceived(String message);
    }

    public SmsBroadcastReceiver(OnMessageReceived listener) {
        Log.d(TAG, "SmsBroadcastReceiver: start");
        this.listener = listener;
    }

    public SmsBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: receive");

        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs;
        String strMessage = "";
        String format = bundle.getString("format");

        Object[] pdus = (Object[]) bundle.get(pdu_type);
        if (pdus != null) {
            boolean isVersionM = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);
            msgs = new SmsMessage[pdus.length];
            for (int i = 0; i < msgs.length; i++) {
                if (isVersionM) {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                } else {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }
                strMessage += msgs[i].getMessageBody();
               String code =  strMessage.substring(strMessage.length()-5);
                if (listener != null) {
                    listener.onMessageReceived(code);
                }
            }
        }
    }
}
