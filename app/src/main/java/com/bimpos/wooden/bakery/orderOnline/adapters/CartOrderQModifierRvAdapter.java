package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.models.CartQuestionModifier;

import java.util.List;

public class CartOrderQModifierRvAdapter extends RecyclerView.Adapter<CartOrderQModifierRvAdapter.ViewHolder> {

    private final List<CartQuestionModifier> cartQuestionModifierList;
    private final CartProduct cartProduct;

    public CartOrderQModifierRvAdapter(CartProduct cartProduct,List<CartQuestionModifier> cartQuestionModifierList) {
        this.cartQuestionModifierList = cartQuestionModifierList;
        this.cartProduct = cartProduct;
    }

    @NonNull
    @Override
    public CartOrderQModifierRvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cart_order_question_modifier, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartOrderQModifierRvAdapter.ViewHolder holder, int position) {
        CartQuestionModifier cartQuestionModifier = cartQuestionModifierList.get(position);
        holder.modifierDescription.setText(cartQuestionModifier.getDescription());
        if (cartQuestionModifier.getPrice() > 0) {
            holder.price.setText(String.format("+ %s", FormatPrice.getFormattedPrice(cartQuestionModifier.getPrice()*cartProduct.getQty())));
        } else {
            holder.price.setText("");
        }
        holder.qty.setText(String.valueOf(cartProduct.getQty()));
    }

    @Override
    public int getItemCount() {
        return cartQuestionModifierList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView modifierDescription, price, qty;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            price = itemView.findViewById(R.id.recycler_cart_order_question_modifier_price);
            modifierDescription = itemView.findViewById(R.id.recycler_cart_order_question_modifier_description);
            qty = itemView.findViewById(R.id.recycler_cart_order_question_modifier_count);
        }
    }
}
