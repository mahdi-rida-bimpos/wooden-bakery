package com.bimpos.wooden.bakery.branches.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.branches.adapters.BranchesRvAdapter;
import com.bimpos.wooden.bakery.databinding.ActivityBranchesBinding;
import com.bimpos.wooden.bakery.models.Branches;

import java.util.List;
import java.util.Objects;

public class BranchesActivity extends AppCompatActivity {

    private ActivityBranchesBinding binding;
    private List<Branches> branchList;
    private BranchesRvAdapter adapter;
    private InputMethodManager imm;

    private static final String TAG = "BranchesActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBranchesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initRecyclerView();
        initClickListener();
        initSearchView();
    }
    private void initSearchView() {
        binding.searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed: start");
        if (Objects.requireNonNull(binding.searchText.getText()).toString().length() != 0 || binding.searchText.isFocused()) {
            Log.d(TAG, "onBackPressed: focused");
            binding.searchText.setText("");
            binding.searchText.clearFocus();
            imm.hideSoftInputFromWindow(binding.searchText.getWindowToken(), 0);
            return;
        }
        finish();
    }

    private void initVariables() {
        imm = MainApplication.getImm();
        branchList = MainApplication.getDatabase().getBranchesList();
    }

    private void initRecyclerView() {
        adapter = new BranchesRvAdapter(branchList, this, this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);

        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                binding.searchText.clearFocus();
                imm.hideSoftInputFromWindow(binding.searchText.getWindowToken(), 0);
            }
        });
    }

    private void initClickListener() {
        binding.back.setOnClickListener(view -> onBackPressed());
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}