package com.bimpos.wooden.bakery.tables;

public class CountriesTable {

    public static final String TABLE_NAME= "countries";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String COUNTRY_ID = "countryId";
        public static final String NAME = "name";
        public static final String REGION_CODE = "regionCode";
        public static final String COUNTRY_CODE = "countryCode";
        public static final String ZIP_CODE = "zipCode";
        public static final String IS_ACTIVE = "isActive";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
