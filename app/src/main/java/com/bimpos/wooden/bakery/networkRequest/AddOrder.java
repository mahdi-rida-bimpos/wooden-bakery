package com.bimpos.wooden.bakery.networkRequest;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.helpers.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AddOrder extends AsyncTask<String, String, String> {

    private static final String TAG = "AddOrder";
    HttpURLConnection conn;
    private final SharedPreferences prefs;
    private final String data;
    private final OnOrderComplete listener;
    URL url = null;

    public interface OnOrderComplete {
        void onOrderComplete(int status, String result);
    }

    public AddOrder(String data, OnOrderComplete listener) {
        prefs = MainApplication.getPreferences();
        this.data = data;
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... params) {

        try {
            Log.d(TAG, "doInBackground: add order start");

            url = new URL(Constant.BASE_URL + "orders?data=" + data);
            Log.d(TAG, "doInBackground: url " + url);
            conn = (HttpURLConnection) url.openConnection();
            String token = prefs.getString(Constant.TOKEN, "");
            Log.d(TAG, "doInBackground: token is " + token);
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestMethod("POST");
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(10000);
            conn.connect();
            int response_code = conn.getResponseCode();
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result " + result.toString());
                return (result.toString());
            } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                return ("token");
            } else {
                return ("notFound");
            }

        } catch (Exception e) {
            try {
                Log.d(TAG, "doInBackground: error " + e.getMessage() + ", " + conn.getResponseMessage());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, "onPostExecute: result " + result);
        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("notFound")) {
            listener.onOrderComplete(0, result);
        } else if (result.equalsIgnoreCase("token")) {
            listener.onOrderComplete(-1, result);
        } else {
            try {
                JSONObject parentObject = new JSONObject(result);
                String status = parentObject.getString("STATUS");
                if (status.equalsIgnoreCase("success")) {
                    listener.onOrderComplete(1, result);
                } else if (status.equalsIgnoreCase("fail")) {
                    listener.onOrderComplete(0, result);
                }
            } catch (JSONException e) {
                listener.onOrderComplete(0, result);
            }
        }

    }
}
