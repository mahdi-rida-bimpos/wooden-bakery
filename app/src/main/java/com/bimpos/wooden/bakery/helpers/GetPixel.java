package com.bimpos.wooden.bakery.helpers;

import android.content.Context;

public class GetPixel {

    public static int getPixel(Context context, int x) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (x * scale + 0.5f);
    }
}
