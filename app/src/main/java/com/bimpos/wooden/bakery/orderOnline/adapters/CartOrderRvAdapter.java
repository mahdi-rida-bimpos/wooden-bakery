package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.Cart;
import com.bimpos.wooden.bakery.models.CartComboHeader;
import com.bimpos.wooden.bakery.models.CartComboModifier;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.models.CartQuestionHeader;
import com.bimpos.wooden.bakery.models.CartQuestionModifier;
import com.bimpos.wooden.bakery.orderOnline.activities.CartOrderActivity;

import java.util.ArrayList;
import java.util.List;

public class CartOrderRvAdapter extends RecyclerView.Adapter<CartOrderRvAdapter.ViewHolder> implements CustomDialog.DialogEvents {

    private final List<CartProduct> cartItemsList;
    private final Context context;
    private final Cart cartData;
    private final OnEmptyCard listener;
    private final CustomDialog dialog;
    private final CartOrderActivity activity;
    private long mLastClick = 0;
    private final int DIALOG_REMOVE_ITEM = 1;
    private static final String TAG = "CartOrderRv";

    public interface OnEmptyCard {
        void onEmptyCard();

        void onPriceChanged();
    }

    public CartOrderRvAdapter(List<CartProduct> cartItemsList, Context context, OnEmptyCard listener, CartOrderActivity activity) {
        this.listener = listener;
        this.activity = activity;
        this.cartItemsList = cartItemsList;
        this.context = context;
        cartData = Cart.getCartData();
        dialog = new CustomDialog(this);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cart_order, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CartProduct items = cartItemsList.get(position);

        Log.d(TAG, "onBindViewHolder: item price " + items.getPrice());
        double price = items.getPrice();
        holder.productPrice.setText(FormatPrice.getFormattedPrice(price));
        holder.productDesc.setText(items.getProd_desc());

        if (items.getRemark().equalsIgnoreCase("")) {
            holder.remarkLayout.setVisibility(View.GONE);
        } else {
            holder.remarkText.setText(items.getRemark());
            holder.remarkLayout.setVisibility(View.VISIBLE);
        }

        String qtyText = items.getQty() + "";
        holder.productQty.setText(qtyText);

        double totalPrice = price * items.getQty();
        for (int i = 0; i < items.getQuestionList().size(); i++) {
            CartQuestionHeader cartQuestionHeader = items.getQuestionList().get(i);
            for (int j = 0; j < cartQuestionHeader.getCartQuestionModifiers().size(); j++) {
                CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(j);
                totalPrice += modifier.getPrice() * items.getQty();
            }
        }
        for (int i = 0; i < items.getComboList().size(); i++) {
            CartComboHeader cartComboHeader = items.getComboList().get(i);
            for (int j = 0; j < cartComboHeader.getCartComboModifiers().size(); j++) {
                CartComboModifier modifier = cartComboHeader.getCartComboModifiers().get(j);
                totalPrice += modifier.getPrice() * modifier.getCount();
            }
        }

        holder.totalPrice.setText(FormatPrice.getFormattedPrice(totalPrice));

        if (items.getQuestionList().size() != 0 || items.getComboList().size() != 0) {
            List<Object> objectList = new ArrayList<>();
            if (items.getComboList().size() != 0) {
                objectList.addAll(items.getComboList());
            }
            if (items.getQuestionList().size() != 0) {
                objectList.addAll(items.getQuestionList());
            }
            CartOrderRecyclerAdapter adapter = new CartOrderRecyclerAdapter(context, objectList, items);
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.recyclerView.setAdapter(adapter);
        } else {
            holder.recyclerView.setVisibility(View.GONE);
        }

        if (items.getIsRemoved() == 0) {
            holder.plus.setOnClickListener(view -> {
               if(items.getIsRemoved()==0){
                   if (SystemClock.elapsedRealtime() - mLastClick < 300) {
                       return;
                   }
                   mLastClick = SystemClock.elapsedRealtime();

                   int count = Integer.parseInt(holder.productQty.getText().toString());
                   if (count < 999) {
                       cartData.addItem(items.getCartId());
                       listener.onPriceChanged();
                       notifyDataSetChanged();
                   }
               }
            });

            holder.minus.setOnClickListener(view -> {
                if(items.getIsRemoved()==0){
                    if (SystemClock.elapsedRealtime() - mLastClick < 300) {
                        return;
                    }
                    mLastClick = SystemClock.elapsedRealtime();
                    int qty = cartData.getCartProductCount(items.getCartId());
                    if (qty == 1) {
                        showRemoveItemDialog(items);
                    } else {
                        cartData.removeItem(items.getCartId());
                    }
                    if (cartItemsList.size() == 0) {
                        listener.onEmptyCard();
                    } else {
                        listener.onPriceChanged();
                        notifyDataSetChanged();
                    }
                }
            });

            holder.mainLayout.setAlpha(1);
            holder.noLongerLayout.setVisibility(View.GONE);
        }
        else {
            holder.mainLayout.setAlpha(0.4f);
            holder.noLongerLayout.setVisibility(View.VISIBLE);
            holder.noLongerRemove.setOnClickListener(v -> {
                cartItemsList.remove(holder.getAdapterPosition());
                if (cartItemsList.size() == 0) {
                    listener.onEmptyCard();
                } else {
                    listener.onPriceChanged();
                    notifyDataSetChanged();
                }
            });
        }

    }

    private void showRemoveItemDialog(CartProduct items) {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Remove '" + items.getProd_desc() + "' from cart?");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "yes");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "no");
        arguments.putInt(CustomDialog.DIALOG_ID, DIALOG_REMOVE_ITEM);
        arguments.putInt("positiveColor", ContextCompat.getColor(context, R.color.pantoneRed));
        arguments.putSerializable(CartProduct.class.getSimpleName(), items);

        dialog.setArguments(arguments);
        dialog.show(activity.getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        if (dialogId == DIALOG_REMOVE_ITEM) {
            dialog.dismiss();
            CartProduct items = (CartProduct) args.getSerializable(CartProduct.class.getSimpleName());
            cartData.removeItem(items.getCartId());
            if (cartItemsList.size() == 0) {
                listener.onEmptyCard();
            } else {
                listener.onPriceChanged();
                notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        if (dialogId == DIALOG_REMOVE_ITEM) {
            dialog.dismiss();
        }
    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }

    @Override
    public int getItemCount() {
        return cartItemsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView plus, minus;
        RecyclerView recyclerView;
        LinearLayout remarkLayout, noLongerLayout;
        RelativeLayout mainLayout;
        Button noLongerRemove;
        TextView productDesc, productPrice, totalPrice, productQty, remarkText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            productPrice = itemView.findViewById(R.id.recycler_cart_order_productPrice);
            productDesc = itemView.findViewById(R.id.recycler_cart_order_productDescript);
            totalPrice = itemView.findViewById(R.id.recycler_cart_order_totalPrice);
            productQty = itemView.findViewById(R.id.recycler_cart_order_quantity);
            plus = itemView.findViewById(R.id.recycler_cart_order_plus);
            minus = itemView.findViewById(R.id.recycler_cart_order_minus);
            recyclerView = itemView.findViewById(R.id.recycler_cart_order_recyclerView);
            remarkLayout = itemView.findViewById(R.id.recycler_cart_order_remarkLayout);
            remarkText = itemView.findViewById(R.id.recycler_cart_order_remarkText);
            mainLayout = itemView.findViewById(R.id.recycler_cart_order_mainLayout);
            noLongerLayout = itemView.findViewById(R.id.recycler_cart_order_noLongerLayout);
            noLongerRemove = itemView.findViewById(R.id.recycler_cart_order_noLongerLayout_remove);
        }
    }
}
