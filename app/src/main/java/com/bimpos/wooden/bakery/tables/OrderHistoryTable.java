package com.bimpos.wooden.bakery.tables;

public class OrderHistoryTable {

    public static final String TABLE_NAME="orderHistory";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String IS_NEW = "isNew";
        public static final String TRANS_ID = "transId";
        public static final String TRANS_DATE = "transDate";
        public static final String SCHEDULE_DATE = "scheduleDate";
        public static final String TOTAL_ITEMS = "totalItems";
        public static final String TOTAL_PRICE = "totalPrice";
        public static final String CONVERSION_RATE = "conversionRate";
        public static final String DEFAULT_CURRENCY = "defaultCurrency";
        public static final String PRICE_LIST_CURRENCY = "priceListCurrency";
        public static final String BranchName = "address";
        public static final String REMARK = "remark";
        public static final String ORDER_TYPE = "orderType";
        public static final String ORDER_STATUS = "orderStatus";
        public static final String CART_DATA = "cartData";
        public static final String DELIVERY_CHARGE = "deliveryCharge";
        public static final String BRANCH_ID = "branchId";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
