package com.bimpos.wooden.bakery.networkRequest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.wooden.bakery.BuildConfig;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.models.Department;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SendFeedback extends AsyncTask<String, String, String> {

    private static final String TAG = "SendFeedback";
    HttpURLConnection conn;
    private final SharedPreferences prefs;
    private final Department department;
    private final OnFeedbackComplete listener;
    private final String message;
    URL url = null;

    public interface OnFeedbackComplete {
        void onFeedbackComplete(int status);
    }

    public SendFeedback(Department department, String message, OnFeedbackComplete listener) {
        prefs = MainApplication.getPreferences();
        this.department = department;
        this.listener = listener;
        this.message = message;
    }

    @Override
    protected String doInBackground(String... params) {

        try {

            url = new URL(Constant.BASE_URL + "feedbacks/" + department.getDepId()
                    + "?data={\"id\":" + department.getDepId()
                    + ", \"mobile\":\"" + department.getDepMobile() + "\""
                    + ", \"msg\": \"" + message + "\""
                    + ", \"type\": 1, \"isactive\": 1}");
            Log.d(TAG, "doInBackground: url " + url);
            conn = (HttpURLConnection) url.openConnection();
            String token = prefs.getString(Constant.TOKEN, "");
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestMethod("POST");
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(10000);
            conn.connect();
            int response_code = conn.getResponseCode();
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result " + result.toString());
                return (result.toString());
            } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                return ("token");
            } else {
                return ("notFound");
            }

        } catch (Exception e) {
            try {
                Log.d(TAG, "doInBackground: error " + e.getMessage() + ", " + conn.getResponseMessage());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, "onPostExecute: result " + result);
        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("notFound")) {
            listener.onFeedbackComplete(-2);
        } else if (result.equalsIgnoreCase("token")) {
            listener.onFeedbackComplete(-1);
        } else {
            try {
                JSONObject parentObject = new JSONObject(result);
                String status = parentObject.getString("STATUS");
                if (status.equalsIgnoreCase("success")) {
                    listener.onFeedbackComplete(1);
                } else {
                    listener.onFeedbackComplete(-2);
                }
            } catch (JSONException e) {
                listener.onFeedbackComplete(-2);
            }
        }

    }
}
