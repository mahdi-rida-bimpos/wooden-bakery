package com.bimpos.wooden.bakery.cakeGallery.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.cakeGallery.activities.CakeGalleryActivity;
import com.bimpos.wooden.bakery.cakeGallery.activities.CakeItemsActivity;
import com.bimpos.wooden.bakery.cakeGallery.activities.CakeItemsFavoriteActivity;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.models.CakeGallery;
import com.bimpos.wooden.bakery.models.CakeItem;

import java.util.List;

public class CakeGalleryRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CakeGallery> cakeGalleryList;
    private final Context context;
    private final CakeGalleryActivity activity;
    private final DatabaseHelper databaseHelper;
    private long mLastClick = 0;

    public CakeGalleryRvAdapter(List<CakeGallery> cakeGalleryList, Context context, CakeGalleryActivity activity) {
        this.cakeGalleryList = cakeGalleryList;
        this.context = context;
        this.activity = activity;
        databaseHelper = MainApplication.getDatabase();
    }

    @Override
    public int getItemViewType(int position) {
        if (cakeGalleryList.get(position).getGalleryId() == -1) {
            return 1;
        }
        return 2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == 1) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cake_favorite, parent, false);
            return new FavoriteViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cake_gallery, parent, false);
            return new GalleryViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {

        CakeGallery gallery = cakeGalleryList.get(position);

        if (holder1 instanceof GalleryViewHolder) {
            GalleryViewHolder holder = (GalleryViewHolder) holder1;
            holder.galleryName.setText(gallery.getGalleryName());
            List<CakeItem> cakeItemList = gallery.getGalleryPhotosList();
            int photoCount = cakeItemList.size();
            String galleryPhotoText = gallery.getGalleryName() + " (" + photoCount + ")";
            holder.galleryName.setText(galleryPhotoText);

            holder.itemView.setOnClickListener(v -> {
                if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                    return;
                }
                mLastClick = SystemClock.elapsedRealtime();
                Intent intent = new Intent(context, CakeItemsActivity.class);
                intent.putExtra("galleryId", gallery.getGalleryId());
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            });

        } else {
            FavoriteViewHolder holder = (FavoriteViewHolder) holder1;
            int favoriteCounts = databaseHelper.getFavoriteCakesCount();
            String favoriteText = "Favorites (" + favoriteCounts + ")";
            holder.favoriteText.setText(favoriteText);

            if (favoriteCounts > 0) {
                holder.itemView.setOnClickListener(v -> {
                    if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                        return;
                    }
                    mLastClick = SystemClock.elapsedRealtime();
                    Intent intent = new Intent(context, CakeItemsFavoriteActivity.class);
                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                });
            } else {
                holder.itemView.setOnClickListener(null);
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<CakeGallery> newData) {
        this.cakeGalleryList = newData;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return cakeGalleryList.size();
    }

    static class GalleryViewHolder extends RecyclerView.ViewHolder {
        TextView galleryName;
        ImageView imageView;

        public GalleryViewHolder(@NonNull View itemView) {
            super(itemView);
            galleryName = itemView.findViewById(R.id.recycler_cake_gallery_name);
            imageView = itemView.findViewById(R.id.recycler_cake_gallery_image);
        }
    }

    static class FavoriteViewHolder extends RecyclerView.ViewHolder {
        TextView favoriteText;


        public FavoriteViewHolder(@NonNull View itemView) {
            super(itemView);
            favoriteText = itemView.findViewById(R.id.recycler_cake_favorite_count);

        }
    }
}
