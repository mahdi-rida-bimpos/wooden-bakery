package com.bimpos.wooden.bakery.models;

public class QuestionGroup {

    private final int questionGroupId;
    private final String description;

    public QuestionGroup(int qGId, String description) {
        this.questionGroupId = qGId;
        this.description = description;
    }

    public int getQuestionGroupId() {
        return questionGroupId;
    }


    public String getDescription() {
        return description;
    }

}
