package com.bimpos.wooden.bakery.models;

public class ProductStock {

    private int prodNum, branchId, variation1, variation2, stock;
    private String updatedAt;

    public ProductStock() {
    }

    public ProductStock(int prodnum, int branchid, int variation1, int variation2, int stock, String stock_updated_at) {
        this.prodNum = prodnum;
        this.branchId = branchid;
        this.variation1 = variation1;
        this.variation2 = variation2;
        this.stock = stock;
        this.updatedAt = stock_updated_at;
    }

    public int getProdNum() {
        return prodNum;
    }


    public int getBranchId() {
        return branchId;
    }


    public int getVariation1() {
        return variation1;
    }


    public int getVariation2() {
        return variation2;
    }


    public int getStock() {
        return stock;
    }


    public String getUpdatedAt() {
        return updatedAt;
    }

}

