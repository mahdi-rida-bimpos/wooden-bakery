package com.bimpos.wooden.bakery.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.models.Address;
import com.bimpos.wooden.bakery.models.BranchCities;
import com.bimpos.wooden.bakery.models.Branches;
import com.bimpos.wooden.bakery.models.CakeItemFavorite;
import com.bimpos.wooden.bakery.models.Categories;
import com.bimpos.wooden.bakery.models.Cities;
import com.bimpos.wooden.bakery.models.ComboGroup;
import com.bimpos.wooden.bakery.models.ComboHeader;
import com.bimpos.wooden.bakery.models.ComboModifier;
import com.bimpos.wooden.bakery.models.CakeGallery;
import com.bimpos.wooden.bakery.models.CakeItem;
import com.bimpos.wooden.bakery.models.Member;
import com.bimpos.wooden.bakery.models.Order;
import com.bimpos.wooden.bakery.models.OrderHistory;
import com.bimpos.wooden.bakery.models.PriceList;
import com.bimpos.wooden.bakery.models.ProductStock;
import com.bimpos.wooden.bakery.models.Products;
import com.bimpos.wooden.bakery.models.QuestionGroup;
import com.bimpos.wooden.bakery.models.QuestionHeader;
import com.bimpos.wooden.bakery.models.QuestionModifier;
import com.bimpos.wooden.bakery.tables.AddressTable;
import com.bimpos.wooden.bakery.tables.BranchCitiesTable;
import com.bimpos.wooden.bakery.tables.BranchesTable;
import com.bimpos.wooden.bakery.tables.CakeItemFavoriteTable;
import com.bimpos.wooden.bakery.tables.CategoriesTable;
import com.bimpos.wooden.bakery.tables.CitiesTable;
import com.bimpos.wooden.bakery.tables.ComboGroupTable;
import com.bimpos.wooden.bakery.tables.ComboHeaderTable;
import com.bimpos.wooden.bakery.tables.ComboModifierTable;
import com.bimpos.wooden.bakery.tables.CountriesTable;
import com.bimpos.wooden.bakery.tables.CakeGalleryTable;
import com.bimpos.wooden.bakery.tables.CakeItemTable;
import com.bimpos.wooden.bakery.tables.MemberTable;
import com.bimpos.wooden.bakery.tables.OrderHistoryTable;
import com.bimpos.wooden.bakery.tables.OrderTable;
import com.bimpos.wooden.bakery.tables.ParentTable;
import com.bimpos.wooden.bakery.tables.PriceListTable;
import com.bimpos.wooden.bakery.tables.ProductStockTable;
import com.bimpos.wooden.bakery.tables.ProductsTable;
import com.bimpos.wooden.bakery.tables.QuestionHeaderTable;
import com.bimpos.wooden.bakery.tables.QuestionModifierTable;
import com.bimpos.wooden.bakery.tables.QuestionsGroupTable;
import com.bimpos.wooden.bakery.tables.RegionsTable;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper {
    private final AppDatabase database;
    private static final String TAG = "DatabaseHelper";
    Context context;

    public DatabaseHelper(Context context) {
        Log.d(TAG, "DatabaseHelper: start");
        this.context = context;
        database = AppDatabase.getInstance(context);
    }

    //#region Cleaners

    public void cleanTable(String TableName) {
        database.getWritableDatabase().execSQL(String.format("delete from %s;", TableName));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", TableName));
    }

    public void cleanAllTables() {
        database.getWritableDatabase().execSQL(String.format("delete from %s;", AddressTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", AddressTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", BranchesTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", BranchesTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", CategoriesTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", CategoriesTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", CitiesTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", CitiesTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", CountriesTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", CountriesTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", MemberTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", MemberTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", ParentTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", ParentTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", ProductsTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", ProductsTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", ProductStockTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", ProductStockTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", QuestionHeaderTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", QuestionHeaderTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", QuestionModifierTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", QuestionModifierTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", QuestionsGroupTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", QuestionsGroupTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", RegionsTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", RegionsTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", ComboGroupTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", ComboGroupTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", ComboHeaderTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", ComboHeaderTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", ComboModifierTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", ComboModifierTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", CitiesTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", CitiesTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", BranchCitiesTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", BranchCitiesTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", CakeGalleryTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", CakeGalleryTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", CakeItemTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", CakeItemTable.TABLE_NAME));


    }

    //#endregion

    //#region Categories

    public void insertCategoryList(List<Categories> categoriesList) {

        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();

        for (int i = 0; i < categoriesList.size(); i++) {
            Categories category = categoriesList.get(i);
            contentValues.put(CategoriesTable.Columns.CAT_ID, category.getCatId());
            contentValues.put(CategoriesTable.Columns.MENU_ID, category.getMenuId());
            contentValues.put(CategoriesTable.Columns.PARENT_ID, category.getParentId());
            contentValues.put(CategoriesTable.Columns.CAT_NAME, category.getCatName());
            contentValues.put(CategoriesTable.Columns.DESCRIPTION, category.getDescription());
            contentValues.put(CategoriesTable.Columns.DESCRIPTION_2, category.getDescription2());
            contentValues.put(CategoriesTable.Columns.PIC_PATH, category.getPicpath());
            contentValues.put(CategoriesTable.Columns.PICTURE, category.getPicture());
            contentValues.put(CategoriesTable.Columns.THUMB_PICTURE, category.getThumb_picture());
            contentValues.put(CategoriesTable.Columns.HEADER_PIC_PATH, category.getHeaderpicpath());
            contentValues.put(CategoriesTable.Columns.FOOTER_PIC_PATH, category.getFooterpicpath());
            contentValues.put(CategoriesTable.Columns.BIG_PIC_PATH, category.getBgpicpath());
            contentValues.put(CategoriesTable.Columns.SEQ, category.getSeq());

            db.insert(CategoriesTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    public List<Integer> getAllCategoriesId() {

        String query = "SELECT "
                + CategoriesTable.Columns.CAT_ID
                + " FROM " + CategoriesTable.TABLE_NAME;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<Integer> categoriesList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Log.d(TAG, "getAllCategoriesId: " + cursor.getInt(0));
            categoriesList.add(cursor.getInt(0));
        }
        cursor.close();

        return categoriesList;
    }

    @SuppressLint("Range")
    public List<Categories> getMenuCategories() {
        String query = " SELECT " + CategoriesTable.Columns.CAT_ID + ", "
                + CategoriesTable.Columns.DESCRIPTION + ", "
                + CategoriesTable.Columns.PIC_PATH + ", "
                + CategoriesTable.Columns.SEQ
                + " FROM " + CategoriesTable.TABLE_NAME
                + " WHERE " + CategoriesTable.Columns.PARENT_ID + " = " + MainApplication.getPreferences().getInt(Constant.SETTINGS_ORDER_MENU_CATALOG_ID, 1)
                + " ORDER BY " + CategoriesTable.Columns.DESCRIPTION;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<Categories> categoriesList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Log.d(TAG, "getAllCategoriesId: " + cursor.getInt(0));
            Categories categories = new Categories();
            categories.setCatId(cursor.getInt(0));
            categories.setDescription(cursor.getString(1));
            categories.setPicpath(cursor.getString(2));
            categories.setSeq(cursor.getInt(3));
            categoriesList.add(categories);
        }
        cursor.close();

        return categoriesList;
    }

    @SuppressLint("Range")
    public List<Categories> getCatalogCategories() {
        String query = " SELECT " + CategoriesTable.Columns.CAT_ID + ", "
                + CategoriesTable.Columns.DESCRIPTION + ", "
                + CategoriesTable.Columns.PIC_PATH + ", "
                + CategoriesTable.Columns.SEQ
                + " FROM " + CategoriesTable.TABLE_NAME
                + " WHERE " + CategoriesTable.Columns.PARENT_ID + " = " + MainApplication.getPreferences().getInt(Constant.SETTINGS_PRODUCT_CATALOG_ID, 1)
                + " ORDER BY " + CategoriesTable.Columns.DESCRIPTION;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<Categories> categoriesList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Log.d(TAG, "getAllCategoriesId: " + cursor.getInt(0));
            Categories categories = new Categories();
            categories.setCatId(cursor.getInt(0));
            categories.setDescription(cursor.getString(1));
            categories.setPicpath(cursor.getString(2));
            categories.setSeq(cursor.getInt(3));
            categoriesList.add(categories);
        }
        cursor.close();

        return categoriesList;
    }
    //#endregion

    //#region Cities
    public void insertCitiesList(List<Cities> citiesList) {

        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < citiesList.size(); i++) {
            Cities city = citiesList.get(i);
            contentValues.put(CitiesTable.Columns.CITY_CODE, city.getCityCode());
            contentValues.put(CitiesTable.Columns.ZIP_CODE, city.getZipCode());
            contentValues.put(CitiesTable.Columns.IS_ACTIVE, city.getIsActive());
            contentValues.put(CitiesTable.Columns.REGION_CODE, city.getRegionCode());
            contentValues.put(CitiesTable.Columns.COUNTRY_CODE, city.getCountryCode());
            contentValues.put(CitiesTable.Columns.CITY_NAME, city.getCityName());

            db.insert(CitiesTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    public void insertBranchCities(List<BranchCities> branchCitiesList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < branchCitiesList.size(); i++) {
            BranchCities branchCities = branchCitiesList.get(i);
            contentValues.put(BranchCitiesTable.Columns.BRANCH_ID, branchCities.getBranchId());
            contentValues.put(BranchCitiesTable.Columns.CITY_CODE, branchCities.getCityCode());
            db.insert(BranchCitiesTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    @SuppressLint("Range")
    public List<Cities> getAllCities() {
        Cursor cursor = database.getReadableDatabase().rawQuery("select * from " + CitiesTable.TABLE_NAME + " ORDER BY " + CitiesTable.Columns.CITY_NAME, null);
        List<Cities> citiesList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Cities cities = new Cities(
                    cursor.getInt(cursor.getColumnIndex(CitiesTable.Columns.CITY_CODE)),
                    cursor.getInt(cursor.getColumnIndex(CitiesTable.Columns.REGION_CODE)),
                    cursor.getString(cursor.getColumnIndex(CitiesTable.Columns.CITY_NAME)),
                    cursor.getInt(cursor.getColumnIndex(CitiesTable.Columns.ZIP_CODE)),
                    cursor.getInt(cursor.getColumnIndex(CitiesTable.Columns.COUNTRY_CODE)),
                    cursor.getInt(cursor.getColumnIndex(CitiesTable.Columns.IS_ACTIVE))
            );

            citiesList.add(cities);
        }
        cursor.close();

        return citiesList;
    }

    @SuppressLint("Range")
    public Cities getCityByCityId(int cityCode) {
        Cursor cursor = database.getReadableDatabase().rawQuery("select * from " + CitiesTable.TABLE_NAME + " where " + CitiesTable.Columns.CITY_CODE + " = " + cityCode, null);
        Cities city = null;
        if (cursor.moveToNext()) {
            city = new Cities(
                    cursor.getInt(cursor.getColumnIndex(CitiesTable.Columns.CITY_CODE)),
                    cursor.getInt(cursor.getColumnIndex(CitiesTable.Columns.REGION_CODE)),
                    cursor.getString(cursor.getColumnIndex(CitiesTable.Columns.CITY_NAME)),
                    cursor.getInt(cursor.getColumnIndex(CitiesTable.Columns.ZIP_CODE)),
                    cursor.getInt(cursor.getColumnIndex(CitiesTable.Columns.COUNTRY_CODE)),
                    cursor.getInt(cursor.getColumnIndex(CitiesTable.Columns.IS_ACTIVE))
            );
        }
        cursor.close();

        return city;
    }
    //#endregion

    //#region Products
    public void insertProductList(List<Products> productsList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < productsList.size(); i++) {
            Products products = productsList.get(i);
            Log.d(TAG, "insertProductList: product " + products);
            contentValues.put(ProductsTable.Columns.PROD_NUM, products.getProdNum());
            contentValues.put(ProductsTable.Columns.CAT_ID, products.getCatId());
            contentValues.put(ProductsTable.Columns.DESCRIPTION, products.getDescription());
            contentValues.put(ProductsTable.Columns.DESCRIPTION_2, products.getDescription2());
            contentValues.put(ProductsTable.Columns.PROD_INFO, products.getProdInfo());
            contentValues.put(ProductsTable.Columns.PROD_INFO_2, products.getProdInfo2());
            contentValues.put(ProductsTable.Columns.PRICE, products.getPrice());
            contentValues.put(ProductsTable.Columns.PRICE2, products.getPrice2());
            contentValues.put(ProductsTable.Columns.PIC_PATH, products.getPicPath());
            contentValues.put(ProductsTable.Columns.PICTURE, products.getPicture());
            contentValues.put(ProductsTable.Columns.PRODUCT_PICTURE, products.getProduct_picture());
            contentValues.put(ProductsTable.Columns.TITLE, products.getTitle());
            contentValues.put(ProductsTable.Columns.P_ID, products.getpId());
            contentValues.put(ProductsTable.Columns.THUMB_PICTURE, products.getThumb_picture());
            contentValues.put(ProductsTable.Columns.BRAND, products.getBrand());
            contentValues.put(ProductsTable.Columns.REF_CODE_1, products.getRefCode1());
            contentValues.put(ProductsTable.Columns.REF_CODE_2, products.getRefCode2());
            contentValues.put(ProductsTable.Columns.IS_TAXABLE_1, products.getIsTaxable1());
            contentValues.put(ProductsTable.Columns.IS_TAXABLE_2, products.getIsTaxable2());
            contentValues.put(ProductsTable.Columns.IS_TAXABLE_3, products.getIsTaxable3());
            contentValues.put(ProductsTable.Columns.MODIFIERS_GROUP_ID, products.getModifiersGroupId());
            contentValues.put(ProductsTable.Columns.COMBO_GROUP_ID, products.getComboGroupId());
            contentValues.put(ProductsTable.Columns.TAGS, products.getTags());
            db.insert(ProductsTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    @SuppressLint("Range")
    public List<Products> getProductsByCatId(int catId) {
        boolean shouldShowSecondCurr = shouldShowInPriceListCurrency();

        String query = "SELECT * FROM " + ProductsTable.TABLE_NAME
                + " WHERE " + ProductsTable.Columns.CAT_ID
                + " = " + catId
                + " ORDER BY " + ProductsTable.Columns.DESCRIPTION;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<Products> productsList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Products products = new Products();

            products.setProduct_picture(cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.PRODUCT_PICTURE)));
            products.setProdNum(cursor.getInt(cursor.getColumnIndex(ProductsTable.Columns.PROD_NUM)));
            products.setCatId(cursor.getInt(cursor.getColumnIndex(ProductsTable.Columns.CAT_ID)));
            products.setDescription(cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.DESCRIPTION)));
            products.setDescription2(cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.DESCRIPTION_2)));
            products.setProdInfo(cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.PROD_INFO)));
            products.setProdInfo2(cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.PROD_INFO_2)));
            products.setPrice(cursor.getDouble(cursor.getColumnIndex(ProductsTable.Columns.PRICE)));
            products.setPrice2(cursor.getDouble(cursor.getColumnIndex(ProductsTable.Columns.PRICE2)));
            products.setPicPath(cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.PIC_PATH)));
            products.setTitle(cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.TITLE)));
            products.setBrand(cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.BRAND)));
            products.setRefCode1(cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.REF_CODE_1)));
            products.setRefCode2(cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.REF_CODE_2)));
            products.setIsTaxable1(cursor.getInt(cursor.getColumnIndex(ProductsTable.Columns.IS_TAXABLE_1)));
            products.setIsTaxable2(cursor.getInt(cursor.getColumnIndex(ProductsTable.Columns.IS_TAXABLE_2)));
            products.setIsTaxable3(cursor.getInt(cursor.getColumnIndex(ProductsTable.Columns.IS_TAXABLE_3)));
            products.setModifiersGroupId(cursor.getInt(cursor.getColumnIndex(ProductsTable.Columns.MODIFIERS_GROUP_ID)));
            products.setComboGroupId(cursor.getInt(cursor.getColumnIndex(ProductsTable.Columns.COMBO_GROUP_ID)));
            products.setTags(cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.TAGS)));

            if (shouldShowSecondCurr) {
                products.setFinalPrice(products.getPrice2());
            } else {
                products.setFinalPrice(products.getPrice());
            }

            productsList.add(products);
        }
        cursor.close();

        return productsList;
    }

    public int getProductNewPrice(int productId) {
        String query = "SELECT " + ProductsTable.Columns.PRICE
                + " FROM " + ProductsTable.TABLE_NAME
                + " WHERE " + ProductsTable.Columns.PROD_NUM
                + " = " + productId;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        if (cursor.moveToNext()) {
            int price = cursor.getInt(0);
            cursor.close();
            return price;
        }
        cursor.close();

        return 0;
    }

    public void removeProduct(int prodNum) {
        String query = "DELETE FROM " + ProductsTable.TABLE_NAME
                + " WHERE " + ProductsTable.Columns.PROD_NUM
                + " = " + prodNum;

        database.getWritableDatabase().execSQL(query);


    }

    public void updateProduct(int prodNum, double price, double price2) {
        SQLiteDatabase db = database.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(ProductsTable.Columns.PRICE, price);
        values.put(ProductsTable.Columns.PRICE2, price2);

        db.update(ProductsTable.TABLE_NAME, values, ProductsTable.Columns.PROD_NUM + " = " + prodNum, null);
        db.close();
    }

    //#endregion

    //#region Branches

    public void insertBranchList(List<Branches> branchesList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < branchesList.size(); i++) {
            Branches branches = branchesList.get(i);
            contentValues.put(BranchesTable.Columns.BRANCH_ID, branches.getId());
            contentValues.put(BranchesTable.Columns.NAME, branches.getName());
            contentValues.put(BranchesTable.Columns.ADDRESS, branches.getAddress());
            contentValues.put(BranchesTable.Columns.PHONE, branches.getPhone());
            contentValues.put(BranchesTable.Columns.EMAIL, branches.getEmail());
            contentValues.put(BranchesTable.Columns.LONGITUDE, branches.getLongitude());
            contentValues.put(BranchesTable.Columns.LATITUDE, branches.getLatitude());
            contentValues.put(BranchesTable.Columns.CLIENT_ID, branches.getClientId());
            contentValues.put(BranchesTable.Columns.PIC_PATH, branches.getPicPath());
            contentValues.put(BranchesTable.Columns.IS_ACTIVE, branches.getIsActive());
            contentValues.put(BranchesTable.Columns.OPENING_HOURS, branches.getOpeningHours());
            contentValues.put(BranchesTable.Columns.PICTURE, branches.getPicture());
            contentValues.put(BranchesTable.Columns.ACCEPTS_DELIVERY, branches.getAcceptsDelivery());
            contentValues.put(BranchesTable.Columns.ACCEPTS_TAKE_AWAY, branches.getAcceptsTakeAway());
            contentValues.put(BranchesTable.Columns.DELIVERY_HOURS, branches.getDeliveryHours());

            db.insert(BranchesTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    @SuppressLint("Range")
    public List<Branches> getBranchesList() {

        String query = "SELECT " + BranchesTable.Columns.BRANCH_ID + ", "
                + BranchesTable.Columns.IS_ACTIVE + ", "
                + BranchesTable.Columns.ACCEPTS_DELIVERY + ", "
                + BranchesTable.Columns.ACCEPTS_TAKE_AWAY + ", "
                + BranchesTable.Columns.DELIVERY_HOURS + ", "
                + BranchesTable.Columns.NAME + ", "
                + BranchesTable.Columns.ADDRESS + ", "
                + BranchesTable.Columns.PHONE + ", "
                + BranchesTable.Columns.EMAIL + ", "
                + BranchesTable.Columns.LONGITUDE + ", "
                + BranchesTable.Columns.LATITUDE + ", "
                + BranchesTable.Columns.CLIENT_ID + ", "
                + BranchesTable.Columns.PIC_PATH + ", "
                + BranchesTable.Columns.OPENING_HOURS + ", "
                + BranchesTable.Columns.PICTURE
                + " FROM " + BranchesTable.TABLE_NAME
                + " ORDER BY " + BranchesTable.Columns.NAME;

        List<Branches> branchesList = new ArrayList<>();
        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        while (cursor.moveToNext()) {
            branchesList.add(new Branches(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getInt(2),
                    cursor.getInt(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6),
                    cursor.getString(7),
                    cursor.getString(8),
                    cursor.getDouble(9),
                    cursor.getDouble(10),
                    cursor.getString(11),
                    cursor.getString(12),
                    cursor.getString(13),
                    cursor.getString(14)
            ));
        }
        cursor.close();

        return branchesList;
    }

    @SuppressLint("Range")
    public List<Branches> getPickUpBranches() {
        List<Branches> branchesList = new ArrayList<>();
        String query = "SELECT * FROM " + BranchesTable.TABLE_NAME
                + " WHERE " + BranchesTable.Columns.ACCEPTS_TAKE_AWAY
                + " =1";
        Cursor cursor = database.getReadableDatabase().rawQuery(query, null, null);

        while (cursor.moveToNext()) {
            branchesList.add(new Branches(
                    cursor.getInt(cursor.getColumnIndex(BranchesTable.Columns.BRANCH_ID)),
                    cursor.getInt(cursor.getColumnIndex(BranchesTable.Columns.IS_ACTIVE)),
                    cursor.getInt(cursor.getColumnIndex(BranchesTable.Columns.ACCEPTS_DELIVERY)),
                    cursor.getInt(cursor.getColumnIndex(BranchesTable.Columns.ACCEPTS_TAKE_AWAY)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.DELIVERY_HOURS)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.NAME)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.ADDRESS)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.PHONE)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.EMAIL)),
                    cursor.getDouble(cursor.getColumnIndex(BranchesTable.Columns.LONGITUDE)),
                    cursor.getDouble(cursor.getColumnIndex(BranchesTable.Columns.LATITUDE)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.CLIENT_ID)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.PIC_PATH)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.OPENING_HOURS)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.PICTURE))
            ));
        }
        cursor.close();

        return branchesList;
    }

    @SuppressLint("Range")
    public List<Branches> getBranchFilteredList(int cityCode) {
        List<Branches> branchesList = new ArrayList<>();
        String query = "SELECT * FROM " + BranchesTable.TABLE_NAME
                + " WHERE " + BranchesTable.Columns.BRANCH_ID
                + " IN ("
                + " SELECT DISTINCT " + BranchCitiesTable.Columns.BRANCH_ID
                + " FROM " + BranchCitiesTable.TABLE_NAME
                + " WHERE " + BranchCitiesTable.Columns.CITY_CODE
                + " = " + cityCode + ")"
                + " ORDER BY " + BranchesTable.Columns.NAME;
        Cursor cursor = database.getReadableDatabase().rawQuery(query, null, null);

        while (cursor.moveToNext()) {
            branchesList.add(new Branches(
                    cursor.getInt(cursor.getColumnIndex(BranchesTable.Columns.BRANCH_ID)),
                    cursor.getInt(cursor.getColumnIndex(BranchesTable.Columns.IS_ACTIVE)),
                    cursor.getInt(cursor.getColumnIndex(BranchesTable.Columns.ACCEPTS_DELIVERY)),
                    cursor.getInt(cursor.getColumnIndex(BranchesTable.Columns.ACCEPTS_TAKE_AWAY)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.DELIVERY_HOURS)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.NAME)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.ADDRESS)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.PHONE)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.EMAIL)),
                    cursor.getDouble(cursor.getColumnIndex(BranchesTable.Columns.LONGITUDE)),
                    cursor.getDouble(cursor.getColumnIndex(BranchesTable.Columns.LATITUDE)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.CLIENT_ID)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.PIC_PATH)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.OPENING_HOURS)),
                    cursor.getString(cursor.getColumnIndex(BranchesTable.Columns.PICTURE))
            ));
        }
        cursor.close();

        return branchesList;
    }

    public Branches getBranchById(int branchId) {
        String query = "SELECT " + BranchesTable.Columns.BRANCH_ID + ", "
                + BranchesTable.Columns.IS_ACTIVE + ", "
                + BranchesTable.Columns.ACCEPTS_DELIVERY + ", "
                + BranchesTable.Columns.ACCEPTS_TAKE_AWAY + ", "
                + BranchesTable.Columns.DELIVERY_HOURS + ", "
                + BranchesTable.Columns.NAME + ", "
                + BranchesTable.Columns.ADDRESS + ", "
                + BranchesTable.Columns.PHONE + ", "
                + BranchesTable.Columns.EMAIL + ", "
                + BranchesTable.Columns.LONGITUDE + ", "
                + BranchesTable.Columns.LATITUDE + ", "
                + BranchesTable.Columns.CLIENT_ID + ", "
                + BranchesTable.Columns.PIC_PATH + ", "
                + BranchesTable.Columns.OPENING_HOURS + ", "
                + BranchesTable.Columns.PICTURE
                + " from " + BranchesTable.TABLE_NAME
                + " where " + BranchesTable.Columns.BRANCH_ID
                + " = " + branchId;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        if (cursor.moveToNext()) {

            return new Branches(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getInt(2),
                    cursor.getInt(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6),
                    cursor.getString(7),
                    cursor.getString(8),
                    cursor.getDouble(9),
                    cursor.getDouble(10),
                    cursor.getString(11),
                    cursor.getString(12),
                    cursor.getString(13),
                    cursor.getString(14)
            );
        }
        cursor.close();

        return null;
    }

    //#endregion

    //#region Questions

    public void insertQuestionGroupList(List<QuestionGroup> questionGroupList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < questionGroupList.size(); i++) {
            QuestionGroup questionGroup = questionGroupList.get(i);
            contentValues.put(QuestionsGroupTable.Columns.QUESTION_GROUP_ID, questionGroup.getQuestionGroupId());
            contentValues.put(QuestionsGroupTable.Columns.DESCRIPTION, questionGroup.getDescription());

            db.insert(QuestionsGroupTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    public void insertQuestionHeaderList(List<QuestionHeader> questionHeaderList) {

        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();

        for (int i = 0; i < questionHeaderList.size(); i++) {
            QuestionHeader questionHeader = questionHeaderList.get(i);
            contentValues.put(QuestionHeaderTable.Columns.QUESTION_GROUP_ID, questionHeader.getQuestionGroupId());
            contentValues.put(QuestionHeaderTable.Columns.QUESTION_HEADER_ID, questionHeader.getQuestionHeaderId());
            contentValues.put(QuestionHeaderTable.Columns.MIN, questionHeader.getMin());
            contentValues.put(QuestionHeaderTable.Columns.MAX, questionHeader.getMax());
            contentValues.put(QuestionHeaderTable.Columns.REQUIRED, questionHeader.getRequired());
            contentValues.put(QuestionHeaderTable.Columns.SEQ, questionHeader.getSeq());
            contentValues.put(QuestionHeaderTable.Columns.DESCRIPTION, questionHeader.getDescription());

            db.insert(QuestionHeaderTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    public void insertQuestionModifierList(List<QuestionModifier> questionModifierList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();

        for (int i = 0; i < questionModifierList.size(); i++) {
            QuestionModifier modifier = questionModifierList.get(i);
            contentValues.put(QuestionModifierTable.Columns.PROD_NUM, modifier.getProdNum());
            contentValues.put(QuestionModifierTable.Columns.CAT_ID, modifier.getCatId());
            contentValues.put(QuestionModifierTable.Columns.DESCRIPTION, modifier.getDescription());
            contentValues.put(QuestionModifierTable.Columns.DESCRIPTION_2, modifier.getDescription2());
            contentValues.put(QuestionModifierTable.Columns.PROD_INFO, modifier.getProdInfo());
            contentValues.put(QuestionModifierTable.Columns.PROD_INFO_2, modifier.getProdInfo2());
            contentValues.put(QuestionModifierTable.Columns.PRICE, modifier.getPrice());
            contentValues.put(QuestionModifierTable.Columns.PIC_PATH, modifier.getPicPath());
            contentValues.put(QuestionModifierTable.Columns.PICTURE, modifier.getPicture());
            contentValues.put(QuestionModifierTable.Columns.QUESTION_HEADER_ID, modifier.getQuestionHeaderId());
            contentValues.put(QuestionModifierTable.Columns.QUESTION_GROUP_ID, modifier.getGroupId());
            contentValues.put(QuestionModifierTable.Columns.P_ID, modifier.getPid());
            contentValues.put(QuestionModifierTable.Columns.THUMB_PICTURE, modifier.getThumbPicture());
            contentValues.put(QuestionModifierTable.Columns.BRAND, modifier.getBrand());
            contentValues.put(QuestionModifierTable.Columns.REF_CODE_1, modifier.getRefCode1());
            contentValues.put(QuestionModifierTable.Columns.REF_CODE_2, modifier.getRefCode2());
            contentValues.put(QuestionModifierTable.Columns.IS_TAXABLE_1, modifier.getIsTaxable1());
            contentValues.put(QuestionModifierTable.Columns.IS_TAXABLE_2, modifier.getIsTaxable2());
            contentValues.put(QuestionModifierTable.Columns.IS_TAXABLE_3, modifier.getIsTaxable3());
            contentValues.put(QuestionModifierTable.Columns.VERSION_ID, modifier.getVersionId());
            contentValues.put(QuestionModifierTable.Columns.CREATED_AT, modifier.getCreatedAt());
            contentValues.put(QuestionModifierTable.Columns.UPDATED_AT, modifier.getUpdatedAt());

            db.insert(QuestionModifierTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    @SuppressLint("Range")
    public List<QuestionHeader> getQuestionHeaderByQgId(int questionGroupId) {

        String query = "SELECT * FROM " + QuestionHeaderTable.TABLE_NAME
                + " WHERE " + QuestionHeaderTable.Columns.QUESTION_GROUP_ID
                + " = " + questionGroupId
                + " ORDER BY " + QuestionHeaderTable.Columns.SEQ;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<QuestionHeader> questionHeaderList = new ArrayList<>();
        while (cursor.moveToNext()) {
            QuestionHeader questionHeader = new QuestionHeader(
                    cursor.getInt(cursor.getColumnIndex(QuestionHeaderTable.Columns.QUESTION_GROUP_ID)),
                    cursor.getInt(cursor.getColumnIndex(QuestionHeaderTable.Columns.QUESTION_HEADER_ID)),
                    cursor.getInt(cursor.getColumnIndex(QuestionHeaderTable.Columns.MIN)),
                    cursor.getInt(cursor.getColumnIndex(QuestionHeaderTable.Columns.MAX)),
                    cursor.getInt(cursor.getColumnIndex(QuestionHeaderTable.Columns.REQUIRED)),
                    cursor.getInt(cursor.getColumnIndex(QuestionHeaderTable.Columns.SEQ)),
                    cursor.getString(cursor.getColumnIndex(QuestionHeaderTable.Columns.DESCRIPTION))
            );
            questionHeaderList.add(questionHeader);
        }
        cursor.close();

        return questionHeaderList;
    }

    @SuppressLint("Range")
    public List<QuestionModifier> getQuestionModifierByQhId(int questionGroupId, int questionHeaderId) {

        boolean shouldShowSecondCurr = shouldShowInPriceListCurrency();

        String query = "SELECT "
                + QuestionModifierTable.Columns.PROD_NUM + ", "
                + QuestionModifierTable.Columns.PIC_PATH + ", "
                + QuestionModifierTable.Columns.DESCRIPTION + ", "
                + QuestionModifierTable.Columns.ENABLED + ", "
                + QuestionModifierTable.Columns.PRICE
                + " FROM " + QuestionModifierTable.TABLE_NAME
                + " WHERE " + QuestionModifierTable.Columns.QUESTION_GROUP_ID
                + " = " + questionGroupId
                + " AND " + QuestionModifierTable.Columns.QUESTION_HEADER_ID
                + " = " + questionHeaderId;


        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<QuestionModifier> questionModifierList = new ArrayList<>();
        while (cursor.moveToNext()) {
            QuestionModifier questionModifier = new QuestionModifier();
            questionModifier.setProdNum(cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.PROD_NUM)));
            questionModifier.setPicPath(cursor.getString(cursor.getColumnIndex(QuestionModifierTable.Columns.PIC_PATH)));
            questionModifier.setDescription(cursor.getString(cursor.getColumnIndex(QuestionModifierTable.Columns.DESCRIPTION)));
            questionModifier.setEnabled(cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.ENABLED)));
            questionModifier.setPrice(cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.PRICE)));

            double price = cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.PRICE));
            if (shouldShowSecondCurr) {
                questionModifier.setFinalPrice(price / getPriceList().getConversion());
            } else {
                questionModifier.setFinalPrice(price);
            }
            questionModifierList.add(questionModifier);
        }
        cursor.close();

        return questionModifierList;
    }

    //#endregion

    //#region Combos

    public void insertComboGroupList(List<ComboGroup> comboGroupList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();

        for (int i = 0; i < comboGroupList.size(); i++) {
            ComboGroup comboGroup = comboGroupList.get(i);
            contentValues.put(ComboGroupTable.Columns.COMBO_GROUP_ID, comboGroup.getComboGroupId());

            db.insert(ComboGroupTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    public void insertComboHeaderList(List<ComboHeader> comboHeaderList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < comboHeaderList.size(); i++) {
            ComboHeader comboHeader = comboHeaderList.get(i);
            contentValues.put(ComboHeaderTable.Columns.COMBO_GROUP_ID, comboHeader.getComboGroupId());
            contentValues.put(ComboHeaderTable.Columns.COMBO_HEADER_ID, comboHeader.getComboHeaderId());
            contentValues.put(ComboHeaderTable.Columns.MIN, comboHeader.getMin());
            contentValues.put(ComboHeaderTable.Columns.MAX, comboHeader.getMax());
            contentValues.put(ComboHeaderTable.Columns.REQUIRED, comboHeader.getRequired());
            contentValues.put(ComboHeaderTable.Columns.SEQ, comboHeader.getSeq());
            contentValues.put(ComboHeaderTable.Columns.DESCRIPTION, comboHeader.getDescription());

            db.insert(ComboHeaderTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    public void insertComboModifierList(List<ComboModifier> comboModifierList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < comboModifierList.size(); i++) {
            ComboModifier modifier = comboModifierList.get(i);
            contentValues.put(ComboModifierTable.Columns.PROD_NUM, modifier.getProdNum());
            contentValues.put(ComboModifierTable.Columns.CAT_ID, modifier.getCatId());
            contentValues.put(ComboModifierTable.Columns.DESCRIPTION, modifier.getDescription());
            contentValues.put(ComboModifierTable.Columns.DESCRIPTION_2, modifier.getDescription2());
            contentValues.put(ComboModifierTable.Columns.PROD_INFO, modifier.getProdInfo());
            contentValues.put(ComboModifierTable.Columns.PROD_INFO_2, modifier.getProdInfo2());
            contentValues.put(ComboModifierTable.Columns.PRICE, modifier.getPrice());
            contentValues.put(ComboModifierTable.Columns.PIC_PATH, modifier.getPicPath());
            contentValues.put(ComboModifierTable.Columns.PICTURE, modifier.getPicture());
            contentValues.put(ComboModifierTable.Columns.COMBO_HEADER_ID, modifier.getComboHeaderId());
            contentValues.put(ComboModifierTable.Columns.COMBO_GROUP_ID, modifier.getGroupId());
            contentValues.put(ComboModifierTable.Columns.P_ID, modifier.getPid());
            contentValues.put(ComboModifierTable.Columns.THUMB_PICTURE, modifier.getThumbPicture());
            contentValues.put(ComboModifierTable.Columns.BRAND, modifier.getBrand());
            contentValues.put(ComboModifierTable.Columns.REF_CODE_1, modifier.getRefCode1());
            contentValues.put(ComboModifierTable.Columns.REF_CODE_2, modifier.getRefCode2());
            contentValues.put(ComboModifierTable.Columns.IS_TAXABLE_1, modifier.getIsTaxable1());
            contentValues.put(ComboModifierTable.Columns.IS_TAXABLE_2, modifier.getIsTaxable2());
            contentValues.put(ComboModifierTable.Columns.IS_TAXABLE_3, modifier.getIsTaxable3());
            contentValues.put(ComboModifierTable.Columns.VERSION_ID, modifier.getVersionId());
            contentValues.put(ComboModifierTable.Columns.CREATED_AT, modifier.getCreatedAt());
            contentValues.put(ComboModifierTable.Columns.UPDATED_AT, modifier.getUpdatedAt());

            db.insert(ComboModifierTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    @SuppressLint("Range")
    public List<ComboHeader> getComboHeaderByGroupId(int comboGroupId) {

        String query = "SELECT * FROM " + ComboHeaderTable.TABLE_NAME
                + " WHERE " + ComboHeaderTable.Columns.COMBO_GROUP_ID
                + " = " + comboGroupId
                + " ORDER BY " + ComboHeaderTable.Columns.SEQ;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<ComboHeader> comboHeaderList = new ArrayList<>();
        while (cursor.moveToNext()) {
            ComboHeader comboHeader = new ComboHeader(
                    cursor.getInt(cursor.getColumnIndex(ComboHeaderTable.Columns.COMBO_GROUP_ID)),
                    cursor.getInt(cursor.getColumnIndex(ComboHeaderTable.Columns.COMBO_HEADER_ID)),
                    cursor.getInt(cursor.getColumnIndex(ComboHeaderTable.Columns.MIN)),
                    cursor.getInt(cursor.getColumnIndex(ComboHeaderTable.Columns.MAX)),
                    cursor.getInt(cursor.getColumnIndex(ComboHeaderTable.Columns.REQUIRED)),
                    cursor.getInt(cursor.getColumnIndex(ComboHeaderTable.Columns.SEQ)),
                    cursor.getString(cursor.getColumnIndex(ComboHeaderTable.Columns.DESCRIPTION))
            );
            comboHeaderList.add(comboHeader);
        }
        cursor.close();

        return comboHeaderList;
    }

    @SuppressLint("Range")
    public List<ComboModifier> getComboModifierByQuestionHeaderId(int comboGroupId, int comboHeaderId) {
        boolean shouldShowSecondCurr = shouldShowInPriceListCurrency();

        String query = "SELECT "
                + ComboModifierTable.Columns.PIC_PATH + ", "
                + ComboModifierTable.Columns.PROD_NUM + ", "
                + ComboModifierTable.Columns.DESCRIPTION + ", "
                + ComboModifierTable.Columns.ENABLED + ", "
                + ComboModifierTable.Columns.PRICE
                + " FROM " + ComboModifierTable.TABLE_NAME
                + " WHERE " + ComboModifierTable.Columns.COMBO_GROUP_ID
                + " = " + comboGroupId
                + " AND " + ComboModifierTable.Columns.COMBO_HEADER_ID
                + " = " + comboHeaderId;


        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<ComboModifier> comboModifierList = new ArrayList<>();
        while (cursor.moveToNext()) {
            ComboModifier comboModifier = new ComboModifier();
            comboModifier.setPicPath(cursor.getString(cursor.getColumnIndex(QuestionModifierTable.Columns.PIC_PATH)));
            comboModifier.setProdNum(cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.PROD_NUM)));
            comboModifier.setDescription(cursor.getString(cursor.getColumnIndex(QuestionModifierTable.Columns.DESCRIPTION)));
            comboModifier.setEnabled(cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.ENABLED)));
            comboModifier.setPrice(cursor.getDouble(cursor.getColumnIndex(QuestionModifierTable.Columns.PRICE)));

            double price = cursor.getDouble(cursor.getColumnIndex(QuestionModifierTable.Columns.PRICE));
            if (shouldShowSecondCurr) {
                comboModifier.setFinalPrice(price / getPriceList().getConversion());
            } else {
                comboModifier.setFinalPrice(price);
            }
            comboModifierList.add(comboModifier);
        }
        cursor.close();
        return comboModifierList;
    }

    //#endregion

    //#region Gallery

    public void insertCakeGalleryList(List<CakeGallery> cakeGalleryList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();

        for (int i = 0; i < cakeGalleryList.size(); i++) {
            CakeGallery cakeGallery = cakeGalleryList.get(i);
            contentValues.put(CakeGalleryTable.Columns.GALLERY_ID, cakeGallery.getGalleryId());
            contentValues.put(CakeGalleryTable.Columns.GALLERY_NAME, cakeGallery.getGalleryName());
            contentValues.put(CakeGalleryTable.Columns.IS_ACTIVE, cakeGallery.getIsActive());

            db.insert(CakeGalleryTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    public void insertCakeItemList(List<CakeItem> cakeItemList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();

        for (int i = 0; i < cakeItemList.size(); i++) {
            CakeItem cakeItem = cakeItemList.get(i);
            contentValues.put(CakeItemTable.Columns.CAKE_GALLERY_ID, cakeItem.getCakeGalleryId());
            contentValues.put(CakeItemTable.Columns.CAKE_ITEM_ID, cakeItem.getCakeId());
            contentValues.put(CakeItemTable.Columns.IS_ACTIVE, cakeItem.getIsActive());
            contentValues.put(CakeItemTable.Columns.DESCRIPTION, cakeItem.getDescription());
            contentValues.put(CakeItemTable.Columns.MIN_ORDER_QTY, cakeItem.getMinOrderQty());
            contentValues.put(CakeItemTable.Columns.PIC_PATH, cakeItem.getPicPath());
            contentValues.put(CakeItemTable.Columns.PRICE, cakeItem.getPrice());
            contentValues.put(CakeItemTable.Columns.TITLE, cakeItem.getTitle());
            contentValues.put(CakeItemTable.Columns.REF_NUM, cakeItem.getRefNum());

            db.insert(CakeItemTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    @SuppressLint("Range")
    public List<CakeGallery> getCakeGalleryList() {

        String query = "SELECT * FROM " + CakeGalleryTable.TABLE_NAME
                + " WHERE " + CakeGalleryTable.Columns.IS_ACTIVE
                + " = " + 1
                + " ORDER BY " + CakeGalleryTable.Columns.GALLERY_NAME;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<CakeGallery> cakeGalleryList = new ArrayList<>();
        //first gallery is for favorite
        cakeGalleryList.add(new CakeGallery(-1, -1, "-1"));
        while (cursor.moveToNext()) {
            CakeGallery cakeGallery = new CakeGallery(
                    cursor.getInt(cursor.getColumnIndex(CakeGalleryTable.Columns.GALLERY_ID)),
                    cursor.getInt(cursor.getColumnIndex(CakeGalleryTable.Columns.IS_ACTIVE)),
                    cursor.getString(cursor.getColumnIndex(CakeGalleryTable.Columns.GALLERY_NAME))
            );
            List<CakeItem> cakeItemList = getCakeItemsByGroupId(cakeGallery.getGalleryId());
            cakeGallery.setGalleryPhotosList(cakeItemList);

            cakeGalleryList.add(cakeGallery);
        }
        cursor.close();

        return cakeGalleryList;
    }

    @SuppressLint("Range")
    public CakeGallery getCakeGallery(int galleryId) {

        String query = "SELECT * FROM " + CakeGalleryTable.TABLE_NAME
                + " WHERE " + CakeGalleryTable.Columns.IS_ACTIVE
                + " = " + 1
                + " AND " + CakeGalleryTable.Columns.GALLERY_ID
                + " = " + galleryId;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        if (cursor.moveToNext()) {
            CakeGallery cakeGallery = new CakeGallery(
                    cursor.getInt(cursor.getColumnIndex(CakeGalleryTable.Columns.GALLERY_ID)),
                    cursor.getInt(cursor.getColumnIndex(CakeGalleryTable.Columns.IS_ACTIVE)),
                    cursor.getString(cursor.getColumnIndex(CakeGalleryTable.Columns.GALLERY_NAME))
            );
            List<CakeItem> cakeItemList = getCakeItemsByGroupId(cakeGallery.getGalleryId());
            cakeGallery.setGalleryPhotosList(cakeItemList);
            cursor.close();

            return cakeGallery;
        }
        cursor.close();

        return null;
    }

    @SuppressLint("Range")
    public List<CakeItemFavorite> getFavoriteCakes() {

        List<CakeItemFavorite> cakeItemFavoriteList = new ArrayList<>();

        String query = "SELECT * FROM " + CakeItemFavoriteTable.TABLE_NAME;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);

        while (cursor.moveToNext()) {
            CakeItemFavorite cakeItemFavorite = new CakeItemFavorite(
                    cursor.getInt(cursor.getColumnIndex(CakeItemFavoriteTable.Columns.CAKE_GALLERY_ID)),
                    cursor.getInt(cursor.getColumnIndex(CakeItemFavoriteTable.Columns.CAKE_ITEM_ID)),
                    cursor.getInt(cursor.getColumnIndex(CakeItemFavoriteTable.Columns.PRICE)),
                    cursor.getInt(cursor.getColumnIndex(CakeItemFavoriteTable.Columns.MIN_ORDER_QTY)),
                    cursor.getString(cursor.getColumnIndex(CakeItemFavoriteTable.Columns.TITLE)),
                    cursor.getString(cursor.getColumnIndex(CakeItemFavoriteTable.Columns.DESCRIPTION)),
                    cursor.getString(cursor.getColumnIndex(CakeItemFavoriteTable.Columns.PIC_PATH)),
                    cursor.getString(cursor.getColumnIndex(CakeItemFavoriteTable.Columns.REF_NUM)),
                    cursor.getBlob(cursor.getColumnIndex(CakeItemFavoriteTable.Columns.IMAGE))
            );

            cakeItemFavoriteList.add(cakeItemFavorite);
        }
        cursor.close();

        return cakeItemFavoriteList;
    }

    @SuppressLint("Range")
    public boolean isCakeFavorite(int cakeId) {

        String query = "SELECT * FROM " + CakeItemFavoriteTable.TABLE_NAME
                + " WHERE " + CakeItemFavoriteTable.Columns.CAKE_ITEM_ID
                + " = " + cakeId;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);

        if (cursor.moveToNext()) {
            cursor.close();

            return true;
        }
        cursor.close();

        return false;
    }

    @SuppressLint("Range")
    public List<CakeItem> getCakeItemsByGroupId(int galleryId) {

        String query = "SELECT "
                + CakeItemTable.Columns.CAKE_GALLERY_ID + ", "
                + CakeItemTable.Columns.CAKE_ITEM_ID + ", "
                + CakeItemTable.Columns.IS_ACTIVE + ", "
                + CakeItemTable.Columns.DESCRIPTION + ", "
                + CakeItemTable.Columns.MIN_ORDER_QTY + ", "
                + CakeItemTable.Columns.PIC_PATH + ", "
                + CakeItemTable.Columns.REF_NUM + ", "
                + CakeItemTable.Columns.PRICE + ", "
                + CakeItemTable.Columns.TITLE
                + " FROM " + CakeItemTable.TABLE_NAME
                + " WHERE " + CakeItemTable.Columns.CAKE_GALLERY_ID
                + " = " + galleryId
                + " AND " + CakeItemTable.Columns.IS_ACTIVE
                + " = 1";


        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<CakeItem> cakeItemList = new ArrayList<>();
        while (cursor.moveToNext()) {
            CakeItem cakeItem = new CakeItem(
                    cursor.getInt(cursor.getColumnIndex(CakeItemTable.Columns.CAKE_GALLERY_ID)),
                    cursor.getInt(cursor.getColumnIndex(CakeItemTable.Columns.CAKE_ITEM_ID)),
                    cursor.getInt(cursor.getColumnIndex(CakeItemTable.Columns.PRICE)),
                    cursor.getInt(cursor.getColumnIndex(CakeItemTable.Columns.IS_ACTIVE)),
                    cursor.getInt(cursor.getColumnIndex(CakeItemTable.Columns.MIN_ORDER_QTY)),
                    cursor.getString(cursor.getColumnIndex(CakeItemTable.Columns.TITLE)),
                    cursor.getString(cursor.getColumnIndex(CakeItemTable.Columns.DESCRIPTION)),
                    cursor.getString(cursor.getColumnIndex(CakeItemTable.Columns.PIC_PATH)),
                    cursor.getString(cursor.getColumnIndex(CakeItemTable.Columns.REF_NUM))
            );

            cakeItemList.add(cakeItem);
        }
        cursor.close();

        return cakeItemList;
    }

    public void insertFavoriteCake(CakeItemFavorite cakeItem) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();

        contentValues.put(CakeItemFavoriteTable.Columns.CAKE_GALLERY_ID, cakeItem.getCakeGalleryId());
        contentValues.put(CakeItemFavoriteTable.Columns.CAKE_ITEM_ID, cakeItem.getCakeId());
        contentValues.put(CakeItemFavoriteTable.Columns.DESCRIPTION, cakeItem.getDescription());
        contentValues.put(CakeItemFavoriteTable.Columns.MIN_ORDER_QTY, cakeItem.getMinOrderQty());
        contentValues.put(CakeItemFavoriteTable.Columns.PIC_PATH, cakeItem.getPicPath());
        contentValues.put(CakeItemFavoriteTable.Columns.PRICE, cakeItem.getPrice());
        contentValues.put(CakeItemFavoriteTable.Columns.TITLE, cakeItem.getTitle());
        contentValues.put(CakeItemFavoriteTable.Columns.REF_NUM, cakeItem.getRefNum());
        if (cakeItem.getImage() != null) {
            contentValues.put(CakeItemFavoriteTable.Columns.IMAGE, cakeItem.getImage());
        }

        db.insert(CakeItemFavoriteTable.TABLE_NAME, null, contentValues);
        contentValues.clear();

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    public void deleteFavoriteCake(int cakeId) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.delete(CakeItemFavoriteTable.TABLE_NAME, CakeItemFavoriteTable.Columns.CAKE_ITEM_ID + "=" + cakeId, null);
    }

    public int getFavoriteCakesCount() {
        String query = "SELECT COUNT(*)"
                + " FROM " + CakeItemFavoriteTable.TABLE_NAME;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        if (cursor.moveToNext()) {
            int count = cursor.getInt(0);
            cursor.close();

            return count;
        }
        cursor.close();

        return 0;
    }

    //#endregion

    //#region Member

    public void insertMember(Member member) {
        String query =
                "INSERT INTO " + MemberTable.TABLE_NAME + " ( "
                        + MemberTable.Columns.MEMBER_ID + ", "
                        + MemberTable.Columns.MEMBER_HS_ID + ", "
                        + MemberTable.Columns.FIRST_NAME + ", "
                        + MemberTable.Columns.LAST_NAME + ", "
                        + MemberTable.Columns.MOBILE + ", "
                        + MemberTable.Columns.MOBILE_VALIDATED + ", "
                        + MemberTable.Columns.EMAIL + ", "
                        + MemberTable.Columns.DATE_OF_BIRTH + ") "
                        + "VALUES ("
                        + "" + member.getMemberId() + ", "
                        + "" + member.getMemberHsId() + ", "
                        + "'" + member.getFirstName() + "', "
                        + "'" + member.getLastName() + "', "
                        + "'" + member.getMobile() + "', "
                        + "" + member.getMobileValidated() + ", "
                        + "'" + member.getEmail() + "', "
                        + "'" + member.getDateOfBirth() + "') ";

        Log.d(TAG, "insertParent: " + query);
        database.getWritableDatabase().execSQL(query);

    }

    public long updateMember(Member member) {
        SQLiteDatabase db = database.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (member.getImage() != null) {
            values.put(MemberTable.Columns.IMAGE, member.getImage());
        }
        values.put(MemberTable.Columns.FIRST_NAME, member.getFirstName());
        values.put(MemberTable.Columns.LAST_NAME, member.getLastName());
        values.put(MemberTable.Columns.EMAIL, member.getEmail());
        values.put(MemberTable.Columns.MEMBER_HS_ID, member.getMemberHsId());
        values.put(MemberTable.Columns.MOBILE, member.getMobile());
        values.put(MemberTable.Columns.MOBILE_VALIDATED, member.getMobileValidated());
        values.put(MemberTable.Columns.DATE_OF_BIRTH, member.getDateOfBirth());

        long status = db.update(MemberTable.TABLE_NAME, values, null, null);
        db.close();
        return status;
    }

    public Bitmap getMemberPhoto() {
        SQLiteDatabase db = database.getWritableDatabase();
        String selectQuery = "SELECT "
                + MemberTable.Columns.IMAGE
                + " FROM " + MemberTable.TABLE_NAME;

        Cursor cursor = db.rawQuery(selectQuery, null);
        byte[] imgByte;
        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                if (cursor.getBlob(0) != null) {
                    imgByte = cursor.getBlob(0);
                    return BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length);
                }
                db.close();
                cursor.close();

            } else {
                db.close();
                cursor.close();
                return null;
            }
        }
        db.close();
        cursor.close();
        return null;
    }

    @SuppressLint("Range")
    public Member getMember() {
        String query = "SELECT "
                + MemberTable.Columns.MEMBER_ID + ", "
                + MemberTable.Columns.MEMBER_HS_ID + ", "
                + MemberTable.Columns.FIRST_NAME + ", "
                + MemberTable.Columns.LAST_NAME + ", "
                + MemberTable.Columns.MOBILE + ", "
                + MemberTable.Columns.MOBILE_VALIDATED + ", "
                + MemberTable.Columns.EMAIL + ", "
                + MemberTable.Columns.DATE_OF_BIRTH
                + " FROM " + MemberTable.TABLE_NAME
                + " LIMIT 1";

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);

        if (cursor.moveToNext()) {
            Member member = new Member(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getInt(5),
                    cursor.getString(6),
                    cursor.getString(7)
            );
            cursor.close();

            return member;
        }
        cursor.close();

        return null;
    }

    public void removeMember() {
        SQLiteDatabase db = database.getWritableDatabase();
        db.delete(MemberTable.TABLE_NAME, null, null);
        db.delete(AddressTable.TABLE_NAME, null, null);
        db.delete(OrderHistoryTable.TABLE_NAME, null, null);
        db.close();
    }

    //#endregion

    //#region Address

    public void insertAddress(Address address) {
        String query =
                "INSERT INTO " + AddressTable.TABLE_NAME + " ( "
                        + AddressTable.Columns.HS_ADDRESS_ID + ", "
                        + AddressTable.Columns.CITY_CODE + ", "
                        + AddressTable.Columns.FLOOR + ", "
                        + AddressTable.Columns.DEFAULT_ADDRESS + ", "
                        + AddressTable.Columns.ADDRESS_TYPE + ", "
                        + AddressTable.Columns.GEO_LAT + ", "
                        + AddressTable.Columns.GEO_LONG + ", "
                        + AddressTable.Columns.BUILDING + ", "
                        + AddressTable.Columns.STREET + ", "
                        + AddressTable.Columns.CITY_NAME + ", "
                        + AddressTable.Columns.DESCRIPTION + ", "
                        + AddressTable.Columns.DIRECTION + ", "
                        + AddressTable.Columns.PHONE + ") "
                        + "VALUES ("
                        + "" + address.getHsAddressId() + ", "
                        + "" + address.getCityCode() + ", "
                        + "'" + address.getFloor() + "', "
                        + "" + address.getDefaultAddress() + ", "
                        + "" + address.getAddressType() + ", "
                        + "" + address.getGeoLat() + ", "
                        + "" + address.getGeoLong() + ", "
                        + "'" + address.getBldg() + "', "
                        + "'" + address.getStreet() + "', "
                        + "'" + address.getCityName() + "', "
                        + "'" + address.getDescription() + "', "
                        + "'" + address.getDirection() + "', "
                        + "'" + address.getPhone() + "') ";

        Log.d(TAG, "insertParent: " + query);
        database.getWritableDatabase().execSQL(query);

    }

    @SuppressLint("Range")
    public List<Address> getAddressList() {
        List<Address> addressList = new ArrayList<>();
        String query = "SELECT "
                + AddressTable.Columns.INTERNAL_ID + ", "
                + AddressTable.Columns.HS_ADDRESS_ID + ", "
                + AddressTable.Columns.CITY_CODE + ", "
                + AddressTable.Columns.FLOOR + ", "
                + AddressTable.Columns.DEFAULT_ADDRESS + ", "
                + AddressTable.Columns.ADDRESS_TYPE + ", "
                + AddressTable.Columns.GEO_LAT + ", "
                + AddressTable.Columns.GEO_LONG + ", "
                + AddressTable.Columns.BUILDING + ", "
                + AddressTable.Columns.STREET + ", "
                + AddressTable.Columns.CITY_NAME + ", "
                + AddressTable.Columns.DESCRIPTION + ", "
                + AddressTable.Columns.DIRECTION + ", "
                + AddressTable.Columns.PHONE
                + " FROM " + AddressTable.TABLE_NAME;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);

        while (cursor.moveToNext()) {
            Address address = new Address(
                    cursor.getInt(cursor.getColumnIndex(AddressTable.Columns.INTERNAL_ID)),
                    cursor.getInt(cursor.getColumnIndex(AddressTable.Columns.ADDRESS_TYPE)),
                    cursor.getInt(cursor.getColumnIndex(AddressTable.Columns.CITY_CODE)),
                    cursor.getInt(cursor.getColumnIndex(AddressTable.Columns.HS_ADDRESS_ID)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.FLOOR)),
                    cursor.getInt(cursor.getColumnIndex(AddressTable.Columns.DEFAULT_ADDRESS)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.DESCRIPTION)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.PHONE)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.DIRECTION)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.STREET)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.BUILDING)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.CITY_NAME)),
                    cursor.getDouble(cursor.getColumnIndex(AddressTable.Columns.GEO_LONG)),
                    cursor.getDouble(cursor.getColumnIndex(AddressTable.Columns.GEO_LAT))
            );
            addressList.add(address);
        }
        cursor.close();

        return addressList;
    }

    public void updateAddress(Address address) {
        SQLiteDatabase db = database.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(AddressTable.Columns.HS_ADDRESS_ID, address.getHsAddressId());
        values.put(AddressTable.Columns.CITY_CODE, address.getCityCode());
        values.put(AddressTable.Columns.FLOOR, address.getFloor());
        values.put(AddressTable.Columns.DEFAULT_ADDRESS, address.getDefaultAddress());
        values.put(AddressTable.Columns.ADDRESS_TYPE, address.getAddressType());
        values.put(AddressTable.Columns.GEO_LAT, address.getGeoLat());
        values.put(AddressTable.Columns.GEO_LONG, address.getGeoLong());
        values.put(AddressTable.Columns.BUILDING, address.getBldg());
        values.put(AddressTable.Columns.STREET, address.getStreet());
        values.put(AddressTable.Columns.CITY_NAME, address.getCityName());
        values.put(AddressTable.Columns.DIRECTION, address.getDirection());
        values.put(AddressTable.Columns.DESCRIPTION, address.getDescription());
        values.put(AddressTable.Columns.PHONE, address.getPhone());

        db.update(AddressTable.TABLE_NAME, values, AddressTable.Columns.INTERNAL_ID + " = " + address.getAddressId(), null);
        db.close();
    }

    public void resetDefaultAddress(int addressId) {
        String query = "UPDATE " + AddressTable.TABLE_NAME
                + " SET " + AddressTable.Columns.DEFAULT_ADDRESS
                + " = 0 "
                + " WHERE " + AddressTable.Columns.INTERNAL_ID
                + " = " + addressId;

        database.getWritableDatabase().execSQL(query);

    }

    public int getDefaultAddressId() {
        String query = "SELECT " + AddressTable.Columns.CITY_CODE
                + " FROM " + AddressTable.TABLE_NAME
                + " WHERE " + AddressTable.Columns.DEFAULT_ADDRESS
                + " = 1";
        Cursor cursor = database.getWritableDatabase().rawQuery(query, null);
        if (cursor.moveToNext()) {
            int cityCode = cursor.getInt(0);
            cursor.close();

            return cityCode;
        }
        cursor.close();

        return -1;
    }

    @SuppressLint("Range")
    public Address getDefaultAddress() {
        Address address;
        String query = "SELECT *  FROM " + AddressTable.TABLE_NAME
                + " WHERE " + AddressTable.Columns.DEFAULT_ADDRESS
                + " = 1";
        Cursor cursor = database.getWritableDatabase().rawQuery(query, null);
        if (cursor.moveToNext()) {
            address = new Address(
                    cursor.getInt(cursor.getColumnIndex(AddressTable.Columns.INTERNAL_ID)),
                    cursor.getInt(cursor.getColumnIndex(AddressTable.Columns.ADDRESS_TYPE)),
                    cursor.getInt(cursor.getColumnIndex(AddressTable.Columns.CITY_CODE)),
                    cursor.getInt(cursor.getColumnIndex(AddressTable.Columns.HS_ADDRESS_ID)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.FLOOR)),
                    cursor.getInt(cursor.getColumnIndex(AddressTable.Columns.DEFAULT_ADDRESS)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.DESCRIPTION)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.PHONE)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.DIRECTION)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.STREET)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.BUILDING)),
                    cursor.getString(cursor.getColumnIndex(AddressTable.Columns.CITY_NAME)),
                    cursor.getDouble(cursor.getColumnIndex(AddressTable.Columns.GEO_LONG)),
                    cursor.getDouble(cursor.getColumnIndex(AddressTable.Columns.GEO_LAT))
            );

            cursor.close();

            return address;
        }
        cursor.close();

        return null;
    }

    public void deleteAddress(int addressId) {
        String query = "DELETE FROM " + AddressTable.TABLE_NAME
                + " WHERE " + AddressTable.Columns.INTERNAL_ID
                + " = " + addressId;

        database.getWritableDatabase().execSQL(query);


    }

    //#endregion

    //#region Order history

    public void insertOrderHistory(OrderHistory orderHistory) {
        SQLiteDatabase db = database.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(OrderHistoryTable.Columns.IS_NEW, orderHistory.getIsNew());
        values.put(OrderHistoryTable.Columns.TRANS_ID, orderHistory.getOrderId());
        values.put(OrderHistoryTable.Columns.TRANS_DATE, orderHistory.getTransDate());
        values.put(OrderHistoryTable.Columns.SCHEDULE_DATE, orderHistory.getScheduleDate());
        values.put(OrderHistoryTable.Columns.TOTAL_ITEMS, orderHistory.getTotalItems());
        values.put(OrderHistoryTable.Columns.TOTAL_PRICE, orderHistory.getTotalPrice());
        values.put(OrderHistoryTable.Columns.ORDER_TYPE, orderHistory.getOrderType());
        values.put(OrderHistoryTable.Columns.CART_DATA, orderHistory.getCartItemList());
        values.put(OrderHistoryTable.Columns.DELIVERY_CHARGE, orderHistory.getDeliveryCharge());
        values.put(OrderHistoryTable.Columns.BranchName, orderHistory.getBranchName());
        values.put(OrderHistoryTable.Columns.ORDER_STATUS, orderHistory.getOrderStatus());
        values.put(OrderHistoryTable.Columns.ORDER_STATUS, orderHistory.getRemark());
        values.put(OrderHistoryTable.Columns.BRANCH_ID, orderHistory.getBranchId());
        values.put(OrderHistoryTable.Columns.REMARK, orderHistory.getRemark());
        values.put(OrderHistoryTable.Columns.CONVERSION_RATE, orderHistory.getConversionRate());
        values.put(OrderHistoryTable.Columns.DEFAULT_CURRENCY, orderHistory.getDefaultCurrency());
        values.put(OrderHistoryTable.Columns.PRICE_LIST_CURRENCY, orderHistory.getPriceListCurrency());

        db.insert(OrderHistoryTable.TABLE_NAME, null, values);
        db.close();
    }

    @SuppressLint("Range")
    public List<OrderHistory> getOrderHistoryList(int count) {

        String query = "SELECT * FROM " + OrderHistoryTable.TABLE_NAME
                + " ORDER BY " + OrderHistoryTable.Columns.TRANS_DATE
                + " DESC ";
        if (count != -1) {
            query += " LIMIT " + count;
        }

        List<OrderHistory> orderHistoryList = new ArrayList<>();
        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        while (cursor.moveToNext()) {
            orderHistoryList.add(new OrderHistory(
                    cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.IS_NEW)),
                    cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.TRANS_ID)),
                    cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.ORDER_TYPE)),
                    cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.ORDER_STATUS)),
                    cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.TOTAL_ITEMS)),
                    cursor.getDouble(cursor.getColumnIndex(OrderHistoryTable.Columns.TOTAL_PRICE)),
                    cursor.getDouble(cursor.getColumnIndex(OrderHistoryTable.Columns.DELIVERY_CHARGE)),
                    cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.BRANCH_ID)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.CART_DATA)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.TRANS_DATE)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.SCHEDULE_DATE)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.BranchName)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.REMARK)),
                    cursor.getDouble(cursor.getColumnIndex(OrderHistoryTable.Columns.CONVERSION_RATE)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.DEFAULT_CURRENCY)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.PRICE_LIST_CURRENCY))
            ));
        }
        cursor.close();

        return orderHistoryList;
    }

    @SuppressLint("Range")
    public List<OrderHistory> getPendingOrderHistoryList() {

        String query = "SELECT * FROM " + OrderHistoryTable.TABLE_NAME
                + " WHERE " + OrderHistoryTable.Columns.ORDER_STATUS
                + " NOT IN (5,11)"
                + " ORDER BY " + OrderHistoryTable.Columns.TRANS_DATE
                + " DESC ";


        List<OrderHistory> orderHistoryList = new ArrayList<>();
        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        while (cursor.moveToNext()) {
            Log.d(TAG, "getPendingOrderHistoryList: order " + cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.TRANS_ID)));
            Log.d(TAG, "getPendingOrderHistoryList: status " + cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.ORDER_STATUS)));
            orderHistoryList.add(new OrderHistory(
                    cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.IS_NEW)),
                    cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.TRANS_ID)),
                    cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.ORDER_TYPE)),
                    cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.ORDER_STATUS)),
                    cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.TOTAL_ITEMS)),
                    cursor.getDouble(cursor.getColumnIndex(OrderHistoryTable.Columns.TOTAL_PRICE)),
                    cursor.getDouble(cursor.getColumnIndex(OrderHistoryTable.Columns.DELIVERY_CHARGE)),
                    cursor.getInt(cursor.getColumnIndex(OrderHistoryTable.Columns.BRANCH_ID)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.CART_DATA)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.TRANS_DATE)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.SCHEDULE_DATE)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.BranchName)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.REMARK)),
                    cursor.getDouble(cursor.getColumnIndex(OrderHistoryTable.Columns.CONVERSION_RATE)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.DEFAULT_CURRENCY)),
                    cursor.getString(cursor.getColumnIndex(OrderHistoryTable.Columns.PRICE_LIST_CURRENCY))
            ));
        }
        cursor.close();

        return orderHistoryList;
    }

    public int getCartHistoryCount() {
        String query = "SELECT COUNT(*)"
                + " FROM " + OrderHistoryTable.TABLE_NAME;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        if (cursor.moveToNext()) {
            int count = cursor.getInt(0);
            cursor.close();

            return count;
        }
        cursor.close();

        return 0;
    }

    public void updateOrderHistory(OrderHistory history) {
        SQLiteDatabase db = database.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(OrderHistoryTable.Columns.IS_NEW, history.getIsNew());
        values.put(OrderHistoryTable.Columns.ORDER_STATUS, history.getOrderStatus());
        values.put(OrderHistoryTable.Columns.TRANS_ID, history.getOrderId());

        db.update(OrderHistoryTable.TABLE_NAME, values, OrderHistoryTable.Columns.TRANS_ID + "=" + history.getOrderId(), null);
        db.close();
    }

    //#endregion

    //#region Order

    public void replaceOrder(Order order) {
        cleanTable(OrderTable.TABLE_NAME);

        String query = "INSERT INTO " + OrderTable.TABLE_NAME + " ( "
                + OrderTable.Columns.CART_ITEM_LIST + ") "
                + "VALUES ("
                + "'" + order.getCartItemList() + "')";

        Log.d(TAG, "insert order: " + query);
        database.getWritableDatabase().execSQL(query);

    }

    @SuppressLint("Range")
    public Order getCurrentOrder() {
        Order order = null;
        String query = "SELECT " + OrderTable.Columns.CART_ITEM_LIST
                + " FROM " + OrderTable.TABLE_NAME;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        if (cursor.moveToNext()) {
            order = new Order(cursor.getString(cursor.getColumnIndex(OrderTable.Columns.CART_ITEM_LIST)));
        }
        cursor.close();

        return order;
    }

    //#endregion

    //#region Price List

    public void insertPriceList(PriceList priceList) {
        SQLiteDatabase db = database.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PriceListTable.Columns.CURRENCY_DESCRIPTION, priceList.getCurrencyName());
        contentValues.put(PriceListTable.Columns.CONVERSION, priceList.getConversion());
        contentValues.put(PriceListTable.Columns.DECIMALS, priceList.getDecimals());
        contentValues.put(PriceListTable.Columns.MASK, priceList.getMask());
        contentValues.put(PriceListTable.Columns.SHOW_IN_PRICE_LIST_CURRENCY, priceList.getShouldShowCurr());
        db.insert(PriceListTable.TABLE_NAME, null, contentValues);
        db.close();
    }

    @SuppressLint("Range")
    public PriceList getPriceList() {
        PriceList priceList = null;
        String query = "SELECT * from " + PriceListTable.TABLE_NAME;
        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        if (cursor.moveToNext()) {
            priceList = new PriceList(
                    cursor.getInt(cursor.getColumnIndex(PriceListTable.Columns.DECIMALS)),
                    cursor.getDouble(cursor.getColumnIndex(PriceListTable.Columns.CONVERSION)),
                    cursor.getString(cursor.getColumnIndex(PriceListTable.Columns.CURRENCY_DESCRIPTION)),
                    cursor.getString(cursor.getColumnIndex(PriceListTable.Columns.MASK))
            );
            priceList.setShouldShowCurr(cursor.getInt(cursor.getColumnIndex(PriceListTable.Columns.SHOW_IN_PRICE_LIST_CURRENCY)));
        }
        cursor.close();
        return priceList;
    }

    public Boolean shouldShowInPriceListCurrency() {
        boolean result = false;
        PriceList priceList = getPriceList();
        if (priceList != null) {
            result = priceList.getShouldShowCurr() == 1;
        }
        return result;
    }

    //#endregion

    //#region Others

    public void insertStock(ProductStock stock) {
        String query =
                "INSERT INTO " + ProductStockTable.TABLE_NAME + "("
                        + ProductStockTable.Columns.PROD_NUM + ", "
                        + ProductStockTable.Columns.BRANCH_ID + ", "
                        + ProductStockTable.Columns.VARIATION_1 + ", "
                        + ProductStockTable.Columns.VARIATION_2 + ", "
                        + ProductStockTable.Columns.STOCK + ", "
                        + ProductStockTable.Columns.UPDATED_AT + ")"
                        + "VALUES("
                        + "" + stock.getProdNum() + ", "
                        + "" + stock.getBranchId() + ", "
                        + "" + stock.getVariation1() + ", "
                        + "" + stock.getVariation2() + ", "
                        + "" + stock.getStock() + ", "
                        + "'" + stock.getUpdatedAt() + "')";

        Log.d(TAG, "insertCategory: " + query);
        database.getWritableDatabase().execSQL(query);

    }

    public int getProductStock(int prodNum) {
        String query = "SELECT " + ProductStockTable.Columns.STOCK
                + " FROM " + ProductStockTable.TABLE_NAME
                + " WHERE " + ProductStockTable.Columns.PROD_NUM
                + " = " + prodNum;
        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        if (cursor.moveToNext()) {
            int stock = cursor.getInt(0);
            cursor.close();

            return stock;
        }
        cursor.close();

        return 0;
    }

    private void insertCategories() {
        List<Categories> categoriesList = new ArrayList<>();
        int catId, menuId, parentId, seq;
        String description, description2, catName, picPath, picture, thumb_picture, footerPicPath, bigPicPath, headerPicPath;

        cleanTable(CategoriesTable.TABLE_NAME);
        for (int i = 0; i < 10; i++) {
            catId = i;
            menuId = i;
            parentId = 0;
            headerPicPath = "";
            seq = 0;
            description = "Description " + i;
            description2 = "";
            catName = "Cat " + i;
            picPath = "";
            picture = "";
            thumb_picture = "";
            footerPicPath = "";
            bigPicPath = "";

            Categories category = new Categories(catId, menuId, parentId, catName, description, description2, picPath, picture, thumb_picture, headerPicPath, footerPicPath, bigPicPath, seq);
            categoriesList.add(category);
        }
        insertCategoryList(categoriesList);
    }

    public void updateAllPrices() {
        PriceList priceList = getPriceList();
    }

    //#endregion
}
