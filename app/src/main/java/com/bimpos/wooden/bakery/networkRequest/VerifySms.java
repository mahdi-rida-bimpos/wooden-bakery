package com.bimpos.wooden.bakery.networkRequest;

import static com.bimpos.wooden.bakery.helpers.Constant.UID;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.helpers.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class VerifySms extends AsyncTask<String, String, String> {

    private static final String TAG = "VerifySms";
    HttpURLConnection conn;
    private final SharedPreferences prefs;
    private final OnSmsVerified listener;
    private final String mobileNumber;
    private final String validationCode;
    URL url = null;

    public interface OnSmsVerified {
        void onSmsVerified(int status);
    }

    public VerifySms(String mobileNumber, OnSmsVerified listener, String validationCode) {
        prefs = MainApplication.getPreferences();
        this.listener = listener;
        this.mobileNumber = mobileNumber;
        this.validationCode = validationCode;
    }

    @Override
    protected String doInBackground(String... params) {

        try {
            url = new URL(Constant.BASE_URL + "devices"
                    + "?data={\"deviceIdentifier\":\"" + prefs.getString(UID,"-1")+"\""
                    + ", \"mobile\":\""+mobileNumber+"\""
                    + ", \"validationCode\":\""+validationCode+"\"}");

            Log.d(TAG, "doInBackground: url " + url);
            conn = (HttpURLConnection) url.openConnection();
            String token = prefs.getString(Constant.TOKEN, "");
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestMethod("PUT");
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(10000);
            conn.connect();
            int response_code = conn.getResponseCode();
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result " + result);
                return (result.toString());
            } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                return ("wrong");
            } else {
                return ("notFound");
            }

        } catch (Exception e) {
            try {
                Log.d(TAG, "doInBackground: error " + e.getMessage() + ", " + conn.getResponseMessage());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, "onPostExecute: result " + result);
        try {
            JSONObject parentObject = new JSONObject(result);
            String status = parentObject.getString("STATUS");
            if (status.equalsIgnoreCase("success")) {
                listener.onSmsVerified(1);
            } else {
                listener.onSmsVerified(0);
            }
        } catch (JSONException e) {
            listener.onSmsVerified(0);
        }

    }
}
