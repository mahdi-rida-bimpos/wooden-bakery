package com.bimpos.wooden.bakery.models;

import java.util.ArrayList;
import java.util.List;

public class CakeGallery {
    private final int galleryId;
    private int isActive;
    private final String galleryName;
    private List<CakeItem> cakeItemList;

    public CakeGallery(int galleryId, int isActive, String galleryName) {
        this.galleryId = galleryId;
        this.isActive = isActive;
        this.galleryName = galleryName;
        this.cakeItemList = new ArrayList<>();
    }

    public int getGalleryId() {
        return galleryId;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getGalleryName() {
        return galleryName;
    }

    public List<CakeItem> getGalleryPhotosList() {
        return cakeItemList;
    }

    public void setGalleryPhotosList(List<CakeItem> cakeItemList) {
        this.cakeItemList = cakeItemList;
    }
}
