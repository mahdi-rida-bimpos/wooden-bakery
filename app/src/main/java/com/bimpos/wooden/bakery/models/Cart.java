package com.bimpos.wooden.bakery.models;


import android.util.Log;

import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.helpers.AddCurrentOrder;
import com.bimpos.wooden.bakery.helpers.Constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Cart {

    private List<CartProduct> cartItemsList = new ArrayList<>();
    private static Cart cartData = null;
    private String remark;
    private static final String TAG = "Cart";

    public static Cart getCartData() {
        if (cartData == null) {
            cartData = new Cart();
        }
        return cartData;
    }

    public void setCartItemsList(List<CartProduct> cartItemsList) {
        this.cartItemsList = cartItemsList;
        replaceCurrentOrder();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void addProduct(Products newProduct, String remark, int qty) {
        Log.d(TAG, "addProduct: start " + remark);
        int found = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct product = cartItemsList.get(i);
            if ((product.getProdNum() == newProduct.getProdNum()) && product.getComboList().size() == 0 && product.getQuestionList().size() == 0 && product.getRemark().equalsIgnoreCase(remark)) {
                Log.d(TAG, "addProduct: found " + product.getRemark());
                int currQty = product.getQty();
                currQty = currQty + qty;
                product.setQty(currQty);
                found = 1;
                break;
            }
        }
        Log.d(TAG, "addProduct: found " + found);
        if (found == 0) {
            cartItemsList.add(new CartProduct(
                    Constant.cartId,
                    newProduct.getProdNum(),
                    newProduct.getDescription(),
                    qty,
                    newProduct.getFinalPrice(),
                    newProduct.getComboGroupId(),
                    newProduct.getModifiersGroupId(),
                    remark,
                    new ArrayList<>(),
                    new ArrayList<>()
            ));
            Constant.cartId++;
        }
        replaceCurrentOrder();
    }

    public void removeProduct(Products products) {
        Log.d(TAG, "removeProduct: start " + cartItemsList.size());
        for (int i = cartItemsList.size() - 1; i >= 0; i--) {
            Log.d(TAG, "removeProduct:  i=" + i);
            CartProduct cartItems = cartItemsList.get(i);
            if (cartItems.getProdNum() == products.getProdNum()) {
                int qty = cartItems.getQty();
                if (qty > 1) {
                    qty--;
                    Log.d(TAG, "removeProduct: set qty " + qty);
                    cartItems.setQty(qty);
                    replaceCurrentOrder();
                    return;
                }
                Log.d(TAG, "removeProduct: remove product");
                cartItemsList.remove(cartItems);
                replaceCurrentOrder();
                return;
            }
        }
    }

    public void removeProductFromCart(Products products) {
        for (int i = cartItemsList.size() - 1; i >= 0; i--) {
            Log.d(TAG, "removeProduct:  i=" + i);
            CartProduct cartItems = cartItemsList.get(i);
            if (cartItems.getProdNum() == products.getProdNum()) {
                cartItemsList.remove(cartItems);
            }
        }
        replaceCurrentOrder();
    }

    public void addProductWithModifiers(Products products, String remark, List<CartQuestionHeader> cartQuestionHeaderList, List<CartComboHeader> cartComboHeaderList, int qty) {

        cartItemsList.add(new CartProduct(
                Constant.cartId,
                products.getProdNum(),
                products.getDescription(),
                qty,
                products.getFinalPrice(),
                products.getComboGroupId(),
                products.getModifiersGroupId(),
                remark,
                cartQuestionHeaderList,
                cartComboHeaderList
        ));
        replaceCurrentOrder();
        Constant.cartId++;
    }

    public void addItem(int cartId) {
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItems = cartItemsList.get(i);
            if (cartItems.getCartId() == cartId) {
                int qty = cartItems.getQty();
                qty++;
                cartItems.setQty(qty);
                Log.d(TAG, "addItem: item added");
                replaceCurrentOrder();
                return;
            }
        }
    }

    public void removeItem(int cartId) {
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItems = cartItemsList.get(i);
            if (cartItems.getCartId() == cartId) {
                int qty = cartItems.getQty();
                if (qty > 1) {
                    qty--;
                    cartItems.setQty(qty);
                    replaceCurrentOrder();
                    return;
                }
                cartItemsList.remove(cartItems);
                replaceCurrentOrder();
                Log.d(TAG, "removeItem: item removed");
                return;
            }
        }
    }

    public int getProductCount(int prodNum) {
        int qty = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            if (cartItem.getProdNum() == prodNum) {
                qty += cartItem.getQty();
            }
        }
        return qty;
    }

    public int getProductCountInCart(int prodNum) {
        int qty = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            if (cartItem.getProdNum() == prodNum) {
                qty++;
            }
        }
        return qty;
    }

    public List<CartProduct> getCartByProductId(int prodNum) {
        List<CartProduct> productList = new ArrayList<>();
        for (int i = 0; i < cartItemsList.size(); i++) {
            if (cartItemsList.get(i).getProdNum() == prodNum) {
                productList.add(cartItemsList.get(i));
            }
        }
        return productList;
    }

    public int getCartProductCount(int cartId) {
        int qty = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            if (cartItem.getCartId() == cartId) {
                qty += cartItem.getQty();
            }
        }
        return qty;
    }

    public void modifyCartProducts(HashMap<Integer, Integer> list) {
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartProduct = cartItemsList.get(i);
            int cartId = cartProduct.getCartId();
            if (list.containsKey(cartId) && list.get(cartId) != null) {
                if (list.get(cartId) == 0) {
                    cartItemsList.remove(cartProduct);
                } else {
                    cartItemsList.get(i).setQty(list.get(cartId));
                }

            }
        }
    }

    public int getCartCount() {
        int count = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            if (cartItem.getIsRemoved() == 0) {
                count += cartItem.getQty();
            }
        }
        return count;
    }

    public double getOrderTotalPrice() {
        double price = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            if (cartItem.getIsRemoved() == 0) {
                price += (cartItem.getPrice() * cartItem.getQty());

                if (null != cartItem.getQuestionList() && cartItem.getQuestionList().size() != 0) {
                    for (int ii = 0; ii < cartItem.getQuestionList().size(); ii++) {
                        CartQuestionHeader cartQuestionHeader = cartItem.getQuestionList().get(ii);
                        for (int iii = 0; iii < cartQuestionHeader.getCartQuestionModifiers().size(); iii++) {
                            CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(iii);
                            price += modifier.getPrice() * cartItem.getQty();
                        }
                    }
                }
                if (null != cartItem.getComboList() && cartItem.getComboList().size() != 0) {
                    for (int ii = 0; ii < cartItem.getComboList().size(); ii++) {
                        CartComboHeader cartComboHeader = cartItem.getComboList().get(ii);
                        for (int iii = 0; iii < cartComboHeader.getCartComboModifiers().size(); iii++) {
                            CartComboModifier modifier = cartComboHeader.getCartComboModifiers().get(iii);
                            price += modifier.getPrice() * cartItem.getQty() * modifier.getCount();
                        }
                    }
                }
            }

        }
        return price;
    }

    public double getCartTotalPrice() {
        double price = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            if (cartItem.getIsRemoved() == 0) {
                price += (cartItem.getPrice() * cartItem.getQty());

                if (null != cartItem.getQuestionList() && cartItem.getQuestionList().size() != 0) {
                    for (int ii = 0; ii < cartItem.getQuestionList().size(); ii++) {
                        CartQuestionHeader cartQuestionHeader = cartItem.getQuestionList().get(ii);
                        for (int iii = 0; iii < cartQuestionHeader.getCartQuestionModifiers().size(); iii++) {
                            CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(iii);
                            price += modifier.getPrice() * cartItem.getQty();
                        }
                    }
                }
                if (null != cartItem.getComboList() && cartItem.getComboList().size() != 0) {
                    for (int ii = 0; ii < cartItem.getComboList().size(); ii++) {
                        CartComboHeader cartComboHeader = cartItem.getComboList().get(ii);
                        for (int iii = 0; iii < cartComboHeader.getCartComboModifiers().size(); iii++) {
                            CartComboModifier modifier = cartComboHeader.getCartComboModifiers().get(iii);
                            price += modifier.getPrice() * cartItem.getQty() * modifier.getCount();
                        }
                    }
                }
            }
        }
        return price + Constant.SETTINGS_DELIVERY_CHARGE;
    }

    public List<CartProduct> getCartItemsList() {
        return cartItemsList;
    }

    public void clear() {
        cartItemsList.clear();
        clearCurrentOrder();
    }

    private void clearCurrentOrder() {
        AddCurrentOrder.removeCurrentOrder();
    }

    public void replaceCurrentOrder() {
        AddCurrentOrder.replaceOrder(cartItemsList);
    }

}


