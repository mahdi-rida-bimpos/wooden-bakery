package com.bimpos.wooden.bakery.helpers;

import android.annotation.SuppressLint;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GetTime {


    @SuppressLint("SimpleDateFormat")
    public static String getFullTime(Long timeStamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timeStamp);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        return sdf.format(d);
    }

    @SuppressLint("SimpleDateFormat")
    public static String getFullTimeAmPm(Long timeStamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timeStamp);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
        return sdf.format(d);
    }

    public static boolean isDifferentDay(Long date1, Long date2) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(date1);
        int firstDay = c.get(Calendar.DAY_OF_MONTH);
        c.setTimeInMillis(date2);
        int secondDay = c.get(Calendar.DAY_OF_MONTH);
        return firstDay != secondDay;
    }

    @SuppressLint("SimpleDateFormat")
    public static int getSimpleDate(Long timeStamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timeStamp);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return Integer.parseInt(sdf.format(d));
    }

    @SuppressLint("SimpleDateFormat")
    public static String getTimeOnly(Long timeStamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timeStamp);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        return sdf.format(d);
    }

    @SuppressLint("SimpleDateFormat")
    public static String getBirthDate(Long timeStamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timeStamp);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(d);
    }
}
