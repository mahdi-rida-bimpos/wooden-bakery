package com.bimpos.wooden.bakery.models;

import java.io.Serializable;

public class Categories implements Serializable {

    private int catId;
    private int menuId;
    private int parentId;
    private String catName;
    private String description;
    private String description2;
    private String picpath;
    private String picture;
    private String thumb_picture;
    private String headerpicpath;
    private String footerpicpath;
    private String bgpicpath;
    private boolean selected;
    private int seq;

    public Categories() {
    }

    public Categories(int catId, int menuId, int parentid, String catname, String descript, String descript2, String picpath, String picture, String thumb_picture, String headerpicpath, String footerpicpath, String bgpicpath, int seq) {
        this.catId = catId;
        this.menuId = menuId;
        this.parentId = parentid;
        this.catName = catname;
        this.description = descript;
        this.description2 = descript2;
        this.picpath = picpath;
        this.picture = picture;
        this.thumb_picture = thumb_picture;
        this.headerpicpath = headerpicpath;
        this.footerpicpath = footerpicpath;
        this.bgpicpath = bgpicpath;
        this.seq = seq;
        this.selected=false;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public int getMenuId() {
        return menuId;
    }

    public int getParentId() {
        return parentId;
    }

    public String getCatName() {
        return catName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription2() {
        return description2;
    }

    public String getPicpath() {
        return picpath;
    }

    public void setPicpath(String picpath) {
        this.picpath = picpath;
    }

    public String getPicture() {
        return picture;
    }

    public String getThumb_picture() {
        return thumb_picture;
    }

    public String getHeaderpicpath() {
        return headerpicpath;
    }

    public String getFooterpicpath() {
        return footerpicpath;
    }

    public String getBgpicpath() {
        return bgpicpath;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    @Override
    public String toString() {
        return "Categories{" +
                "catId=" + catId +
                ", menuId=" + menuId +
                ", parentId=" + parentId +
                ", catName='" + catName + '\'' +
                ", description='" + description + '\'' +
                ", description2='" + description2 + '\'' +
                ", picpath='" + picpath + '\'' +
                ", picture='" + picture + '\'' +
                ", thumb_picture='" + thumb_picture + '\'' +
                ", headerpicpath='" + headerpicpath + '\'' +
                ", footerpicpath='" + footerpicpath + '\'' +
                ", bgpicpath='" + bgpicpath + '\'' +
                ", selected=" + selected +
                ", seq=" + seq +
                '}';
    }
}
