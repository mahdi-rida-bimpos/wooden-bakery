package com.bimpos.wooden.bakery.orderHistory.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityOrderHistoryDetailsBinding;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.helpers.CustomLoadingOrder;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.helpers.GetStatus;
import com.bimpos.wooden.bakery.helpers.GetTime;
import com.bimpos.wooden.bakery.models.Branches;
import com.bimpos.wooden.bakery.models.Cart;
import com.bimpos.wooden.bakery.models.CartComboHeader;
import com.bimpos.wooden.bakery.models.CartComboModifier;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.models.CartQuestionHeader;
import com.bimpos.wooden.bakery.models.CartQuestionModifier;
import com.bimpos.wooden.bakery.models.OrderHistory;
import com.bimpos.wooden.bakery.networkRequest.CheckNewPrices;
import com.bimpos.wooden.bakery.networkRequest.GetToken;
import com.bimpos.wooden.bakery.orderHistory.adapters.OrderHistoryDetailsRvAdapter;
import com.bimpos.wooden.bakery.orderOnline.activities.CartOrderActivity;
import com.bimpos.wooden.bakery.receivers.CheckConnectivityAsync;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.List;

public class OrderHistoryDetailsActivity extends AppCompatActivity implements CustomDialog.DialogEvents, CheckConnectivityAsync.OnConnectionComplete, CheckNewPrices.OnCheckNewPrice, GetToken.OnTokenReceived {

    private static final int DIALOG_REORDER_CONFIRM_MESSAGE = 1;
    private final int DIALOG_NOTHING = 2;
    private final int DIALOG_EXIT = 4;
    private final int DIALOG_FOUND_DELETED_AND_PROCEED = 3;
    private ActivityOrderHistoryDetailsBinding binding;
    public OrderHistory history;
    private DatabaseHelper databaseHelper;
    private List<CartProduct> cartItemList;
    private CustomDialog dialog;
    private final Calendar calendar = Calendar.getInstance();
    private static final String TAG = "OrderHistoryDetailsActivity";
    private long mLastClick = 0;
    private CustomLoadingOrder loadingOrder;
    private boolean deletedItemsFound = false;
    private int itemPosition = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOrderHistoryDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        getExtras();
        initClickListeners();
    }

    private void initClickListeners() {
        binding.reOrderLayout.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            showMessageDialog("Are you sure you want to place the same order again ?\n\nNote: Prices may change", DIALOG_REORDER_CONFIRM_MESSAGE);
        });

        binding.back.setOnClickListener(v -> onBackPressed());
    }

    private void initVariables() {
        dialog = new CustomDialog(this);
        databaseHelper = MainApplication.getDatabase();
        loadingOrder = new CustomLoadingOrder(this);
    }

    private void getExtras() {
        history = (OrderHistory) getIntent().getSerializableExtra(OrderHistory.class.getSimpleName());
        if (history == null) {
            finish();
        } else {
            fillContent();
            initRecyclerView();
        }
    }

    private void initRecyclerView() {

        Type orderCartType = new TypeToken<List<CartProduct>>() {
        }.getType();
        cartItemList = new Gson().fromJson(history.getCartItemList(), orderCartType);
        OrderHistoryDetailsRvAdapter adapter = new OrderHistoryDetailsRvAdapter(this, this, cartItemList);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);
    }

    private void fillContent() {

        binding.orderNumber.setText(String.valueOf(history.getOrderId()));

        double totalPrice = history.getTotalPrice();
        if (history.getConversionRate() == -1) {
            binding.totalPrice.setText(FormatPrice.getCustomFormattedPrice(totalPrice, history.getDefaultCurrency()));
        } else {
            binding.totalPrice.setText(FormatPrice.getCustomFormattedPrice(totalPrice, history.getPriceListCurrency()));
        }
        binding.orderStatus.setText(GetStatus.parseStatus(history.getOrderStatus()));

        if (history.getRemark().length() != 0) {
            binding.orderRemark.setText(history.getRemark());
        } else {
            binding.orderRemarkLayout.setVisibility(View.GONE);
        }

        if (history.getConversionRate() == -1) {
            binding.priceListLayout.setVisibility(View.GONE);
        } else {
            binding.priceListLayout.setVisibility(View.VISIBLE);
            binding.conversionRate.setText(String.format("1 %s -> %s %s",
                    history.getPriceListCurrency(),
                    history.getConversionRate(),
                    history.getDefaultCurrency()));
        }

        if (history.getDeliveryCharge() == 0) {
            binding.deliveryChargeLayout.setVisibility(View.GONE);
        } else {
            binding.deliveryChargeLayout.setVisibility(View.VISIBLE);

            if (history.getConversionRate() == -1) {
                binding.deliveryCharge.setText(FormatPrice.getCustomFormattedPrice(history.getDeliveryCharge(), history.getDefaultCurrency()));
            } else {
                binding.deliveryCharge.setText(FormatPrice.getCustomFormattedPrice(history.getDeliveryCharge() / history.getConversionRate(), history.getPriceListCurrency()));
            }
        }

        if (GetTime.isDifferentDay(Long.parseLong(history.getTransDate()), calendar.getTimeInMillis())) {
            binding.orderDate.setText(GetTime.getFullTimeAmPm(Long.parseLong(history.getTransDate())));
        } else {
            binding.orderDate.setText(GetTime.getTimeOnly(Long.parseLong(history.getTransDate())));
        }

        if (history.getScheduleDate().equalsIgnoreCase(history.getTransDate())) {
            //now
            binding.scheduleLayout.setVisibility(View.GONE);
            binding.scheduleText.setVisibility(View.GONE);
        } else {
            if (GetTime.isDifferentDay(Long.parseLong(history.getScheduleDate()), calendar.getTimeInMillis())) {
                binding.orderScheduleDate.setText(GetTime.getFullTimeAmPm(Long.parseLong(history.getScheduleDate())));
            } else {
                binding.orderScheduleDate.setText(GetTime.getTimeOnly(Long.parseLong(history.getScheduleDate())));
            }
            binding.scheduleLayout.setVisibility(View.VISIBLE);
            binding.scheduleText.setVisibility(View.VISIBLE);
        }

        String orderType;
        Branches branch = databaseHelper.getBranchById(history.getBranchId());

        if (branch != null) {
            if (history.getOrderType() == 1) {
                orderType = "Delivery (" + branch.getName() + " )";
            } else {
                orderType = "Pickup (" + branch.getName() + " )";
            }
            binding.orderType.setText(orderType);

            String phone = branch.getPhone();
            if (phone.length() != 0) {
                binding.call.setOnClickListener(v -> {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + phone));
                    startActivity(intent);
                });
            } else {
                binding.call.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.branch_call_grey_icon));
            }


            double longitude = branch.getLongitude();
            double latitude = branch.getLatitude();
            if (longitude != 0 || latitude != 0) {
                binding.map.setOnClickListener(v -> {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:<" + latitude + ">,<" + longitude + ">?q=<" + latitude + ">,<" + longitude + ">(" + branch.getName() + ")"));
                    startActivity(intent);
                });
            } else {
                binding.map.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.branch_map_grey_icon));
            }

        } else {

            if (history.getOrderType() == 1) {
                orderType = "Delivery";
            } else {
                orderType = "Pickup";
            }
            binding.orderType.setText(orderType);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void checkConnectivity() {
        loadingOrder.show();
        loadingOrder.startAnimation(R.raw.web_loading);

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            CheckConnectivityAsync checkConnectivityAsync = new CheckConnectivityAsync(this);
            checkConnectivityAsync.execute();
        } else {
            showMessageDialog("Please check your connection", DIALOG_NOTHING);
        }
    }

    @Override
    public void onConnectionComplete(boolean status) {
        if (status) {
            checkItemPrice();
        } else {
            showMessageDialog("Please check your connection", DIALOG_NOTHING);
        }
    }

    private void checkItemPrice() {
        Log.d(TAG, "checkNewPrices: start");
        CartProduct cartProduct = cartItemList.get(itemPosition);
        Log.d(TAG, "checkNewPrices: item " + cartProduct.toString());
        int prodNum = cartProduct.getProdNum();
        int lastItem = 0;
        if (itemPosition == cartItemList.size() - 1) {
            lastItem = 1;
        }
        new CheckNewPrices(cartProduct.getCartId(), prodNum, lastItem, this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        itemPosition++;
    }

    @Override
    public void onCheckNewPrice(int status, int lastItem, int prodNum, int cartId, double newPrice, double newPrice2) {
        Log.d(TAG, "onCheckNewPrice:  status " + status + ", last item " + lastItem + ", prod num " + prodNum + ", new price " + newPrice);
        if (status == -2) {
            showMessageDialog("Please try again later", DIALOG_NOTHING);
            itemPosition = 0;
            return;
        }
        if (status == -1) {
            //get token
            Log.d(TAG, "onCheckNewPrice: get token ");
            itemPosition = 0;
            new GetToken(OrderHistoryDetailsActivity.this).execute();

        } else if (status == 1) {
            for (int i = 0; i < cartItemList.size(); i++) {
                if (cartItemList.get(i).getProdNum() == prodNum && cartId == cartItemList.get(i).getCartId()) {
                    if (databaseHelper.shouldShowInPriceListCurrency()) {
                        cartItemList.get(i).setPrice(newPrice2);
                    } else {
                        cartItemList.get(i).setPrice(newPrice);
                    }
                    databaseHelper.updateProduct(prodNum, newPrice, newPrice2);
                }
            }
            if (lastItem == 1) {
                itemPosition = 0;
                if (deletedItemsFound) {
                    deletedItemsFound = false;
                    if (cartItemList.size() > 0) {
                        showMessageDialog("Some items no longer available", DIALOG_FOUND_DELETED_AND_PROCEED);
                    } else {
                        showMessageDialog("Items no longer exist", DIALOG_EXIT);
                    }
                } else {
                    reOrder();
                }
            } else {
                checkItemPrice();
            }

        } else {
            //prod not found, proceed anw
            for (int i = 0; i < cartItemList.size(); i++) {
                if (cartItemList.get(i).getProdNum() == prodNum && cartId == cartItemList.get(i).getCartId()) {
                    cartItemList.remove(i);
                    itemPosition--;
                    deletedItemsFound = true;
                    databaseHelper.removeProduct(prodNum);
                    break;
                }
            }
            if (lastItem == 1) {
                itemPosition = 0;
                if (deletedItemsFound) {
                    deletedItemsFound = false;
                    if (cartItemList.size() > 0) {
                        showMessageDialog("Some items no longer exist", DIALOG_FOUND_DELETED_AND_PROCEED);
                    } else {
                        showMessageDialog("Items no longer exist", DIALOG_EXIT);
                    }
                } else {
                    reOrder();
                }
            } else {
                checkItemPrice();
            }
        }
    }

    private void reOrder() {
        Cart cart = Cart.getCartData();
        cart.setCartItemsList(cartItemList);
        if (history.getRemark().length() != 0) {
            cart.setRemark(history.getRemark());
        }
        loadingOrder.dismiss();
        startActivity(new Intent(this, CartOrderActivity.class));
        OrderHistoryActivity.orderHistoryActivity.finish();
        finish();
    }

    @Override
    public void onTokenReceived(int status) {
        if (status == 1) {
            checkItemPrice();
        } else {
            itemPosition = 0;
            showMessageDialog("Please try again later", DIALOG_NOTHING);
        }
    }

    private void showMessageDialog(String message, int dialogId) {
        if (loadingOrder.isShowing()) {
            loadingOrder.dismiss();
        }
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, message);
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        switch (dialogId) {

            case DIALOG_EXIT:
            case DIALOG_FOUND_DELETED_AND_PROCEED:
            case DIALOG_NOTHING:
                arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Ok");
                arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
                break;

            case DIALOG_REORDER_CONFIRM_MESSAGE:
                arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Yes");
                arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "No");
                break;
        }
        arguments.putInt(CustomDialog.DIALOG_ID, dialogId);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        switch (dialogId) {
            case DIALOG_NOTHING:
                dialog.dismiss();
                break;

            case DIALOG_REORDER_CONFIRM_MESSAGE:
                dialog.dismiss();
                checkConnectivity();
                break;

            case DIALOG_FOUND_DELETED_AND_PROCEED:
                dialog.dismiss();
                reOrder();
                break;

            case DIALOG_EXIT:
                dialog.dismiss();
                finish();
                break;
        }
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        dialog.dismiss();
    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }


}