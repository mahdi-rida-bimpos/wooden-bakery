package com.bimpos.wooden.bakery.profile.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bimpos.wooden.bakery.BuildConfig;
import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.databinding.ActivityLoginBinding;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.helpers.CustomLoadingOrder;
import com.bimpos.wooden.bakery.helpers.NetworkUtil;
import com.bimpos.wooden.bakery.networkRequest.GetToken;
import com.bimpos.wooden.bakery.networkRequest.RequestSms;

public class LoginActivity extends AppCompatActivity implements RequestSms.OnSmsReceived, GetToken.OnTokenReceived, CustomDialog.DialogEvents {

    private static final String TAG = "LoginActivity";

    private ActivityLoginBinding binding;
    private String[] mobileStrings;
    private String strAll;
    private CustomLoadingOrder loading;
    private CustomDialog dialog;
    private int count=0;
    private SharedPreferences preferences;
    private boolean gotoWaitingSms = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initClickListener();
        binding.phoneNumber.requestFocus();
    }

    private void initVariables() {
        preferences = MainApplication.getPreferences();
        dialog = new CustomDialog(this);
        loading = new CustomLoadingOrder(this);
        SharedPreferences preferences = MainApplication.getPreferences();
        String mobilePrefix = preferences.getString(Constant.SETTINGS_MOBILE_PREFIX, "03,70,71,78,81");
        Log.d(TAG, "initVariables: mobile prefix " + mobilePrefix);
        mobileStrings = mobilePrefix.split(",");
    }

    private void initClickListener() {

        binding.back.setOnClickListener(v -> onBackPressed());

        binding.phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean isfound = false;

                strAll = "";
                for (String strmobile : mobileStrings) {
                    strAll = new StringBuilder().append(strAll).append(strmobile).append("\n").toString();
                    if (s.length() <= strmobile.length()) {
                        if (strmobile.substring(0, s.length()).equals(s.toString())) {
                            isfound = true;
                        }
                    } else {
                        isfound = true;
                        setLength();
                    }
                }
                if (!isfound) {
                    binding.phoneNumber.setError("Your mobile number should start with \n" + strAll);
                    binding.phoneNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(s.length())});
                } else {
                    setLength();
                }
            }
        });

        binding.login.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(binding.phoneNumber.getText().toString()) && binding.phoneNumber.getText().length() == 8) {
                if(NetworkUtil.getConnectivityStatusString(this)){
                    showAlertDialog();
                }else{
                    showNoInternetDialog();
                }
            }
        });

        binding.phoneNumber.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
    }

    private void showNoInternetDialog() {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Internet connection is needed to be able to Register");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Exit");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
        arguments.putInt(CustomDialog.DIALOG_ID, 2);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    private void showAlertDialog() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_sms_confirm);
        Button negativeButton = dialog.findViewById(R.id.btnEditSms);
        Button positiveButton = dialog.findViewById(R.id.btnYesSms);
        TextView txtNumber = dialog.findViewById(R.id.txtnumber);

        txtNumber.setText(binding.phoneNumber.getText().toString());

        positiveButton.setOnClickListener(view -> {
            dialog.dismiss();
            sendSmsRequest();
        });

        negativeButton.setOnClickListener(view -> dialog.dismiss());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.create();
        dialog.show();
    }

    private void sendSmsRequest() {
        loading.show();
        loading.startAnimation(R.raw.web_loading);
        RequestSms requestSms = new RequestSms(binding.phoneNumber.getText().toString(), this);
        requestSms.execute();
    }

    @Override
    public void onSmsReceived(int status) {
        if (status == -1) {
            if(count==2){
                showErrorDialog();
            }
            count++;
            getToken();
        } else if (status == 1) {
            gotoWaitingActivity();
        } else {
            loading.dismiss();
            showErrorDialog();
        }
    }

    private void gotoWaitingActivity() {
        loading.dismiss();
        preferences.edit().putInt(Constant.IS_SMS_SENT, 1).apply();
        preferences.edit().putString(Constant.PHONE_NUMBER, binding.phoneNumber.getText().toString()).apply();

        startActivity(new Intent(this,
                SmsVerificationActivity.class)
                .putExtra("phone", binding.phoneNumber.getText().toString()));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        gotoWaitingSms = true;
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        if (!gotoWaitingSms) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    public void setLength() {
        binding.phoneNumber.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(8)});
    }

    private void showErrorDialog() {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Something went wrong while sending request\nPlease try again in few minutes");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "ok");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
        arguments.putInt(CustomDialog.DIALOG_ID, 1);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    private void getToken() {
        GetToken getToken = new GetToken( this);
        getToken.execute();
    }

    @Override
    public void onTokenReceived(int status) {
        if (status == 1) {
            sendSmsRequest();
        }
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        dialog.dismiss();
        if(dialogId==1){
            finish();
        }
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {

    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }
}