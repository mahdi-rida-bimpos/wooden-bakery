package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.CartComboHeader;
import com.bimpos.wooden.bakery.models.CartComboModifier;
import com.bimpos.wooden.bakery.orderOnline.activities.ProductInfoActivity;

import java.util.List;

public class ComboModifierRvAdapter extends RecyclerView.Adapter<ComboModifierRvAdapter.ModifierSelectionViewHolder> {

    private final CartComboHeader cartComboHeader;
    private final List<CartComboModifier> cartComboModifierList;
    private final ProductInfoActivity activity;
    private static final String TAG = "ComboModifierRv";

    public ComboModifierRvAdapter(CartComboHeader cartComboHeader, ProductInfoActivity activity) {
        this.activity = activity;
        this.cartComboHeader = cartComboHeader;
        cartComboModifierList = cartComboHeader.getCartComboModifiers();
    }

    @NonNull
    @Override
    public ModifierSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_combo_modifier, parent, false);
        return new ModifierSelectionViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull ModifierSelectionViewHolder holder, int position) {

        CartComboModifier cartComboModifier = cartComboModifierList.get(position);

        holder.description.setText(cartComboModifier.getDescription());

        int max = cartComboHeader.getMax();

        Log.d(TAG, "onBindViewHolder: combo modifier id " + cartComboModifier.getDescription() + ", " + cartComboModifier.getModifierId());
        int cartComboHeaderCount = cartComboModifier.getCount();
        holder.count.setText(String.valueOf(cartComboHeaderCount));
        if (cartComboModifier.getPrice() > 0) {
            holder.price.setText(String.format("+ %s", FormatPrice.getFormattedPrice(cartComboModifier.getPrice())));
            holder.price.setVisibility(View.VISIBLE);
        } else {
            holder.price.setVisibility(View.GONE);
        }

        holder.plus.setOnClickListener(v -> {
            if (cartComboHeader.getSelectedItemCount() < max) {
                int count = cartComboModifier.getCount();
                count++;
                cartComboModifier.setCount(count);
                int selectedItemCount = cartComboHeader.getSelectedItemCount();
                selectedItemCount++;
                cartComboHeader.setSelectedItemCount(selectedItemCount);
                holder.count.setText(String.valueOf(count));
                if (cartComboModifier.getPrice() > 0) {
                    calculateExtraPrice();
                }
            }
        });

        holder.minus.setOnClickListener(v -> {
            int count = cartComboModifier.getCount();
            if (count > 0) {
                count--;
                cartComboModifier.setCount(count);
                holder.count.setText(String.valueOf(count));
                int selectedItemCount = cartComboHeader.getSelectedItemCount();
                selectedItemCount--;
                cartComboHeader.setSelectedItemCount(selectedItemCount);
                if (cartComboModifier.getPrice() > 0) {
                    calculateExtraPrice();
                }
            }
        });
    }

    public void calculateExtraPrice() {

        int totalPrice = 0;
        List<CartComboHeader> cartComboHeaderList = activity.recyclerAdapter.cartComboHeaderList;
        for (int i = 0; i < cartComboHeaderList.size(); i++) {
            CartComboHeader cartComboHeader = cartComboHeaderList.get(i);
            for (int j = 0; j < cartComboHeader.getCartComboModifiers().size(); j++) {
                CartComboModifier cartComboModifier = cartComboHeader.getCartComboModifiers().get(j);
                if (cartComboModifier.getCount() > 0) {
                    double price = cartComboModifier.getPrice() * cartComboModifier.getCount();
                    totalPrice += price;
                }
            }
        }
        if (totalPrice == 0) {
            activity.binding.extraLayout.animate().alpha(0f).setDuration(500).start();
        } else {
            if (activity.binding.extraLayout.getAlpha() == 0) {
                activity.binding.extraLayout.animate().alpha(1f).setDuration(500).start();
            }
        }
        int prodCount = Integer.parseInt(activity.binding.quantity.getText().toString());
        activity.binding.extraPrice.setText(String.format("+ %s", FormatPrice.getFormattedPrice(totalPrice * prodCount)));

    }

    @Override
    public int getItemCount() {
        return cartComboModifierList.size();
    }

    static class ModifierSelectionViewHolder extends RecyclerView.ViewHolder {
        TextView description, count, price;
        ImageView plus, minus;

        public ModifierSelectionViewHolder(@NonNull View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.recycler_combo_modifier_description);
            count = itemView.findViewById(R.id.recycler_combo_modifier_qty);
            plus = itemView.findViewById(R.id.recycler_combo_modifier_plus);
            minus = itemView.findViewById(R.id.recycler_combo_modifier_minus);
            price = itemView.findViewById(R.id.recycler_combo_modifier_price);
        }
    }

}