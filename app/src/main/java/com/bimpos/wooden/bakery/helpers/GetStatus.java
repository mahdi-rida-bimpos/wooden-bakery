package com.bimpos.wooden.bakery.helpers;

public class GetStatus {

    public static String parseStatus(int status) {

        switch (status) {
            case 0:
                return "Order Not Sent";

            case 1:
                return "Order Received";

            case 2:
            case 8:
                return "In Kitchen";

            case 7:
                return "Order Prepared and ready";

            case 10:
            case 3:
                return "Packing";//todo ask john

            case 4:
                return "Delivering";

            case 5:
                return "Delivered";

            case 6:
                return "Pending Accept";

            case 11:
                return "Order not Accepted";
        }
        return "";
    }
}
