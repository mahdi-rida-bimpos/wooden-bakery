package com.bimpos.wooden.bakery.about_us;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.databinding.ActivityAboutUsBinding;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomLoadingOrder;

public class AboutUsActivity extends AppCompatActivity {

    private ActivityAboutUsBinding binding;
    private CustomLoadingOrder loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAboutUsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initClickListeners();
        initWebView();
    }

    private void initClickListeners() {
        binding.back.setOnClickListener(view -> finish());
    }

    private void initWebView() {
        loading.show();
        loading.startAnimation(R.raw.web_loading);
        binding.webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        binding.webView.getSettings().setJavaScriptEnabled(true);

        binding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                loading.dismiss();
            }
        });

        binding.webView.loadUrl(Constant.WOODEN_BAKERY_URL);
    }

    private void initVariables() {
        loading = new CustomLoadingOrder(this);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}