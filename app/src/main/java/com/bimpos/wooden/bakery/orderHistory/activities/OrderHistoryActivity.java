package com.bimpos.wooden.bakery.orderHistory.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityOrderHistoryBinding;
import com.bimpos.wooden.bakery.models.OrderHistory;
import com.bimpos.wooden.bakery.networkRequest.GetToken;
import com.bimpos.wooden.bakery.networkRequest.ListAnOrder;
import com.bimpos.wooden.bakery.orderHistory.adapters.OrderHistoryRvAdapter;

import java.util.List;

public class OrderHistoryActivity extends AppCompatActivity implements ListAnOrder.OnListOrderComplete, GetToken.OnTokenReceived {

    private static final String TAG = "OrderHistoryActivity";
    private ActivityOrderHistoryBinding binding;
    private List<OrderHistory> orderHistoryList;
    private OrderHistoryRvAdapter adapter;
    private DatabaseHelper databaseHelper;
    private int sentItems = 0;
    public static OrderHistoryActivity orderHistoryActivity;
    private int receivedItems = 0;
    private boolean canLookup = true;
    private int firstEnter = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOrderHistoryBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initRecyclerView();
        initClickListener();
        initRadioButtonGroup();
        getOrderHistoryStatus();
    }

    private void initRadioButtonGroup() {
        binding.radioButtonPickUp.setOnPositionChangedListener((button, currentPosition, lastPosition) -> {
            if (currentPosition == 1) {
                //last 10
                orderHistoryList = databaseHelper.getOrderHistoryList(10);
            } else if (currentPosition == 2) {
                //all orders
                orderHistoryList = databaseHelper.getOrderHistoryList(-1);
            } else {
                //pending
                orderHistoryList = databaseHelper.getPendingOrderHistoryList();
            }
            adapter.setData(orderHistoryList);
        });
    }

    private void getOrderHistoryStatus() {
        binding.refreshLayout.setRefreshing(true);
        for (int i = 0; i < orderHistoryList.size(); i++) {
            OrderHistory history = orderHistoryList.get(i);
            if (history.getIsNew() == 1) {
                if (canLookup) {
                    sentItems++;
                    Log.d(TAG, "getOrderHistoryStatus: sent items " + sentItems);
                    new ListAnOrder(this,  history).execute();
                }
            }
        }
        if(sentItems==0){
            binding.refreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onListOrderComplete(int status, OrderHistory history) {
        Log.d(TAG, "onListOrderComplete: received order: " + history.getOrderId() + ", status: " + status);
        receivedItems++;
        if (status == -1) {
            Log.d(TAG, "onListOrderComplete: get token");

            new GetToken( this).execute();

        } else if (status == -2) {
            Log.d(TAG, "onListOrderComplete: ignored");
            if (sentItems == receivedItems) {
                Log.d(TAG, "onListOrderComplete: all items done");
                sentItems = 0;
                receivedItems = 0;
                binding.refreshLayout.setRefreshing(false);

                if (firstEnter == 1) {
                    firstEnter = 0;
                    orderHistoryList = databaseHelper.getPendingOrderHistoryList();
                    binding.radioButtonPickUp.setPosition(0);
                } else {
                    int currentPosition = binding.radioButtonPickUp.getPosition();
                    if (currentPosition == 1) {
                        orderHistoryList = databaseHelper.getOrderHistoryList(10);
                    } else if (currentPosition == 2) {
                        orderHistoryList = databaseHelper.getOrderHistoryList(-1);
                    } else {//0
                        orderHistoryList = databaseHelper.getPendingOrderHistoryList();
                    }
                }
                adapter.setData(orderHistoryList);
            }
        } else {
            Log.d(TAG, "onListOrderComplete: change history status");

            if (status == 5 || status == 11) {
                Log.d(TAG, "onListOrderComplete: set history as old");
                history.setIsNew(0);
            }
            history.setOrderStatus(status);
            databaseHelper.updateOrderHistory(history);
            if (sentItems == receivedItems) {
                Log.d(TAG, "onListOrderComplete: all items done");
                sentItems = 0;
                receivedItems = 0;
                binding.refreshLayout.setRefreshing(false);

                if (firstEnter == 1) {
                    firstEnter = 0;
                    orderHistoryList = databaseHelper.getPendingOrderHistoryList();
                    binding.radioButtonPickUp.setPosition(0);
                } else {
                    int currentPosition = binding.radioButtonPickUp.getPosition();
                    if (currentPosition == 1) {
                        orderHistoryList = databaseHelper.getOrderHistoryList(10);
                    } else if (currentPosition == 2) {
                        orderHistoryList = databaseHelper.getOrderHistoryList(-1);
                    } else {//0
                        orderHistoryList = databaseHelper.getPendingOrderHistoryList();
                    }
                }
                adapter.setData(orderHistoryList);
            }
        }
    }

    @Override
    public void onTokenReceived(int status) {
        Log.d(TAG, "onTokenReceived: start " + status);
        sentItems = 0;
        receivedItems = 0;
        if (status == 1) {
            canLookup = true;
            getOrderHistoryStatus();
        } else {
            binding.refreshLayout.setRefreshing(false);
        }
    }

    private void initClickListener() {
        binding.back.setOnClickListener(v -> finish());
        binding.refreshLayout.setOnRefreshListener(this::getOrderHistoryStatus);
    }

    private void initVariables() {
        orderHistoryActivity=this;
        databaseHelper = MainApplication.getDatabase();
        orderHistoryList = databaseHelper.getOrderHistoryList(10);
        adapter = new OrderHistoryRvAdapter(this, this, orderHistoryList);
    }

    private void initRecyclerView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


}