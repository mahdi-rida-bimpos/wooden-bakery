package com.bimpos.wooden.bakery.orderHistory.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.models.CartQuestionModifier;
import com.bimpos.wooden.bakery.orderHistory.activities.OrderHistoryDetailsActivity;

import java.util.List;

public class CartOrderHistoryQModifierRvAdapter extends RecyclerView.Adapter<CartOrderHistoryQModifierRvAdapter.ViewHolder> {

    private final List<CartQuestionModifier> cartQuestionModifierList;
    private final OrderHistoryDetailsActivity activity;
    private final CartProduct cartProduct;

    public CartOrderHistoryQModifierRvAdapter(OrderHistoryDetailsActivity activity, List<CartQuestionModifier> cartQuestionModifierList, CartProduct cartProduct) {
        this.cartQuestionModifierList = cartQuestionModifierList;
        this.activity = activity;
        this.cartProduct = cartProduct;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cart_order_question_modifier, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CartQuestionModifier cartQuestionModifier = cartQuestionModifierList.get(position);
        holder.modifierDescription.setText(String.format("%s", cartQuestionModifier.getDescription()));
        holder.qty.setText(String.valueOf(cartProduct.getQty()));

        double lastPrice = cartQuestionModifier.getPrice();

        if (lastPrice > 0) {
            if (activity.history.getConversionRate() == -1) {
                holder.price.setText(String.format("+ %s", FormatPrice.getCustomFormattedPrice(lastPrice * cartProduct.getQty(), activity.history.getDefaultCurrency())));
            } else {
                holder.price.setText(String.format("+ %s", FormatPrice.getCustomFormattedPrice(lastPrice * cartProduct.getQty(), activity.history.getPriceListCurrency())));
            }
        } else {
            holder.price.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return cartQuestionModifierList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView modifierDescription, price, qty;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            qty = itemView.findViewById(R.id.recycler_cart_order_question_modifier_count);
            price = itemView.findViewById(R.id.recycler_cart_order_question_modifier_price);
            modifierDescription = itemView.findViewById(R.id.recycler_cart_order_question_modifier_description);

        }
    }
}
