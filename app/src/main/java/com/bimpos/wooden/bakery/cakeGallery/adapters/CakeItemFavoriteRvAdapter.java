package com.bimpos.wooden.bakery.cakeGallery.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.cakeGallery.activities.CakeImageActivity;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.CakeItemFavorite;

import java.util.List;

public class CakeItemFavoriteRvAdapter extends RecyclerView.Adapter<CakeItemFavoriteRvAdapter.ViewHolder> {

    private final List<CakeItemFavorite> cakeItemFavoriteList;
    private final DatabaseHelper databaseHelper;
    private final Context context;
    private final OnEmptyFavoriteList listener;
    private static final String TAG = "CakeItemFavoriteRvAdapter";
    private long mLastClick = 0;

    public interface OnEmptyFavoriteList {
        void onEmptyFavoriteList();
    }


    public CakeItemFavoriteRvAdapter(List<CakeItemFavorite> cakeItemFavoriteList, Context context, OnEmptyFavoriteList listener) {
        this.context = context;
        this.listener = listener;
        this.cakeItemFavoriteList = cakeItemFavoriteList;
        databaseHelper = MainApplication.getDatabase();
    }

    @NonNull
    @Override
    public CakeItemFavoriteRvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cake_item, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull CakeItemFavoriteRvAdapter.ViewHolder holder, int position) {

        CakeItemFavorite cakeItemFavorite = cakeItemFavoriteList.get(position);
        Log.d(TAG, "onBindViewHolder: size " + cakeItemFavoriteList.size());


        holder.name.setText(cakeItemFavorite.getTitle());
        holder.description.setText(cakeItemFavorite.getDescription());
        holder.minOrder.setText(String.valueOf(cakeItemFavorite.getMinOrderQty()));
        holder.refNum.setText(cakeItemFavorite.getRefNum());
        holder.price.setText(FormatPrice.getFormattedPrice(cakeItemFavorite.getPrice()));
        holder.favImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_fill));

        holder.favImage.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            databaseHelper.deleteFavoriteCake(cakeItemFavorite.getCakeId());
            cakeItemFavoriteList.remove(cakeItemFavorite);
            notifyItemRemoved(holder.getAdapterPosition());
            if (cakeItemFavoriteList.size() == 0) {
                listener.onEmptyFavoriteList();
            }
        });

        if (cakeItemFavorite.getImage() != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(cakeItemFavorite.getImage(), 0, cakeItemFavorite.getImage().length);
            holder.image.setImageBitmap(bitmap);
        }

        if (cakeItemFavorite.getImage() != null) {
            holder.image.setOnClickListener(v -> {
                if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                    return;
                }
                mLastClick = SystemClock.elapsedRealtime();
                context.startActivity(new Intent(context, CakeImageActivity.class)
                        .putExtra("PictureUrl", "")
                        .putExtra("bitmap", cakeItemFavorite.getImage())
                        .putExtra("cakeTitle", cakeItemFavorite.getTitle()));
            });
        }
    }

    @Override
    public int getItemCount() {
        return cakeItemFavoriteList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, price, minOrder, refNum, description;
        ImageView image, favImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.recycler_cake_item_name);
            price = itemView.findViewById(R.id.recycler_cake_item_price);
            minOrder = itemView.findViewById(R.id.recycler_cake_item_minOrder);
            refNum = itemView.findViewById(R.id.recycler_cake_item_refNum);
            description = itemView.findViewById(R.id.recycler_cake_item_description);
            image = itemView.findViewById(R.id.recycler_cake_item_image);
            favImage = itemView.findViewById(R.id.recycler_cake_item_favBtn);
        }
    }

}
