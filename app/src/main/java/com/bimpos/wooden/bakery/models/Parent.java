package com.bimpos.wooden.bakery.models;

public class Parent {

    private int id;
    private final int isActive;
    private final int versionId;
    private final String descript;

    public Parent(int id, int isactive, int versionid, String descript) {
        this.id = id;
        this.isActive = isactive;
        this.versionId = versionid;
        this.descript = descript;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsActive() {
        return isActive;
    }

    public int getVersionId() {
        return versionId;
    }

    public String getDescript() {
        return descript;
    }

}
