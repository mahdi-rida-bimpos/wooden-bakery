package com.bimpos.wooden.bakery.models;

import java.util.ArrayList;
import java.util.List;

public class QuestionHeader {

    private final int questionGroupId;
    private final int questionHeaderId;
    private final int min;
    private final int max;
    private final int required;
    private final int seq;
    private final String description;
    private List<QuestionModifier> modifiersList;

    public QuestionHeader(int questionGroupId, int questionHeaderId, int min, int max, int required, int seq, String descript) {
        this.questionGroupId = questionGroupId;
        this.questionHeaderId = questionHeaderId;
        this.min = min;
        this.max = max;
        this.required = required;
        this.seq = seq;
        this.description = descript;
        modifiersList = new ArrayList<>();
    }

    public List<QuestionModifier> getModifiersList() {
        return modifiersList;
    }

    public void setModifiersList(List<QuestionModifier> modifiersList) {
        this.modifiersList = modifiersList;
    }

    public String getDescription() {
        return description;
    }


    public int getQuestionGroupId() {
        return questionGroupId;
    }


    public int getQuestionHeaderId() {
        return questionHeaderId;
    }


    public int getMin() {
        return min;
    }


    public int getMax() {
        return max;
    }


    public int getRequired() {
        return required;
    }


    public int getSeq() {
        return seq;
    }


    @Override
    public String toString() {
        return "QuestionHeader{" +
                "questionGroupId=" + questionGroupId +
                ", questionHeaderId=" + questionHeaderId +
                ", description='" + description + '\'' +
                '}';
    }
}
