package com.bimpos.wooden.bakery.tables;

public class AddressTable {

   public static final String TABLE_NAME = "address";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String HS_ADDRESS_ID = "hsAddressId";
        public static final String DESCRIPTION = "description";
        public static final String ADDRESS_TYPE = "addressType";
        public static final String PHONE = "phone";
        public static final String GEO_LONG = "geoLong";
        public static final String GEO_LAT = "geoLat";
        public static final String CITY_CODE = "cityCode";
        public static final String STREET = "street";
        public static final String DIRECTION = "direction";
        public static final String CITY_NAME = "cityName";
        public static final String BUILDING = "building";
        public static final String FLOOR = "floor";
        public static final String DEFAULT_ADDRESS = "defaultAddress";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
