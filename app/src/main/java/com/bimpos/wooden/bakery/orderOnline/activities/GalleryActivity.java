package com.bimpos.wooden.bakery.orderOnline.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

public class GalleryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        PhotoView imageView = findViewById(R.id.activity_gallery_imageView);
        ImageView close = findViewById(R.id.activity_gallery_close);

        String url = getIntent().getStringExtra(Constant.PICTURE_URL);
        if (url.equalsIgnoreCase(Constant.MEMBER)) {
            imageView.setImageBitmap(MainApplication.getDatabase().getMemberPhoto());
        } else {
            Glide.with(this)
                    .load(url)
                    .placeholder(R.drawable.category_place_holder)
                    .into(imageView);
        }

        imageView.setOnClickListener(v -> finish());

        close.setOnClickListener(v -> finish());
    }
}