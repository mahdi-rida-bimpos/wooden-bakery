package com.bimpos.wooden.bakery.orderHistory.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.models.Branches;

import java.util.ArrayList;
import java.util.List;

public class BranchSearchAdapter extends RecyclerView.Adapter<BranchSearchAdapter.ViewHolder> implements Filterable {

    private final List<Branches> branchesList;
    private List<Branches> filteredList;
    private final OnBranchSelected listener;

    public interface OnBranchSelected {
        void onBranchSelected(Branches branches);
    }

    public BranchSearchAdapter(List<Branches> branchesList, OnBranchSelected listener) {
        this.branchesList = branchesList;
        this.filteredList = branchesList;
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    filteredList = branchesList;
                } else {
                    List<Branches> newFilteredList = new ArrayList<>();
                    for (Branches branch : branchesList) {
                        if (branch.getName().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            newFilteredList.add(branch);
                        }
                    }
                    filteredList = newFilteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @SuppressLint("NotifyDataSetChanged")
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<Branches>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_search_cities, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Branches branches = filteredList.get(position);
        holder.cityName.setText(branches.getName());
        holder.mainLayout.setOnClickListener(v -> listener.onBranchSelected(branches));
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView cityName;
        LinearLayout mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cityName = itemView.findViewById(R.id.recycler_search_cities_cityName);
            mainLayout = itemView.findViewById(R.id.recycler_search_cities_mainLayout);
        }
    }
}
