package com.bimpos.wooden.bakery.orderOnline.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bimpos.wooden.bakery.orderOnline.activities.CartOrderActivity;
import com.bimpos.wooden.bakery.databinding.FragmentCartOrderBinding;
import com.bimpos.wooden.bakery.models.Cart;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.orderOnline.adapters.CartOrderRvAdapter;

import java.util.List;

public class CartOrderFragment extends Fragment implements CartOrderRvAdapter.OnEmptyCard {

    private Cart cartData;
    private FragmentCartOrderBinding binding;
    public CartOrderRvAdapter adapter;
    private final CartOrderActivity cartOrderActivity;

    public CartOrderFragment(CartOrderActivity cartOrderActivity) {
        this.cartOrderActivity = cartOrderActivity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getActivity() != null) {
            binding = FragmentCartOrderBinding.inflate(getActivity().getLayoutInflater());
        }
        initVariables();
        initRecyclerView();

        return binding.getRoot();
    }

    private void initVariables() {
        cartData = Cart.getCartData();
    }

    private void initRecyclerView() {
        List<CartProduct> cartItemsList = cartData.getCartItemsList();
        adapter = new CartOrderRvAdapter(cartItemsList, getActivity(), this,cartOrderActivity);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void onEmptyCard() {
        cartOrderActivity.finish();
    }

    @Override
    public void onPriceChanged() {
        cartOrderActivity.setTotalPrice();
    }
}