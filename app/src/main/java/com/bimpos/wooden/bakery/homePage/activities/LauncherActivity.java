package com.bimpos.wooden.bakery.homePage.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bimpos.wooden.bakery.BuildConfig;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityLauncherBinding;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.helpers.NetworkUtil;
import com.bimpos.wooden.bakery.models.MessageEvent;
import com.bimpos.wooden.bakery.networkRequest.WoodenNetwork;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class LauncherActivity extends AppCompatActivity implements CustomDialog.DialogEvents {
    private static final String TAG = "LauncherActivity";
    private WoodenNetwork woodenNetwork;
    private ActivityLauncherBinding binding;
    private DatabaseHelper databaseHelper;
    private final int DIALOG_NO_INTERNET = 1;
    private final int DIALOG_ERROR = 2;
    private final int DIALOG_MIN_VERSION = 3;
    private final int DIALOG_CURR_VERSION = 4;
    private final int DIALOG_MAINTENANCE_MODE = 5;
    private final int DIALOG_NO_INTERNET_BUT_ENTER = 6;
    private boolean isDone = false;
    private int topLevel = 0;
    private CustomDialog dialog;
    private SharedPreferences sharedPreferences;
    private Handler handler;
    private Runnable periodicUpdate;

    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLauncherBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        launch();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(Constant.INTENT_FILTER_LAUNCHER);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
//            registerReceiver(receiver, intentFilter, Context.RECEIVER_NOT_EXPORTED);
//        } else {
//            registerReceiver(receiver, intentFilter, Context.RECEIVER_NOT_EXPORTED);
//        }
//        ContextCompat.registerReceiver(
//                this,
//                receiver,
//                intentFilter,
//                ContextCompat.RECEIVER_NOT_EXPORTED
//        );
        EventBus.getDefault().register(this);
    }

    public void launch() {
        if (checkConnection()) {
            if (isFirstSetup()) {
                getTokenAndGetData();
            } else {
                getData();
            }
        } else {
            if (checkDatabase()) {
                showAlertDialog("No internet connection is detected. Menu may not be up-to-date", "Enter", "Exit", DIALOG_NO_INTERNET_BUT_ENTER);
            } else {
                showNoInternetDialog();
            }
        }
    }//internet connection is needed to be able to download the menu

    private void showNoInternetDialog() {
        showAlertDialog("Internet connection is needed to be able to download the menu", "Exit", "null", DIALOG_NO_INTERNET);
    }

    private boolean checkDatabase() {
        return sharedPreferences.getBoolean(Constant.DATABASE_EXISTS, false);
    }

    private void getData() {
        woodenNetwork.new GetSettings().execute();
    }

    private void getTokenAndGetData() {
        woodenNetwork.new FirstSetup().execute();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        // Do something
        String status = event.getStatus();
        String message = event.getMessage();
        int level = event.getLevel();
        Log.d(TAG, "onReceive: received " + status + ", " + message);
        if (level == -1) {
            checkStatus(status);
        } else {
            periodicUpdate = () -> {
                topLevel = topLevel + 1;
                binding.viewClip.getBackground().setLevel(topLevel * 100);
                handler.postDelayed(periodicUpdate, 10);
                if (topLevel == level) {
                    handler.removeCallbacksAndMessages(null);
                    topLevel = level;
                }
            };
            handler.post(periodicUpdate);

            if (status.equalsIgnoreCase("success") && message.equalsIgnoreCase("done")) {
                sharedPreferences.edit().putBoolean(Constant.DATABASE_EXISTS, true).apply();
                gotoMainActivity();
            }
        }
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String status = intent.getStringExtra("status");
            String message = intent.getStringExtra("message");
            int level = intent.getIntExtra("level", 0);
            Log.d(TAG, "onReceive: received " + status + ", " + message);
            if (level == -1) {
                checkStatus(status);
            } else {
                periodicUpdate = () -> {
                    topLevel = topLevel + 1;
                    binding.viewClip.getBackground().setLevel(topLevel * 100);
                    handler.postDelayed(periodicUpdate, 10);
                    if (topLevel == level) {
                        handler.removeCallbacksAndMessages(null);
                        topLevel = level;
                    }
                };
                handler.post(periodicUpdate);

                if (status.equalsIgnoreCase("success") && message.equalsIgnoreCase("done")) {
                    sharedPreferences.edit().putBoolean(Constant.DATABASE_EXISTS, true).apply();
                    gotoMainActivity();
                }
            }
        }
    };

    private void checkStatus(String status) {
        if (status.equalsIgnoreCase("maintenance")) {
            showAlertDialog("Sorry, we are in maintenance", "Exit", "null", DIALOG_MAINTENANCE_MODE);

        } else if (status.equalsIgnoreCase("shouldUpdate")) {
            sharedPreferences.edit().putBoolean(Constant.DATABASE_EXISTS, false).apply();
            databaseHelper.cleanAllTables();
            showAlertDialog("Your app is out of date\nYou should update it", "Update", "Exit", DIALOG_MIN_VERSION);

        } else if (status.equalsIgnoreCase("canUpdate")) {
            showAlertDialog("There is a new version of this app\nDo you want update now ?", "Update", "Later", DIALOG_CURR_VERSION);

        } else if (status.equalsIgnoreCase("error")) {
            sharedPreferences.edit().putBoolean(Constant.DATABASE_EXISTS, false).apply();
            databaseHelper.cleanAllTables();
            topLevel = 0;
            showAlertDialog("Something went wrong while getting data", "Retry", "Exit", DIALOG_ERROR);
        }
    }

    private void showAlertDialog(String message, String positiveButton, String negativeButton, int dialogId) {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, message);
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, positiveButton);
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, negativeButton);
        arguments.putInt(CustomDialog.DIALOG_ID, dialogId);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    private void gotoMainActivity() {
        isDone = true;
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private boolean isFirstSetup() {
        return sharedPreferences.getBoolean(Constant.FIRST_SETUP, true);
    }

    private void initVariables() {
        dialog = new CustomDialog(this);
        handler = new Handler();
        databaseHelper = MainApplication.getDatabase();
        sharedPreferences = MainApplication.getPreferences();
        woodenNetwork = new WoodenNetwork(this);

        String versionName = "version " + BuildConfig.VERSION_NAME;
        binding.version.setText(versionName);
    }

    private boolean checkConnection() {
        return NetworkUtil.getConnectivityStatusString(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
//        unregisterReceiver(receiver);
        if (!isDone) {
            sharedPreferences.edit().putBoolean(Constant.DATABASE_EXISTS, false).apply();
            databaseHelper.cleanAllTables();
        }
        super.onPause();
    }

    private void goToPlayStore() {
        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(goToMarket);
        finish();
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        switch (dialogId) {
            case DIALOG_MAINTENANCE_MODE:
            case DIALOG_NO_INTERNET:
                dialog.dismiss();
                finish();
                break;

            case DIALOG_ERROR:
                dialog.dismiss();
                launch();
                break;

            case DIALOG_CURR_VERSION:
            case DIALOG_MIN_VERSION:
                dialog.dismiss();
                goToPlayStore();
                break;

            case DIALOG_NO_INTERNET_BUT_ENTER:
                dialog.dismiss();
                continueLoading();
                break;
        }
    }

    private void continueLoading() {

        topLevel = 0;
        int level = 100;
        periodicUpdate = () -> {
            topLevel = topLevel + 1;
            binding.viewClip.getBackground().setLevel(topLevel * 100);
            handler.postDelayed(periodicUpdate, 0);
            if (topLevel == level) {
                handler.removeCallbacksAndMessages(null);
                topLevel = level;
                gotoMainActivity();
            }
        };
        handler.post(periodicUpdate);

    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        switch (dialogId) {

            case DIALOG_MIN_VERSION:
            case DIALOG_NO_INTERNET_BUT_ENTER:
            case DIALOG_ERROR:
                dialog.dismiss();
                finish();
                break;

            case DIALOG_CURR_VERSION:
                dialog.dismiss();
                woodenNetwork.new GetCategories().execute();
                break;

        }
    }

    @Override
    public void onDialogCancelled(int dialogId) {
    }
}