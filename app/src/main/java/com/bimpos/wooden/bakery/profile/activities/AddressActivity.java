package com.bimpos.wooden.bakery.profile.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.helpers.Helpers;
import com.bimpos.wooden.bakery.models.Address;
import com.bimpos.wooden.bakery.models.Cities;
import com.bimpos.wooden.bakery.profile.adapters.CitiesSearchAdapter;
import com.davidmiguel.multistateswitch.MultiStateSwitch;
import com.davidmiguel.multistateswitch.State;
import com.davidmiguel.multistateswitch.StateListener;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import co.ceryle.radiorealbutton.RadioRealButtonGroup;
import io.github.softmedtanzania.MaskedEditText;


public class AddressActivity extends AppCompatActivity implements CustomDialog.DialogEvents,
        LocationListener, CitiesSearchAdapter.OnCitySelected {

    private final int DIALOG_ADDRESS_DELETE = 1;
    private final int DIALOG_DISMISS = 2;
    private DatabaseHelper databaseHelper;
    private List<Cities> citiesList;
    private Cities selectedCity;
    private List<Address> addressDataList;
    private Address address;
    private String[] mobileStrings;
    private String strAll;
    private CustomDialog dialog;
    private Dialog searchDialog;
    private MaskedEditText phoneNumberInput;
    private Button saveAddress, deleteAddress;
    private MultiStateSwitch switchState;
    private EditText buildingInput, floorInput, streetInput, descriptionInput, directionInput;
    private TextInputLayout descriptionLayout, phoneLayout;
    private LinearLayout defaultAddressLayout, companyLayout;
    private Switch switchDefault;
    private ImageView back;
    private TextView selectedCityText;
    private LinearLayout grantPermission;
    private CardView grantLayout;
    private LinearLayout cityLayout;
    private int currentAddressPosition = 0;

    private TextView floorReq, buildingReq, streetReq, requiredMessage, companyReq, directionsReq;

    private double latitude = 0.0;
    private double longitude = 0.0;
    private LocationManager locationManager;
    private boolean gotLocation = false;

    private static final String TAG = "AddressActivity";
    private long mLastClick = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_address);

        initVariables();
        initPhoneMask();
        checkIntent();
        initRadioButtons();
        initClickListeners();
        initTextChangeListeners();
        checkPermission();
    }

    private void initVariables() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        dialog = new CustomDialog(this);
        searchDialog = new Dialog(this);
        phoneNumberInput = findViewById(R.id.activity_address_phoneNumber);
        switchState = findViewById(R.id.stateSwitch);
        requiredMessage = findViewById(R.id.activity_address_requiredMessage);
        back = findViewById(R.id.activity_address_back);
        saveAddress = findViewById(R.id.activity_address_saveAddress);
        deleteAddress = findViewById(R.id.activity_address_deleteAddress);
        buildingInput = findViewById(R.id.activity_address_building);
        floorInput = findViewById(R.id.activity_address_floor);
        streetInput = findViewById(R.id.activity_address_street);
        directionInput = findViewById(R.id.activity_address_direction);
        switchDefault = findViewById(R.id.activity_address_switchDefault);
        phoneLayout = findViewById(R.id.activity_address_phoneInputLayout);
        cityLayout = findViewById(R.id.activity_address_cityLayout);
        descriptionLayout = findViewById(R.id.activity_address_descriptionInputLayout);
        companyLayout = findViewById(R.id.activity_address_companyLayout);
        descriptionInput = findViewById(R.id.activity_address_descriptionInputText);
        defaultAddressLayout = findViewById(R.id.activity_address_defaultAddressLayout);
        streetReq = findViewById(R.id.activity_address_streetReq);
        selectedCityText = findViewById(R.id.activity_address_selectedCityText);
        floorReq = findViewById(R.id.activity_address_floorReq);
        directionsReq = findViewById(R.id.activity_address_directionsReq);
        companyReq = findViewById(R.id.activity_address_companyReq);
        buildingReq = findViewById(R.id.activity_address_buildingReq);
        grantLayout = findViewById(R.id.activity_address_grantLayout);
        grantPermission = findViewById(R.id.activity_address_grantPermission);

        floorInput.setFilters(new InputFilter[]{Helpers.filter, new InputFilter.LengthFilter(4)});
        streetInput.setFilters(new InputFilter[]{Helpers.filter, new InputFilter.LengthFilter(50)});
        directionInput.setFilters(new InputFilter[]{Helpers.filter, new InputFilter.LengthFilter(200)});
        buildingInput.setFilters(new InputFilter[]{Helpers.filter, new InputFilter.LengthFilter(30)});
        descriptionInput.setFilters(new InputFilter[]{Helpers.filter, new InputFilter.LengthFilter(40)});

        databaseHelper = MainApplication.getDatabase();

        citiesList = databaseHelper.getAllCities();
        addressDataList = databaseHelper.getAddressList();
        selectedCity = citiesList.get(0);
        selectedCityText.setText(selectedCity.getCityName());

        String mobilePrefix = MainApplication.getPreferences().getString(Constant.SETTINGS_MOBILE_PREFIX, "03,70,71,78,81,01,04,05,06,07,08,09");
        Log.d(TAG, "initVariables: mobile prefix " + mobilePrefix);
        if (!mobilePrefix.contains("01")) {
            mobilePrefix += ",01";
        }
        if (!mobilePrefix.contains("04")) {
            mobilePrefix += ",04";
        }
        if (!mobilePrefix.contains("05")) {
            mobilePrefix += ",05";
        }
        if (!mobilePrefix.contains("06")) {
            mobilePrefix += ",06";
        }
        if (!mobilePrefix.contains("07")) {
            mobilePrefix += ",07";
        }
        if (!mobilePrefix.contains("08")) {
            mobilePrefix += ",08";
        }
        if (!mobilePrefix.contains("09")) {
            mobilePrefix += ",09";
        }
        mobileStrings = mobilePrefix.split(",");
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        if (!gotLocation) {
            gotLocation = true;
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    private void saveAddressWithLocation() {
        String databaseMode;
        if (address == null) {
            //add mode
            address = new Address();
            databaseMode = "insert";
        } else {
            //save mode
            databaseMode = "update";
        }

        String building = buildingInput.getText().toString().trim().replace("'", "");
        String street = streetInput.getText().toString().trim().replace("'", "");
        String floor = floorInput.getText().toString().trim().replace("'", "");
        String phone = Objects.requireNonNull(phoneNumberInput.getText()).toString().trim().replace("'", "");
        String description = descriptionInput.getText().toString().trim().replace("'", "");
        String directions = directionInput.getText().toString().trim().replace("'", "");

        address.setGeoLat(latitude);
        address.setGeoLong(longitude);
        address.setAddressType(currentAddressPosition);
        address.setBldg(building);
        address.setFloor(floor);
        address.setStreet(street);

        List<String> characters = new ArrayList<>();
        String ch = MainApplication.getPreferences().getString(Constant.SETTINGS_PHONE_MASK, "(##)(######)");
        for (int i = 0; i < ch.length(); i++) {

            if (!String.valueOf(ch.charAt(i)).equalsIgnoreCase("#")) {
                characters.add(String.valueOf(ch.charAt(i)));
            }
        }
        for (int i = 0; i < characters.size(); i++) {
            phone = phone.replace(characters.get(i), "");
        }

        if (phone.length() < 6) {
            phone = "";
        }
        Log.d(TAG, "initClickListeners: final phone " + phone);

        address.setPhone(phone);
        address.setDirection(directions);

        address.setCityCode(selectedCity.getCityCode());
        address.setCityName(selectedCity.getCityName());

        if (switchDefault.getVisibility() == View.VISIBLE) {
            if (switchDefault.isChecked()) {
                for (int i = 0; i < addressDataList.size(); i++) {
                    databaseHelper.resetDefaultAddress(addressDataList.get(i).getAddressId());
                }
                address.setDefaultAddress(1);
            } else {
                address.setDefaultAddress(0);
            }
        } else {
            address.setDefaultAddress(1);
        }
        if (addressDataList.size() == 0)
            address.setDefaultAddress(1);

        if (currentAddressPosition == 0) {
            address.setDescription("Home");
        } else {
            address.setDescription(description);
        }

        if (databaseMode.equalsIgnoreCase("insert")) {
            databaseHelper.insertAddress(address);
        } else {
            databaseHelper.updateAddress(address);
        }

        finish();
    }


    private void initClickListeners() {

        back.setOnClickListener(v -> finish());

        saveAddress.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            saveAddressFunction();
        });

        deleteAddress.setOnClickListener(v -> showDeleteDialog());

        grantPermission.setOnClickListener(v -> {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Log.d(TAG, "initClickListeners: should ask again");
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        1
                );
            } else {
                Log.d(TAG, "initClickListeners: should not ask again");
                Toast.makeText(this, "Please Allow Location Permission", Toast.LENGTH_LONG).show();
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, 3);
            }
        });

        cityLayout.setOnClickListener(v -> showCitySearchDialog());

        phoneNumberInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s1) {
                Log.d(TAG, "afterTextChanged: text " + s1.toString());
                String s = s1.toString().replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("-", "");
                Log.d(TAG, "afterTextChanged: text 1 " + s);
                boolean isFound = false;

                strAll = "";
                for (String strmobile : mobileStrings) {
                    strAll = new StringBuilder().append(strAll).append(strmobile).append("\n").toString();
                    if (s.length() <= strmobile.length()) {
                        if (strmobile.substring(0, s.length()).equals(s.toString())) {
                            isFound = true;
                        }
                    } else {
                        isFound = true;
                        setLength();
                    }
                }
                if (!isFound) {
                    phoneNumberInput.setError("Your mobile number should start with \n" + strAll);
                    phoneNumberInput.setFilters(new InputFilter[]{new InputFilter.LengthFilter(s.length())});
                } else {
                    setLength();
                }
            }
        });
    }

    public void setLength() {
        phoneNumberInput.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(13)});
    }

    private void saveAddressFunction() {
        String building = buildingInput.getText().toString().trim();
        String street = streetInput.getText().toString().trim();
        String floor = floorInput.getText().toString().trim();
        String description = descriptionInput.getText().toString().trim();
        String directions = directionInput.getText().toString().trim();

        if (currentAddressPosition > 0) {
            if (TextUtils.isEmpty(description)) {
                showMessageDialog("The Address description cannot be empty.");
                descriptionInput.requestFocus();
                return;
            }
        }

        if (TextUtils.isEmpty(street)) {
            showMessageDialog("The Street cannot be empty.");
            streetInput.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(building)) {
            showMessageDialog("The Building cannot be empty.");
            buildingInput.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(floor)) {
            showMessageDialog("The Floor cannot be empty.");
            floorInput.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(directions)) {
            showMessageDialog("The Direction cannot be empty.");
            directionInput.requestFocus();
            return;
        }

        saveAddressWithLocation();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: request code " + requestCode);
        Log.d(TAG, "onActivityResult: result code " + resultCode);
        if (requestCode == 3) {
            checkPermission();
        }
    }

    protected void checkPermission() {
        Log.d(TAG, "checkPermission: start");
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION)
                + ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "checkPermission: not granted");
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Log.d(TAG, "checkPermission: should ask again");
                grantLayout.setVisibility(View.VISIBLE);
            } else {
                Log.d(TAG, "checkPermission: should not ask again");
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        1
                );
            }
        } else {
            grantLayout.setVisibility(View.GONE);
            checkGps();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if ((grantResults.length > 0) && (grantResults[0] + grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                grantLayout.setVisibility(View.GONE);
                checkGps();
            } else {
                grantLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    private void checkGps() {
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        if (!gps_enabled && !network_enabled) {
            // notify user
            new AlertDialog.Builder(this)
                    .setTitle("Location request")
                    .setMessage("Please turn your location service on")
                    .setPositiveButton("settings", (paramDialogInterface, paramInt) -> startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 2))
                    .setNegativeButton("cancel", (dialog, which) -> dialog.dismiss())
                    .show();
        } else {
            getLastLocation();
        }
    }

    private void checkIntent() {
        address = (Address) getIntent().getSerializableExtra(Address.class.getSimpleName());

        if (address == null) {
            //add mode
            currentAddressPosition=0;
            switchState.selectState(0);
            deleteAddress.setVisibility(View.GONE);
            streetInput.requestFocus();
        } else {
            if (addressDataList.size() >= 1 && address.getDefaultAddress() == 0) {
                defaultAddressLayout.setVisibility(View.VISIBLE);
            } else {
                defaultAddressLayout.setVisibility(View.GONE);
            }
            //edit mode
            buildingReq.setVisibility(View.INVISIBLE);
            floorReq.setVisibility(View.INVISIBLE);
            streetReq.setVisibility(View.INVISIBLE);
            directionsReq.setVisibility(View.INVISIBLE);
            requiredMessage.setVisibility(View.INVISIBLE);
            phoneNumberInput.setText(address.getPhone());
            streetInput.setText(address.getStreet());
            buildingInput.setText(address.getBldg());
            floorInput.setText(String.valueOf(address.getFloor()));
            phoneNumberInput.setText(address.getPhone());
            directionInput.setText(address.getDirection());
            switchState.selectState(address.getAddressType());
            switchDefault.setChecked(address.getDefaultAddress() == 1);

            if (address.getAddressType() > 0) {
                companyLayout.setVisibility(View.VISIBLE);
                descriptionInput.setText(address.getDescription());
            }

            selectedCity = databaseHelper.getCityByCityId(address.getCityCode());
            selectedCityText.setText(selectedCity.getCityName());

        }
    }

    private void initPhoneMask() {
        String ch = MainApplication.getPreferences().getString(Constant.SETTINGS_PHONE_MASK, "(##)(######)");
        phoneNumberInput.setMask("(##)(######)");
    }

    private void initTextChangeListeners() {
        streetInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    streetReq.setVisibility(View.VISIBLE);
                } else {
                    if (streetReq.getVisibility() == View.VISIBLE) {
                        streetReq.setVisibility(View.INVISIBLE);
                    }
                }
                checkVisibility();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        buildingInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    buildingReq.setVisibility(View.VISIBLE);
                } else {
                    if (buildingReq.getVisibility() == View.VISIBLE) {
                        buildingReq.setVisibility(View.INVISIBLE);
                    }
                }

                checkVisibility();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        floorInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    floorReq.setVisibility(View.VISIBLE);
                } else {
                    if (floorReq.getVisibility() == View.VISIBLE) {
                        floorReq.setVisibility(View.INVISIBLE);
                    }
                }
                checkVisibility();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        descriptionInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    companyReq.setVisibility(View.VISIBLE);
                } else {
                    if (companyReq.getVisibility() == View.VISIBLE) {
                        companyReq.setVisibility(View.INVISIBLE);
                    }
                }
                checkVisibility();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        directionInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    directionsReq.setVisibility(View.VISIBLE);
                } else {
                    if (directionsReq.getVisibility() == View.VISIBLE) {
                        directionsReq.setVisibility(View.INVISIBLE);
                    }
                }
                checkVisibility();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void checkVisibility() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (floorReq.getVisibility() == View.VISIBLE
                        || buildingReq.getVisibility() == View.VISIBLE
                        || streetReq.getVisibility() == View.VISIBLE
                        || directionsReq.getVisibility() == View.VISIBLE) {
                    requiredMessage.setVisibility(View.VISIBLE);
                } else {
                    if (currentAddressPosition == 0) {
                        requiredMessage.setVisibility(View.INVISIBLE);
                    } else if (descriptionInput.getText().length() == 0) {
                        requiredMessage.setVisibility(View.VISIBLE);
                    }
                }
            }
        }, 350);
    }

    private void initRadioButtons() {
        List<String> states = new ArrayList<>();
        states.add("Home");
        states.add("Work");
        states.add("Other");
        switchState.addStatesFromStrings(states);

        switchState.addStateListener((i, state) -> {
            switch (i) {
                case 0:
                    currentAddressPosition = 0;
                    descriptionLayout.setHint("");
                    descriptionInput.setText("");
                    checkVisibility();
                    companyLayout.setVisibility(View.GONE);
                    phoneLayout.setHint("Home Phone");
                    break;
                case 1:
                    currentAddressPosition = 1;
                    companyLayout.setVisibility(View.VISIBLE);
                    descriptionLayout.setHint("Company Name");
                    descriptionInput.setText("");
                    checkVisibility();
                    phoneLayout.setHint("Work Phone");
                    break;
                case 2:
                    currentAddressPosition = 2;
                    companyLayout.setVisibility(View.VISIBLE);
                    descriptionLayout.setHint("Hotel, Bank, Chalet...");
                    descriptionInput.setText("");
                    checkVisibility();
                    phoneLayout.setHint("Other Phone");
                    break;
            }
            descriptionInput.requestFocus();

        });
    }


    @SuppressLint("SetTextI18n")
    private void showCitySearchDialog() {
        View messageView = getLayoutInflater().inflate(R.layout.dialog_cities_search, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(messageView);

        TextView close = messageView.findViewById(R.id.dialog_cities_search_close);
        TextView title = messageView.findViewById(R.id.dialog_cities_search_title);
        RecyclerView recyclerView = messageView.findViewById(R.id.dialog_cities_search_recyclerView);
        EditText searchText = messageView.findViewById(R.id.dialog_cities_search_searchText);
        title.setText("Select City");
        CitiesSearchAdapter adapter = new CitiesSearchAdapter(citiesList, this);

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        close.setOnClickListener(v -> searchDialog.dismiss());

        searchDialog = builder.create();
        searchDialog.getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);
        searchDialog.setCanceledOnTouchOutside(false);
        searchDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        searchDialog.getWindow().getDecorView().setPadding(50, 50, 50, 0);
        searchDialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        searchDialog.show();
    }

    @Override
    public void onCitySelected(Cities cities) {
        this.selectedCity = cities;
        selectedCityText.setText(selectedCity.getCityName());
        searchDialog.dismiss();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    private void showDeleteDialog() {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Are you sure do you want delete this address?");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Yes");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "No");
        arguments.putInt(CustomDialog.DIALOG_ID, DIALOG_ADDRESS_DELETE);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    private void showMessageDialog(String message) {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, message);
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Ok");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
        arguments.putInt(CustomDialog.DIALOG_ID, DIALOG_DISMISS);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(true);
    }

    private void performDelete() {
        if (address.getDefaultAddress() == 1) {
            databaseHelper.deleteAddress(address.getAddressId());
            addressDataList = databaseHelper.getAddressList();
            if (addressDataList.size() > 0) {
                addressDataList.get(addressDataList.size() - 1).setDefaultAddress(1);
                databaseHelper.updateAddress(addressDataList.get(addressDataList.size() - 1));
            }
        } else {
            databaseHelper.deleteAddress(address.getAddressId());
        }
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        if (dialogId == DIALOG_ADDRESS_DELETE) {
            performDelete();
            finish();
        } else if (dialogId == DIALOG_DISMISS) {
            dialog.dismiss();
        }
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        if (dialogId == DIALOG_ADDRESS_DELETE) {
            dialog.dismiss();
        }
    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }

}