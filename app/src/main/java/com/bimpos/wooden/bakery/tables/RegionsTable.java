package com.bimpos.wooden.bakery.tables;

public class RegionsTable {

    public static final String TABLE_NAME= "regions";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String REGION_ID = "regionId";
        public static final String NAME = "name";
        public static final String PHONE_CODE = "phoneCode";
        public static final String IS_ACTIVE = "isActive";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
