package com.bimpos.wooden.bakery.tables;

public class ParentTable {

    public static final String TABLE_NAME= "parent";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String PARENT_ID = "parentId";
        public static final String DESCRIPTION = "description";
        public static final String IS_ACTIVE = "isActive";
        public static final String VERSION_ID = "versionId";
        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
