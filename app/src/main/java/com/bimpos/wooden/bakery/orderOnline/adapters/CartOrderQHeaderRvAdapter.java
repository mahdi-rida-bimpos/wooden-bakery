package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.models.CartQuestionHeader;

import java.util.List;

public class CartOrderQHeaderRvAdapter extends RecyclerView.Adapter<CartOrderQHeaderRvAdapter.ViewHolder> {

    private final List<CartQuestionHeader> cartQuestionHeaderList;
    private final Context context;
    private final CartProduct cartProduct;

    public CartOrderQHeaderRvAdapter(Context context ,CartProduct cartProduct) {
        this.cartQuestionHeaderList = cartProduct.getQuestionList();
        this.cartProduct=cartProduct;
        this.context = context;
    }

    @NonNull
    @Override
    public CartOrderQHeaderRvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cart_order_question_header, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartOrderQHeaderRvAdapter.ViewHolder holder, int position) {
        CartQuestionHeader cartQuestionHeader = cartQuestionHeaderList.get(position);
        if(cartQuestionHeader.getCartQuestionModifiers().size()!=0){
            holder.headerDescription.setText(cartQuestionHeader.getDescription());
            CartOrderQModifierRvAdapter adapter = new CartOrderQModifierRvAdapter(cartProduct,cartQuestionHeader.getCartQuestionModifiers());
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.recyclerView.setAdapter(adapter);
        }

    }

    @Override
    public int getItemCount() {
        return cartQuestionHeaderList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;
        TextView headerDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recyclerView = itemView.findViewById(R.id.recycler_cart_order_question_header_recyclerView);
            headerDescription = itemView.findViewById(R.id.recycler_cart_order_question_header_description);
        }
    }
}
