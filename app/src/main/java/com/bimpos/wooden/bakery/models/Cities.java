package com.bimpos.wooden.bakery.models;

public class Cities {

    private int cityCode;
    private final int regionCode;
    private final String cityName;
    private final int zipCode;
    private int isActive;
    private final int countryCode;

    public Cities(int cityCode, int regionCode, String cityName, int zipCode,int countryCode, int isActive) {
        this.cityCode = cityCode;
        this.regionCode = regionCode;
        this.cityName = cityName;
        this.zipCode = zipCode;
        this.isActive = isActive;
        this.countryCode = countryCode;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public int getCityCode() {
        return cityCode;
    }

    public void setCityCode(int cityCode) {
        this.cityCode = cityCode;
    }

    public int getRegionCode() {
        return regionCode;
    }

    public String getCityName() {
        return cityName;
    }

    public int getZipCode() {
        return zipCode;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }
}
