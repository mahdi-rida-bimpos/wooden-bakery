package com.bimpos.wooden.bakery.receivers;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class CheckConnectivityAsync extends AsyncTask<Boolean, String, Boolean> {
    private static final String TAG = "CheckConnectivityAsync";
    private final OnConnectionComplete listener;

    public interface OnConnectionComplete{
        void onConnectionComplete(boolean status);
    }

    public CheckConnectivityAsync(OnConnectionComplete listener) {
        this.listener = listener;
    }

    @Override
    protected Boolean doInBackground(Boolean... booleans) {

            try {
                HttpURLConnection url = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                url.setRequestProperty("User-Agent", "Test");
                url.setRequestProperty("Connection", "close");
                url.setConnectTimeout(3000); //choose your own timeframe
                url.setReadTimeout(4000); //choose your own timeframe
                url.connect();
                Log.d(TAG, "doInBackground: response "+url.getResponseCode());
                return (url.getResponseCode() >= 200 && url.getResponseCode()<=300);
            } catch (IOException e) {
                Log.d(TAG, "doInBackground: error "+e.getMessage());
                return (false);  //connectivity exists, but no internet.
            }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {

        listener.onConnectionComplete(aBoolean);
    }
}
