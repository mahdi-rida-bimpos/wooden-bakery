package com.bimpos.wooden.bakery.models;

import java.io.Serializable;
import java.util.List;

public class CartProduct implements Serializable {

    private final int cartId;
    private int prodNum;
    private final String prod_desc;
    private int qty;
    private final String remark;
    private double price;
    private int comboGroupId;
    private int isRemoved = 0;
    private final List<CartQuestionHeader> questionList;
    private final List<CartComboHeader> comboList;

    public CartProduct(int cartId, int prodNum, String prod_desc, int qty, double price, int comboGroupId, int questionGroupId, String remark, List<CartQuestionHeader> questionList, List<CartComboHeader> comboList) {
        this.cartId = cartId;
        this.prodNum = prodNum;
        this.prod_desc = prod_desc;
        this.qty = qty;
        this.remark = remark;
        this.price = price;
        this.questionList = questionList;
        this.comboList = comboList;
        this.comboGroupId = comboGroupId;
    }

    public int getIsRemoved() {
        return isRemoved;
    }

    public void setIsRemoved(int isRemoved) {
        this.isRemoved = isRemoved;
    }

    public int getComboGroupId() {
        return comboGroupId;
    }

    public void setComboGroupId(int comboGroupId) {
        this.comboGroupId = comboGroupId;
    }

    public List<CartComboHeader> getComboList() {
        return comboList;
    }

    public String getRemark() {
        return remark;
    }

    public int getCartId() {
        return cartId;
    }

    public int getProdNum() {
        return prodNum;
    }

    public void setProdNum(int prodNum) {
        this.prodNum = prodNum;
    }

    public String getProd_desc() {
        return prod_desc;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<CartQuestionHeader> getQuestionList() {
        return questionList;
    }

    @Override
    public String toString() {
        return "CartProduct{" +
                "cartId=" + cartId +
                ", prodNum=" + prodNum +
                ", prod_desc='" + prod_desc + '\'' +
                ", qty=" + qty +
                ", remark='" + remark + '\'' +
                ", price=" + price +
                ", comboGroupId=" + comboGroupId +
                ", questionList=" + questionList +
                ", comboList=" + comboList +
                '}';
    }
}
