package com.bimpos.wooden.bakery.models;

public class BranchCities {

    private int branchId, cityCode;

    public BranchCities(int branchId, int cityCode) {
        this.branchId = branchId;
        this.cityCode = cityCode;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getCityCode() {
        return cityCode;
    }

    public void setCityCode(int cityCode) {
        this.cityCode = cityCode;
    }
}
