package com.bimpos.wooden.bakery.branches.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.databinding.ActivityBranchesDetailsBinding;
import com.bimpos.wooden.bakery.models.Branches;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class BranchesDetailsActivity extends AppCompatActivity implements
        OnMapReadyCallback {

    private ActivityBranchesDetailsBinding binding;
    private Branches branch;
    private static final String TAG = "BranchesDetailsActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBranchesDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        checkIntent();
        initClickListener();
    }


    private void checkIntent() {
        branch = (Branches) getIntent().getSerializableExtra(Branches.class.getSimpleName());
        if(branch ==null){
            finish();
        }else{
            Log.d(TAG, "checkIntent: branch "+branch);
            initMapFragment();
            fillContent();
        }
    }

    @SuppressLint("SetTextI18n")
    private void fillContent() {
        binding.name.setText(branch.getName());
        if(branch.getOpeningHours().length()==0){
            binding.isOpenText.setText("The store is closed now");
            binding.isOpenText.setTextColor(ContextCompat.getColor(this,R.color.pantoneRed));
            binding.openingHours.setText("Not available");
        }else{  
            binding.isOpenText.setText("The store is open now");
            binding.isOpenText.setTextColor(ContextCompat.getColor(this,R.color.green));
            binding.openingHours.setText(branch.getOpeningHours());
        }

        Glide.with(this)
                .load(branch.getPicPath())
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(R.drawable.wooden_branch)
                .into(binding.image);

        binding.phone.setText(branch.getPhone());
        Log.d(TAG, "fillContent: branch "+branch.toString());
        if(branch.getAcceptsDelivery()==1){
            binding.deliveryIcon.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_check));
        }else{
            binding.deliveryIcon.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_close_red));
        }

        if(branch.getAcceptsTakeAway()==1){
            binding.takeAwayIcon.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_check));
        }else{
            binding.takeAwayIcon.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_close_red));
        }

        if(branch.getLongitude()!=0 || branch.getLatitude()!=0){
            binding.mapBtn.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:<" + branch.getLatitude() + ">,<" + branch.getLongitude() + ">?q=<" + branch.getLatitude() + ">,<" + branch.getLongitude() + ">(" + branch.getName() + ")"));
                startActivity(intent);
            });
        }else{
            binding.mapBtn.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.branch_map_grey_icon));
        }

        if(branch.getPhone().length()!=0){
            binding.callBtn.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + branch.getPhone() + ""));
                startActivity(intent);

            });
        }else{
            binding.callBtn.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.branch_call_grey_icon));
        }


        if(branch.getEmail().length()!=0){
            binding.emailBtn.setOnClickListener(v -> {
                Intent i = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", branch.getEmail(), null));
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(i, "Send email"));
            });
        }else{
            binding.emailBtn.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.branch_email_grey_icon));
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        supportFinishAfterTransition();
    }

    private void initVariables() {
        binding.isOpenText.setVisibility(View.GONE);
        binding.back.setOnClickListener(v -> finish());
    }
    private void initClickListener() {
        binding.back.setOnClickListener(view -> onBackPressed());
    }

    private void initMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.activity_branches_details_mapFragment);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        LatLng location = new LatLng(branch.getLatitude(), branch.getLongitude());
        googleMap.addMarker(new MarkerOptions()
                .position(location)
                .title(branch.getName()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
    }
}