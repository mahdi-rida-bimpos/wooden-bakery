package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.models.Categories;
import com.bimpos.wooden.bakery.orderOnline.activities.BigCategoriesActivity;
import com.bimpos.wooden.bakery.orderOnline.activities.OrdersActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;

public class BigCategoriesRvAdapter extends RecyclerView.Adapter<BigCategoriesRvAdapter.ViewHolder> {

    private final List<Categories> categoriesList;
    private final Context context;
    private final BigCategoriesActivity activity;
    private long mLastClick=0;
    private static final String TAG = "CategoriesCatalogRvAdapter";


    public BigCategoriesRvAdapter(List<Categories> categoriesList, Context context, BigCategoriesActivity activity) {
        this.categoriesList = categoriesList;
        this.activity = activity;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_categories_catalog, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Categories category = categoriesList.get(position);
        Log.d(TAG, "onBindViewHolder: category seq " + category.getSeq());
        holder.categoryDescription.setText(category.getDescription());

        Glide.with(context)
                .load(category.getPicpath())
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(R.drawable.category_place_holder)
                .into(holder.categoryImage);

        holder.itemView.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            Intent intent = new Intent(context, OrdersActivity.class);
            Log.d(TAG, "onBindViewHolder: seq " + category.getCatId());
            intent.putExtra("catId", category.getCatId());
            context.startActivity(intent);
            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView categoryImage;
        TextView categoryDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryImage = itemView.findViewById(R.id.recycler_categories_catalog_image);
            categoryDescription = itemView.findViewById(R.id.recycler_categories_catalog_catDescription);
        }
    }
}
