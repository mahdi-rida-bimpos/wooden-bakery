package com.bimpos.wooden.bakery.orderHistory.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.orderHistory.activities.OrderHistoryDetailsActivity;

import java.util.List;

public class OrderHistoryDetailsRvAdapter extends RecyclerView.Adapter<OrderHistoryDetailsRvAdapter.ViewHolder> {


    private final List<CartProduct> cartItemsList;
    private final Context context;
    private final OrderHistoryDetailsActivity activity;

    public OrderHistoryDetailsRvAdapter(OrderHistoryDetailsActivity activity, Context context, List<CartProduct> cartItemsList) {
        this.cartItemsList = cartItemsList;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override
    public OrderHistoryDetailsRvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_order_details_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderHistoryDetailsRvAdapter.ViewHolder holder, int position) {

        CartProduct items = cartItemsList.get(position);

        double lastPrice = items.getPrice();

        if (activity.history.getConversionRate() == -1) {
            holder.productPrice.setText(FormatPrice.getCustomFormattedPrice(lastPrice * items.getQty(), activity.history.getDefaultCurrency()));
        } else {
            holder.productPrice.setText(FormatPrice.getCustomFormattedPrice(lastPrice * items.getQty(), activity.history.getPriceListCurrency()));
        }

        holder.productDesc.setText(items.getProd_desc());

        if (items.getRemark().equalsIgnoreCase("")) {
            holder.remarkLayout.setVisibility(View.GONE);
        } else {
            holder.remarkText.setText(items.getRemark());
            holder.remarkLayout.setVisibility(View.VISIBLE);
        }

        String qtyText = items.getQty() + "";
        holder.productQty.setText(qtyText);

        if (items.getQuestionList().size() != 0) {
            CartOrderHistoryQHeaderRvAdapter adapter = new CartOrderHistoryQHeaderRvAdapter(context, activity, items);
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.recyclerView.setAdapter(adapter);
        } else if (items.getComboList().size() != 0) {
            CartOrderHistoryComboHeaderRvAdapter adapter = new CartOrderHistoryComboHeaderRvAdapter(context, activity, items);
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public int getItemCount() {
        return cartItemsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;
        LinearLayout remarkLayout;
        TextView productDesc, productPrice, productQty, remarkText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            productPrice = itemView.findViewById(R.id.recycler_order_history_productPrice);
            productDesc = itemView.findViewById(R.id.recycler_order_history_productDescript);
            productQty = itemView.findViewById(R.id.recycler_order_history_qty);
            recyclerView = itemView.findViewById(R.id.recycler_order_history_recyclerView);
            remarkLayout = itemView.findViewById(R.id.recycler_order_history_remarkLayout);
            remarkText = itemView.findViewById(R.id.recycler_order_history_remarkText);
        }
    }

}
