package com.bimpos.wooden.bakery.tables;

public class ComboHeaderTable {

    public static final String TABLE_NAME= "comboHeader";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String COMBO_GROUP_ID = "comboGroupId";
        public static final String COMBO_HEADER_ID = "comboHeaderId";
        public static final String MIN = "min";
        public static final String MAX = "max";
        public static final String REQUIRED = "required";
        public static final String SEQ = "seq";
        public static final String DESCRIPTION = "description";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
