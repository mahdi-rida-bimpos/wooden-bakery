package com.bimpos.wooden.bakery.models;

public class CartQuestionModifier {

   private String description;
   private boolean selected;
   private final int modifierId;
   private double price;

    public CartQuestionModifier(int modifierId,String description, boolean selected, double price) {
        this.description = description;
        this.selected = selected;
        this.price = price;
        this.modifierId=modifierId;
    }

    public int getModifierId() {
        return modifierId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "CartQuestionModifier{" +
                "description='" + description + '\'' +
                ", selected=" + selected +
                ", modifierId=" + modifierId +
                ", price=" + price +
                '}';
    }
}
