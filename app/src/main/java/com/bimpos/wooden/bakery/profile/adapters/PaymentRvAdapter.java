package com.bimpos.wooden.bakery.profile.adapters;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.models.PaymentData;

import java.util.List;

public class PaymentRvAdapter extends RecyclerView.Adapter<PaymentRvAdapter.ViewHolder> {

    private final Context context;
    private final List<PaymentData> paymentDataList;

    public PaymentRvAdapter(Context context, List<PaymentData> paymentDataList) {
        this.paymentDataList = paymentDataList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_payment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaymentData paymentData = paymentDataList.get(position);
        holder.description.setText(paymentData.getDescription());
        holder.imageLogo.setBackgroundResource(paymentData.getPicture());

        if (paymentData.getDefaultPayment() == 1) {
            int colorFrom = Color.WHITE;
            ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, ContextCompat.getColor(context, R.color.selectedAddress));
            colorAnimation.setDuration(250); // milliseconds
            colorAnimation.addUpdateListener(animator -> holder.mainLayout.setBackgroundColor((int) animator.getAnimatedValue()));
            colorAnimation.start();
            holder.imageCheck.setVisibility(View.VISIBLE);
            // holder.base_parent.setBackgroundColor(Color.parseColor("#e3e3e3"));
        } else {
            holder.mainLayout.setBackgroundColor(Color.WHITE);
            holder.imageCheck.setVisibility(View.GONE);
        }


        holder.mainLayout.setOnClickListener(v -> {
//           if(paymentData.getDefaultPayment()!=1){
               paymentData.setDefaultPayment(1);
               for (int i = 0; i < paymentDataList.size(); i++) {
                   if (i != holder.getAdapterPosition()) {
                       paymentDataList.get(i).setDefaultPayment(0);
                   }
               }
               notifyItemChanged(holder.getAdapterPosition());
//           }
        });
    }

    @Override
    public int getItemCount() {
        return paymentDataList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView description;
        ImageView imageLogo;
        ImageButton imageCheck;
        LinearLayout mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.recycler_payment_description);
            imageLogo = itemView.findViewById(R.id.recycler_payment_imageLogo);
            mainLayout = itemView.findViewById(R.id.recycler_payment_mainLayout);
            imageCheck = itemView.findViewById(R.id.recycler_payment_imageCheck);
        }
    }
}
