package com.bimpos.wooden.bakery.networkRequest;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.helpers.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetDeliveryCharge extends AsyncTask<Integer, Void, String> {

    private static final String TAG = "GetDeliveryCharge";
    private final OnGotDelivery listener;
    HttpURLConnection conn;
    private final double totalPrice;
    private final SharedPreferences prefs;
    URL url = null;
    private final int cityCode;
    private final int branchId;

    public interface OnGotDelivery {
        void onGotDelivery(double deliveryCharge);
    }

    public GetDeliveryCharge(OnGotDelivery listener, double totalPrice, int cityCode, int branchId) {
        this.totalPrice = totalPrice;
        this.listener = listener;
        this.cityCode = cityCode;
        this.branchId = branchId;
        prefs = MainApplication.getPreferences();
    }

    @Override
    protected String doInBackground(Integer... ints) {

        try {
            Log.d(TAG, "doInBackground: get delivery charge start");
            url = new URL(Constant.BASE_URL + "charges?branch=" + branchId + "&city=" + cityCode);
            Log.d(TAG, "doInBackground: url " + url);
            conn = (HttpURLConnection) url.openConnection();
            String token = prefs.getString(Constant.TOKEN, "");
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestMethod("GET");
            int READ_TIMEOUT = 15000;
            conn.setReadTimeout(READ_TIMEOUT);
            int CONNECTION_TIMEOUT = 10000;
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.connect();

            int response_code = conn.getResponseCode();

            Log.d(TAG, "doInBackground: response code " + response_code);
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                return (result.toString());
            } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                return ("token");
            } else {
                return ("notFound");
            }
        } catch (Exception e) {
            try {
                Log.d(TAG, "doInBackground: error " + e.getMessage() + ", " + conn.getResponseMessage());
            } catch (IOException ioException) {
                Log.d(TAG, "doInBackground: error " + ioException.getMessage());
            }
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, "onPostExecute: result " + result);
        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("notFound")) {
//            listener.onGotDelivery(50000);
            listener.onGotDelivery(0);
        } else if (result.equalsIgnoreCase("token")) {
            listener.onGotDelivery(-1);
        } else {
            try {
                JSONArray parentArray = new JSONArray(result);
                JSONObject object = parentArray.getJSONObject(0);
                int byAmount = object.getInt("byamount");
                int byPercent = object.getInt("bypercent");
                int setAmount = object.getInt("setamount");
                if (byAmount == 1) {
                    listener.onGotDelivery(setAmount);
                } else if (byPercent == 1) {
                    double charge = (totalPrice * setAmount / 100);
                    listener.onGotDelivery(charge);
                } else {
                    listener.onGotDelivery(0);
                }

            } catch (JSONException e) {
                listener.onGotDelivery(0);
            }
        }
    }
}
