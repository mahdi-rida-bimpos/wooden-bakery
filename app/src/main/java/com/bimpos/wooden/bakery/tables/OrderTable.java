package com.bimpos.wooden.bakery.tables;

public class OrderTable {

    public static final String TABLE_NAME= "orders";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String CART_ITEM_LIST = "CartItemList";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
