package com.bimpos.wooden.bakery.homePage.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.models.Member;
import com.bimpos.wooden.bakery.homePage.activities.WriteToUsActivity;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;



public class ContactUsFragment extends BottomSheetDialogFragment {

    LinearLayout call, whatsapp, writeToUs, reportToUs;
    private Member member;
    private long mLastClick = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_us, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        call = view.findViewById(R.id.fragment_contact_us_call);
        whatsapp = view.findViewById(R.id.fragment_contact_us_whatsapp);
        writeToUs = view.findViewById(R.id.fragment_contact_us_writeToUs);
        DatabaseHelper databaseHelper = MainApplication.getDatabase();
        member = databaseHelper.getMember();

        call.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:+96176062062"));
            startActivity(intent);
        });

        whatsapp.setOnClickListener(v -> {
                    if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                        return;
                    }
                    mLastClick = SystemClock.elapsedRealtime();
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(
                                    "https://api.whatsapp.com/send?phone=+9613007790&text="
                            )));
                }
        );

        writeToUs.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();

            Intent intent = new Intent(getActivity(), WriteToUsActivity.class);
            if (getActivity() != null)
                getActivity().startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

    }

}
