package com.bimpos.wooden.bakery.profile.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.models.Cities;

import java.util.ArrayList;
import java.util.List;

public class CitiesSearchAdapter extends RecyclerView.Adapter<CitiesSearchAdapter.ViewHolder> implements Filterable {

    private final List<Cities> citiesList;
    private List<Cities> filteredList;
    private final OnCitySelected listener;

    public interface OnCitySelected {
        void onCitySelected(Cities cities);
    }

    public CitiesSearchAdapter(List<Cities> citiesList, OnCitySelected listener) {
        this.citiesList = citiesList;
        this.filteredList = citiesList;
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    filteredList = citiesList;
                } else {
                    List<Cities> newFilteredList = new ArrayList<>();
                    for (Cities cities : citiesList) {
                        if (cities.getCityName().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            newFilteredList.add(cities);
                        }
                    }
                    filteredList = newFilteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @SuppressLint("NotifyDataSetChanged")
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<Cities>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_search_cities, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Cities cities = filteredList.get(position);
        holder.cityName.setText(cities.getCityName());
        holder.mainLayout.setOnClickListener(v -> listener.onCitySelected(cities));
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView cityName;
        LinearLayout mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cityName = itemView.findViewById(R.id.recycler_search_cities_cityName);
            mainLayout = itemView.findViewById(R.id.recycler_search_cities_mainLayout);
        }
    }
}
