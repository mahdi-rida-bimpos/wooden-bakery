package com.bimpos.wooden.bakery.tables;

public class CitiesTable {

    public static final String TABLE_NAME= "cities";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String CITY_CODE = "cityCode";
        public static final String CITY_NAME = "name";
        public static final String REGION_CODE = "regionCode";
        public static final String ZIP_CODE = "zipCode";
        public static final String COUNTRY_CODE = "countryCode";
        public static final String IS_ACTIVE = "isActive";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
