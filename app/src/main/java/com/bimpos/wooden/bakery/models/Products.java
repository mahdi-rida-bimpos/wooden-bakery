package com.bimpos.wooden.bakery.models;

import java.io.Serializable;
import java.util.List;

public class Products implements Serializable {

    private int prodNum, catId, pId, isTaxable1, isTaxable2, isTaxable3, modifiersGroupId, comboGroupId, seq;
    private String description, description2, prodInfo, prodInfo2, picPath, picture, product_picture,
            title, thumb_picture, brand, refCode1, refCode2, tags;
    private double price, price2, finalPrice;
    private List<ProductStock> productStockList;

    public Products() {
    }

    public Products(int prodNum, int catId, int pId, int isTaxable1, int isTaxable2, int isTaxable3, int modifiresGroupId, int comboGroupId, String descript, String descript2, String prodInfo, String productInfo2, String picPath, String picture, String product_picture, String title, String thumb_picture, String brand, String refCode1, String refCode2, String tags, double price,double price2) {
        this.prodNum = prodNum;
        this.catId = catId;
        this.pId = pId;
        this.isTaxable1 = isTaxable1;
        this.isTaxable2 = isTaxable2;
        this.isTaxable3 = isTaxable3;
        this.modifiersGroupId = modifiresGroupId;
        this.comboGroupId = comboGroupId;
        this.description = descript;
        this.description2 = descript2;
        this.prodInfo = prodInfo;
        this.prodInfo2 = productInfo2;
        this.picPath = picPath;
        this.picture = picture;
        this.product_picture = product_picture;
        this.title = title;
        this.thumb_picture = thumb_picture;
        this.brand = brand;
        this.refCode1 = refCode1;
        this.refCode2 = refCode2;
        this.tags = tags;
        this.price = price;
        this.price2=price2;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getProdNum() {
        return prodNum;
    }

    public void setProdNum(int prodNum) {
        this.prodNum = prodNum;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public int getpId() {
        return pId;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }

    public int getIsTaxable1() {
        return isTaxable1;
    }

    public void setIsTaxable1(int isTaxable1) {
        this.isTaxable1 = isTaxable1;
    }

    public int getIsTaxable2() {
        return isTaxable2;
    }

    public void setIsTaxable2(int isTaxable2) {
        this.isTaxable2 = isTaxable2;
    }

    public int getIsTaxable3() {
        return isTaxable3;
    }

    public void setIsTaxable3(int isTaxable3) {
        this.isTaxable3 = isTaxable3;
    }

    public int getModifiersGroupId() {
        return modifiersGroupId;
    }

    public void setModifiersGroupId(int modifiersGroupId) {
        this.modifiersGroupId = modifiersGroupId;
    }

    public int getComboGroupId() {
        return comboGroupId;
    }

    public void setComboGroupId(int comboGroupId) {
        this.comboGroupId = comboGroupId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getProdInfo() {
        return prodInfo;
    }

    public void setProdInfo(String prodInfo) {
        this.prodInfo = prodInfo;
    }

    public String getProdInfo2() {
        return prodInfo2;
    }

    public void setProdInfo2(String prodInfo2) {
        this.prodInfo2 = prodInfo2;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getProduct_picture() {
        return product_picture;
    }

    public void setProduct_picture(String product_picture) {
        this.product_picture = product_picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumb_picture() {
        return thumb_picture;
    }

    public void setThumb_picture(String thumb_picture) {
        this.thumb_picture = thumb_picture;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getRefCode1() {
        return refCode1;
    }

    public void setRefCode1(String refCode1) {
        this.refCode1 = refCode1;
    }

    public String getRefCode2() {
        return refCode2;
    }

    public void setRefCode2(String refCode2) {
        this.refCode2 = refCode2;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice2() {
        return price2;
    }

    public void setPrice2(double price2) {
        this.price2 = price2;
    }

    @Override
    public String toString() {
        return "Products{" +
                "prodNum=" + prodNum +
                ", catId=" + catId +
                ", modifiersGroupId=" + modifiersGroupId +
                ", comboGroupId=" + comboGroupId +
                ", descript='" + description + '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", price 2=" + price2 +
                '}';
    }
}
