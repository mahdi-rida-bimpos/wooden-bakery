package com.bimpos.wooden.bakery.orderOnline.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityOrdersBinding;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.Cart;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.models.Categories;
import com.bimpos.wooden.bakery.models.Products;
import com.bimpos.wooden.bakery.orderOnline.adapters.CategoriesRvAdapter;
import com.bimpos.wooden.bakery.orderOnline.adapters.ProductsRvAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OrdersActivity extends AppCompatActivity implements CustomDialog.DialogEvents {

    private static final String TAG = "OrdersActivity";
    private final int DIALOG_CLEAR_PRODUCT = 1;
    public ActivityOrdersBinding binding;
    public List<Categories> categoriesList = new ArrayList<>();
    public List<Products> productsList = new ArrayList<>();
    public List<Object> itemList = new ArrayList<>();
    private DatabaseHelper databaseHelper;
    public ProductsRvAdapter productsRvAdapter;
    public LinearLayoutManager categoriesLayoutManager, productsLayoutManager;
    public CategoriesRvAdapter categoriesRvAdapter;
    private InputMethodManager imm;
    public static OrdersActivity ordersActivity;
    private Cart cartData;
    public int position = -1;
    private CustomDialog dialog;
    private int intentCatId;
    private Products holdProduct;
    private int intentSequence;
    private boolean firstLaunch = true;
    public ProductsRvAdapter.ProductsViewHolder finalViewHolder;
    private long mLastClick = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOrdersBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initClickListeners();

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(binding.productRecyclerView);
    }

    private void checkIntent() {
        Intent intent = getIntent();

        if (intent != null) {
            intentCatId = intent.getIntExtra("catId", -1);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: start");

        if (firstLaunch) {
            Log.d(TAG, "onResume: first launch");
            initSearchView();
            initCategoriesRV();
            initProductsRV();
            new Handler().post(() -> {
                checkIntent();
                getProductFromDb();
            });
            firstLaunch = false;
        }
        updateCart();

    }

    private void getProductFromDb() {
        int newSeqSize = 0;

        for (int i = 0; i < categoriesList.size(); i++) {
            Categories categories = categoriesList.get(i);
            itemList.add(categories);

            if (i == 0) {
                newSeqSize += productsList.size();
            } else {
                newSeqSize += productsList.size() + 1;
            }
            if (i == 0) {
                categories.setSeq(0);
            } else {
                categories.setSeq(newSeqSize);
            }
            if (intentCatId == categories.getCatId()) {
                intentSequence = newSeqSize;
            }

            productsList = databaseHelper.getProductsByCatId(categories.getCatId());

            for (int j = 0; j < productsList.size(); j++) {
                productsList.get(j).setSeq(i);
                itemList.add(productsList.get(j));
            }
        }
        productsRvAdapter.setData(itemList);

        binding.productRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                imm.hideSoftInputFromWindow(binding.searchText.getWindowToken(), 0);
                productsRvAdapter.scrollCategories = true;
                productsRvAdapter.hideMiniCart();
            }
        });

        if (intentCatId != -1) {
            categoriesRvAdapter.setSelectedCat(intentCatId, intentSequence);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private void updateCart() {
        int cartCount = cartData.getCartCount();
        double price = cartData.getOrderTotalPrice();
        Log.d(TAG, "updateCartCount: cart count " + cartCount);
        if (cartCount > 0) {
            binding.priceLayout.setEnabled(true);
            binding.priceLayout.setClickable(true);
            binding.priceLayout.setVisibility(View.VISIBLE);
            binding.priceLayout.refreshDrawableState();
            binding.totalItems.setText(String.valueOf(cartCount));
            binding.totalPrice.setText(FormatPrice.getFormattedPrice(price));
//            binding.priceLayout.animate().scaleX(1).setDuration(200).start();

        } else if (cartCount == 0) {
            binding.priceLayout.setEnabled(false);
            binding.priceLayout.setClickable(false);
            binding.priceLayout.setVisibility(View.GONE);
//            binding.priceLayout.animate().scaleX(0).setDuration(200).start();
            binding.totalItems.setText(String.valueOf(0));
        }
        productsRvAdapter.notifyDataSetChanged();
    }


    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        boolean canDelete = false;

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        //
        @Override
        public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
            if (viewHolder instanceof ProductsRvAdapter.ProductsViewHolder) {
                int count;
                Products products = (Products) productsRvAdapter.objectFilteredList.get(viewHolder.getAdapterPosition());

                if (isRemoved(products.getProdNum())) {
                    return 0;
                }
                count = cartData.getProductCount(products.getProdNum());
                if (count > 0) {
                    canDelete = true;
                    setDefaultSwipeDirs(ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT);
                } else {
                    setDefaultSwipeDirs(ItemTouchHelper.LEFT);
                }
                return super.getMovementFlags(recyclerView, viewHolder);
            }
            return 0;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            if (direction == ItemTouchHelper.LEFT) {
                Intent intent = new Intent(OrdersActivity.this, ProductInfoActivity.class);
                Products products = (Products) productsRvAdapter.objectFilteredList.get(viewHolder.getAdapterPosition());
                intent.putExtra(Products.class.getSimpleName(), products);
                finalViewHolder = (ProductsRvAdapter.ProductsViewHolder) viewHolder;
                position = viewHolder.getAdapterPosition();
                someActivityResultLauncher.launch(intent);
                new Handler().postDelayed(() -> viewHolder.itemView.setX(0), 500);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                Products products = (Products) productsRvAdapter.objectFilteredList.get(viewHolder.getAdapterPosition());
                finalViewHolder = (ProductsRvAdapter.ProductsViewHolder) viewHolder;
                position = viewHolder.getAdapterPosition();
                holdProduct = products;
                showDeleteDialog();
            }
        }

        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            Bitmap icon;
            View itemView = viewHolder.itemView;
            float height = (float) itemView.getBottom() - (float) itemView.getTop();
            float width = height / 3;
            Paint p = new Paint();

            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                if (dX < 0) {
                    p.setColor(ContextCompat.getColor(OrdersActivity.this, R.color.gold));
                    RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                    c.drawRect(background, p);

                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.info);
                    RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                    c.drawBitmap(icon, null, icon_dest, p);

                } else {
                    p.setColor(ContextCompat.getColor(OrdersActivity.this, R.color.pantoneRed));
                    RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                    c.drawRect(background, p);

                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.icon_delete);
                    RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                    c.drawBitmap(icon, null, icon_dest, p);
                }
            }

            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }

    };

    private boolean isRemoved(int prodNum) {
        for (CartProduct cartProduct : cartData.getCartItemsList()) {
            if (cartProduct.getProdNum() == prodNum) {
                if (cartProduct.getIsRemoved() == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    private void showDeleteDialog() {
        if (dialog.isAdded()) {
            dialog.dismiss();
        }
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Are you sure you want to remove all of this item ?");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Remove Item");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Yes");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "No");
        arguments.putInt("positiveColor", ContextCompat.getColor(this, R.color.pantoneRed));
        arguments.putInt(CustomDialog.DIALOG_ID, DIALOG_CLEAR_PRODUCT);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    public ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == 2) {
                    productsRvAdapter.animateProduct(finalViewHolder, position);
                }
            });

    private void initClickListeners() {
        binding.back.setOnClickListener(view -> onBackPressed());

        binding.priceLayout.setOnClickListener(view -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            Intent intent = new Intent(OrdersActivity.this, CartOrderActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
    }

    @Override
    public void onBackPressed() {
        if (Objects.requireNonNull(binding.searchText.getText()).toString().length() != 0 || binding.searchText.isFocused() || binding.categoryRecyclerView.getVisibility() == View.GONE) {
            binding.searchText.setText("");
            binding.searchText.clearFocus();
            imm.hideSoftInputFromWindow(binding.searchText.getWindowToken(), 0);
            changeSelectedCategory(0);
            binding.categoryRecyclerView.setVisibility(View.VISIBLE);
            return;
        }
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void initSearchView() {

        binding.searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                productsRvAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.searchText.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                binding.categoryRecyclerView.setVisibility(View.GONE);
                productsRvAdapter.hideMiniCart();
                new Handler().postDelayed(() -> binding.searchText.requestFocus(), 100);
            }
        });
    }

    private void initCategoriesRV() {
        Log.d(TAG, "initCategoriesRV: start");
        categoriesList = databaseHelper.getMenuCategories();
        categoriesRvAdapter = new CategoriesRvAdapter(this, this, categoriesList);
        binding.categoryRecyclerView.setLayoutManager(categoriesLayoutManager);
        binding.categoryRecyclerView.setAdapter(categoriesRvAdapter);

        binding.categoryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                productsRvAdapter.hideMiniCart();
            }
        });
    }

    private void initProductsRV() {
        productsRvAdapter = new ProductsRvAdapter(this, this);
        binding.productRecyclerView.setLayoutManager(productsLayoutManager);
        binding.productRecyclerView.setAdapter(productsRvAdapter);
    }

    public void changeSelectedCategory(int position) {
        categoriesRvAdapter.changeSelectedCategory(position);
        binding.categoryRecyclerView.smoothScrollToPosition(position);
    }

    private void initVariables() {

        dialog = new CustomDialog(this);
        ordersActivity = this;
        cartData = Cart.getCartData();
        imm = MainApplication.getImm();
        databaseHelper = MainApplication.getDatabase();
        categoriesLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        productsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        if (dialogId == DIALOG_CLEAR_PRODUCT) {
            dialog.dismiss();
            cartData.removeProductFromCart(holdProduct);
            updateCart();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    productsRvAdapter.animateFadeProduct(finalViewHolder, position);
                }
            }, 300);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        dialog.dismiss();
        productsRvAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }

    @SuppressLint("NotifyDataSetChanged")
    public void removeProduct(int prodNum) {
        for (Object object : itemList) {
            if (object instanceof Products) {
                if (prodNum == ((Products) object).getProdNum()) {
                    itemList.remove(object);
                    break;
                }
            }
        }
        productsRvAdapter.notifyDataSetChanged();
    }
}