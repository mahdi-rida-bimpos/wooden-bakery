package com.bimpos.wooden.bakery.models;

import java.io.Serializable;

public class Branches implements Serializable {

    private int id, isActive, acceptsDelivery, acceptsTakeAway;
    private double longitude, latitude;
    private String name, address, phone, email,
            clientId, picPath, openingHours, picture, deliveryHours;

    public Branches() {
    }

    public Branches(int id, int isActive, int acceptsDelivery, int acceptsTakeAway, String deliveryHours, String name, String address, String phone, String email, double longitude, double latitude, String clientId, String picPath, String openingHours, String picture) {
        this.id = id;
        this.isActive = isActive;
        this.acceptsDelivery = acceptsDelivery;
        this.acceptsTakeAway = acceptsTakeAway;
        this.deliveryHours = deliveryHours;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.longitude = longitude;
        this.latitude = latitude;
        this.clientId = clientId;
        this.picPath = picPath;
        this.openingHours = openingHours;
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsActive() {
        return isActive;
    }

    public int getAcceptsDelivery() {
        return acceptsDelivery;
    }

    public int getAcceptsTakeAway() {
        return acceptsTakeAway;
    }

    public String getDeliveryHours() {
        return deliveryHours;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getClientId() {
        return clientId;
    }

    public String getPicPath() {
        return picPath;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public String getPicture() {
        return picture;
    }

    @Override
    public String toString() {
        return "Branches{" +
                "id=" + id +
                ", isActive=" + isActive +
                ", acceptsDelivery=" + acceptsDelivery +
                ", acceptsTakeAway=" + acceptsTakeAway +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", clientId='" + clientId + '\'' +
                ", picPath='" + picPath + '\'' +
                ", openingHours='" + openingHours + '\'' +
                ", picture='" + picture + '\'' +
                ", deliveryHours='" + deliveryHours + '\'' +
                '}';
    }
}
