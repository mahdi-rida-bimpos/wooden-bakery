package com.bimpos.wooden.bakery.homePage.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.databinding.ActivityWriteToUsBinding;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.helpers.CustomLoadingOrder;
import com.bimpos.wooden.bakery.networkRequest.GetDepartments;
import com.bimpos.wooden.bakery.networkRequest.GetToken;
import com.bimpos.wooden.bakery.networkRequest.SendFeedback;
import com.bimpos.wooden.bakery.models.Department;

import java.util.HashMap;
import java.util.List;

public class WriteToUsActivity extends AppCompatActivity implements GetDepartments.OnGetDepartmentsComplete,
        GetToken.OnTokenReceived, CustomDialog.DialogEvents, SendFeedback.OnFeedbackComplete {

    private CustomLoadingOrder loadingOrder;
    private ActivityWriteToUsBinding binding;
    List<Department> departmentList;
    private Department department;
    private CustomDialog dialog;
    private final HashMap<Integer, Department> departmentMap = new HashMap<>();
    private long mLastClick = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityWriteToUsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initClickListeners();
        getDepartments();
    }

    private void getDepartments() {
        loadingOrder.show();
        loadingOrder.startAnimation(R.raw.web_loading);
        new GetDepartments( this).execute();
    }

    private void initClickListeners() {

        binding.back.setOnClickListener(v -> finish());

        binding.send.setOnClickListener(v -> sendMessage());
    }

    private void sendMessage() {
        if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
            return;
        }
        mLastClick = SystemClock.elapsedRealtime();
        String inputMessage = binding.inputMessage.getText().toString();
        if (TextUtils.isEmpty(inputMessage)) {
            Toast.makeText(this, "Please write your message first", Toast.LENGTH_SHORT).show();
        } else {
            loadingOrder.show();
            loadingOrder.startAnimation(R.raw.web_loading);

            if (departmentList.size() == 1) {
                department = departmentList.get(0);
            } else {
                department = departmentMap.get(binding.departmentSpinner.getSelectedItemPosition());
            }
            new SendFeedback(department, inputMessage, this).execute();

        }
    }

    private void initVariables() {
        loadingOrder = new CustomLoadingOrder(this);
        dialog = new CustomDialog(this);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onTokenReceived(int status) {
        if (status == 1) {
            new GetDepartments( this).execute();
        }
    }

    @Override
    public void onGetDepartmentsComplete(int status, List<Department> departmentList) {
        if (loadingOrder.isShowing()) {
            loadingOrder.dismiss();
        }
        if (status == -1) {
            new GetToken( this).execute();
        } else if (status == 1) {
            this.departmentList = departmentList;
            if (departmentList.size() == 1) {
                department = departmentList.get(0);
            } else {
                binding.departmentsLayout.setVisibility(View.VISIBLE);
                fillSpinner(departmentList);
            }
        } else {
            showErrorDialog();
        }
    }

    private void showErrorDialog() {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Our departments are busy right now, please try again later");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Sorry");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "exit");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
        arguments.putInt(CustomDialog.DIALOG_ID, 1);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    private void showCompleteDialog() {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Your feedback has been sent to " + department.getDepName() + " department");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Done");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "exit");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
        arguments.putInt(CustomDialog.DIALOG_ID, 2);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    private void fillSpinner(List<Department> departmentList) {
        String[] depArray = new String[departmentList.size()];
        for (int i = 0; i < departmentList.size(); i++) {
            departmentMap.put(i, departmentList.get(i));
            depArray[i] = departmentList.get(i).getDepName();
        }
        ArrayAdapter<String> depAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, depArray);
        binding.departmentSpinner.setAdapter(depAdapter);
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        if (dialogId == 1 || dialogId == 2) {
            finish();
        }
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {

    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }

    @Override
    public void onFeedbackComplete(int status) {
        loadingOrder.dismiss();

        if (status == 1) {
            showCompleteDialog();
        } else if (status == -2) {
            showErrorDialog();
        }
    }
}