package com.bimpos.wooden.bakery.tables;

public class CakeItemFavoriteTable {

   public static final String TABLE_NAME = "cakeItemFavorite";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String CAKE_ITEM_ID = "cakeItemId";
        public static final String CAKE_GALLERY_ID = "cakeGalleryId";
        public static final String MIN_ORDER_QTY = "minOrderQty";
        public static final String TITLE = "title";
        public static final String PRICE = "price";
        public static final String PIC_STORAGE_PATH = "picStoragePath";
        public static final String REF_NUM = "refNum";
        public static final String DESCRIPTION = "description";
        public static final String PIC_PATH = "picPath";
        public static final String IMAGE = "image";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
