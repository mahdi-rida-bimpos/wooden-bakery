package com.bimpos.wooden.bakery.helpers;


import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.models.CartProduct;
import com.bimpos.wooden.bakery.models.Order;
import com.bimpos.wooden.bakery.tables.OrderTable;
import com.google.gson.Gson;

import java.util.List;

public class AddCurrentOrder {

    public static void replaceOrder(List<CartProduct> cartItemList){
        String currentCartDate = new Gson().toJson(cartItemList);
        Order order = new Order(currentCartDate);
        MainApplication.getDatabase().replaceOrder(order);
    }

    public static void removeCurrentOrder(){
        MainApplication.getDatabase().cleanTable(OrderTable.TABLE_NAME);
    }
}
