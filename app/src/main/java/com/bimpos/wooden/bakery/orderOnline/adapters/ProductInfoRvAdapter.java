package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.models.CartComboHeader;
import com.bimpos.wooden.bakery.models.CartComboModifier;
import com.bimpos.wooden.bakery.models.CartQuestionHeader;
import com.bimpos.wooden.bakery.models.CartQuestionModifier;
import com.bimpos.wooden.bakery.models.ComboHeader;
import com.bimpos.wooden.bakery.models.ComboModifier;
import com.bimpos.wooden.bakery.models.QuestionHeader;
import com.bimpos.wooden.bakery.models.QuestionModifier;
import com.bimpos.wooden.bakery.orderOnline.activities.ProductInfoActivity;

import java.util.ArrayList;
import java.util.List;

public class ProductInfoRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ProductInfoActivity productInfoActivity;
    private final int QUESTION_HEADER_VIEW = 2;
    private final Context context;
    private static final String TAG = "ProductInfoRvAdapter";
    public List<CartComboHeader> cartComboHeaderList = new ArrayList<>();
    public List<CartQuestionHeader> cartQuestionHeaderList = new ArrayList<>();
    private final List<Object> objectList;

    public ProductInfoRvAdapter(ProductInfoActivity activity, Context context, List<Object> objectList) {
        this.productInfoActivity = activity;
        this.context = context;
        this.objectList = objectList;
    }

    @Override
    public int getItemViewType(int position) {
        if (objectList.get(position) instanceof ComboHeader) {
            return 1;
        } else {
            return QUESTION_HEADER_VIEW;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view;
        if(viewType==QUESTION_HEADER_VIEW){
           view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_question_header,parent,false);
           return new QuestionViewHolder(view);
       }else{
            view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_combo_header,parent,false);
            return new ComboViewHolder(view);
       }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {
        Object object =  objectList.get(position);

        if(holder1 instanceof ComboViewHolder){

            ComboViewHolder holder = (ComboViewHolder) holder1;
            ComboHeader comboHeader = (ComboHeader) object;

            Log.d(TAG, "onBindViewHolder: combo "+comboHeader.toString());
            List<CartComboModifier> cartComboModifiers = new ArrayList<>();
            for (int i = 0; i < comboHeader.getModifiersList().size(); i++) {
                ComboModifier comboModifier = comboHeader.getModifiersList().get(i);
                cartComboModifiers.add(new CartComboModifier(
                        comboModifier.getProdNum(),
                        comboModifier.getDescription(),
                        0,
                        comboModifier.getFinalPrice()));
            }

            if(cartComboModifiers.size()>0){
                holder.itemView.setVisibility(View.VISIBLE);
                holder.comboDescription.setText(comboHeader.getDescription());
                if (comboHeader.getMin() ==  comboHeader.getMax()) {
                    String maxItems = "Must Choose " + comboHeader.getMax();
                    holder.maxItems.setText(maxItems);
                    holder.maxItems.setVisibility(View.VISIBLE);
                }
                else if (comboHeader.getMin() == 0 && comboHeader.getMax() >= 1) {
                    String text = "(Max Selection: " + comboHeader.getMax() + ")";
                    holder.maxItems.setText(text);
                    holder.maxItems.setVisibility(View.VISIBLE);
                }
                holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));

                CartComboHeader cartComboHeader = new CartComboHeader(
                        comboHeader.getDescription(),
                        comboHeader.getMin(),
                        comboHeader.getMax(),
                        comboHeader.getRequired(),
                        0,
                        cartComboModifiers);
                cartComboHeaderList.add(cartComboHeader);
                ComboModifierRvAdapter comboModifierRvAdapter = new ComboModifierRvAdapter(cartComboHeader,productInfoActivity);
                holder.recyclerView.setAdapter(comboModifierRvAdapter);

            }else{
                holder.itemView.setVisibility(View.GONE);
            }
        }

        else{
            Log.d(TAG, "onBindViewHolder: question");
            QuestionViewHolder holder = (QuestionViewHolder) holder1;
            QuestionHeader questionHeader = (QuestionHeader) object;
            int min = questionHeader.getMin();
            int max = questionHeader.getMax();

            holder.questionDescription.setText(questionHeader.getDescription());
            if (min == max ) {
                String text = "";
                if (min != 0) {
                    if(min==1){
                        text = "Must choose " + min + " modifier";
                    }else{
                        text = "Must choose " + min + " modifiers";
                    }
                }

                holder.maxItems.setText(text);
                holder.maxItems.setVisibility(View.VISIBLE);

            } else if (min == 0 && max >= 1) {
                String text;
                if(max==1){
                    text= "Choose up to " + questionHeader.getMax() + " modifier";
                }else{
                    text= "Choose up to " + questionHeader.getMax() + " modifiers";
                }
                holder.maxItems.setText(text);
                holder.maxItems.setVisibility(View.VISIBLE);
            }
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            List<CartQuestionModifier> cartQuestionModifierList = new ArrayList<>();
            for (int i = 0; i < questionHeader.getModifiersList().size(); i++) {
                QuestionModifier questionModifier = questionHeader.getModifiersList().get(i);
                cartQuestionModifierList.add(new CartQuestionModifier(
                                questionModifier.getProdNum(),
                                questionModifier.getDescription(),
                                false,
                                questionModifier.getFinalPrice()
                        )
                );
            }
            CartQuestionHeader cartQuestionHeader = new CartQuestionHeader(
                    questionHeader.getDescription(),
                    questionHeader.getMin(),
                    questionHeader.getMax(),
                    questionHeader.getRequired(),
                    cartQuestionModifierList);
            cartQuestionHeaderList.add(cartQuestionHeader);
            QuestionModifierRvAdapter questionModifierAdapter = new QuestionModifierRvAdapter(productInfoActivity, cartQuestionHeader);
            holder.recyclerView.setAdapter(questionModifierAdapter);
        }
    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }

    static class ComboViewHolder extends RecyclerView.ViewHolder {
        TextView comboDescription, maxItems;
        RecyclerView recyclerView;

        public ComboViewHolder(@NonNull View itemView) {
            super(itemView);
            comboDescription = itemView.findViewById(R.id.recycler_combo_header_descript);
            recyclerView = itemView.findViewById(R.id.recycler_combo_header_recyclerView);
            maxItems = itemView.findViewById(R.id.recycler_combo_header_maxItems);
        }
    }

    static class QuestionViewHolder extends RecyclerView.ViewHolder {
        TextView questionDescription, maxItems;
        RecyclerView recyclerView;

        public QuestionViewHolder(@NonNull View itemView) {
            super(itemView);
            questionDescription = itemView.findViewById(R.id.recycler_question_header_descript);
            recyclerView = itemView.findViewById(R.id.recycler_question_header_recyclerView);
            maxItems = itemView.findViewById(R.id.recycler_question_header_maxItems);
        }
    }
}
