package com.bimpos.wooden.bakery.product_catalog.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.Bundle;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.ActivityCategoriesCatalogBinding;
import com.bimpos.wooden.bakery.models.Categories;
import com.bimpos.wooden.bakery.product_catalog.adapters.CategoriesCatalogRvAdapter;

import java.util.List;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class CategoriesCatalogActivity extends AppCompatActivity {

    private ActivityCategoriesCatalogBinding binding;
    private CategoriesCatalogRvAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCategoriesCatalogBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initRecyclerView();
        initClickListeners();
    }

    private void initClickListeners() {
        binding.back.setOnClickListener(v -> finish());
    }

    private void initVariables() {
        List<Categories> categoriesList = MainApplication.getDatabase().getCatalogCategories();
        adapter = new CategoriesCatalogRvAdapter(categoriesList, this,this);
    }


    private void initRecyclerView() {

        binding.recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        binding.recyclerView.setAdapter(adapter);
        OverScrollDecoratorHelper.setUpOverScroll(binding.recyclerView,OverScrollDecoratorHelper.ORIENTATION_VERTICAL);

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }
}