package com.bimpos.wooden.bakery.tables;

public class PriceListTable {

    public static final String TABLE_NAME = "priceList";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String CURRENCY_DESCRIPTION = "currDescription";
        public static final String CONVERSION = "conversion";
        public static final String MASK = "mask";
        public static final String DECIMALS = "decimals";
        public static final String SHOW_IN_PRICE_LIST_CURRENCY = "ShowInPriceListCurrency";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
