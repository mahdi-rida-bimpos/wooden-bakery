package com.bimpos.wooden.bakery.tables;

public class BranchCitiesTable {

   public static final String TABLE_NAME = "branchCities";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String BRANCH_ID = "branchId";
        public static final String CITY_CODE = "cityCode";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
