package com.bimpos.wooden.bakery.models;

import java.util.List;

public class CartQuestionHeader {

    private String description;
    private int min,max,required,count;
    private List<CartQuestionModifier> cartQuestionModifiers;

    public CartQuestionHeader(String description,int min,int max,int required, List<CartQuestionModifier> cartQuestionModifiers) {
        this.description = description;
        this.cartQuestionModifiers = cartQuestionModifiers;
        this.min = min;
        this.max = max;
        this.count=0;
        this.required = required;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getRequired() {
        return required;
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CartQuestionModifier> getCartQuestionModifiers() {
        return cartQuestionModifiers;
    }

    public void setCartQuestionModifiers(List<CartQuestionModifier> cartQuestionModifiers) {
        this.cartQuestionModifiers = cartQuestionModifiers;
    }
}
