package com.bimpos.wooden.bakery.orderOnline.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.databinding.FragmentCheckOutOrderBinding;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.CustomDialog;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.helpers.GetTime;
import com.bimpos.wooden.bakery.helpers.Helpers;
import com.bimpos.wooden.bakery.models.Address;
import com.bimpos.wooden.bakery.models.Branches;
import com.bimpos.wooden.bakery.models.Cart;
import com.bimpos.wooden.bakery.models.Member;
import com.bimpos.wooden.bakery.models.PaymentData;
import com.bimpos.wooden.bakery.networkRequest.GetDeliveryCharge;
import com.bimpos.wooden.bakery.networkRequest.GetToken;
import com.bimpos.wooden.bakery.orderHistory.adapters.BranchSearchAdapter;
import com.bimpos.wooden.bakery.orderOnline.activities.CartOrderActivity;
import com.bimpos.wooden.bakery.profile.activities.AddressActivity;
import com.bimpos.wooden.bakery.profile.activities.LoginActivity;
import com.bimpos.wooden.bakery.profile.adapters.AddressRvAdapter;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class CheckOutOrderFragment extends Fragment implements AddressRvAdapter.OnDefaultAddressChanged,
        CustomDialog.DialogEvents,
        GetDeliveryCharge.OnGotDelivery,
        GetToken.OnTokenReceived, BranchSearchAdapter.OnBranchSelected {

    private static final String TAG = "CheckOutOrderFragment";
    private final int DIALOG_TIME_MESSAGE = 1;
    private FragmentCheckOutOrderBinding binding;
    private Dialog searchDialog;
    private Calendar calendar = Calendar.getInstance();
    private final Calendar calendarNow = Calendar.getInstance();
    private final int maxDay = calendar.get(Calendar.DAY_OF_MONTH);
    public static String ScheduleTime = "Now";
    private CustomDialog dialog;
    private DatabaseHelper databaseHelper;
    private List<Branches> branchesList;
    private List<Branches> branchesListFiltered;
    private List<Address> addressList;
    private Branches selectedBranch = new Branches();
    private long date;
    private Member member;
    private int cityCode;
    private final List<PaymentData> paymentList = new ArrayList<>();
    private long mLastClick = 0;
    private final CartOrderActivity activity;
    private boolean isGettingDeliveryCharge = false;
    public boolean canCheckDeliveryCharge = false;
    private Cart cart;
    private List<String> stateList = new ArrayList();

    private ArrayAdapter<String> paymentSpinnerAdapter;
    private final String[] paymentSpinnerArray = new String[1];
    private int pickUp = 0;
    private int orderTypePosition = 0;

    public CheckOutOrderFragment(CartOrderActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: start");
        binding = FragmentCheckOutOrderBinding.inflate(getLayoutInflater());

        initVariables();

        if (null != cart.getRemark() && cart.getRemark().length() != 0) {
            binding.remarkText.setText(cart.getRemark());
        }

        return binding.getRoot();
    }


    @SuppressLint("SetTextI18n")
    private void showBranchSearchDialog(List<Branches> branchesList) {
        View messageView = getLayoutInflater().inflate(R.layout.dialog_cities_search, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder.setView(messageView);

        TextView close = messageView.findViewById(R.id.dialog_cities_search_close);
        TextView title = messageView.findViewById(R.id.dialog_cities_search_title);
        RecyclerView recyclerView = messageView.findViewById(R.id.dialog_cities_search_recyclerView);
        EditText searchText = messageView.findViewById(R.id.dialog_cities_search_searchText);
        title.setText("Select City");
        BranchSearchAdapter adapter = new BranchSearchAdapter(branchesList, this);

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));
        recyclerView.setAdapter(adapter);
        close.setOnClickListener(v -> searchDialog.dismiss());

        searchDialog = builder.create();
        searchDialog.getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);
        searchDialog.setCanceledOnTouchOutside(false);
        searchDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        searchDialog.getWindow().getDecorView().setPadding(50, 50, 50, 0);
        searchDialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        searchDialog.show();
    }

    @Override
    public void onBranchSelected(Branches branches) {
        this.selectedBranch = branches;
        Log.d(TAG, "onBranchSelected: branch selected " + branches.toString());
        binding.selectedBranchText.setText(selectedBranch.getName());
        searchDialog.dismiss();
        getDeliveryCharge();
    }

    public String getRemark() {
        Log.d(TAG, "getRemark: remark " + binding.remarkText.getText().toString());
        if (binding.remarkText.length() == 0) {
            return "";
        } else {
            return binding.remarkText.getText().toString().trim().replace("'", " ");
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        initRadioButtonGroup();
        initClickListeners();
        initAddressRecyclerView();
        initPaymentSpinner();
        initDeliveryBranchesAdapter();
        if (pickUp == 1) {
            pickUp = 0;
            removeDeliveryCharge();
            initPickUpBranchesAdapter();
            binding.addressLayout.setVisibility(View.GONE);
            binding.branchText.setText("Pickup order from branch");
            activity.binding.deliveryChargeLayout.setVisibility(View.GONE);
            paymentSpinnerArray[0] = "Cash on pick-up";
            paymentSpinnerAdapter.notifyDataSetChanged();
        }
    }

    private void initVariables() {
        stateList.add("Delivery");
        stateList.add("Pickup");
        binding.stateSwitch.addStatesFromStrings(stateList);
        searchDialog = new Dialog(requireActivity());
        cart = Cart.getCartData();
        dialog = new CustomDialog(this);
        paymentList.add(new PaymentData(1, "Cash On Delivery", 1, R.drawable.ic_cash_in_hand));
        databaseHelper = MainApplication.getDatabase();
        member = databaseHelper.getMember();
        branchesList = databaseHelper.getBranchesList();
        branchesListFiltered = new ArrayList<>();
        binding.remarkText.setFilters(new InputFilter[]{Helpers.filter, new InputFilter.LengthFilter(50)});

        binding.remarkText.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
    }

    private void initClickListeners() {

        binding.scheduleText.setOnClickListener(v -> showDatePickerDialog());

        binding.addAddress.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            member = databaseHelper.getMember();
            if (member == null) {
                startActivity(new Intent(getActivity(), LoginActivity.class));
            } else {
                startActivity(new Intent(getActivity(), AddressActivity.class));
            }
            requireActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
    }

    @SuppressLint("SetTextI18n")
    private void initRadioButtonGroup() {
        binding.stateSwitch.addStateListener((i, state) -> {
            switch (i) {
                case 0:
                    binding.addressLayout.setVisibility(View.VISIBLE);
                    initDeliveryBranchesAdapter();
                    binding.branchText.setText("Order will be dispatched from branch");
                    paymentSpinnerArray[0] = "Cash on delivery";
                    paymentSpinnerAdapter.notifyDataSetChanged();
                    orderTypePosition = 0;
                    break;
                case 1:
                    removeDeliveryCharge();
                    initPickUpBranchesAdapter();
                    binding.addressLayout.setVisibility(View.GONE);
                    binding.branchText.setText("Pickup order from branch");
                    activity.binding.deliveryChargeLayout.setVisibility(View.GONE);
                    paymentSpinnerArray[0] = "Cash on pick-up";
                    paymentSpinnerAdapter.notifyDataSetChanged();
                    orderTypePosition = 1;
                    break;
            }
        });

//        binding.radioButtonTimingPickUp.setOnPositionChangedListener((button, currentPosition, lastPosition) -> {
//            switch (currentPosition) {
//                case 0:
//                    binding.scheduleText.setVisibility(View.GONE);
//                    break;
//
//                case 1:
//                    showDatePickerDialog();
//                    break;
//            }
//        });

    }

    private void removeDeliveryCharge() {
        Constant.SETTINGS_DELIVERY_CHARGE = 0;
        activity.setTotalPrice();
    }

    private void initDeliveryBranchesAdapter() {

        Log.d(TAG, "initDeliveryBranchesAdapter: start");
        cityCode = databaseHelper.getDefaultAddressId();
        Log.d(TAG, "initDeliveryBranchesAdapter: city code from database " + cityCode);
        branchesListFiltered = new ArrayList<>();
        branchesList = new ArrayList<>();
        if (cityCode != -1) {
            branchesList = databaseHelper.getBranchFilteredList(cityCode);
        }
        for (int i = 0; i < branchesList.size(); i++) {
            Branches branches = branchesList.get(i);
            if (branches.getAcceptsDelivery() == 1) {
                branchesListFiltered.add(branches);
            }
        }
        if (branchesListFiltered.size() > 0) {
            binding.selectedBranchText.setText(branchesListFiltered.get(0).getName());
            binding.selectedBranchText.setTextColor(ContextCompat.getColor(activity, R.color.black));
            selectedBranch = branchesListFiltered.get(0);
            if (branchesListFiltered.size() <= 1) {
                binding.branchesLayout.setOnClickListener(null);
            } else {
                binding.branchesLayout.setOnClickListener(v -> showBranchSearchDialog(branchesListFiltered));
            }
            getDeliveryCharge();
        } else {
            binding.selectedBranchText.setText("There is no branch that deliver to your address");
            binding.selectedBranchText.setTextColor(ContextCompat.getColor(activity, R.color.pantoneRed));
            selectedBranch = null;
            binding.branchesLayout.setOnClickListener(null);
        }
    }

    private void initPickUpBranchesAdapter() {

        branchesList = databaseHelper.getPickUpBranches();

        if (branchesList.size() > 0) {
            binding.selectedBranchText.setText(branchesList.get(0).getName());
            binding.selectedBranchText.setTextColor(ContextCompat.getColor(activity, R.color.black));
            selectedBranch = branchesList.get(0);
            if (branchesList.size() <= 1) {
                binding.branchesLayout.setOnClickListener(null);
            } else {
                binding.branchesLayout.setOnClickListener(v -> showBranchSearchDialog(branchesList));
            }
        } else {
            binding.selectedBranchText.setText("There is no branch that accepts take away");
            binding.selectedBranchText.setTextColor(ContextCompat.getColor(activity, R.color.pantoneRed));
        }
        Log.d(TAG, "initPickUpBranchesAdapter: selected branch " + selectedBranch);
    }

    public void getDeliveryCharge() {
        Log.d(TAG, "getDeliveryCharge: can check " + canCheckDeliveryCharge);
        Log.d(TAG, "getDeliveryCharge: is getting " + isGettingDeliveryCharge);
        if (canCheckDeliveryCharge && !isGettingDeliveryCharge) {
            isGettingDeliveryCharge = true;
            Log.d(TAG, "getDeliveryCharge: start");
            new Handler().postDelayed(() -> {
                if (getOrderType() == 1) {
                    if (selectedBranch != null) {
                        int branchId = selectedBranch.getId();
                        activity.loadingOrder.show();
                        activity.loadingOrder.startAnimation(R.raw.web_loading);
                        GetDeliveryCharge getDeliveryCharge = new GetDeliveryCharge(CheckOutOrderFragment.this, Cart.getCartData().getOrderTotalPrice(), cityCode, branchId);
                        getDeliveryCharge.execute();
                    } else {
                        isGettingDeliveryCharge = false;
                    }
                } else {
                    isGettingDeliveryCharge = false;
                }
            }, 550);
        }
    }

    @Override
    public void onGotDelivery(double deliveryCharge) {
        Log.d(TAG, "onGotDelivery: " + deliveryCharge);
        if (deliveryCharge == -1) {
            isGettingDeliveryCharge = false;
            getToken();
        } else {
            activity.loadingOrder.dismiss();
            if (databaseHelper.shouldShowInPriceListCurrency()) {
                deliveryCharge = deliveryCharge / databaseHelper.getPriceList().getConversion();
            }
            Constant.SETTINGS_DELIVERY_CHARGE = deliveryCharge;
            activity.setTotalPrice();
            String price = FormatPrice.getFormattedPrice(deliveryCharge);
            activity.binding.txtDeliveryCharge.setText(price);
            if (deliveryCharge == 0) {
                activity.binding.deliveryChargeLayout.setVisibility(View.GONE);
            } else {
                activity.binding.deliveryChargeLayout.setVisibility(View.VISIBLE);
            }
            isGettingDeliveryCharge = false;
        }
    }

    public boolean hasBranchToDeliver() {
//        return deliverySpinnerArray.length > 0;
        Log.d(TAG, "hasBranchToDeliver: branch list size " + branchesListFiltered.size());
        return branchesListFiltered.size() > 0;
    }

    public boolean hasBranchToTakeAway() {
        Log.d(TAG, "hasBranchToTakeAway: branch list size " + branchesList.size());
        return branchesList.size() > 0;
    }

    private void initAddressRecyclerView() {
        addressList = databaseHelper.getAddressList();
        AddressRvAdapter addressRvAdapter = new AddressRvAdapter(getActivity(), addressList, this);
        binding.addressRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.addressRecyclerView.setAdapter(addressRvAdapter);
    }

    private void initPaymentSpinner() {

        paymentSpinnerArray[0] = "Cash on delivery";
        if (paymentSpinnerArray.length < 2) {
            binding.spinnerPayment.setEnabled(false);
            binding.spinnerPayment.setClickable(false);
        }
        paymentSpinnerAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_text_style, paymentSpinnerArray);
        binding.spinnerPayment.setAdapter(paymentSpinnerAdapter);
    }

    public boolean hasAddress() {
        return addressList.size() > 0;
    }

    public Address getDefaultAddress() {
        for (int i = 0; i < addressList.size(); i++) {
            Address address = addressList.get(i);
            if (address.getDefaultAddress() == 1) {
                return address;
            }
        }
        return null;
    }

    public int getBranchId() {
        return selectedBranch.getId();
    }

    public int getAddressType() {
        Address address = getDefaultAddress();
        return address.getAddressType();
    }

    public String getBranchName() {
        int branchId = getBranchId();
        Branches branches = databaseHelper.getBranchById(branchId);
        return branches.getName();
    }

    public int getOrderDate() {
        date = new Date().getTime();
        Log.d(TAG, "getOrderDate: date " + date);

        if (orderTypePosition == 0) {
            return GetTime.getSimpleDate(date);
        } else {
            return GetTime.getSimpleDate(calendar.getTimeInMillis());
        }
    }

    public String getOrderTime() {
        Log.d(TAG, "getOrderTime: date " + date);
        if (orderTypePosition == 0) {
            return GetTime.getFullTime(date);
        } else {
            return GetTime.getFullTime(calendar.getTimeInMillis());
        }
    }

    public String getNowDate() {
        return String.valueOf(date);
    }

    public String getHistoryDate() {
        Log.d(TAG, "getHistoryDate: date " + date);
        if (orderTypePosition == 0) {
            return String.valueOf(date);
        } else {
            return String.valueOf(calendar.getTimeInMillis());
        }
    }

    public int getOrderType() {
        return orderTypePosition + 1;
    }

    private void showDatePickerDialog() {
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), dateListener, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        dialog.setOnCancelListener(dialog1 -> {
            orderTypePosition = 1;
            binding.stateSwitch.selectState(0);
        });
        dialog.show();
    }

    DatePickerDialog.OnDateSetListener dateListener = (view, year, monthOfYear, dayOfMonth) -> {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        new Handler().postDelayed(this::showTimePicker, 100);
    };

    private void showTimePicker() {
        TimePickerDialog timePickerDialog;

        timePickerDialog = TimePickerDialog.newInstance(
                null,
                calendar.get((Calendar.HOUR_OF_DAY)),
                calendar.get(Calendar.MINUTE),
                false);

        timePickerDialog.setTimeInterval(1, 15, 30);
        //AlertDialog.THEME_HOLO_LIGHT
        timePickerDialog.setOnCancelListener(dialog ->
        {
            orderTypePosition = 1;
            binding.stateSwitch.selectState(0);
            calendar = calendarNow;
        });
        timePickerDialog.setOnTimeSetListener((view, hourOfDay, minute, second) -> {
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            if (calendar.getTimeInMillis() < calendarNow.getTimeInMillis()) {
                orderTypePosition = 1;
                binding.stateSwitch.selectState(0);
                showTimeErrorDialog();
            } else {
                setDateTime();
            }
        });
        timePickerDialog.show(activity.getSupportFragmentManager(), "");
    }

    private void showTimeErrorDialog() {
        calendar = calendarNow;
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Your scheduled date should be later than current date");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "cancel");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
        arguments.putInt(CustomDialog.DIALOG_ID, DIALOG_TIME_MESSAGE);
        dialog.setArguments(arguments);
        dialog.show(requireActivity().getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    private void setDateTime() {
        Log.d(TAG, "setDateTime: max date " + maxDay);
        Log.d(TAG, "setDateTime:  chooesne date " + calendar.get(Calendar.DAY_OF_MONTH));
        if (calendar.get(Calendar.DAY_OF_MONTH) == maxDay) {
            binding.scheduleText.setText(GetTime.getTimeOnly(calendar.getTimeInMillis()));
        } else {
            binding.scheduleText.setText(GetTime.getFullTimeAmPm(calendar.getTimeInMillis()));
        }
        binding.scheduleText.setVisibility(View.VISIBLE);
        ScheduleTime = GetTime.getFullTimeAmPm(calendar.getTimeInMillis());
    }

    @Override
    public void onDefaultAddressChanged(int cityCode) {
        initDeliveryBranchesAdapter();
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        if (dialogId == DIALOG_TIME_MESSAGE) {
            dialog.dismiss();
        }
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {

    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }


    private void getToken() {
        Log.d(TAG, "getToken: start");
        new GetToken(this).execute();
    }

    @Override
    public void onTokenReceived(int status) {
        Log.d(TAG, "onTokenReceived: start");
        if (status == 1) {
            getDeliveryCharge();
        }
    }

    public void setPickup(int pickup) {
        this.pickUp = pickup;
    }
}