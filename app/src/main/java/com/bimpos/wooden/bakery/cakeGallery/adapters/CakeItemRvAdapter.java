package com.bimpos.wooden.bakery.cakeGallery.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.cakeGallery.activities.CakeImageActivity;
import com.bimpos.wooden.bakery.cakeGallery.activities.CakeItemsActivity;
import com.bimpos.wooden.bakery.database.DatabaseHelper;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.CakeItem;
import com.bimpos.wooden.bakery.models.CakeItemFavorite;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class CakeItemRvAdapter extends RecyclerView.Adapter<CakeItemRvAdapter.ViewHolder> {

    private final List<CakeItem> cakeItemList;
    private final DatabaseHelper databaseHelper;
    private final Context context;
    private final CakeItemsActivity activity;

    private static final String TAG = "CakeItemRvAdapter";
    private long mLastClick = 0;

    public CakeItemRvAdapter(List<CakeItem> cakeItemList, Context context, CakeItemsActivity activity) {
        this.context = context;
        this.activity = activity;
        this.cakeItemList = cakeItemList;
        databaseHelper = MainApplication.getDatabase();
    }

    @NonNull
    @Override
    public CakeItemRvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cake_item, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull CakeItemRvAdapter.ViewHolder holder, int position) {

        CakeItem cakeItem = cakeItemList.get(position);
        holder.name.setText(cakeItem.getTitle());
        holder.description.setText(cakeItem.getDescription());
        holder.minOrder.setText(String.valueOf(cakeItem.getMinOrderQty()));
        holder.refNum.setText(cakeItem.getRefNum());
        holder.price.setText(FormatPrice.getFormattedPrice(cakeItem.getPrice()));

        if (cakeItem.getPicPath().length() != 0) {
            Glide.with(context)
                    .load(cakeItem.getPicPath())
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .placeholder(R.drawable.category_place_holder)
                    .into(holder.image);
        }

        if (databaseHelper.isCakeFavorite(cakeItem.getCakeId())) {
            holder.favImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_fill));
        } else {
            holder.favImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_empty));
        }

        holder.favImage.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            if (databaseHelper.isCakeFavorite(cakeItem.getCakeId())) {
                databaseHelper.deleteFavoriteCake(cakeItem.getCakeId());
                notifyItemChanged(holder.getAdapterPosition());
            } else {
                activity.loadingOrder.show();
                activity.loadingOrder.startAnimation(R.raw.web_loading);
                insertCakeFavorite(cakeItem, holder);
            }
        });

        if (cakeItem.getPicPath().length() != 0) {
            holder.image.setOnClickListener(v -> {
                if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                    return;
                }
                mLastClick = SystemClock.elapsedRealtime();
                context.startActivity(new Intent(context, CakeImageActivity.class)
                        .putExtra("PictureUrl", cakeItem.getPicPath())
                        .putExtra("cakeTitle", cakeItem.getTitle()));
            });
        }


    }

    private void insertCakeFavorite(CakeItem cakeItem, ViewHolder holder) {
        CakeItemFavorite cakeItemFavorite = new CakeItemFavorite(cakeItem.getCakeGalleryId(),
                cakeItem.getCakeId(), cakeItem.getPrice(), cakeItem.getMinOrderQty(), cakeItem.getTitle(), cakeItem.getDescription(),
                cakeItem.getPicPath(), cakeItem.getRefNum(), null);

        Picasso.get().load(cakeItem.getPicPath())
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        Log.d(TAG, "onBitmapLoaded: loaded");
                        activity.loadingOrder.dismiss();
                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                        byte[] data = outputStream.toByteArray();
                        cakeItemFavorite.setImage(data);
                        databaseHelper.insertFavoriteCake(cakeItemFavorite);
                        notifyItemChanged(holder.getAdapterPosition());
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        Log.d(TAG, "onBitmapFailed: failed");
                        activity.loadingOrder.dismiss();
                        databaseHelper.insertFavoriteCake(cakeItemFavorite);
                        notifyItemChanged(holder.getAdapterPosition());
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

    }

    @Override
    public int getItemCount() {
        return cakeItemList.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, price, minOrder, refNum, description;
        ImageView image, favImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.recycler_cake_item_name);
            price = itemView.findViewById(R.id.recycler_cake_item_price);
            minOrder = itemView.findViewById(R.id.recycler_cake_item_minOrder);
            refNum = itemView.findViewById(R.id.recycler_cake_item_refNum);
            description = itemView.findViewById(R.id.recycler_cake_item_description);
            image = itemView.findViewById(R.id.recycler_cake_item_image);
            favImage = itemView.findViewById(R.id.recycler_cake_item_favBtn);
        }
    }

}
