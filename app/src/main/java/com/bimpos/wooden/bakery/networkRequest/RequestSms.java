package com.bimpos.wooden.bakery.networkRequest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.wooden.bakery.BuildConfig;
import com.bimpos.wooden.bakery.application.MainApplication;
import com.bimpos.wooden.bakery.helpers.Constant;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RequestSms extends AsyncTask<String, String, String> {

    private static final String TAG = "RequestSms";
    HttpURLConnection conn;
    private final SharedPreferences prefs;
    private final OnSmsReceived listener;
    private final String mobileNumber;
    URL url = null;

    public interface OnSmsReceived {
        void onSmsReceived(int status);
    }

    public RequestSms( String mobileNumber, OnSmsReceived listener) {
        prefs = MainApplication.getPreferences();
        this.listener = listener;
        this.mobileNumber = mobileNumber;
    }

    @Override
    protected String doInBackground(String... params) {

        try {

            url = new URL(Constant.BASE_URL + "devices"
                    + "?data={\"deviceIdentifier\":\"" + prefs.getString(Constant.UID,"-1")+"\""
                    + ", \"deviceOS\":\"android\""
                    + ", \"deviceAppId\":1"
                    +", \"deviceAppVersion\":\""+BuildConfig.VERSION_NAME+"\""
                    + ", \"mobile\":\""+mobileNumber+"\"}");

            Log.d(TAG, "doInBackground: url " + url);
            conn = (HttpURLConnection) url.openConnection();
            String token = prefs.getString(Constant.TOKEN, "");
            Log.d(TAG, "doInBackground: token "+token);
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestMethod("POST");
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(10000);
            conn.connect();
            int response_code = conn.getResponseCode();
            Log.d(TAG, "doInBackground: response code "+response_code);
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result " + result.toString());
                return (result.toString());
            } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                return ("token");
            } else {
                return ("notFound");
            }

        } catch (Exception e) {
            try {
                Log.d(TAG, "doInBackground: error " + e.getMessage() + ", " + conn.getResponseMessage());
            } catch (IOException ioException) {
                Log.d(TAG, "doInBackground: error "+ioException.getMessage());
            }
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, "onPostExecute: result " + result);
        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("notFound")) {
            listener.onSmsReceived(0);
        } else if (result.equalsIgnoreCase("token")) {
            listener.onSmsReceived(-1);
        } else {
            listener.onSmsReceived(1);
//            try {
//                JSONObject parentObject = new JSONObject(result);
//                String status = parentObject.getString("STATUS");
//                if (status.equalsIgnoreCase("success")) {
//                    listener.onSmsReceived(1);
//                } else {
//                    listener.onSmsReceived(0);
//                }
//            } catch (JSONException e) {
//                listener.onSmsReceived(0);
//            }
        }

    }
}
