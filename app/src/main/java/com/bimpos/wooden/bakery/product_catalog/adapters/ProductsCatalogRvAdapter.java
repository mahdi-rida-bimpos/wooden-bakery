package com.bimpos.wooden.bakery.product_catalog.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.helpers.Constant;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.Branches;
import com.bimpos.wooden.bakery.models.Products;
import com.bimpos.wooden.bakery.orderOnline.activities.GalleryActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class ProductsCatalogRvAdapter extends RecyclerView.Adapter<ProductsCatalogRvAdapter.ViewHolder> implements Filterable {

    private List<Products> productsList, filteredList;
    private final Context context;
    private long mLastClick = 0;


    public ProductsCatalogRvAdapter(List<Products> productsList, Context context) {
        this.context = context;
        this.productsList = productsList;
        this.filteredList = productsList;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    filteredList = productsList;
                } else {
                    List<Products> newFilteredList = new ArrayList<>();
                    for (Products products : productsList) {
                        if (products.getDescription().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            newFilteredList.add(products);
                        }
                    }
                    filteredList = newFilteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @SuppressLint("NotifyDataSetChanged")
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<Products>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_products_catalog, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (holder.getAdapterPosition() % 2 == 0) {
            holder.mainLayout.setBackgroundColor(Color.parseColor("#f9f9f9"));
        } else {
            holder.mainLayout.setBackgroundColor(Color.WHITE);
        }

        Products product = filteredList.get(position);
        holder.productDescription.setText(product.getDescription());
        holder.price.setText(FormatPrice.getFormattedPrice(product.getFinalPrice()));

        Glide.with(context)
                .load(product.getProduct_picture())
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(R.drawable.category_place_holder)
                .into(holder.productImage);

        holder.itemView.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();

            context.startActivity(new Intent(context, GalleryActivity.class)
                    .putExtra("PictureUrl", product.getProduct_picture()));
        });
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView productImage;
        TextView productDescription, price;
        RelativeLayout mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.recycler_products_catalog_image);
            productDescription = itemView.findViewById(R.id.recycler_products_catalog_productDescription);
            price = itemView.findViewById(R.id.recycler_products_catalog_price);
            mainLayout = itemView.findViewById(R.id.recycler_products_catalog_mainLayout);
        }
    }
}
