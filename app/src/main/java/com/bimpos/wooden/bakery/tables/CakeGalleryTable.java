package com.bimpos.wooden.bakery.tables;

public class CakeGalleryTable {

   public static final String TABLE_NAME = "cakeGallery";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String GALLERY_ID = "galleryId";
        public static final String IS_ACTIVE = "isActive";
        public static final String GALLERY_NAME = "galleryName";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
