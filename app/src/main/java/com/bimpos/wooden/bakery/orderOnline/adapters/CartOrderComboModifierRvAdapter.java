package com.bimpos.wooden.bakery.orderOnline.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.wooden.bakery.R;
import com.bimpos.wooden.bakery.helpers.FormatPrice;
import com.bimpos.wooden.bakery.models.CartComboModifier;
import com.bimpos.wooden.bakery.models.CartProduct;

import java.util.List;

public class CartOrderComboModifierRvAdapter extends RecyclerView.Adapter<CartOrderComboModifierRvAdapter.ViewHolder> {

    private final List<CartComboModifier> cartComboModifiers;
    private final CartProduct cartProduct;

    public CartOrderComboModifierRvAdapter(CartProduct cartProduct, List<CartComboModifier> cartComboModifiers) {
        this.cartComboModifiers = cartComboModifiers;
        this.cartProduct = cartProduct;
    }

    @NonNull
    @Override
    public CartOrderComboModifierRvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cart_order_combo_modifier, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartOrderComboModifierRvAdapter.ViewHolder holder, int position) {
        CartComboModifier cartComboModifier = cartComboModifiers.get(position);
        holder.modifierDescription.setText(cartComboModifier.getDescription());
        holder.qty.setText(String.valueOf(cartComboModifier.getCount() * cartProduct.getQty()));
        if (cartComboModifier.getPrice() > 0) {
            holder.price.setText(String.format("+ %s", FormatPrice.getFormattedPrice(cartComboModifier.getPrice()*cartComboModifier.getCount()*cartProduct.getQty())));
        } else {
            holder.price.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return cartComboModifiers.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView modifierDescription, qty, price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            modifierDescription = itemView.findViewById(R.id.recycler_cart_order_combo_modifier_description);
            qty = itemView.findViewById(R.id.recycler_cart_order_combo_modifier_count);
            price = itemView.findViewById(R.id.recycler_cart_order_combo_modifier_price);
        }
    }
}
